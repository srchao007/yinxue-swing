package fun.codedesign.jdk.util.io;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class DataOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        String file = "basic/dos.dat";
        FileOutputStream fos = new FileOutputStream(file);
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeInt(5); //00 00 00 05  �ĸ��ֽ�
        dos.writeInt(-5); // 0xfffffffb
        dos.writeLong(-5L); //0xfffffffffffffffb
        dos.writeDouble(5.0);
        dos.writeUTF("123");
        //UTF-16BE
        dos.writeChars("321");
        dos.close();
        fos.close();
        IOUtils.printHex(file);
    }
}
