package fun.codedesign.jdk.util.date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class DateDemo4 {
    public static void main(String[] args) throws ParseException {
        try(Scanner sc = new Scanner(System.in)){
            System.out.println("输入日期:");
            String s = sc.nextLine();
    //        BirthDay  b1 = new BirthDay();
    //        long l =b1.getBirthms(prod);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
            Date prod = sdf.parse(s);
            Date specialDay = specialDay(prod, 3);
            String specialStr = sdf.format(specialDay);
            System.out.println("三个余额后的上两周的星期五:" + specialStr);
        }
    }

    public static Date specialDay(Date prod, int exp) {
        Calendar c = new GregorianCalendar();
        c.setTime(prod);
        c.add(Calendar.MONTH, exp);
        c.add(Calendar.WEEK_OF_YEAR, -2);
        c.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
        return c.getTime();
    }
}
