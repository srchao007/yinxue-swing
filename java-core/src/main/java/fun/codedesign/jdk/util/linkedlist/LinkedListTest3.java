package fun.codedesign.jdk.util.linkedlist;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;


public class LinkedListTest3 {

    private LinkedList<String> list = new LinkedList<String>();

    @Test
    public void testConstructor() throws Exception {
        list = new LinkedList<String>();
        list = new LinkedList<String>(new ArrayList<String>(10));
        assertEquals(10, list.size());
    }


}
