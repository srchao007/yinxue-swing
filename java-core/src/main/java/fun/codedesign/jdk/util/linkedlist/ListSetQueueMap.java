package fun.codedesign.jdk.util.linkedlist;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class ListSetQueueMap {

    public static void main(String[] args) {

        List<?> list1;
        Set<?> set1;
        Queue<?> queue;

        queue = new ArrayBlockingQueue<String>(123);

        list1 = new ArrayList<>();
        List<?> list2 = new LinkedList<>();
        List<?> list3 = new Vector<>();

        set1 = new HashSet<>();
        Set<?> set2 = new TreeSet<>();

        System.out.println(queue);
        System.out.println(list1);
        System.out.println(list2);
        System.out.println(list3);
        System.out.println(set1);
        System.out.println(set2);
    }
}
