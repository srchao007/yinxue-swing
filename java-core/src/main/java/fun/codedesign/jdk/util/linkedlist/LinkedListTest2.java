package fun.codedesign.jdk.util.linkedlist;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;


public class LinkedListTest2 {

    private LinkedList<String> linkedList = new LinkedList<String>();

    @Test
    public void testConstructor() throws Exception {
        linkedList = new LinkedList<String>(); //空构造
        /*
         * 1. collection转数组(各数据结构分别实现)，
         * 2. 数组遍历出对象
         * 3. 将对象包装成Node节点link关联 size == 数组长度
         */
        linkedList = new LinkedList<String>(new ArrayList<String>(10)); //传入一个collection进行构造
        assertEquals(0, linkedList.size());
    }

    /**
     * 遍历所有的节点，设置为null，并且首节点和尾节点都设置为null，帮助GC，使得它没有root连接
     */
    @Test
    public void testClear() {
        linkedList.clear();
    }

    @Test
    public void testAdd() {
        linkedList.add("123"); // 默认尾部添加
        linkedList.add(linkedList.size(), "321"); // 按size()大小是尾部添加
        linkedList.addFirst("123"); // deque方法，首部增加
        linkedList.addLast("123"); // deque方法，尾部增加
        linkedList.addAll(Arrays.asList("123", "123")); // 增加collection集合，从末尾加
        linkedList.addAll(linkedList.size(), Arrays.asList("123", "123")); // 从指定位置加
        linkedList.offer("123");
        linkedList.offerFirst("123");
        linkedList.offerLast("123");
        System.out.println(linkedList.toString());
    }

    @Test
    public void testRight() {
        int size = 2;
        size = 6 >> 2;
        System.out.println(size);
    }

    @Test
    public void testQuery() {
        linkedList.peek(); // 取出第一个节点元素，没有为空
        linkedList.peekFirst(); // 取第一个节点，实现同上
        linkedList.peekLast(); // 取出最后一个
        linkedList.element(); // 取第一个节点，null则抛出异常
        linkedList.descendingIterator(); // 返回一个往前迭代器实现，适配Itr
        linkedList.contains("123"); // 从第一个开始遍历，遍历到第一个返回下标，下标!=-1即存在
        linkedList.get(0); // 1. index判断是在左半部分还是右半部分，，左从first开始查起，右从end开始往前查起
        linkedList.listIterator(); // 适配以下方法，iterator的扩展类
        linkedList.iterator(); // 同上，返回迭代器
        linkedList.listIterator(3); //从下标3开始迭代
        linkedList.poll(); // 弹出第一个，空返回null
        linkedList.pollFirst(); // 弹出第一个, 空返回null
        linkedList.pollLast(); // 弹出最后一个

    }

    @Test
    public void testUpdate() {
        linkedList.pop(); // 从第一个开始弹出,并且删除第一个 如果为空抛异常，1.5的方法
        linkedList.toArray(); // new 一个新数组然后逐个放进去
        linkedList.toArray(new Object[1]);
    }
}
