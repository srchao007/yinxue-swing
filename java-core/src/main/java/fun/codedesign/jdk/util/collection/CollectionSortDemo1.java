package fun.codedesign.jdk.util.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CollectionSortDemo1 {
    public static void main(String[] args) {
        int i = "jerry".compareTo("tom");
        System.out.println(i);
        i = "tom".compareTo("jerry");
        System.out.println(i);
        i = "tom".compareTo("tom");
        System.out.println(i);

        List<String> names = new ArrayList<String>();
        names.add("Tom");
        names.add("Jerry");
        names.add("Tomson");
        names.add("Andy");
        Collections.sort(names);
        System.out.println(names);
        Collections.sort(names, new ByLength());
        System.out.println(names);

    }
}

class ByLength implements Comparator<String> {
    public int compare(String o1, String o2) {
        return o2.length() - o1.length();
    }

}

