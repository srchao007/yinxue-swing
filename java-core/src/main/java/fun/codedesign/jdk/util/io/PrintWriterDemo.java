package fun.codedesign.jdk.util.io;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class PrintWriterDemo {
    public static void main(String[] args) throws IOException {
        String file = "basic/pw.dat";
        String encoding = "gbk";

        FileOutputStream fos = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fos, encoding);

        PrintWriter pw = new PrintWriter(file);

        pw.println("Hello,1123");
        pw.println("Hello,123");
        pw.println("Hello,jessie");

        pw.close();
        osw.close();
        fos.close();
    }
}
