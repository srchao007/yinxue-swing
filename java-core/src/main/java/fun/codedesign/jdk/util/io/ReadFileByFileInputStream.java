package fun.codedesign.jdk.util.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ReadFileByFileInputStream implements AutoCloseable {
    public static void main(String[] args) throws IOException {
        try(ReadFileByFileInputStream is = new ReadFileByFileInputStream()) {
            System.out.println(is.showByte("hello.txt", 6));
        }
    }

    /**
     * 一个字节一个字节读取
     *
     * @param path
     * @param num
     * @return
     * @throws IOException
     */
    public StringBuffer showByte(String path, int num) throws IOException {
        File f = new File(path);
        StringBuffer sb = new StringBuffer();
        try(InputStream is = new FileInputStream(f)){
            for (int i = 0; i < num; i++) {
                char c = (char) is.read();
                sb.append(c);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sb;
    }

    @Override
    public void close() {
        
    }
}
