package fun.codedesign.jdk.util.linkedlist;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ZLinkedListTest {

    private ZLinkedList<String> list = new ZLinkedList<String>();


    @Test
    public void testAdd() throws Exception {
        list.clear();
        list.add("smith");
        list.add("hello");
        list.add("hello");
        list.add("ffff");
        assertEquals(4, list.size());
        assertEquals("smith", list.get(0));
        assertEquals("hello", list.get(1));
        assertEquals("hello", list.get(2));
        assertEquals("ffff", list.get(3));
    }

    @Test
    public void testAddWithIndex01() {
        list.clear();
        list.add(0, "s1");
        list.add(0, "s2");
        list.add(0, "s3");
        list.add(0, "s4");
        list.add(0, "s5");
        assertEquals("s5,s4,s3,s2,s1", list.toString());
    }

    @Test
    public void testAddWithIndex02() {
        list.clear();
        list.add("s1");
        list.add(0, "s2");
        list.add(1, "s3");
        list.add(2, "s4");
        list.add(3, "s5");
        assertEquals("s2,s3,s4,s5,s1", list.toString());
        assertEquals("s2", list.get(0));
        assertEquals("s3", list.get(1));
        assertEquals("s4", list.get(2));
        assertEquals("s5", list.get(3));
        assertEquals(5, list.size());
        System.out.println(list.toString());
    }


    @Test
    public void testRemove() throws Exception {
        list.add("0");
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        assertEquals("0", list.remove(0));
        assertEquals("1", list.remove(0));
        assertEquals("2", list.remove(0));
        assertEquals("3", list.remove(0));
        assertEquals("4", list.remove(0));
        assertEquals("5", list.remove(0));
    }

    /**
     * 冒烟测试
     *
     * @throws Exception
     */
    @Test
    public void test01() throws Exception {
        list.add("helloworld");
        list.add(0, "helloworldTwo");
        list.add(1, "hewlloworlthree");
        list.add(2, "hewllofour");
        System.out.println(list.toString());
        System.out.println(list.size());
        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));
        assertEquals(3, list.size());
    }


}
