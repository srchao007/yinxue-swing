package fun.codedesign.jdk.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class CalendarDemo {
    public static void main(String[] args) throws ParseException {
        try(Scanner sc = new Scanner(System.in)){
            System.out.println("输入时间:");
            String s = sc.nextLine();
            SimpleDateFormat marDay = new SimpleDateFormat("yyyy-MM-dd");
            Date x = marDay.parse(s);
            Date finalDate = theSat(x, 28);
            String t = marDay.format(finalDate);
            System.out.println("28年后的上一个余额的第一个星期六:" + t);
        }
    }


    public static Date theSat(Date birth, int years) {
        // 这里应该用
        Calendar c = GregorianCalendar.getInstance();
        // Calendar c = new GregorianCalendar();
        c.setTime(birth);
        c.add(Calendar.YEAR, years);
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DAY_OF_MONTH, Calendar.SATURDAY);
        return c.getTime();
    }
}
