package fun.codedesign.jdk.util.hashmap;

import java.util.HashMap;
import java.util.Scanner;

import fun.codedesign.jdk.lang.oop.User;

public class HashMapDemo {
    public static void main(String[] args) {
        HashMap<String, User> users = new HashMap<String, User>();

        users.put("11", new User("11", "1234", 36));
        users.put("22", new User("22", "1234", 30));
        users.put("33", new User("33", "123456", 58));
        users.put("44", new User("44", "123456", 32));

        try(Scanner sc = new Scanner(System.in)) {
            while (true) {
                System.out.println("请输入用户名:");
                String name = sc.nextLine();
    
                if (!users.containsKey(name)) {
                    System.out.println("没这个名字");
                    continue;
                }
                System.out.println("请输入密码");
                String pwd = sc.nextLine();
    
                User user = users.get(name);
                if (!user.getPwdString().equals(pwd)) {
                    System.out.println("密码错误");
                } else {
                    System.out.println("用户：" + user.getName() + "登录成功");
                    break;
                }
            }
        }
    }
}
