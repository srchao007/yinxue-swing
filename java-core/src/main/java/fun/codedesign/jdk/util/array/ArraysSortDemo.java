package fun.codedesign.jdk.util.array;

import java.util.Arrays;

/**
 * array归并排序
 */
public class ArraysSortDemo {

    public static void main(String[] args) {
        int[] ary = {1, 5, 6, 3, 2, 8, 7, 4, 9};
        System.out.println(Arrays.toString(ary));
        Arrays.sort(ary);
        System.out.println(Arrays.toString(ary));
        String[] names = {"Tom", "Jack", "Mike", "summer", "Rose", "0"};
        Arrays.sort(names);
        System.out.println(Arrays.toString(names));
    }
}
