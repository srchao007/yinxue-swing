package fun.codedesign.jdk.util.hashmap;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class HashMapTest2 {

    private HashMap<String, Object> hashMap = new HashMap<>();

    @Test
    public void testConstructor() throws Exception {
        hashMap = new HashMap<String, Object>();
        hashMap = new HashMap<String, Object>(10);
        hashMap = new HashMap<String, Object>(new HashMap<String, Object>());
        hashMap = new HashMap<String, Object>(10, 0.7F);
    }

    @Test
    public void testPut() throws Exception {
        hashMap.clear();
        hashMap.put("123", 123); // put是放入新值，返回老值
        int a = (int) hashMap.put("123", 10086);
        System.out.println(a);
        hashMap.get("123"); // 根据key的hash找到对应的Entry[k,v]再从Entry取到value值
        hashMap.put(null, 16666);
        hashMap.get(null);
        int b = (int) hashMap.put(null, 13323123);
        System.out.println(b);
        hashMap.keySet().iterator();
        hashMap.values().iterator();

    }

    @Test
    public void testInt() throws Exception {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 1);
        map.get(1);
    }
}
