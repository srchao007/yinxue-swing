package fun.codedesign.jdk.util.treeset;

import java.util.TreeSet;

/**
 * TreeSet排序问题
 *
 * @author zengjian
 * @create 2018-03-22 15:51
 * @since 1.0.0
 */
public class TreeSetOrder {
    public static void main(String[] args) {
        TreeSet<String> treeSet = new TreeSet<>();
        treeSet.add("d");
        treeSet.add("c");
        treeSet.add("e");
        treeSet.add("a");
        for (String s : treeSet) {
            System.out.println(s);
        }

    }
}
