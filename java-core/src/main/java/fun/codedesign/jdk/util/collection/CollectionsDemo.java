package fun.codedesign.jdk.util.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionsDemo {
    public static void main(String[] args) {
        List<String> names = new ArrayList<String>();
        names.add("tom");
        names.add("jerry");
        names.add("andy");

        Collections.sort(names);
        System.out.println(names);

        Collections.shuffle(names);
        System.out.println(names);

        int index = Collections.binarySearch(names, "tom");
        System.out.println(index);

    }
}
