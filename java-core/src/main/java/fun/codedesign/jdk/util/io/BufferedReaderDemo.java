package fun.codedesign.jdk.util.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BufferedReaderDemo {
    public static void main(String[] args) throws IOException {
        String s1;
        System.out.println("输入String字符串");
        BufferedReader b1 = new BufferedReader(new InputStreamReader(System.in));

        do {
            s1 = b1.readLine();
            System.out.println(s1);
        } while (!s1.equals("end"));
    }
}
