package fun.codedesign.jdk.util.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fun.codedesign.jdk.lang.oop.basic.card.Card;


public class CardCompareDemo {
    public static void main(String[] args) {
        List<Card> cards = new ArrayList<Card>();
        cards.add(new Card(Card.HEART, Card.EIGHT));
        cards.add(new Card(Card.HEART, Card.SEVEN));
        cards.add(new Card(Card.HEART, Card.SIX));
        cards.add(new Card(Card.HEART, Card.NINE));
        cards.add(new Card(Card.HEART, Card.KING));
        cards.add(new Card(Card.HEART, Card.QUEEN));

        System.out.println(cards);
        Collections.sort(cards, new ByCardsort());
        System.out.println(cards);

    }
}

class ByCardsort implements Comparator<Card> {


    public int compare(Card o1, Card o2) {

        return o2.getRank() - o1.getRank();
    }


}
	

