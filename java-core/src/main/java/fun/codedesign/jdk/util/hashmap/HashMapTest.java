package fun.codedesign.jdk.util.hashmap;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class HashMapTest {

    private HashMap<String, Object> map = new HashMap<>();

    @Test
    public void testConstructor() throws Exception {
        map = new HashMap<String, Object>();
        map = new HashMap<String, Object>(10);
        map = new HashMap<String, Object>(new HashMap<String, Object>());
        map = new HashMap<String, Object>(10, 0.7F);
    }

    @Test
    public void test01() throws Exception {
        map.clear();
        map.put("123", 123);
    }

    @Test
    public void testInt() throws Exception {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 1);
        map.get(1);
    }
}
