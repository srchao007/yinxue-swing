package fun.codedesign.jdk.util.io;

import java.io.IOException;

public class IOCopyDemo {
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        IOUtils.copy2("java.flv", "basic/1.flv");
        //IOUtils.copy1("C:/Users/Administrator/Desktop/11.docx", "basic/2.docx");

        long end = System.currentTimeMillis();
        System.out.println("消耗时间" + (end - start) + "ms");

    }
}
