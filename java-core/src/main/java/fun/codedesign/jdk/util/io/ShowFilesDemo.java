package fun.codedesign.jdk.util.io;

import java.io.File;


public class ShowFilesDemo {
    public static void main(String[] args) {
        File f1 = new File("G:/");
        showFiles(f1);
    }

    public static void showFiles(File Folder) {
        File[] files = Folder.listFiles();
        if (files != null) {
            for (File b : files) {
                if (b.isDirectory()) {
                    showFiles(b);
                } else {
                    System.out.println(b.getName());
                }
            }
        }
    }
}




