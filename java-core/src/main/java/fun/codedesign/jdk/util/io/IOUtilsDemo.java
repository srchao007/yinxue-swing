package fun.codedesign.jdk.util.io;

import java.io.IOException;
import java.util.Arrays;

public class IOUtilsDemo {
    public static void main(String[] args) throws IOException {
        IOUtils.printHex("basic/raf.dat");
        System.out.println(); //41 42 61 ff ff ff 61 ff ff ff d6 d0
        IOUtils.printHex("basic/raf.dat");
        System.out.println();  //41 42 61 ff ff ff 61 ff ff ff d6 d0
        byte[] buf = IOUtils.read("basic/raf.dat");
        System.out.println(Arrays.toString(buf)); //[65, 66, 97, -1, -1, -1, 97, -1, -1, -1, -42, -48]
    }
}
