package fun.codedesign.jdk.util.system;

import java.util.Scanner;

/**
 * 输入退出
 */
public class InputString {
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.print("输入字符串:");
            String say = sc.nextLine();
    
            for (int count = 1; count <= 10; count++) {
                if (say.equals("quit")) {
                    System.out.println("退出");
                    break;
                } else if (say.equals("start")) {
                    System.out.println("开始");
                }
            }
        }
    }
}

