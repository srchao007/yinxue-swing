package fun.codedesign.jdk.util.hashmap;

import java.util.Map;
import java.util.TreeMap;

/**
 * 链表map顺序测试
 *
 * @author zengjian
 * @create 2018-03-14 15:57
 * @since 1.0.0
 */
public class LinkedHashMapTemplate {

    public static void main(String[] args) {
        /*LinkedHashMap<String ,Object> map = new LinkedHashMap();
        map.put("1","aaaa");
        map.put("2","bbbb");
        map.put("3","bbbb");
        map.put("4","bbbb");

        map.put("1","cccc");
        map.put("2","dddd");*/

        /*HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("1","1");
        hashMap.put("2","2");
        hashMap.put("3","3");
        hashMap.put("4","4");
        hashMap.put("5","5");
        hashMap.put("6","6");
        for (Map.Entry entry:hashMap.entrySet()){
            System.out.println(entry.getKey());
        }*/

        TreeMap<String, String> treeMap = new TreeMap<>();
        treeMap.put("f", null);
        treeMap.put("a", null);
        treeMap.put("e", null);
        treeMap.put("c", null);
        treeMap.put("d", null);
        treeMap.put("x", null);
        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            System.out.println(entry.getKey());
        }


    }

}
