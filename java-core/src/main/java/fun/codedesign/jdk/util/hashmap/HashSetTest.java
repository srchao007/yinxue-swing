package fun.codedesign.jdk.util.hashmap;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

public class HashSetTest {

    private HashSet<String> hashSet = new HashSet<>();

    @Test
    public void testConstructor() {
        hashSet.clear();  // 调用hashMap的clear方法，使用工具类Arrays.fill 填充数组为null
        hashSet = new HashSet<>(); // 数据结构通过HashMap实现
        hashSet = new HashSet<>(16);
        hashSet = new HashSet<>(Arrays.asList("", "1")); // 同hashMap的实现
        hashSet = new HashSet<>(16, 0.75f);
    }

    @Test
    public void testPut() {
        hashSet.add("");
        hashSet.addAll(Arrays.asList("1", "2"));
        hashSet.add("");
        hashSet.add(null);
        hashSet.size();
        hashSet.remove("");
        // hashSet存储结构完全基于HashMap实现，实现的方法均调用HashMap方法
    }

}
