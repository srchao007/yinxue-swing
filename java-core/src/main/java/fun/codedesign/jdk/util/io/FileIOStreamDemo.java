package fun.codedesign.jdk.util.io;

import java.io.*;


public class FileIOStreamDemo {
    public static void main(String[] args) throws IOException {
        String file = "basic/x.txt";
        FileOutputStream fos = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fos, "gbk");
        osw.write("123");
        osw.write("11111");
        osw.close();
        fos.close();
        IOUtils.printHex(file);

        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, "gbk");
        int c;
        while ((c = isr.read()) != -1) {
            System.out.println((char) c);
        }
        isr.close();
        fis.close();
    }
}
