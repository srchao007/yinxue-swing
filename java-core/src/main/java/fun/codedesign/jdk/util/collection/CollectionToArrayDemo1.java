package fun.codedesign.jdk.util.collection;

import java.util.ArrayList;
import java.util.Arrays;

public class CollectionToArrayDemo1 {
    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<String>();
        names.add("zhangsan");
        names.add("lisi");

        Object[] ary1 = names.toArray();
        System.out.println(Arrays.toString(ary1));

        String[] ary2 = names.toArray(new String[]{});
        System.out.println(Arrays.toString(ary2));
    }
}
