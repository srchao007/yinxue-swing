package fun.codedesign.jdk.util.file;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileDemo {
    public static void main(String[] args)
            throws IOException {
        File file = new File(".");
        String path = file.getCanonicalPath(); //getCanonicalPath()
        System.out.println(path);

        File demo = new File(file, "basic");
        if (!demo.exists()) {
            demo.mkdir();
            System.out.println(demo.getPath() + "创建成功");
        }

        File test = new File(demo, "test.txt");
        if (!test.exists()) {
            test.createNewFile();
            System.out.println(test.getPath() + "创建成功");
        }
        System.out.println("test" + test.length());

        long l = test.lastModified();
        Date d = new Date(l);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String time = sdf.format(d);
        System.out.println("文件时间:" + time);

        //test.delete();
        //demo.delete();
    }
}
