package fun.codedesign.jdk.util.io;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("basic/fos.dat", true);
        fos.write('A');
        fos.write('B');
        int i = 3;
        fos.write(i >>> 24);
        fos.write(i >>> 16);
        fos.write(i >>> 8);
        fos.write(i);

        String s = "hello";
        byte[] buf = s.getBytes();
        fos.write(buf);
        fos.close();
        IOUtils.printHex("basic/fos.dat");
    }
}
