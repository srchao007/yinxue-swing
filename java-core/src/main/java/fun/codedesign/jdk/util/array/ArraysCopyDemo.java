package fun.codedesign.jdk.util.array;
// ��������

import java.util.Arrays;

public class ArraysCopyDemo {
    public static void main(String[] args) {
        int[] ary = {2, 3, 4};
        int[] ary1 = Arrays.copyOf(ary, ary.length);

        System.out.println(Arrays.toString(ary1));

        String[] list = {"ya", "yes", "tom"};
        System.out.println(list.length);//3
        list = Arrays.copyOf(list, list.length + 1);
        list[list.length - 1] = "hello";
        System.out.println(Arrays.toString(list));

    }
}
