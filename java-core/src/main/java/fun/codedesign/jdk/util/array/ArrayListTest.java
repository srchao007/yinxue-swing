package fun.codedesign.jdk.util.array;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

public class ArrayListTest {
    private ArrayList<String> list = new ArrayList<>();

    @Test
    public void testConstructor1() throws Exception {
        //初始数组0
        list = new ArrayList<String>(); 
        // 拷贝传入集合大小的数组
        list = new ArrayList<String>(new LinkedList<String>());
        // 100大小数组
        list = new ArrayList<String>(100); 
    }

    @Test
    public void testConstructor2() throws Exception {
        list.clear();
        list = new ArrayList<String>(100);
        System.out.println(Arrays.toString(list.toArray())); //[]
    }

    @Test
    public void testAdd() throws Exception {
        list.clear();
        list.add("123");
         // index==size，从末尾增加
        list.add(1, "000");
        list.addAll(new LinkedList<String>());
        /*collection.addAll(16, new LinkedList<String>());*/ //越界异常
        assertEquals(2, list.size());
        list.addAll(2, new LinkedList<String>());
        System.out.println(list.toString());

        list = new ArrayList<String>();

    }

    @Test
    public void testArraysAsList() throws Exception {
        list.clear();
        list = (ArrayList<String>) Arrays.asList("1", "2", "3");
    }

    @Test
    public void testRemove() {
        ArrayList<String> list = new ArrayList<>();
        list.remove("1");
        list.remove(1);
    }

}
