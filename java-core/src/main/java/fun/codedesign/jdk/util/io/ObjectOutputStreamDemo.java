package fun.codedesign.jdk.util.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


public class ObjectOutputStreamDemo {
    public static void main(String[] args) throws Exception {
        List<Foo> list1 = new ArrayList<Foo>();
        list1.add(new Foo());
        list1.add(new Foo());
        String file = "basic/object.dat";

        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        for (Foo foo : list1) {
            oos.writeObject(foo);
        }
        oos.close();
        fos.close();

        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        List<Foo> list2 = new ArrayList<Foo>();
        list2.add((Foo) ois.readObject());
        list2.add((Foo) ois.readObject());

        System.out.println(list1 == list2);
        System.out.println(list1.get(0) == list2.get(0));
        System.out.println(list1.get(1).equals(list2.get(1)));

        ois.close();
        fis.close();

    }
}
