package fun.codedesign.jdk.util.array;

import java.util.Arrays;

public class ArraysToStringDemo {
    public static void main(String[] args) {
        int[] ary = new int[]{3, 4, 5};
        int[] ary1 = ary;
        int[] ary2 = {4, 5, 6, 7};
        System.out.println(ary);//[I@8916a2
        System.out.println(ary1);//[I@8916a2
        System.out.println(Arrays.toString(ary));//[3, 4, 5]
        System.out.println(Arrays.toString(ary1));//[3, 4, 5]
        System.out.println(Arrays.toString(ary2));
    }
}
