package fun.codedesign.jdk.util.io;
import java.io.*;


public class ObjectInputStreamDemo {

    /**
     * @param args
     * @throws IOException
     * @throws Exception
     */
    public static void main(String[] args) throws IOException, Exception {
        String file = "basic/obj.dat";

        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Foo f1 = new Foo();
        Foo f2 = new Foo();
        oos.writeObject(f1);
        oos.writeObject(f2);

        oos.close();
        fos.close();

        IOUtils.printHex(file);

        FileInputStream fis = new FileInputStream(file);
        try(ObjectInputStream ois = new ObjectInputStream(fis)){
            Foo foo1 = (Foo) ois.readObject();
            Foo foo2 = (Foo) ois.readObject();
            System.out.println();
            System.out.println(foo1.a + " " + foo2.a);
        }
    }
}

class Foo implements Serializable {
    int a = 1;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + a;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Foo other = (Foo) obj;
        return a == other.a;
    }
}
