package fun.codedesign.jdk.util.io;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class WriteFileByOutputStreamWriter {
    public static void main(String[] args) throws IOException {
        OutputStream os = new FileOutputStream(new File("s2.txt"));
        OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);

        osw.append("hello");
        osw.append("\r\n");
        osw.append("word");
        osw.close();

        InputStream is = new FileInputStream("s2.txt");
        try( InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8)){
            StringBuffer sb1 = new StringBuffer();
            while (isr.ready()) {
                char c = (char) isr.read();
                sb1.append(c);
                //System.out.println(c);
            }
            System.out.println(sb1);
        }
    }
}
