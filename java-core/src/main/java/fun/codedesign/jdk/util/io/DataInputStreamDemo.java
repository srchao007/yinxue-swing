package fun.codedesign.jdk.util.io;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class DataInputStreamDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("basic/dos.dat");
        try(DataInputStream dis = new DataInputStream(fis)){
            int i = dis.readInt();
            System.out.println(i);
            i = dis.readInt();
            long l = dis.readLong();
            System.out.println(l);
            double d = dis.readDouble();
            System.out.println(d);
    
            String s = dis.readUTF();
            System.out.println(s);
        }
    }
}
