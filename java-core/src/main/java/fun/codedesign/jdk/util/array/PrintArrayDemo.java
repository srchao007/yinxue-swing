package fun.codedesign.jdk.util.array;

import java.util.Arrays;

public class PrintArrayDemo {
    public static void main(String[] args) {
        int[] ary = {45, 56, 67, 32};
        System.out.println(ary);
        System.out.println(Arrays.toString(ary));

        String[] names = {"Tom", "Jack", "Rose", "Apple", "Summer"};
        System.out.println(names);
        System.out.println(Arrays.toString(names));
    }
}
	
	
	
