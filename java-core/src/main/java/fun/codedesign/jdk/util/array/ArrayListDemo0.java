package fun.codedesign.jdk.util.array;

import java.util.ArrayList;

import fun.codedesign.jdk.lang.oop.basic.card.Card;

public class ArrayListDemo0 {
    public static void main(String[] args) {
        ArrayList<Card> cards = new ArrayList<Card>();
        cards.add(new Card(Card.SPADE, Card.TEN));
        cards.add(new Card(Card.SPADE, Card.JACK));
        cards.add(new Card(Card.SPADE, Card.QUEEN));
        cards.add(new Card(Card.SPADE, Card.KING));
        cards.add(new Card(Card.SPADE, Card.ACE));

        ArrayList<Card> others = new ArrayList<Card>();
        others.add(new Card(Card.SPADE, Card.ACE));
        others.add(new Card(Card.JOKER, Card.BLACK));
        others.add(new Card(Card.JOKER, Card.COLOR));
        cards.addAll(others);
        System.out.println(cards);
        cards.removeAll(others);
        System.out.println(cards.removeAll(others));
        System.out.println(cards);
//			cards.removeAll(others);
//			System.out.println(cards);
        //System.out.println(cards.isEmpty());
//			cards = null;
//			System.out.println(cards.get(0));
    }

}
