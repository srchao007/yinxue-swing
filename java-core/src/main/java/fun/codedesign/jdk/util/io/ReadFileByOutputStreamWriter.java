package fun.codedesign.jdk.util.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 * 可自定义编码格式gbk，或者utf-8
 */
public class ReadFileByOutputStreamWriter {
    public static void main(String[] args) throws Exception {
        File file = new File("e:/");
        String[] filename = file.list();
        //System.out.println(Arrays.toString(names));
        String x = "filename.txt";
        FileOutputStream fos = new FileOutputStream(x);
        OutputStreamWriter osw = new OutputStreamWriter(fos, "gbk");
        String s = null;
        for (String n : filename) {
            s = n + "\r\n";
            osw.write(s);
        }
        osw.close();
        fos.close();
    }

    public static void byCheck(String s) {
    }
}

