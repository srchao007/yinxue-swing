package fun.codedesign.jdk.util.array;

import java.util.Arrays;


public class ArraysCopyDemo2 {
    public static void main(String[] args) {
        int[] line0 = {0, 1, 0, 1, 0, 1, 0, 0};
        int[] line1 = {0, 1, 1, 1, 1, 1, 0, 0};
        int[] line2 = {1, 1, 0, 1, 1, 1, 0, 1};
        int[] line3 = {1, 1, 1, 1, 1, 1, 1, 1};

//	int[] line = Arrays.copyOf(line0,8);
//	System.out.println(Arrays.toString(line));

        System.arraycopy(line2, 0, line3, 0, line2.length);
        System.arraycopy(line1, 0, line2, 0, line1.length);
        System.arraycopy(line0, 0, line1, 0, line0.length);

        Arrays.fill(line0, 8);
        System.arraycopy(line0, 0, line1, 0, line0.length);
        System.arraycopy(line0, 0, line2, 0, line0.length);
        System.arraycopy(line0, 0, line3, 0, line0.length);

        System.out.println(Arrays.toString(line0));
        System.out.println(Arrays.toString(line1));
        System.out.println(Arrays.toString(line2));
        System.out.println(Arrays.toString(line3));


    }
}
