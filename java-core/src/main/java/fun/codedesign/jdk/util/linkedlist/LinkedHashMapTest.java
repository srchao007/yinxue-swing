package fun.codedesign.jdk.util.linkedlist;

import org.junit.Test;

import java.util.LinkedHashMap;

public class LinkedHashMapTest {
    /**
     * 采用内部类实现
     * Entry<K,V> 存储数据
     */

    private static LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<>(16);

    @Test
    public void test() {
        linkedHashMap = new LinkedHashMap<>(); // 调用父类HashMap 初始容量16,扩容因子0.75f
        linkedHashMap.clear(); // 给数组填充null值
        linkedHashMap.get(null); // 调用的还是hashMap的get方法
        linkedHashMap.put(null, "string"); // put调用hashMap的方法
        linkedHashMap.equals(new LinkedHashMap<String, Object>());
        boolean flag = linkedHashMap.containsValue("string");
        System.out.println(flag);

        // linkedHashMap扩展的迭代器

    }
}
