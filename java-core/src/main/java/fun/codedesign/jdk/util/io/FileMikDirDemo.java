package fun.codedesign.jdk.util.io;

import java.io.File;
import java.util.Arrays;

public class FileMikDirDemo {
    public static void main(String[] args) {

        // 创建的文件夹，在创建文件之前需要先创建好相应的文件夹
        new File("money/rmb/fff/bbb/cc/dd/kk").mkdirs();
        new File("money2").mkdir();
        new File("momey3").mkdirs();

        File f = new File("momey3");
        f.delete();

        File f2 = new File("money");
        f2.delete();
        deleteFolder(f2);

        System.out.println(Arrays.toString(f2.listFiles()));
        File f16 = new File(".");
        System.out.println(Arrays.toString(f16.list()));
    }

    /**
     * 递归删除文件夹
     *
     * @param folder
     */
    public static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
            folder.delete();
        }
    }
}
