package fun.codedesign.jdk.util.date;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateDemo {
    
    @SuppressWarnings("all")
    public static void main(String[] args) {
        long now = System.currentTimeMillis();
        long year = 1970 + now / 1000 / 60 / 60 / 24 / 365;
        System.out.println(year);   //2017
        Date date = new Date(1000 * 60 * 60 * 24);
        System.out.println("11" + date);  // 11 Fri Jan 02 08:00:00 CST 1970

        System.out.println(date.getYear());//70

        int y = date.getYear() + 1900;
        System.out.println(y);//1970
        int m = date.getMonth() + 1;
        System.out.println(m);//1
        System.out.println(date);//Fri Jan 02 08:00:00 CST 1970

        Calendar c = new GregorianCalendar(1970, Calendar.JANUARY, 1);
        System.out.println(c.get(Calendar.YEAR) + "," + (c.get(Calendar.MONTH) + 1));
    }
}
