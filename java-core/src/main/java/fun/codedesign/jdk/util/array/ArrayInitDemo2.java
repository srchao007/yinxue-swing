package fun.codedesign.jdk.util.array;

/**
 * 初始赋值
 */
public class ArrayInitDemo2 {
    public static void main(String[] args) {
        int[] ary = {4, 5, 6};
        System.out.println(ary[0]);
        System.out.println(ary[1]);
        System.out.println(ary[2]);
    }
}
