package fun.codedesign.jdk.util.io;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomAccessFileDemo {
    public static void main(String[] args)
            throws IOException {
        File demo = new File("basic");
        if (!demo.exists()) {
            demo.mkdir();
        }
        File file = new File(demo, "raf.dat");
        if (!file.exists()) {
            file.createNewFile();
        }

        RandomAccessFile raf = new RandomAccessFile(file, "rw");
        System.out.println(raf.getFilePointer());
        raf.write('A'); //01000001
        System.out.println(raf.getFilePointer());
        raf.write('B'); //01000010

        int i = 0x61ffffff;
        raf.write(i >>> 24);//0x0000007f   0x7f
        raf.write(i >>> 16);//0x00007fff  0xff
        raf.write(i >>> 8); //0x007ffff   0xff
        raf.write(i); //0xff

        raf.writeInt(i);

        String s = "��";
        byte[] gbk = s.getBytes("GBK");
        raf.write(gbk);
        System.out.println(raf.length());
        System.out.println(raf.getFilePointer());

        System.out.println(raf.read());
        raf.seek(0);
//	System.out.println(raf.read());
//	System.out.println(raf.read());
//	System.out.println(raf.read());
//	System.out.println(raf.read());
        byte[] buf = new byte[(int) raf.length()];
        raf.read(buf);
        for (byte b : buf) {
            System.out.print(Integer.toHexString(b & 0xff) + " "); // 41 42 61 ffffffff ffffffff ffffffff 61 ffffffff ffffffff ffffffff ffffffd6 ffffffd0
        }
        raf.close();
    }
}
