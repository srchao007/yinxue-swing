package fun.codedesign.jdk.util.array;

import java.util.Arrays;

/**
 * 二分查找
 */
public class ArraysBinarySearch {
    public static void main(String[] args) {
        String[] hotel = {"Tom", "Andy", "Jerry", "Mike", "Nemo"};
        /**
         * A  J   M   N  T
         */
        int index = Arrays.binarySearch(hotel, "Jerry");
        System.out.println(index);//2

        index = Arrays.binarySearch(hotel, "Nemo");
        System.out.println(index);//4

        Arrays.sort(hotel);
        System.out.println(Arrays.toString(hotel));

        index = Arrays.binarySearch(hotel, "Jerry");
        System.out.println(index);//1

        index = Arrays.binarySearch(hotel, "Tom");
        System.out.println(index);//4

        index = Arrays.binarySearch(hotel, "Robin");
        System.out.println(index);//-5

    }
}
