package fun.codedesign.jdk.util.linkedlist;

public class LinkedListDemo1 {
    public static void main(String[] args) {
        Node head = new Node("hi");
        head.next = new Node("hello");
        head.next.next = new Node("word");
        System.out.println(head);
    }

}

class Node {
    Object value;
    Node next;

    public Node(Object obj) {
        value = obj;
    }

    public String toString() {
        return next == null ? value.toString() : value + "," + next;
    }
}