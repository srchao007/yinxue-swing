package fun.codedesign.jdk.util.array;

import java.util.Arrays;

/**
 * Arrays.equals()
 */
public class ArraysEqualsDemo {
    public static void main(String[] args) {
        char[] input = {'b', 'y', 'e'};
        char[] bye = {'b', 'y', 'e'};
        System.out.println(input = bye);// false
        System.out.println(Arrays.equals(input, bye)); //true

        String[] names = {"Tom", "Jerry", "Nemo"};
        System.out.println(names);
        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }
        String str = Arrays.toString(names);
        System.out.println(str);
        //System.out.println(bye);
    }
}
