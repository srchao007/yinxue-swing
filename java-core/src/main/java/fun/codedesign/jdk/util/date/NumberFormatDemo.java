package fun.codedesign.jdk.util.date;

import java.text.DecimalFormat;
import java.text.ParseException;

public class NumberFormatDemo {
    public static void main(String[] args) throws ParseException {
        String s = "7.8%";
        DecimalFormat format = new DecimalFormat("0.##%");
        double d = format.parse(s).doubleValue();
        System.out.println(d * 10); // 0.78

        d = 0.07891;
        System.out.println(format.format(d)); // 7.89%
    }
}
