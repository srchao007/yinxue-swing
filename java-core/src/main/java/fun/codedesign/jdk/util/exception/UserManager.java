package fun.codedesign.jdk.util.exception;

import java.util.HashMap;
import java.util.Map;

public class UserManager {
    private Map<String, User> users = new HashMap<>();
    private int id = 1;

    public User register(String name, String pwd)
            throws UserExistException {
        if (users.containsKey(name)) {
            throw new UserExistException("用户名不存在：" + name);
        }
        User user = new User(name, id++, pwd);
        users.put(name, user);
        return user;
    }

    public User login(String name, String pwd) throws UserOrPwdException {
        if (!users.containsKey(name)) {
            throw new UserOrPwdException("用户名错误");
        }
        User user = users.get(name);
        if (!user.getPwd().equals(pwd)) {
            throw new UserOrPwdException("密码错误");
        }
        return user;
    }

    static class UserExistException extends Exception {
        public UserExistException(String message) {
            super(message);
        }
    }

    static class UserOrPwdException extends Exception {
        public UserOrPwdException(String message) {
            super(message);
        }
    }
}





