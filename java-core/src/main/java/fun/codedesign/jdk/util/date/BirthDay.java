package fun.codedesign.jdk.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class BirthDay {
    public static void main(String[] args) throws ParseException {
        try(Scanner sc = new Scanner(System.in)){
            System.out.println("输入出生日期:");
            String s = sc.nextLine();
            long i = getBirthms(s);
            System.out.println("出生天数:" + getBirthDay(i));
        }
    }

    public static long getBirthms(String s) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        Date date = sdf.parse(s);
        long l = date.getTime();
        return l;
    }

    public static long getBirthDay(long l) {
        long i = System.currentTimeMillis();
        return (i - l) / 1000 / 3600 / 24;
    }
}
