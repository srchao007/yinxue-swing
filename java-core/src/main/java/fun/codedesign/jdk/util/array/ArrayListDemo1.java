package fun.codedesign.jdk.util.array;

import java.util.ArrayList;

import fun.codedesign.jdk.lang.oop.basic.card.Card;

public class ArrayListDemo1 {
    public static void main(String[] args) {
        ArrayList<Card> cards = new ArrayList<Card>();
        cards.add(new Card(Card.SPADE, Card.TEN));
        cards.add(new Card(Card.SPADE, Card.JACK));
        cards.add(new Card(Card.SPADE, Card.QUEEN));
        cards.add(new Card(Card.SPADE, Card.KING));
        cards.add(new Card(Card.SPADE, Card.ACE));
        System.out.println(cards);

        Card ace = new Card(Card.SPADE, Card.ACE);
        System.out.println(ace);

        cards.remove(ace);
        System.out.println(cards);
        cards.remove(0);
        System.out.println(cards);

//	for (int i = 0; i < cards.size(); i++) {
//		cards.remove(i);
//	}
//	System.out.println(cards);

//	for (int i = cards.size()-1; i >=0; i--) {
//		cards.remove(i);
//	}
//	System.out.println(cards);
        Card joker = new Card(Card.JOKER, Card.COLOR);
        System.out.println(cards.set(0, joker));
        System.out.println(cards);
    }
}
