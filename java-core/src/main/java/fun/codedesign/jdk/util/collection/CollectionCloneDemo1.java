package fun.codedesign.jdk.util.collection;

import java.util.ArrayList;

public class CollectionCloneDemo1 {

    @SuppressWarnings("all")
    public static void main(String[] agrs) {
        ArrayList<Foo> list1 = new ArrayList<>();
        list1.add(new Foo());
        list1.add(new Foo());
        ArrayList<Foo> list2 = (ArrayList<Foo>) list1.clone();
        System.out.println(list1.containsAll(list2));
        System.out.println(list2.containsAll(list1));
        System.out.println(list1 == list2);
        System.out.println(list1.get(0) == list2.get(0));
    }
}

class Foo {
    int a = 1;
}