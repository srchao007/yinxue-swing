package fun.codedesign.jdk.util.array;

import java.util.Arrays;
import java.util.Random;

public class ArraysSortTime {
    public static void main(String[] args) {
        int[] ary1 = new int[50000];
        Random rd = new Random();

        for (int i = 0; i < ary1.length; i++) {
            ary1[i] = rd.nextInt();
        }
        // System.out.println(Arrays.toString(ary1));
        int[] ary2 = Arrays.copyOf(ary1, ary1.length);

        long t1 = System.currentTimeMillis();
        Arrays.sort(ary2);
        long t2 = System.currentTimeMillis();
        System.out.println(t2 - t1);
    }
}
