package fun.codedesign.jdk.util.io;

import java.io.*;

public class WriteFileByFileOutputStream {
    public static void main(String[] args) throws IOException {
        OutputStream os = new FileOutputStream(new File("s1.txt"));
        byte[] b = {11, 22, 33, 44, 55, 66};
        for (byte a : b) {
            os.write(a);
        }
        os.close();
        InputStream is = new FileInputStream(new File("s1.txt"));
        int x = is.available();
        System.out.println("字节数量:" + x);
        try {
            for (int i = 0; i < x; i++) {
                System.out.print(is.read() + " ");
            }
        } catch (Exception e) {
            System.out.println("io异常");
        }
    }
}
