package fun.codedesign.jdk.util.exception;

public class TryCatchDemo {
    public static void main(String[] args) {
        System.out.println(test(null) + "," + test("0") + "," + test(" "));
    }

    public static int test(String s) {
        try {
            return s.charAt(0) - '0';
        } catch (NullPointerException e) {
            e.printStackTrace();
            return 1;
        } catch (RuntimeException e) {
            e.printStackTrace();
            return 2;
        } catch (Exception e) {
            e.printStackTrace();
            return 4;
        } finally {
            System.out.println("finally");
            //return 4;
        }
    }
}
