package fun.codedesign.jdk.util.xml;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;


/**
 * DOM解析
 *
 * @author zengjian
 * @create 2018-04-02 9:08
 * @since 1.0.0
 */
public class DOMXmlParser {
    public static void main(String[] args) throws Exception {
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        URL uri = DOMXmlParser.class.getResource("books.xml");
        Document doc = builder.parse(uri.getFile());
        NodeList list = doc.getElementsByTagName("book");
        for (int i = 0, size = list.getLength(); i < size; i++) {
            Node node = list.item(i);
            NamedNodeMap map = node.getAttributes();
            Node node1 = map.getNamedItem("name");
            System.out.println(node1);
        }
    }
}
