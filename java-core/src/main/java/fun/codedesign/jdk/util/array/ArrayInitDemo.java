package fun.codedesign.jdk.util.array;

/**
 * 数组初始值
 */
public class ArrayInitDemo {
    public static void main(String[] args) {
        int[] ary = new int[]{4, 5, 6};

        System.out.println(ary[0]);//4
        System.out.println(ary[1]);//5
        System.out.println(ary[2]);//6

        ary = new int[3];
        System.out.println(ary[0]);
        System.out.println(ary[1]);
        System.out.println(ary[2]);// 0

        String[] str = new String[3];
        System.out.println(str[0]); // null

        char[] cha = new char[3];
        System.out.println(cha[0]); // 口

        boolean[] boo = new boolean[3];
        System.out.println(boo[0]); // false

        double[] dou = new double[3];
        System.out.println(dou[0]); //0.0

        float[] flo = new float[3];
        System.out.println(flo[0]); //0.0


    }
}
