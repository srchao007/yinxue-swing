package fun.codedesign.jdk.util.linkedlist;

import java.util.LinkedList;

import fun.codedesign.jdk.lang.oop.basic.card.Card;

public class LinkedListDemo {
    public static void main(String[] args) {
        LinkedList<Card> list = new LinkedList<Card>();
        list.add(new Card(Card.SPADE, Card.SIX));
        list.add(new Card(Card.SPADE, Card.TEN));
        list.add(new Card(Card.SPADE, Card.NINE));
        list.add(new Card(Card.SPADE, Card.JACK));
        list.add(new Card(Card.SPADE, Card.FIVE));
        System.out.println(list);

        LinkedList<Card> list2 = new LinkedList<Card>();
        list2.add(new Card(Card.HEART, Card.SIX));
        list2.add(new Card(Card.HEART, Card.TEN));
        list2.add(new Card(Card.HEART, Card.NINE));
        list2.add(new Card(Card.HEART, Card.JACK));
        list2.add(new Card(Card.HEART, Card.FIVE));
        System.out.println(list2);

        list.addAll(list2);
        System.out.println(list);

        list.removeAll(list2);
        System.out.println(list);

        list.remove(0);
//	Card a1 = new Card(Card.SPADE, Card.FIVE);
        list.remove(new Card(Card.SPADE, Card.FIVE));
        System.out.println(list);

        list.set(0, new Card(Card.SPADE, Card.FIVE));
        System.out.println(list);
        System.out.println(list.indexOf(new Card(Card.SPADE, Card.NINE)));

        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));


    }
}
