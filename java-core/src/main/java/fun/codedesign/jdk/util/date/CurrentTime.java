
package fun.codedesign.jdk.util.date;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CurrentTime {
    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        long year = time / 1000 / 60 / 60 / 24 / 365 + 1970;
        System.out.println(year);

        Date date = new Date(time);
        System.out.println("现在时间:" + date);

        Calendar c = new GregorianCalendar();
        c.setTime(date);
        System.out.println(c.getTime());

        c.setTimeInMillis(time);
        System.out.println(c.getTime());

        System.out.println(c.getTimeZone());
        c.clear();
        System.out.println(c.getTime());
        Calendar.getInstance();
        System.out.println(Calendar.getInstance());
    }
}
