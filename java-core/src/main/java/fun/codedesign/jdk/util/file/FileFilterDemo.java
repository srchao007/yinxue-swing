package fun.codedesign.jdk.util.file;
import java.io.File;
import java.io.FileFilter;

public class FileFilterDemo {
    public static void main(String[] args) {
        File dir = new File("dddd/dd");
        File[] files = dir.listFiles(new NameFilter());

        for (File file : files) {
            System.out.println(file.getName());
        }
    }
}

class NameFilter implements FileFilter {
    public boolean accept(File file) {
        return file.getName().endsWith(".java") && file.isFile();
    }
}