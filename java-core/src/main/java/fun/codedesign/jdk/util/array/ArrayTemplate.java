package fun.codedesign.jdk.util.array;

import java.util.Arrays;

/**
 * 数组测试
 *
 * @author zengjian
 * @create 2018-03-15 9:44
 * @since 1.0.0
 */
public class ArrayTemplate {
    public static void main(String[] args) {
        Object[] objs = new Object[]{0, 1, 2, 3, 4};
        objs[3] = null;
        System.arraycopy(objs, 4, objs, 3, 1);
        System.out.println(Arrays.toString(objs));
    }
}
