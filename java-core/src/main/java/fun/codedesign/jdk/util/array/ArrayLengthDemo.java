package fun.codedesign.jdk.util.array;


/**
 *
 */
public class ArrayLengthDemo {
    public static void main(String[] args) {
        int[] ary = {4, 5, 6};
        ary[2] = 8;
        int n = ary[2];
        System.out.println(n);    //8
        //n = ary[3];
        ary = null;
        //n = ary[2];
        //int l = ary.length;
        // ary={7,8,9};
        ary = new int[]{7, 8, 9};

        int l = ary.length;
        System.out.println(l); // l=3

        ary[ary.length - 1] = 10;
        System.out.println(ary[ary.length - 1]);//10

        int[] ary2 = {6, 7, 8, 9, 10};
        System.out.println(ary2[0]);
        ary2[1] = 10;
        System.out.println(ary2[1]);
        l = ary2.length;
        System.out.println(l);

        while (true) {
            int[] ary3 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
            l = ary3.length;
            System.out.println(l);
            break;
        }


    }
}
