package fun.codedesign.jdk.util.exception;

public class ExceptionTest {
    public static void main(String[] args) throws Exception {
        UserManager m = new UserManager();
        m.register("zhangsan", "1234");
        System.out.println("注册成功");
        m.register("lisi", "1234");
        System.out.println("注册成功");

        User user = m.login("zhangsan", "1234");
        System.out.println(user.getName() + "登录成功");
    }
}
