package fun.codedesign.jdk.util.collection;

import java.util.ArrayList;
import java.util.LinkedList;

import fun.codedesign.jdk.lang.oop.basic.card.Card;


public class CollectionCloneDemo2 {
    public static void main(String[] args) {
        ArrayList<Card> cards = new ArrayList<Card>();
        cards.add(new Card(Card.JOKER, Card.BLACK));
        cards.add(new Card(Card.JOKER, Card.COLOR));

        LinkedList<Card> others = new LinkedList<Card>(cards);
        System.out.println(cards);
        System.out.println(others);

        System.out.println(cards.get(0) == others.get(0));
    }
}
