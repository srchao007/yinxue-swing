package fun.codedesign.jdk.util.io;

import java.io.*;

public class IOUtils {
    public static void printHex(String fileName) throws IOException {
        FileInputStream fis = new FileInputStream(fileName);
        int b;
        int i = 1;
        while ((b = fis.read()) != -1) {
            if (b <= 0xf) {
                System.out.print("0");
            }
            System.out.print(Integer.toHexString(b) + " ");
            if (i++ % 10 == 0) {
                System.out.println();
            }
        }
        fis.close();
    }

    public static void printHex(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        int b;
        int i = 1;
        while ((b = fis.read()) != -1) {
            if (b <= 0xf) {
                System.out.print("0");
            }
            System.out.println(Integer.toHexString(b) + "");
            if (i++ % 10 == 0) {
                System.out.println();
            }
        }
        fis.close();
    }

    public static byte[] read(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] buf = new byte[(int) file.length()];
        FileInputStream fis = new FileInputStream(file);
        fis.read(buf);
        fis.close();
        return buf;
    }


    public static void copy1(String src, String dest) throws IOException {
        FileInputStream fis = new FileInputStream(src);
        FileOutputStream fos = new FileOutputStream(dest);
        int b;
        while ((b = fis.read()) != -1) {
            fos.write(b);
        }
        fos.close();
        fis.close();
    }


    public static void copy2(String src, String dest)
            throws IOException {
        InputStream in = new BufferedInputStream(new FileInputStream(src));
        OutputStream out = new BufferedOutputStream(new FileOutputStream(dest));
        int b;
        while ((b = in.read()) != -1) {
            out.write(b);
        }
        out.close();
        in.close();
    }

    public static void copy3(String src, String dest)
            throws IOException {
        FileInputStream fis = new FileInputStream(src);
        FileOutputStream fos = new FileOutputStream(dest);
        byte[] buf = new byte[1024 * 512];
        int count;
        while ((count = fis.read(buf)) != -1) {
            fos.write(buf, 0, count);
        }
        fos.close();
        fis.close();
    }
}



