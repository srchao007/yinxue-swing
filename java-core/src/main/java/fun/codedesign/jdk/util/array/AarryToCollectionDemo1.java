package fun.codedesign.jdk.util.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AarryToCollectionDemo1 {
    public static void main(String[] args) {
        String[] names = {"123", "321"};
        List<String> list = Arrays.asList(names);
        System.out.println(list);

        List<String> list1 = new ArrayList<String>(list);
        list1.add("abc");
        System.out.println(list1);
    }
}
