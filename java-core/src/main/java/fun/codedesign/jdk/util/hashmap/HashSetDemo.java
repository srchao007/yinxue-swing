package fun.codedesign.jdk.util.hashmap;

import java.util.HashSet;
import java.util.Set;

import fun.codedesign.jdk.lang.oop.basic.card.Card;

public class HashSetDemo {
    public static void main(String[] args) {
        Set<Card> cards = new HashSet<Card>();
        cards.add(new Card(Card.JOKER, Card.COLOR));
        cards.add(new Card(Card.JOKER, Card.BLACK));
        System.out.println(cards);
        cards.remove(new Card(Card.JOKER, Card.BLACK));
        System.out.println(cards);
    }
}
