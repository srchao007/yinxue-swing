package fun.codedesign.jdk.math.algorithm.sort;

/**
 * 选择排序<br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-06-25 16:48
 * @since 1.0.0
 */
public class SelectSorter implements Sorter {
    @Override
    public void sort(int[] arr) {
        for (int i = 0, length = arr.length; i < length; i++) {
            int index = i;
            for (int j = i + 1; j < length; j++) {
                if (arr[index] > arr[j]) {
                    index = j;
                }
            }
            if (index != i) {
                int temp = arr[i];
                arr[i] = arr[index];
                arr[index] = temp;
            }
        }
    }
}
