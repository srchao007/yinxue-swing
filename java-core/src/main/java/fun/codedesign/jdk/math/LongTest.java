package fun.codedesign.jdk.math;

import org.junit.Test;

public class LongTest {

    public static void main(String[] args) {
        Long l = 2189117L;
        long l1 = l.longValue() / 1024 / 1024;
        System.out.println(l.longValue() / 1024 / 1024 + ":");
        System.out.println(l1);
    }

    @Test
    public void testRange() throws Exception {
        long l1 = 1;
        for (int i = 1; i <= 1000; i++) {
            l1 *= i;
            System.out.println(l1);
        }
        // long的最小值相乘为0
		/*long l2 = Long.MIN_VALUE;
		System.out.println(l2*l2);*/
    }
}
