package fun.codedesign.jdk.math;

/**
 * 打印数字的个时百千
 */
public class PrintGeShiBaiQian {
    public static void main(String[] args) {
        int a = 9478;
        int gw = a % 10;
        int sw = a / 10 % 10;
        int bw = a / 100 % 10;
        int qw = a / 1000;
        System.out.println("个位：" + gw);
        System.out.println("十位：" + sw);
        System.out.println("百位：" + bw);
        System.out.println("千位：" + qw);
    }
}
