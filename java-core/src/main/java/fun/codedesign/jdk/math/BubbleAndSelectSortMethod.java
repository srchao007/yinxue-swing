package fun.codedesign.jdk.math;

import java.util.Arrays;

/**
 * 冒泡排序和选择排序
 */
public class BubbleAndSelectSortMethod {
    public static void main(String[] args) {
        int[] ary = {8, 5, 1, 7, 6, 9, 2, 4, 1};
        xuanZeSort(ary);
        //maoPaoSort(ary);
        System.out.println(Arrays.toString(ary));
    }

    public static void maoPaoSort(int[] ary) {
        for (int i = 0; i < ary.length - 1; i++) {
            for (int j = 0; j < ary.length - 1 - i; j++) {
                if (ary[j] > ary[j + 1]) {
                    int temp = ary[j];
                    ary[j] = ary[j + 1];
                    ary[j + 1] = temp;
                }
            }
        }
    }

    public static void xuanZeSort(int[] ary) {
        for (int i = 0; i < ary.length - 1; i++) {
            for (int j = i + 1; j < ary.length; j++) {
                if (ary[i] > ary[j]) {
                    int temp = ary[i];
                    ary[i] = ary[j];
                    ary[j] = temp;
                }
            }
        }
    }
}
