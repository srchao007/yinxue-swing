package fun.codedesign.jdk.math.fibonacci;

import java.util.Arrays;
import java.util.Scanner;


public class FibonnacciMethodDemo3 {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            System.out.println("输入数字：");
            int num = sc.nextInt();
    
            int[] ary = new int[num];
            int answer = ary[num - 1];
            System.out.println("得到Fibonnacci数：" + Fibonnacci(num));
            System.out.println(Arrays.toString(ary));
            System.out.println(answer);
        }
    }

    public static int Fibonnacci(int i) {
        if (i == 1 || i == 2) {
            return 1;
        } else {
            return Fibonnacci(i - 1) + Fibonnacci(i - 2);
        }
    }
}
