package fun.codedesign.jdk.math;

/*
 * 百钱买百鸡
 */
public class ChickenDemo {
    public static void main(String[] args) {
        for (int x = 0; x <= 20; x++) {
            for (int y = 0; y <= 33; y++) {
                if ((5 * x + 3 * y + (100 - x - y) / 3 == 100) && ((100 - x - y) % 3 == 0)) {
                    System.out.println("公鸡：" + x + " 母鸡" + y + " 小鸡" + (100 - x - y));
                }
            }
        }
    }
}
