package fun.codedesign.jdk.math.algorithm.test.data;

import fun.codedesign.jdk.math.algorithm.data.Stack;
import org.junit.Assert;
import org.junit.Test;

public class StackTest {

    @Test
    public void push() {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < 100; i++) {
            stack.push(i);
        }
        Assert.assertEquals(100, stack.size());
        Assert.assertEquals(false, stack.isEmpty());
        for (int i = 99; i >= 0; i--) {
            Assert.assertEquals(i, stack.pop().intValue());
        }
        Assert.assertEquals(true, stack.isEmpty());
    }
}