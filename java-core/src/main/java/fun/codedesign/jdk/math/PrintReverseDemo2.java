package fun.codedesign.jdk.math;

public class PrintReverseDemo2 {
    public static void main(String[] args) {
        String s = "123456";
        for (int i = s.length() - 1; i >= 0; i--) {
            System.out.print(s.charAt(i));
        }
    }
}
