package fun.codedesign.jdk.math.reverse;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AnagramsTest {

    private List<String> list = new ArrayList<String>() {
        {
            add("abcde");
            add("bcdefff");
            add("aaabbb");
            add("bbbaaa");
            add("java");
            add("jack");
            add("jkca");
            add("jakc");
        }
    };

    @Test
    public void testGetAnagrams() throws Exception {
        Anagrams anagrams = new Anagrams(list);
        assertEquals("[jack, jkca, jakc]", anagrams.getAnagrams("jack").toString());
        assertEquals("[aaabbb, bbbaaa]", anagrams.getAnagrams("ababab").toString());
        System.out.println(anagrams.getAnagrams("jack"));
        System.out.println(anagrams.getAnagrams("ababab"));
    }
}
