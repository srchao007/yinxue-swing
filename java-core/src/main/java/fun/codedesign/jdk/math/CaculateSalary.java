package fun.codedesign.jdk.math;

import java.util.Scanner;

/**
 * 计算薪水
 */
public class CaculateSalary {
    public static void main(String[] args) {
        int a = 600;
        int b = a >= 500 ? 400 / 3 : 605 % 100;
        System.out.println(b);//133

        try(Scanner sc2 = new Scanner(System.in)) {
            System.out.print("输入薪水：");
            double salary = sc2.nextDouble();
            salary = salary >= 10000 ? salary + salary * 0.15 : salary + salary * 0.25;
            System.out.println("薪水:" + salary);
        }
    }
}
