/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Queue
 * Author:   zengjian
 * Date:     2018/9/18 16:43
 * Description: 先进先出队列
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.jdk.math.algorithm.data;

import java.util.Iterator;

/**
 * 〈先进先出队列〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/18 16:43
 */
public class Queue<Item> implements Iterable<Item> {

    private ItemWrapper first;
    private ItemWrapper last;
    private int size;

    public Queue() {
    }

    public void enqueue(Item item) {
        if (first == null) {
            first = new ItemWrapper(null, item);
            last = first;
        } else {
            ItemWrapper itemWrapper = new ItemWrapper(last, item);
            last.next = itemWrapper;
            this.last = itemWrapper;
        }
        size++;
    }

    public Item dequeue() {
        Item item = first == null ? null : first.item();
        if (item != null) {
            first = first.next;
        }
        size--;
        return item;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return size;
    }

    // TODO
    @Override
    public Iterator<Item> iterator() {
        return null;
    }

    private class ItemWrapper {

        ItemWrapper prev;
        Item item;
        ItemWrapper next;

        public ItemWrapper(ItemWrapper prev, Item item) {
            this.prev = prev;
            this.item = item;
        }

        public ItemWrapper(ItemWrapper prev, Item item, ItemWrapper next) {
            this.prev = prev;
            this.item = item;
            this.next = next;
        }

        Item item() {
            return item;
        }
    }
}