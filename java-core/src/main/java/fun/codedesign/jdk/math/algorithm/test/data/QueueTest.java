package fun.codedesign.jdk.math.algorithm.test.data;

import fun.codedesign.jdk.math.algorithm.data.Queue;
import org.junit.Assert;
import org.junit.Test;

public class QueueTest {

    @Test
    public void enqueue() {
        Queue<Integer> queue = new Queue<>();
        for (int i = 0; i < 100; i++) {
            queue.enqueue(i);
        }
        Assert.assertEquals(100, queue.size());
        Assert.assertEquals(false, queue.isEmpty());
        for (int i = 0; i < 100; i++) {
            Assert.assertEquals(i, queue.dequeue().intValue());
        }
        Assert.assertEquals(0, queue.size());
        Assert.assertEquals(true, queue.isEmpty());
    }
}