package fun.codedesign.jdk.math;

public class SumOperation {
    public static void main(String[] args) {
        int a = 0;
        a++;//a=a+1;
        System.out.println(a);// a=1
        ++a;
        System.out.println(a);//2

        a = 1;
        System.out.println(a);//a=1
        System.out.println(a++);//1  a++=1   a=2

        System.out.println(a);//a=2

        a = 1;
        a = a++;//a++=1   a=2   a=a++=1
        System.out.println(a);
        System.out.println(a++);
    }
}
