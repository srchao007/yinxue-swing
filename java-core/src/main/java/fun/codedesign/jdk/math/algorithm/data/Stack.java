/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Stack
 * Author:   zengjian
 * Date:     2018/9/18 17:14
 * Description: 后进先出
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.jdk.math.algorithm.data;

/**
 * 〈后进先出〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/18 17:14
 */
public class Stack<Item> {

    private int size;
    private Item[] items = null;

    public Stack() {
        items = (Item[]) new Object[10];
    }

    public void push(Item item) {
        if (size() + 1 > items.length) {
            Item[] enlargeItems = (Item[]) new Object[(int) (items.length * 1.5)];
            System.arraycopy(items, 0, enlargeItems, 0, items.length);
            items = enlargeItems;
        }
        items[size++] = item;
    }

    public Item pop() {
        Item item = size() == 0 ? null : items[--size];
        items[size] = null;
        return item;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

}