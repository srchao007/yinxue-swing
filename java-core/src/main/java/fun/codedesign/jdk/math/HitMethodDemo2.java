package fun.codedesign.jdk.math;

public class HitMethodDemo2 {
    public static void main(String[] args) {
        System.out.println(HitMethodDemo2.hit1(2, 2, 3, 4, 9, 6));
        System.out.println(HitMethodDemo2.hit2(5, 5, 5, 8, 3));
    }

    public static boolean hit1(int x, int y, int w, int h, int x1, int y1) {
        boolean s1;
        s1 = x1 < x + w && y1 < y + h;
        return s1;
    }

    public static boolean hit2(int x, int y, int r, int x1, int y1) {
        int a = x1 - x;
        int b = y1 - y;
        double c = Math.sqrt(a * a + b * b);
        return c <= r;
    }
}

