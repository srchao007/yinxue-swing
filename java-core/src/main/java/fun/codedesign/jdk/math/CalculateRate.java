package fun.codedesign.jdk.math;

import java.util.Scanner;


public class CalculateRate {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            System.out.print("输入定期存储年数:");
            int year = sc.nextInt();
            double base = 7.7 / 100;
            double rate = base;//
            if (year <= 1 && year > 0) {
                rate /= 2;
            } else if (year <= 3 && year > 1) {
                rate *= 0.7;
            } else if (year <= 5 && year > 3) {
                rate = base;
            } else {
                rate *= 1.1;
            }
            System.out.println("年利率:" + rate);
        }
    }
}

