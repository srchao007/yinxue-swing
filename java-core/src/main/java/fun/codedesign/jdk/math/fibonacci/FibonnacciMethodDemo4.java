package fun.codedesign.jdk.math.fibonacci;

/*
 * Fibonnacci
 *
 */

public class FibonnacciMethodDemo4 {
    public static void main(String[] args) {
        System.out.println(f(6));
        System.out.println(f(47));
        System.out.println(f(48));
        System.out.println((double) f(47) / f(48));

    }

    public static long f(int n) {
        long f1 = 1, f2 = 1, fn = 1;
        for (int i = 3; i <= n; i++) {
            fn = f1 + f2;    // i=3   fn=1+1=2   f1=f2=1  f2=2
            f1 = f2;
            f2 = fn;
        }
        return fn;
    }
}
