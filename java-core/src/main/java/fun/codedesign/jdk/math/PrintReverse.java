package fun.codedesign.jdk.math;

/**
 * 倒序打印数字
 */
public class PrintReverse {
    public static void main(String[] args) {
        int i = 1;
        i += 2;//i=i+2
        i *= 3;//i=i*3
        i %= 4;//i=i%4
        System.out.println(i);
        int num = 63785;
        num %= 10;
        System.out.print(num);

        num = 63785;
        num = num / 10;
        num %= 10;
        System.out.print(num);

        num = 63785;
        num = num / 100;
        num %= 10;
        System.out.print(num);

        num = 63785;
        num = num / 1000;
        num %= 10;
        System.out.print(num);

        num = 63785;
        num = num / 10000;
        num %= 10;
        System.out.print(num);

    }
}
