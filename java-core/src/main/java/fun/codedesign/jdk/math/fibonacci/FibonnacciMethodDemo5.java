package fun.codedesign.jdk.math.fibonacci;

/**
 * 1  1   2  3  5  8  13   21   34...
 */
public class FibonnacciMethodDemo5 {
    public static void main(String[] args) {
        System.out.println(f(10));
        System.out.println(f1(10));
    }

    public static long f(long n) {

        if (n == 1 || n == 2) {
            return 1;
        }
        return f(n - 1) + f(n - 2);
    }

    public static long f1(long n) {
        long f1 = 1, f2 = 1, fn = 1;
        for (int i = 3; i <= n; i++) {
            fn = f1 + f2;
            f1 = f2;
            f2 = fn;
        }
        return fn;
    }
}