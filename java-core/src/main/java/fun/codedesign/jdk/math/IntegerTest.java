package fun.codedesign.jdk.math;

/**
 * 测试IntegerValueOf
 *
 * @author zengjian
 * @create 2018-03-20 11:11
 * @since 1.0.0
 */
public class IntegerTest {

    public static void main(String[] args) {
        // 2147483647
        // 3635751208
        // Integer i  = Integer.valueOf("3635751208");
        Integer i = Integer.MAX_VALUE;
        System.out.println(i);
    }
}
