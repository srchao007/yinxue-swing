package fun.codedesign.jdk.math.reverse;

import org.junit.Test;

public class LinkedListReverseTest {
    
    @Test
    public void testReverse() throws Exception {
        LinkedList<Integer> four = new LinkedList<Integer>(4, null);
        LinkedList<Integer> three = new LinkedList<Integer>(3, four);
        LinkedList<Integer> two = new LinkedList<Integer>(2, three);
        LinkedList<Integer> one = new LinkedList<Integer>(1, two);
        LinkedList<Integer> result = LinkedList.reverse(one);
        System.out.println(result);
    }
}
