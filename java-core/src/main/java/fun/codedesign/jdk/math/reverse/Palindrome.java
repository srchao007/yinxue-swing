package fun.codedesign.jdk.math.reverse;

public class Palindrome {
    /**
     * 回文，指单词或者短语字母翻转之后得到的拼写依然和原来一致，比如  eve level top spot
     * <p>与翻转String类似，但还需要考虑非字母的字符，仅对字母进行了判断
     */
    public static void main(String[] args) {
		/*System.out.println(isPalindrome("aba"));
		System.out.println(isPalindrome("a1ba"));
		System.out.println(isPalindrome("a123ba"));*/
        System.out.println(isPalindrome("121"));

        System.out.println(strictPalindrome("121")); //true
        System.out.println(strictPalindrome("123")); //false

    }

    /**
     * 回文判断，通过两个指针进行判断
     * <p>如果是 aba 的情况会返回true
     * <p>如果是 a1ba 的情况指针会忽略调1返回true
     * <p>如果是 121 的情况会返回false 因为全部是数字, 可以注释调部分逻辑修改为数字也可以,(可以用翻转的模式)
     *
     * @param s
     * @return
     */
    public static boolean isPalindrome(final String s) {
        final String toCheck = s.toLowerCase();
        int left = 0;
        int right = toCheck.length() - 1;
        // 判断依据，由两个指针判断，各负责String的两侧，当两个指针发生检查就检查完毕
        while (left <= right) {
            // 非字母指针left +1 right指针减1,这里忽略了非字母的情况
            while (left < toCheck.length() && !Character.isLetter(toCheck.charAt(left))) {
                left++;
            }
            while (right > 0 && !Character.isLetter(toCheck.charAt(right))) {
                right--;
            }
            // 指针超过范围返回false
            if (left >= toCheck.length() || right < 0) {
                return false;
            }
            if (toCheck.charAt(left) != toCheck.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

    /**
     * 严格回文检查器
     * <p> 保证逆序后的字符串与原来字符串完全相等即可
     *
     * @param s
     * @return
     * @see Reverse#inPlaceReverse(String)
     */
    public static boolean strictPalindrome(final String s) {
        return s.equals(Reverse.inPlaceReverse(s));
    }
}
