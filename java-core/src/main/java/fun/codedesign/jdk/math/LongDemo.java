package fun.codedesign.jdk.math;

/**
 * 64位整型
 */
public class LongDemo {
    public static void main(String[] args) {
        long now = System.currentTimeMillis();
        long year = now / 1000 / 60 / 60 / 24 / 365 + 1970;
        System.out.println(year); //2017

        long max = Long.MAX_VALUE;
        year = max / 1000 / 60 / 60 / 24 / 365 + 1970;
        System.out.println(year);
    }
}
