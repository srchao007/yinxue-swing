package fun.codedesign.jdk.math.fibonacci;

public class ValueOutOfRangeException extends RuntimeException {

    private static final long serialVersionUID = 188059830718858310L;

    public ValueOutOfRangeException(String message) {
        super(message);
    }

}
