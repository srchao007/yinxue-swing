package fun.codedesign.jdk.math;

import java.util.Random;

/**
 * 随机数表示，比如负载平衡中的随机算法
 */
public class RandomDemo {
    public static void main(String[] args) {
        Random rd = new Random();
        int n = rd.nextInt(26); // [0,26)
        //char c = 'A'+n;
        char c = (char) ('A' + n);
        System.out.println(c);
    }
}
