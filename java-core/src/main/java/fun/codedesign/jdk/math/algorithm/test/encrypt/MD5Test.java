package fun.codedesign.jdk.math.algorithm.test.encrypt;

import org.junit.Test;
import sun.security.provider.MD4;
import sun.security.provider.MD5;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class MD5Test {

    @Test
    public void test01() {
        MD5 md5 = new MD5();
        MessageDigest messageDigest = MD4.getInstance();
        System.out.println(Arrays.toString(messageDigest.digest()));
    }

    @Test
    public void test02() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        String code = "hello";
        byte[] bt = md.digest(code.getBytes());
        System.out.println(bt.length);
        System.out.println(new String(bt));
    }

}