package fun.codedesign.jdk.math;

import java.util.Random;
import java.util.Scanner;

/**
 * 人机猜拳
 */
public class HumanComputerCompare {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            Random rd = new Random();
            int score = 0;
            int count = 1;
            int sum = 0;
    
            System.out.println("是否开始? Y/N");
            String gs = sc.nextLine();
            if (gs.equals("Y") || gs.equals("y")) {
                System.out.println("开始...");
                System.out.println("0，1，2代表石头剪子布：");
                for (count = 1; count <= 10; count++) {
                    //String str = sc.nextLine();
                    int human = sc.nextInt();
                    int pc = rd.nextInt(3);
                    System.out.println(pc);
    
                    if (human - pc == 0) {
                        score = 1;
                    } else if (human - pc == -1) {
                        score = 2;
                    } else if (human - pc == -2) {
                        score = 0;
                    }
                    sum = sum + score;
                }
                System.out.println("最终得分:" + sum);
    
            } else {
                System.out.println("结束游戏");
            }
        }
    }
}
