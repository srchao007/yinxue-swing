package fun.codedesign.jdk.math;

public class SumOperation3 {
    public static void main(String[] args) {

        int a = 2;//
        a = a++ + a + ++a + --a - a + a++ + a + --a + a + a-- + a + a++ + --a + a + a++ + a;
        //a:    3 3 4 3 3 4 4 3 3 2 2 3 2 2 3 3
        //ʽ��2 3 4 3-3 3 4 3 3 3 2 2 2 2 2 3


        System.out.println(a);

        int b = 2 + 3 + 4 + 3 - 3 + 4 + 3 + 3 + 3 + 3 + 2 + 2 + 2 + 2 + 2 + 3;
        System.out.println(b);
    }
}
