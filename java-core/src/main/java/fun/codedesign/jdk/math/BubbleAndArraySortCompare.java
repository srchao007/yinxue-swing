package fun.codedesign.jdk.math;

import java.util.Arrays;
import java.util.Random;

public class BubbleAndArraySortCompare {
    public static void main(String[] args) {
        //int[] ary = {3,4,2,7,9,6,5,1};
        int[] ary1 = new int[1000];
        Random rd = new Random();
        for (int i = 0; i < ary1.length; i++) {
            ary1[i] = rd.nextInt();
        }
        // System.out.println(Arrays.toString(ary1));
        int[] ary2 = Arrays.copyOf(ary1, ary1.length);

        long t1 = System.currentTimeMillis();
        BubbleAndArraySortCompare.sort(ary2);
        long t2 = System.currentTimeMillis();
        System.out.println(t2 - t1);  // ArrayLengthDemo.sort

        t1 = System.currentTimeMillis();
        Arrays.sort(ary2);
        t2 = System.currentTimeMillis();
        System.out.println(t2 - t1);// Arrays.sort
//	    System.out.println(Arrays.toString(ary2));
    }

    public static void sort(int[] ary) {
        for (int i = 0; i < ary.length - 1; i++) {
            for (int j = 0; j < ary.length - 1 - i; j++) {
                if (ary[j] > ary[j + 1]) {
                    int t = ary[j];
                    ary[j] = ary[j + 1];
                    ary[j + 1] = t;
                }
            }
        }
    }
}
