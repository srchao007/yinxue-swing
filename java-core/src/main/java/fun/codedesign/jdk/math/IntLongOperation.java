package fun.codedesign.jdk.math;


public class IntLongOperation {
    public static void main(String[] args) {

        int x = 5;
        int y = 3;
        int z = x + y;
        System.out.println(z);

        long l = x + y;

        System.out.println(l);//8
        l = 5L + y;

        System.out.println(l); //8


        l = Integer.MAX_VALUE + 1L;
        System.out.println(l);//2147483648

        x = 5;
        y = 2;
        z = x / y;
        System.out.println(z);
    }
}
