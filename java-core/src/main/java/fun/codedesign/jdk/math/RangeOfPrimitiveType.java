package fun.codedesign.jdk.math;

public class RangeOfPrimitiveType {
    public static void main(String[] args) {
        int a = 2;
        int b = 3;
        int c = a + b;
        System.out.println(c);

        byte x = 127;
        short y = -32768;

        System.out.println(x); //127
        System.out.println(y);//-32768


        int max = Integer.MAX_VALUE;
        System.out.println(max);
        c = max + 1;
        System.out.println(c); //-2147483648
        int min = Integer.MIN_VALUE;
        System.out.println(min);//-2147483648
        System.out.println(Integer.toBinaryString(max));
        System.out.println(Integer.toBinaryString(min));

    }
}
