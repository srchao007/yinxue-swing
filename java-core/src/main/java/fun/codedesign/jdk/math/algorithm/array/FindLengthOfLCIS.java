package fun.codedesign.jdk.math.algorithm.array;

/**
 * 674. 最长连续递增序列
 * 给定一个未经排序的整数数组，找到最长且 连续递增的子序列，并返回该序列的长度。
 * <p>
 * 连续递增的子序列 可以由两个下标 l 和 r（l < r）确定，如果对于每个 l <= i < r，都有 nums[i] < nums[i + 1] ，那么子序列 [nums[l], nums[l + 1], ..., nums[r - 1], nums[r]] 就是连续递增子序列。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [1,3,5,4,7]
 * 输出：3
 * 解释：最长连续递增序列是 [1,3,5], 长度为3。
 * 尽管 [1,3,5,7] 也是升序的子序列, 但它不是连续的，因为 5 和 7 在原数组里被 4 隔开。
 * 示例 2：
 * <p>
 * 输入：nums = [2,2,2,2,2]
 * 输出：1
 * 解释：最长连续递增序列是 [2], 长度为1。
 * <p>
 * <p>
 * 提示：
 * <p>
 * 0 <= nums.length <= 104
 * -109 <= nums[i] <= 109
 */
public class FindLengthOfLCIS {

    /**
     * 思路：for循环+双指针
     * 固定下标i数字，快指针x往前后，如果比前一位置大，则继续走，否则跳出进下一循环，慢指针赋值
     *
     * @param nums
     * @return
     */
    public static int findLengthOfLCIS(int[] nums) {
        int maxLength = 0;
        int slow = 0;
        int quick = 1;
        if (nums.length == 1) {
            return 1;
        }
        while (quick < nums.length) {
            while (nums[quick] > nums[quick - 1]) {
                quick++;
                if (quick == nums.length) {
                    break;
                }
            }
            maxLength = Math.max(maxLength, quick - slow);
            slow = quick;
            quick++;
        }
        return maxLength;
    }

    public static void main(String[] args) {
        System.out.println(findLengthOfLCIS(new int[]{1, 3, 5, 4, 7}));
        System.out.println(findLengthOfLCIS(new int[]{2, 2, 2, 2}));
        System.out.println(findLengthOfLCIS(new int[]{1, 3, 5, 7}));
        System.out.println(findLengthOfLCIS(new int[]{1, 2}));
        System.out.println(findLengthOfLCIS(new int[]{1}));
    }
}
