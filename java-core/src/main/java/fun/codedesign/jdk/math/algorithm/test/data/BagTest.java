package fun.codedesign.jdk.math.algorithm.test.data;

import fun.codedesign.jdk.math.algorithm.data.Bag;
import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

public class BagTest {

    @Test
    public void test01() {
        Bag<Double> bag = new Bag<>();
        for (int i = 0; i < 100; i++) {
            bag.add(1.3 + i);
        }
        Assert.assertEquals(100, bag.size());
        Assert.assertEquals(false, bag.isEmpty());
    }

    @Test
    public void test02() {
        Bag<Integer> bag = new Bag<>();
        for (int i = 0; i < 1000; i++) {
            bag.add(i);
        }
        Iterator<Integer> iterator = bag.iterator();
        int count = 0;
        while (iterator.hasNext()) {
            Assert.assertEquals((long) count, (long) iterator.next());
            count++;
        }
    }

}