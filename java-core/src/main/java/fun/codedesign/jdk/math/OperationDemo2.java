package fun.codedesign.jdk.math;

/**
 * 混合运算
 */
public class OperationDemo2 {
    public static void main(String[] args) {
        int i = 0;//
        System.out.println(i++ % 4);//i=1  i++=0  0
        System.out.println(i++ % 4);//i=2  i++=1  1
        System.out.println(i++ % 4);//i=3  i++=2  2
        System.out.println(i++ % 4);//i=4  i++=3  3
        System.out.println(i++ % 4);//i=5  i++=4  0
        System.out.println(i++ % 4);//i=6  i++=5  1
        System.out.println(i++ % 4);//i=7  i++=6  2
    }
}
