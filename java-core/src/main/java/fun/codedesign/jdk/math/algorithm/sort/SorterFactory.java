package fun.codedesign.jdk.math.algorithm.sort;
/**
 * 排序工厂 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-06-26 16:16
 * @since 1.0.0
 */
public class SorterFactory {

    public static Sorter  getSorter(Class<? extends Sorter> clazz) throws Exception {
        return clazz.newInstance();
    }

    public static Sorter newBubbleSoreter(){
        return new BubbleSorter();
    }

    public static Sorter newQuickSorter(){
        return new QuickSorter();
    }
}
