package fun.codedesign.jdk.math;

import java.util.Arrays;
import java.util.Random;

/*
 * 7球生成方法封装
 */
public class RandomSevenBallMethod {
    public static void main(String[] args) {
        String[] ary = gen();
        System.out.println(Arrays.toString(ary));
    }

    public static String[] gen() {
        String[] pool = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33"};
        String[] pool2 = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16"}; //��������16��

        String[] ball = new String[6];
        String[] ball2 = new String[1];
        Random rd = new Random();

        boolean[] used = new boolean[pool.length];
        int i = 0;
        while (true) {
            int num = rd.nextInt(pool.length);
            if (used[num]) {
                continue;
            }
            ball[i++] = pool[num];
            used[num] = true;
            if (i == 6) {
                break;
            }
        }
        Arrays.sort(ball);
        //System.out.println(Arrays.toString(ball));
        ball2[0] = pool2[rd.nextInt(pool2.length)];
        ball = Arrays.copyOf(ball, ball.length + 1);
        //System.out.println(Arrays.toString(ball));
        ball[6] = ball2[0];
        //System.out.println(Arrays.toString(ball));
        return ball;
    }
}
