package fun.codedesign.jdk.math;

import java.util.Scanner;


/**
 * 缴税计算
 */
public class CaculateTax {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)){
            System.out.print("输入收入：");
            double salary = sc.nextDouble();
            double tax, income;
            tax = 0;
            income = salary;
            if (salary < 3500) {
                tax = 0;
            } else if (salary > 3500 && salary <= 5000) {
                tax = (salary - 3500) * 3 / 100;
            } else if (salary > 5000 && salary <= 8000) {
                tax = (salary - 5000) * 10 / 100 + 1500 * 3 / 100;
            } else if (salary > 8000 && salary <= 12500) {
                tax = (salary - 8000) * 20 / 100 + 3000 * 10 / 100 + 1500 * 3 / 100;
            }
            income = salary - tax;
            System.out.println("缴纳五险一金" + tax);
            System.out.println("实际收入" + income);
        }
    }
}
