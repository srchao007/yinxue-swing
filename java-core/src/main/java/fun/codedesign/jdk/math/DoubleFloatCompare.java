package fun.codedesign.jdk.math;

public class DoubleFloatCompare {
    public static void main(String[] args) {
        double pi = 3.141592653579397126;

        float f = 3.141592653579397126f;
        System.out.println(pi);
        System.out.println(f);
        double a = 2.6;
        double b = a - 2;
        System.out.println(b);


        double y = Math.sin(pi);
        System.out.println(y);//
        System.out.println(y == 0);//
    }
}
