package fun.codedesign.jdk.math;

public class PrimitiveTypeTransfer {
    public static void main(String[] args) {
        long l = 5;
        long ll = 5l;
        System.out.println(ll);

        l = 8l;

        int x = (int) l;
        System.out.println(x);

        l = 0x747f0000005l;
        System.out.println(l); //8005550604293
        x = (int) l;
        System.out.println(x);//-268435451

        double PI = Math.PI;
        System.out.println(PI); //3.141592653589793
        float f = (float) PI;
        System.out.println(f);//3.1415927


        l = (long) PI;
        System.out.println(l);  //3

        byte b1 = 2;
        byte b2 = 3;
        byte b3 = (byte) (b1 + b2);
        System.out.println(b3);  //5

        char c = 'A' + 1;
        System.out.println(c); //B
        char ch = 'A';
        c = (char) ('A' + 1);
        System.out.println(c); //B
        System.out.println(ch); //A
    }
}