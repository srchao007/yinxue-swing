package fun.codedesign.jdk.math.algorithm.sort;

/**
 * 插入排序 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-06-25 17:02
 * @since 1.0.0
 */
public class InsertSorter implements Sorter {
    @Override
    public void sort(int[] arr) {
        for (int i = 0, length = arr.length; i < length - 1; i++) {
            int index = -1;
            int temp = arr[i + 1];
            for (int j = i; j >= 0; j--) {
                if (arr[j] > temp) {
                    arr[j + 1] = arr[j];
                    index = j;
                }
            }
            if (index != -1) {
                arr[index] = temp;
            }
        }
    }
}
