/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: MaxHeap
 * Author:   zengjian
 * Date:     2018/10/17 15:55
 * Description: 构建最大堆
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.jdk.math.algorithm.sort;

/**
 * 〈MaxHeap〉<br>
 * 〈构建数字最大堆，用于从100万数据中取Top 10位数字〉
 *
 * @author zengjian
 * @create 2018/10/17 15:55
 */
public class MaxHeap {

    private Integer[] entrys;
    private int capcity;
    private int size;

    public MaxHeap(int capcity) {
        this.capcity = capcity;
        entrys = new Integer[capcity];
    }

    public void add(int num) {
        if (size == 0) {
            entrys[size++] = num;
            return;
        }
        // 其他情况遍历加

    }


}