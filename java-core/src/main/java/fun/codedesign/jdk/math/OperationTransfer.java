package fun.codedesign.jdk.math;

/**
 * 隐含的转型关系
 */
public class OperationTransfer {
    public static void main(String[] args) {
        String str = "a";
        str += "b"; //str=str+"b";
        System.out.println(str); //ab

        int a = 1;
        short s = 1;

        a += 2.5; //a=(int)(a+2.5)
        System.out.println(a);//3
        s += 3; //s=(short)(s+3)
        System.out.println(s);

    }
}
