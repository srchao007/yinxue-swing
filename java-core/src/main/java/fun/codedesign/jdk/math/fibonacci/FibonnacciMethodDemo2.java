package fun.codedesign.jdk.math.fibonacci;

import java.util.Scanner;

/** Fibonnacci
 *  1  1   2  3   5   8   13   21   34
 i   1   2   3   4   5   6    7   8    9
 */

public class FibonnacciMethodDemo2 {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            System.out.print("数字:");
            int num = sc.nextInt();
            System.out.println("递归Fibonnacci:" + Fibonnacci(num));
        }
    }

    public static int Fibonnacci(int i) {
        if (i == 1 || i == 2) {
            return 1;
        } else {
            return Fibonnacci(i - 1) + Fibonnacci(i - 2);
        }
    }
}
		

		


