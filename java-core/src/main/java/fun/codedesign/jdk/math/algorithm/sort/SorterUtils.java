
package fun.codedesign.jdk.math.algorithm.sort;

/**
 * 〈SorterUtils〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/16 11:40
 */
public final class SorterUtils {

    private SorterUtils() {
    }

    /**
     * 交换数组中i j下标的值, 可能出现越界异常
     * @param arr
     * @param i
     * @param j
     */
    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}