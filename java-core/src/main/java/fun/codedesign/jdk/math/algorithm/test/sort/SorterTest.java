package fun.codedesign.jdk.math.algorithm.test.sort;

import fun.codedesign.jdk.math.algorithm.sort.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

public class SorterTest {

    @Test
    public void select() {
        int[] arr1 = new int[]{1, 3, 5, 2, 6, 9, 78, 100, 13, 25};
        int[] arr2 = new int[]{9, 6};
        new SelectSorter().sort(arr1);
        new SelectSorter().sort(arr2);
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
    }

    @Test
    public void insert() {
        int[] arr = new int[]{1, 3, 5, 2, 6, 9, 78, 100, 13, 25};
        new InsertSorter().sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    @Test
    public void shell() {
        int[] arr = new int[]{1, 3, 5, 2, 6, 9, 78, 100, 13, 25};
        new ShellSorter().sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    @Test
    public void bubble() {
        int[] arr1 = new int[]{1, 3, 5, 2, 6, 9, 78, 100, 13, 25};
        int[] arr2 = new int[]{9, 6};
        new BubbleSorter().sort(arr1);
        new BubbleSorter().sort(arr2);
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
    }

    @Test
    public void meger() {
        int[] arr1 = new int[]{1, 3, 5, 2, 6, 9, 78, 100, 13, 25};
        int[] arr2 = new int[]{9, 6};
        new ShellSorter().sort(arr1);
        new ShellSorter().sort(arr2);
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
    }

    @Test
    public void quick1() {
        int[] arr1 = new int[]{1, 3, 5, 2, 6, 9, 78, 100, 13, 25};
        int[] arr2 = new int[]{9, 6};
        new QuickSorter().sort(arr1);
        new QuickSorter().sort(arr2);
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
    }


    @Test
    public void testValidate() {
        int[] arr1 = new int[]{};
        new BubbleSorter().sort(arr1);
    }

    @Test
    public void heap() {
        int[] arr1 = new int[]{1, 3, 5, 2, 6, 9, 78, 100, 13, 25};
        int[] arr2 = new int[]{9, 6};
        new HeapSorter().sort(arr1);
        new HeapSorter().sort(arr2);
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
    }

    @Test
    public void count() throws Exception {
        Sorter sorter = SorterFactory.getSorter(CountSorter.class);
        int[] arr1 = new int[]{1, 5, 3, 3, 2, 5, 2, 6, 9, 78, 100, 13, 25};
        int[] arr2 = new int[]{9, 6};
        sorter.sort(arr1);
        sorter.sort(arr2);
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
    }

    @Test
    public void arrCount() {
        int[] a = new int[]{100};
        a[0]--;
        System.out.println(Arrays.toString(a));
    }

    @Test
    public void testArray(){
        int[][] arr = new int[10][3];
        System.out.println(Arrays.toString(arr));
    }
    @Test
    public void radix() throws Exception {
        Sorter sorter = SorterFactory.getSorter(RadixSorter.class);
        int[] arr1 = new int[]{1, 5, 3, 3, 2, 5, 2, 6, 9, 78, 100, 13, 25};
        int[] arr2 = new int[]{9, 6};
        sorter.sort(arr1);
        sorter.sort(arr2);
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
    }

    @Test
    public void integer(){
        // -
        System.out.println(Integer.MIN_VALUE);
    }

    @Test
    public void bucket() throws Exception {
        Sorter sorter = SorterFactory.getSorter(BucketSorter.class);
        int[] arr1 = new int[]{1, 5, 3, 3, 2, 5, 2, 6, 9, 78, 100, 13, 25};
        int[] arr2 = new int[]{9, 6};
        sorter.sort(arr1);
        sorter.sort(arr2);
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
    }

    @Test
    public void charater() {
        char c1 = '0';
        char c2 = '1';
        char c3 = '2';
        System.out.println(c3-c1);
        System.out.println(c2-c1);
    }

    @Test
    public void compare() throws Exception {
        int[] arr  = new int[100_000];
        int[] arr2 = new int[100_000];
        Random rd = new Random();
        for (int i = 0,size=arr.length; i < size; i++) {
            int num = rd.nextInt(100);
            arr[i] = num;
            arr2[i] = num;
        }

        Sorter bubble = SorterFactory.getSorter(BubbleSorter.class);
        Sorter quick = SorterFactory.getSorter(QuickSorter.class);
        long time1 = System.currentTimeMillis();
        bubble.sort(arr);
        long time2 = System.currentTimeMillis();

        quick.sort(arr2);
        long time3 = System.currentTimeMillis();

        System.out.println("冒泡排序耗时:"+(time2-time1)+"\n快速排序耗时:"+(time3-time2));
    }
}