package fun.codedesign.jdk.math;

import java.util.Random;
import java.util.Scanner;

/*
 * 比大小数
 */
public class CaculateRightNum {
    public static void main(String[] args) {
        Random rd = new Random();
        int num = rd.nextInt(100) + 1;

        try(Scanner sc = new Scanner(System.in)) {
            System.out.println("输入你猜的数字：");
            for (int count = 1; count <= 6; count++) {
                int answer = sc.nextInt();
                if (answer > num) {
                    System.out.println("大了");
                } else if (answer < num) {
                    System.out.println("小了");
                } else if (answer == num) {
                    System.out.println("Binggo答对了");
                    break;
                }
            }
            System.out.println("正确答案是" + num);
        }
    }
}

