package fun.codedesign.jdk.math.algorithm.sort;
/**
 * 排序接口 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-06-25 16:47
 * @since 1.0.0
 */
public interface Sorter {

    void sort(int[] arr);
}
