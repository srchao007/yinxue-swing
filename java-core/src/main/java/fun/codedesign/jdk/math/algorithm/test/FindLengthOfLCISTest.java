package fun.codedesign.jdk.math.algorithm.test;


import org.junit.Test;

import static fun.codedesign.jdk.math.algorithm.array.FindLengthOfLCIS.findLengthOfLCIS;
import static junit.framework.TestCase.assertEquals;

public class FindLengthOfLCISTest {

    @Test
    public void testFindLengthOfLCIS() {
        assertEquals(findLengthOfLCIS(new int[]{1, 3, 5, 4, 7}), 3);
        assertEquals(findLengthOfLCIS(new int[]{2, 2, 2, 2}), 1);
        assertEquals(findLengthOfLCIS(new int[]{1, 3, 5, 7}), 4);
        assertEquals(findLengthOfLCIS(new int[]{1, 2}), 2);
        assertEquals(findLengthOfLCIS(new int[]{1}), 1);
    }
}