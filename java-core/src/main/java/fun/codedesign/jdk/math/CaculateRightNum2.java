package fun.codedesign.jdk.math;

import java.util.Random;
import java.util.Scanner;

public class CaculateRightNum2 {
    public static void main(String[] args) {
        int count = 0, num, answer = 0;
        try(Scanner sc = new Scanner(System.in)) {
            Random rd = new Random();
            num = rd.nextInt(100) + 1;
    
            System.out.println(num);
    
            while (true) {
                System.out.println("输入猜的数字:");
                answer = sc.nextInt();
                count++;
                if (num == answer) {
                    System.out.println("猜对了,竞猜次数:" + count);
                    break;
                } else if (num > answer) {
                    System.out.println("小了,竞猜次数:" + count);
                } else {
                    System.out.println("大了,竞猜次数:" + count);
                }
            }
        }
    }
}
