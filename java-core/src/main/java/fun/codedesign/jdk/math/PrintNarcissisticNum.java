package fun.codedesign.jdk.math;

/**
 * 打印水仙花数，各位的3次方相加的数等于原来的数为水仙花数
 */
public class PrintNarcissisticNum {
    public static void main(String[] args) {

        for (int n = 100; n < 999; n++) {
            int num = n;
            int sum = 0;
            do {
                int last = num % 10;
                sum += last * last * last;
                num /= 10;
            } while (num != 0);
            if (sum == n) {
                System.out.println(n + "是水仙花数");
            }
        }
    }

}
