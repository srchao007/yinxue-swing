package fun.codedesign.jdk.math;

/**
 * sum= 1+2+3+4...n  f(n)=n+f(n-1) f(1)=1
 */
public class SumOperationMethod {
    public static void main(String[] args) {
        System.out.println(f2(5000));
        int n = 5000;
        int y = f(n);
        System.out.println(y);
    }

    public static int f(int n) {
        if (n == 1) {
            return 1;
        }
        return n + f(n - 1);
    }

    public static int f2(int n) {
        int sum = 0;
        for (int i = 1; i < n; i++) {
            sum = sum + i;
        }
        return sum;
    }
}

