package fun.codedesign.jdk.lang.method;

/**
 * 7整除的数
 */
public class WhileDemo1 {
    public static void main(String[] args) {
        int i = 0;
        while (i <= 30) {
            i++;
            if (i % 7 == 0) {
                System.out.println(i);
            }
        }
    }
}
