package fun.codedesign.jdk.lang.method;

import java.util.Scanner;


public class ConditionOperation {
    public static void main(String[] args) {

        try(Scanner sc = new Scanner(System.in)) {
            System.out.println("连续输入三个数字：");
            int a, b, c, max;
            a = sc.nextInt();
            b = sc.nextInt();
            c = sc.nextInt();
    
            max = a > b ? a : b;
            max = max > c ? max : c;
            System.out.println("最大的是：" + max);
        }
    }
}
