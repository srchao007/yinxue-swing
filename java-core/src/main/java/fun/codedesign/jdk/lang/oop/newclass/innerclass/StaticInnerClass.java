package fun.codedesign.jdk.lang.oop.newclass.innerclass;

/**
 * 静态内部类
 */
public class StaticInnerClass {
    static int i = 8;

    public static void main(String[] args) {
        Boo b = new Boo();
        System.out.println("��̬�ڲ�����Է����ⲿ��̬����:" + b.add());
    }

    static class Boo {
        int add() {
            return i + 1;
        }
    }
}
