package fun.codedesign.jdk.lang.oop.basic.book;

import java.util.Arrays;

public class BookDemo {
    public static void main(String[] args) {
        Book book;
        book = new Book();
        book.name = "天外有天";
        book.id = 172;
        book.authors = new String[]{"hello", "jetty"};
        System.out.println(Arrays.toString(book.authors));
        System.out.println(book.name);
        System.out.println(book.id);
        book = new Book();
        book.name = "name";
    }
}

