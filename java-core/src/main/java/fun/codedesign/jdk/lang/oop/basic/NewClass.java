package fun.codedesign.jdk.lang.oop.basic;

public class NewClass {
    public static void main(String[] args) {
        Foo foo = new Foo();
        Goo goo = new Goo(5);
        System.out.println(foo);
        System.out.println(goo);
    }
}

class Foo {
    public Foo() {
        System.out.println("Foo()");
    }
}

class Goo {
    public Goo(int i) {
        System.out.println("Goo(i)");
    }
}

