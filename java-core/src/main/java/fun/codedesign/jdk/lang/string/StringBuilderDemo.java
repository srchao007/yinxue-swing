package fun.codedesign.jdk.lang.string;


public class StringBuilderDemo {
    public static void main(String[] args) {
        StringBuilder buf = new StringBuilder();
        buf.append("123").append("321ʿ").append(",啊啊啊啊").insert(0, "666").delete(4, 6).append(".");
        System.out.println(buf);
        System.out.println(buf);
        String s = buf.toString();
        System.out.println(s);
        buf = new StringBuilder(s);
        System.out.println(buf);
    }
}
