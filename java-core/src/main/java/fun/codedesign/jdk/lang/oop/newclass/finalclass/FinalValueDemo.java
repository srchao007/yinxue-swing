package fun.codedesign.jdk.lang.oop.newclass.finalclass;

public class FinalValueDemo {
    public static void main(String[] args) {
        Xoo x = new Xoo();
        System.out.println("x的值：" + x.x); // 2
        System.out.println("final值" + x.y); // 1
        System.out.println("方法值" + x.add()); // 2
        System.out.println("变量值x:" + x.x); // 3
        System.out.println("变量值y:" + x.y); // 1
    }
}

class Xoo {

    int x = 1;
    final int y = x++;

    int add() {
        return x++;
    }

}