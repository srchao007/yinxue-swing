package fun.codedesign.jdk.lang.oop.basic.point;

import java.text.DecimalFormat;

public class PointTest {
    public static void main(String[] args) {
        System.out.println(new Point2()); //Point2 [x=0, y=0]
        Point2 p1 = new Point2();
        /*System.out.println(p.x); */
        System.out.println(p1.getX());

        Point2 p2 = new Point2(26);//
        System.out.println(p2);//Point2 [x=26, y=26]

        Point2 p3 = new Point2(12, 16);
        System.out.println(p3); //Point2 [x=12, y=16]

        System.out.println(p1.distance(p2, p3)); // 17.204650534085253
        System.out.println(p1.distance(p1));// 0.0
        System.out.println(p1.distance(3, 4));
        System.out.println(p1.distance(new Point2(3)));

        double d = 4.246640687119285;
        DecimalFormat df = new DecimalFormat("#.00");
        System.out.println(df.format(d));//4.25

    }
}
