package fun.codedesign.jdk.lang.method;


public class WhileDemo {
    public static void main(String[] args) {
        int x = 5;
        int y = 2;
        int z = x % y;

        System.out.println(z);//1
        System.out.println(-5 % 3);//-2
        System.out.println(-4 % 3);//-1
        System.out.println(-3 % 3);//0
        System.out.println(-2 % 3);//-2
        System.out.println(-1 % 3);//-1
        System.out.println(0 % 3);//0
        System.out.println(1 % 3);//1
        System.out.println(2 % 3);//2
        System.out.println(3 % 3);//0
        System.out.println(4 % 3);//1
        System.out.println(5 % 3);//2

        System.out.println("-----while语句应用得到同样结果:------");

        int a = -5;
        int b = 3;

        while (a < 5) {
            System.out.println(a % b);
            a = a + 1;
        }

    }
}
