package fun.codedesign.jdk.lang.string;

/**
 * 转义符，\\n \\u 等，unicode用16进制表示
 */
public class UnicodeDemo {
    public static void main(String[] args) {
        System.out.print(1111111111);
        char c = '\n';
        System.out.print(c);
        System.out.println(22222222222222222l);

        c = '\u5d14';
        System.out.println(c);

        int a = '\u0000';
        System.out.println(a);

        int b = '\uffff';
        System.out.println(b); //65535
    }
}
