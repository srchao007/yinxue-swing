package fun.codedesign.jdk.lang.oop.newclass.finalclass;

public class FinalVarDemo {
    public static void main(String[] args) {
        final int a = 1;
        // a++;  final
        int c = add(a, 2);
        System.out.println("c:"+ c);
        Aoo a1 = new Aoo();
        Aoo a2 = new Aoo();
        // a2.id=10; final
        // final
        final Poo p = new Poo();
        // p=null; final
        p.a = 5;
        System.out.println(a1.id + "," + a2.id + "," + Aoo.i);
    }

    public static int add(final int a, int b) {
        // a++;
        return a + b;
    }
}

class Poo {
    int a = 1;
}

class Aoo {
    static int i = 0;
    final int id;

    public Aoo() {
        id = i++;
    }
}
