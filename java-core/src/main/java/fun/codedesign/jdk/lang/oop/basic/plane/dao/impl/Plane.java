package fun.codedesign.jdk.lang.oop.basic.plane.dao.impl;

import fun.codedesign.jdk.lang.oop.basic.plane.dao.Flying;
import fun.codedesign.jdk.lang.oop.basic.plane.entity.Point;

public class Plane extends Flying {
    int w;
    int h;

    public Plane(Point p, int w, int h) {
        super.p = p;
        this.w = w;
        this.h = h;
    }

    public boolean shootBy(Point p) {
        return p.x - super.p.x > 0 && p.x - super.p.x <= w && p.y - super.p.y > 0 && p.y - super.p.y <= h;
    }

    @Override
    public String toString() {
        return "Plane [" + "Point2 [" + super.p.x + "," + super.p.y + "] " + "h=" + h + ", w=" + w + "]";
    }
}
