package fun.codedesign.jdk.lang.oop;

import java.util.ArrayList;

public class BallArrayList {
    public static void main(String[] args) {

        ArrayList<Ball> ballRedList = new ArrayList<>();
        ArrayList<Ball> ballBlueList = new ArrayList<>();
        Ball[] ballRed = new Ball[33];
        Ball[] ballBlue = new Ball[7];

        int i, j;
        for (i = 0; i < ballRed.length; i++) {
            ballRed[i] = new Ball(i + 1, Ball.COLOR_RED);
            //System.out.println(ballRed[i]);
            ballRedList.add(ballRed[i]);
        }

        for (j = 0; j < ballBlue.length; j++) {
            ballBlue[j] = new Ball(j + 1, Ball.COLOR_BLUE);
            //System.out.println(ballBlue[j]);
            ballBlueList.add(ballBlue[j]);
        }
        System.out.println(ballRedList);
        System.out.println(ballBlueList);


        System.out.println(ballRedList.size());
        for (int m = 0; m < 26; m++) {
            int index = (int) (Math.random() * ballRedList.size());
            ballRedList.remove(index);
            //System.out.println(ballRedList);
        }

        for (int n = 0; n < 6; n++) {
            int index = (int) (Math.random() * ballBlueList.size());
            ballBlueList.remove(index);
            //System.out.println(ballBlueList);
        }
        ballRedList.addAll(ballBlueList);
        System.out.println(ballRedList);

//    	 Ball ball1[] = new Ball[33];
//    	 Ball ball2[] = new Ball[7];
//    	 int i;
//    	 int j;
//
//    	 for(i=1;i<ball1.length+1;i++){
//
//    		 ball1[i-1]=new Ball(i, Ball.COLOR_RED);
//    		 System.out.println(ball1);
//    	 }
//    	 	System.out.println(ball1[i-1]);
//
//
//    	for (j = 1; j < ball2.length; j++) {
//    		 ball1[j-1]=new Ball(j, Ball.COLOR_BLUE);
//    	}
    }
}

