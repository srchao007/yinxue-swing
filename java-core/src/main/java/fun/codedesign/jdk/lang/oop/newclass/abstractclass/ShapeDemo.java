package fun.codedesign.jdk.lang.oop.newclass.abstractclass;

public class ShapeDemo {
    public static void main(String[] args) {
        // Shape s = newclass Shape();
        Shape s;
        s = new Circle(1, 2, 3);
        System.out.println(s.area());
        System.out.println(s.contains(23, 10));

        //
        Shape t = new Rectangle(0, 0, 5, 9);
        System.out.println(t.area());
        System.out.println(t.contains(8, 4));

    }
}
