package fun.codedesign.jdk.lang.method;

public class IfElseChar2 {
    public static void main(String[] args) {
        char c = 'F';
        if (c >= 'A' && c <= 'Z') {
            System.out.println(c + "是大写字母");
        } else {
            System.out.println(c + "不是大写字母");
        }

        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
            System.out.println(c + "是字母");
        } else {
            System.out.println(c + "不是字母");
        }
    }
}
