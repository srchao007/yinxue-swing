package fun.codedesign.jdk.lang.oop.basic.card;

import java.util.Arrays;

public class Player {

    private int id;
    private String name;
    private Card[] cards = {};

    public Player() {
    }

    public Player(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Card[] getCards() {
        return cards;
    }

    public void setCards(Card[] cards) {
        this.cards = cards;
    }

    public void add(Card card) {
        cards = Arrays.copyOf(cards, cards.length + 1);
        cards[cards.length - 1] = card;


        for (int i = 0; i < cards.length - 1; i++) {
            for (int j = 0; j < cards.length - 1 - i; j++) {
                if (cards[j].getRank() <= cards[j + 1].getRank()) {
//					System.out.println("------------");
                    Card temp = cards[j];
                    cards[j] = cards[j + 1];
                    cards[j + 1] = temp;
                }
            }
        }
    }

    public String toString() {
        return name + ":" + Arrays.toString(cards);

    }
}
		
		

	
	
	

