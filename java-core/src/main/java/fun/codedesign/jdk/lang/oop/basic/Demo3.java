package fun.codedesign.jdk.lang.oop.basic;

public class Demo3 {
    public static void main(String[] args) {
        Super s = new Sub();
        s.a();  //super a

        Sub sub = new Sub();
        //sub.t();
        //sub =s;
        //sub=(Sub)s;
        sub.t();
        sub.a();
        Super x = new Sub();
        System.out.println(x);
        x = new Sub();
        System.out.println(x);

        Super y = new Super();
        System.out.println("---");
        System.out.println(y);
        y = new Super();
        System.out.println(y);

    }
}

class Super {
    public void a() {
        System.out.println("Super a()");
    }
}

class Sub extends Super { //extends��ʾ�̳е���˼
    public void t() {
        System.out.println("Sub t()");
    }

    public void x() {
        System.out.println("x");
    }

}