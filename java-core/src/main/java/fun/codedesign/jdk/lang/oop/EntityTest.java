package fun.codedesign.jdk.lang.oop;

import java.math.BigInteger;

/**
 * 生成的实体类测试
 *
 * @author zengjian
 * @create 2018-03-20 17:23
 * @since 1.0.0
 */
public class EntityTest {
    private String tableCatalog;
    private String tableSchema;
    private String tableName;
    private BigInteger nonUnique;
    private String indexSchema;
    private String indexName;
    private BigInteger seqInIndex;
    private String columnName;
    private String collation;
    private BigInteger cardinality;
    private BigInteger subPart;
    private String packed;
    private String nullable;
    private String indexType;
    private String comment;
    private String indexComment;

    public String getTableCatalog() {
        return tableCatalog;
    }

    public void setTableCatalog(String tableCatalog) {
        this.tableCatalog = tableCatalog;
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public void setTableSchema(String tableSchema) {
        this.tableSchema = tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public BigInteger getNonUnique() {
        return nonUnique;
    }

    public void setNonUnique(BigInteger nonUnique) {
        this.nonUnique = nonUnique;
    }

    public String getIndexSchema() {
        return indexSchema;
    }

    public void setIndexSchema(String indexSchema) {
        this.indexSchema = indexSchema;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public BigInteger getSeqInIndex() {
        return seqInIndex;
    }

    public void setSeqInIndex(BigInteger seqInIndex) {
        this.seqInIndex = seqInIndex;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getCollation() {
        return collation;
    }

    public void setCollation(String collation) {
        this.collation = collation;
    }

    public BigInteger getCardinality() {
        return cardinality;
    }

    public void setCardinality(BigInteger cardinality) {
        this.cardinality = cardinality;
    }

    public BigInteger getSubPart() {
        return subPart;
    }

    public void setSubPart(BigInteger subPart) {
        this.subPart = subPart;
    }

    public String getPacked() {
        return packed;
    }

    public void setPacked(String packed) {
        this.packed = packed;
    }

    public String getNullable() {
        return nullable;
    }

    public void setNullable(String nullable) {
        this.nullable = nullable;
    }

    public String getIndexType() {
        return indexType;
    }

    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getIndexComment() {
        return indexComment;
    }

    public void setIndexComment(String indexComment) {
        this.indexComment = indexComment;
    }
}
