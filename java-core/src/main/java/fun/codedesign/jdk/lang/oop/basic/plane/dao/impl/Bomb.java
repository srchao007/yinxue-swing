package fun.codedesign.jdk.lang.oop.basic.plane.dao.impl;

import fun.codedesign.jdk.lang.oop.basic.plane.dao.Flying;
import fun.codedesign.jdk.lang.oop.basic.plane.entity.Point;

public class Bomb extends Flying {
    int r;

    public Bomb(Point p, int r) {
        super.p = p;
        this.r = r;
    }

    public boolean shootBy(Point p) {
        return super.p.distance(p) <= r;
    }

    @Override
    public String toString() {
        return "Bomb [" + "Point2 [" + super.p.x + "," + super.p.y + "] " + "r=" + r + "]";
    }


}
