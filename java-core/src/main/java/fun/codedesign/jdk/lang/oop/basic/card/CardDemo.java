package fun.codedesign.jdk.lang.oop.basic.card;

import java.util.Arrays;
import java.util.Random;

public class CardDemo {
    public static void main(String[] args) {
        //	Card c = new Card(Card.SPADE, Card.ACE);
        //	System.out.println(c);

        Card[] cards = new Card[54];
        int index = 0;

        for (int rank = Card.THEREE; rank <= Card.TWO; rank++) {
            cards[index++] = new Card(Card.DIAMOND, rank);
            cards[index++] = new Card(Card.CLUB, rank);
            cards[index++] = new Card(Card.HEART, rank);
            cards[index++] = new Card(Card.SPADE, rank);
        }
        cards[index++] = new Card(Card.JOKER, Card.BLACK);
        cards[index++] = new Card(Card.JOKER, Card.COLOR);
        System.out.println(Arrays.toString(cards));


        Random rd = new Random();
        for (int i = cards.length - 1; i > 0; i--) {
            int j = rd.nextInt(i);
            Card temp = cards[i];
            cards[i] = cards[j];
            cards[j] = temp;
        }
        System.out.println(Arrays.toString(cards));

        Player[] players = new Player[3];
        players[0] = new Player(1, "zhangsan");
        players[1] = new Player(2, "lisi");
        players[2] = new Player(3, "wangwu");

        for (int i = 0; i < cards.length; i++) {
            Card card = cards[i];
            Player player = players[i % players.length];
            player.add(card);
        }
        System.out.println(players[0]);
        System.out.println(players[1]);
        System.out.println(players[2]);


    }
}
