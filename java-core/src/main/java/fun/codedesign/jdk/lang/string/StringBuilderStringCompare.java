package fun.codedesign.jdk.lang.string;

public class StringBuilderStringCompare {
    public static void main(String[] args) {
        System.out.println(testStringBuilder(50000));
        System.out.println(testString(50000));
    }

    public static long testStringBuilder(int n) {
        long start = System.currentTimeMillis();
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < n; i++) {
            buf.append(" hello");
        }
        long end = System.currentTimeMillis();
        return end - start;
    }

    public static long testString(int n) {
        long start = System.currentTimeMillis();
        String s = "";
        for (int i = 0; i < n; i++) {
            s += "hello";
        }
        long end = System.currentTimeMillis();
        System.out.println(s.length());
        return end - start;
    }
}
