package fun.codedesign.jdk.lang.oop;


public class ShopDemo {
    public static void main(String[] args) {
        Shop<Food> foodShop = new Shop<Food>(new Food("食物"));
        System.out.println(foodShop.buy());

        Shop<Pet> petShop = new Shop<Pet>(new Pet("宠物"));
        System.out.println(petShop.buy());
    }
}

class Food {
    String name;

    public Food(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}

class Shop<P> {
    P product;

    public Shop(P p) {
        product = p;
    }

    P buy() {
        return product;
    }

}

class Pet {
    String name;

    public Pet(String name) {
        this.name = name;
    }
}
	
	
	
	
