package fun.codedesign.jdk.lang.oop.newclass.innerclass;


/**
 * 局部内部类
 */
public class LocalInnerClass {
    public static void main(String[] args) {
        final int x = 5;
        class Koo {
            int add2() {
                return x + 1;
            }
        }
        Koo k = new Koo();
        System.out.println("数字:" + k.add2());

    }
}
