package fun.codedesign.jdk.lang.oop.basic;

public class Point {
    int x;
    int y;

    public Point() {
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(int x) {
        //	this.x=this.y=x;
        this(x, x);
    }
}
