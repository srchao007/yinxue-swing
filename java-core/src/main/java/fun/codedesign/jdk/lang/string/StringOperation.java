package fun.codedesign.jdk.lang.string;


public class StringOperation {
    public static void main(String[] args) {
        int a = 5;
        int b = 6;
        int c = a + b;
        System.out.println(c);//11
        String s = "5" + 5;
        System.out.println(s);//55

        System.out.println("123" + "ABC");//123ABC
        System.out.println("1" + 2 + 3 + "ABC");//123ABC
        System.out.println("1" + (2 + 3) + "ABC");//15ABC
        System.out.println(1 + 2 + 3 + "ABC");//6ABC
        System.out.println('1' + '2' + '3' + "ABC");//150ABC
        System.out.println(12 + "3" + "ABC");//123ABC
        System.out.println(1 + '2' + 3 + "ABC");//54ABC
        System.out.println(1 + "23" + "ABC");//123ABC
        System.out.println(1 + "2" + 3 + "ABC");//123ABC
        System.out.println('1' + 2 + 3 + "ABC");//54ABC
    }
}
