package fun.codedesign.jdk.lang.oop.basic;

public class Demo2 {
    public static void main(String[] args) {
        int a = 1;
        int c = add(a);//c=2
        Koo koo = new Koo();
        int d = add(koo);//d=2
        System.out.println(a + ":" + koo.a);// 1:1
        System.out.println(c + ":" + d);//2:2

    }

    static int add(int a) {
        a++;
        return a;
    }

    static int add(Koo koo) {
        Koo k = koo;
        k.a++;
        return koo.a;
    }
}

class Koo {
    int a = 1;
}

