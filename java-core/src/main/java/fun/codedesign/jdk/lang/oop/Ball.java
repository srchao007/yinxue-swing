package fun.codedesign.jdk.lang.oop;

public class Ball {
    public static final int COLOR_RED = 0;
    public static final int COLOR_BLUE = 1;
    int id;
    int color;

    public Ball() {
    }

    public Ball(int id, int color) {
        this.id = id;
        this.color = color;
    }

    public static int getColorRed() {
        return COLOR_RED;
    }

    public static int getColorBlue() {
        return COLOR_BLUE;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString() {
        if (color == COLOR_RED) {
            return "[id:" + id + "红色";
        } else return "[id:" + id + "蓝色";
    }
}
