package fun.codedesign.jdk.lang.oop.basic.person;

public class FieldDemo {

    public static void main(String[] args) {
        Student s = new Student("TOM");
        System.out.println(s.name);
        Person p = s;
        System.out.println(p.name);
    }
}

class Person {
    String name;
}

class Student extends Person {
    String name;

    public Student(String name) {
        this.name = name;
        super.name = "F" + name;
    }
}