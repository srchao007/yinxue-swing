package fun.codedesign.jdk.lang.method;

import java.util.Scanner;

public class SwitchScore {

    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            System.out.print("请输入分数:");
            int score = sc.nextInt();
            if (score < 0 || score > 100) {
                System.out.println("分数不在0~100范围内");
                return;
            }
            String level;
            switch (score / 10) {
                case 10:
                case 9:
                    level = "优秀";
                    break;
                case 8:
                    level = "良好";
                    break;
                case 7:
                    level = "合格";
                    break;
                case 6:
                    level = "不及格";
                    break;
                default:
                    level = "糟糕";
                    break;
            }
            System.out.println(level);
        }
    }
}
