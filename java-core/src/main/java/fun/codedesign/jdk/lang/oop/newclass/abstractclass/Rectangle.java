package fun.codedesign.jdk.lang.oop.newclass.abstractclass;

public class Rectangle extends Shape {

    int w;
    int h;
    int x;
    int y;

    public Rectangle() {
    }

    public Rectangle(int x, int y, int w, int h) {
        this.w = w;
        this.h = h;
        this.x = x;
        this.y = y;
    }

    public double area() {
        return w * h;
    }

    public boolean contains(int x, int y) {

        return x - this.x < w && y - this.y < h;
    }
}
