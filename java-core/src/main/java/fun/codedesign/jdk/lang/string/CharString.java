package fun.codedesign.jdk.lang.string;

import java.util.Arrays;

public class CharString {
    public static void main(String[] args) {
        String x = "12321321adaf";
        String s = "adsfafd " + "adsfa";

        char[] c = x.toCharArray();
        System.out.println(Arrays.toString(c));
        System.out.println(c[0]);

        char[] c2 = s.toCharArray();
        System.out.println(Arrays.toString(c2));
    }
}
