package fun.codedesign.jdk.lang.oop.basic.ball;

import javax.swing.*;
import java.awt.*;

public class Ball {

    public final static int LEFT_UP = 0;
    public final static int LEFT_DOWN = 1;
    public final static int RIGHT_UP = 2;
    public final static int RIGHT_DOWN = 3;
    int x;
    int y;
    int r;
    int speed;
    int direction;
    Color color;
    JPanel panel;


    public Ball(int x, int y, int r, int speed, int direction, Color color,
                JPanel panel) {
        super();
        this.x = x;
        this.y = y;
        this.r = r;
        this.speed = speed;
        this.direction = direction;
        this.color = color;
        this.panel = panel;

    }

    public void draw(Graphics g) {
        g.setColor(color);
        g.fillArc(x, y, r * 2, r * 2, 0, 360);  //��������������Բ��
    }

    public void move() {
        switch (direction) {
            case LEFT_UP:
                x -= speed;
                y -= speed;
                if (y <= 0) {
                    this.direction = LEFT_DOWN;
                }
                if (x <= 0) {
                    this.direction = RIGHT_UP;
                }
                break;

            case LEFT_DOWN:
                x -= speed;
                y += speed;
                if (x <= 0) {
                    this.direction = RIGHT_DOWN;
                }
                if (y >= (panel.getHeight() - 2 * r)) {
                    this.direction = LEFT_UP;
                }
                break;
            case RIGHT_UP:
                x += speed;
                y -= speed;
                if (y <= 0) {
                    this.direction = RIGHT_DOWN;
                }
                if (x >= (panel.getWidth() - 2 * r)) {
                    this.direction = LEFT_UP;
                }
                break;
            case RIGHT_DOWN:
                x += speed;
                y += speed;
                if (x >= (panel.getWidth() - 2 * r)) {
                    this.direction = LEFT_DOWN;
                }
                if (y >= (panel.getHeight() - 2 * r)) {
                    this.direction = RIGHT_UP;
                }
                break;
        }
    }
}
	




