package fun.codedesign.jdk.lang.oop.newclass.innerclass;


/**
 * 匿名内部类
 */
public class AnonymousInnerClass {
    public static void main(String[] args) {
        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                int a = 0;
                for (; ; ) {
                    System.out.println("����һ�������ڲ���" + a);
                    a++;
                    if (a == 10) {
                        break;
                    }
                }
            }
        });
        t.start();
    }

}
