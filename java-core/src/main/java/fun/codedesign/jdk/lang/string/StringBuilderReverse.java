package fun.codedesign.jdk.lang.string;

import java.util.Scanner;

public class StringBuilderReverse {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            System.out.println("输入需要逆序的字符串:");
            String s = sc.nextLine();
    
            StringBuilder buf = new StringBuilder(s);
            System.out.println(buf.reverse());
        }
    }
}
