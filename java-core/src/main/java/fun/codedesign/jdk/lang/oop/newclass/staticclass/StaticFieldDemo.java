package fun.codedesign.jdk.lang.oop.newclass.staticclass;

public class StaticFieldDemo {

    public static void main(String[] args) {

        Foo f1 = new Foo();
        Foo f2 = new Foo();
        Foo f3 = new Foo();
        System.out.println(f1.id + " " + f2.id + " " + f3.id + " " + Foo.i);

    }


}

class Foo {
    static int i = 0; //��̬����
    int id;//ʵ������

    public Foo() {
        id = i++;
    }
}