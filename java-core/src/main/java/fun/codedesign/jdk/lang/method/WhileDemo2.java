package fun.codedesign.jdk.lang.method;

import java.util.Scanner;

/*
 * 倒序
 */
public class WhileDemo2 {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            System.out.print("输入数字:");
            int num = sc.nextInt(); //52481
            int sum = 0;
            while (num != 0) {
                int last = num % 10;// 1  8  4  2  5
                sum = sum * 10 + last;//  1   18   184  1842  18425
                num = num / 10;// 5248  524   52  5  0
            }
            System.out.println(sum);
        }
    }
}
