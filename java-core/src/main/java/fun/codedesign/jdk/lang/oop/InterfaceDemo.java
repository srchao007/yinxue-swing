package fun.codedesign.jdk.lang.oop;


public class InterfaceDemo {

    interface Swing {
        void run();

        void stop();
    }

    class abc2 implements Swing {
        int x, y, z = 0;
        String[] ss = {};
        char c = 'f';
        char d = (char) 102;

        @Override
        public void run() {
            System.out.println(d);
        }

        @Override
        public void stop() {
            System.out.println("HelloSwing");
        }
    }
}

