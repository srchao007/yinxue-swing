package fun.codedesign.jdk.lang.method;

import org.junit.Test;

import java.math.BigDecimal;

public class BigDecimalTest {

    private BigDecimal bigDecimal;

    @Test
    public void testConstructor() throws Exception {
        bigDecimal = new BigDecimal(new char[]{'1', '2', '3'});
        System.out.println(bigDecimal.toString());

        // 会产生长串小数点,传入double数据时，bigDecimal的值就不精确了，所以用传入String来用
        // 此处产生了两个对象，一个是 new 的另一个是设置精度的时候返回一个新的对象
        bigDecimal = new BigDecimal(10.34d).setScale(1, BigDecimal.ROUND_HALF_EVEN);
        System.out.println(bigDecimal.toString());
        System.out.println(bigDecimal.doubleValue());
        bigDecimal = new BigDecimal("10.345").setScale(2, BigDecimal.ROUND_HALF_EVEN);
        System.out.println(bigDecimal.doubleValue());

        // 单独设置
        bigDecimal = new BigDecimal(1.6444);
        // 以下需要对设置精度的对象重新赋值，否则不会生效，因为设置精度后返回的也是一个新的BigDecimal对象
        bigDecimal = bigDecimal.setScale(3, BigDecimal.ROUND_HALF_EVEN);
        System.out.println(bigDecimal.toString());
        System.out.println(bigDecimal.doubleValue());
    }
}
