package fun.codedesign.jdk.lang.string;

import java.util.Scanner;

// xxh_595985609@sina.com

public class StringMatchPattern {
    public static void main(String[] args) {
        String s = "^\\w{0,20}[@][A-Za-z]{1,10}[.][com]{1,5}([.][A-Za-z]{0,5})?$";
        try(Scanner sc = new Scanner(System.in)) {
            System.out.println("输入字符串:");
            String x = sc.nextLine();
            if (x.matches(s)) {
                System.out.println(x);
            } else {
                System.out.println("字符串不匹配");
            }
        }
    }
}
