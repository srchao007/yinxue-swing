package fun.codedesign.jdk.lang.method;

import java.util.ArrayList;
import java.util.List;

public class ForDemo1 {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 2; i <= 10; i += 2) {
            sum += i;
        }
        System.out.println(sum);

        sum = 0;
        for (int i = 3; i <= 19; i += 2) {
            sum += i;
        }
        System.out.println(sum);


        List<String> params = new ArrayList<>();
        for (int i = 0, size = params.size(); i < size; i++) {

        }
    }


}
