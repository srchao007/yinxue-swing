package fun.codedesign.jdk.lang.string;

import java.util.Scanner;

public class StringMatchCard {
    public static void main(String[] args) {
        String regex = "^\\d{15}(\\d{2}[0-9xX])?$";
        try(Scanner sc = new Scanner(System.in)) {
            String id;
            while (true) {
                System.out.println("输入身份证号:");
                id = sc.nextLine();
                if (id.matches(regex)) {
                    break;
                }
                System.out.println(id);
            }
            System.out.println("输入正确");
        }
    }
}
