package fun.codedesign.jdk.lang.string;

public class CharDemo {
    public static void main(String[] args) {
        char c = 20013;
        System.out.println(c);

        int i = 'ѧ';
        System.out.println(i); // 23398
        int a = '曾';
        int b = '健';
        System.out.println(a); //26366
        System.out.println(b); //20581

        System.out.println(Integer.toHexString(a));
        System.out.println((int) 'A'); //65
    }
}
