package fun.codedesign.jdk.lang.nashornjs;

import org.junit.Test;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class NashornJsTest {
    @Test
    public void nashornengine() throws ScriptException {
        // java 执行js 脚本
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
        String name = "hello";
        engine.eval("print('" + name + "')");
        Integer result = (Integer) engine.eval("10+2");
        System.out.println(result);
        // 使用命令行 jjs print.js ，可在 js当中编写java代码
    }
}
