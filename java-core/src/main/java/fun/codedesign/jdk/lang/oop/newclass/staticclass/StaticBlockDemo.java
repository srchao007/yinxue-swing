package fun.codedesign.jdk.lang.oop.newclass.staticclass;

public class StaticBlockDemo {
    public static void main(String[] args) {
        new Xoo();
    }
}

class Xoo {

    static {
        System.out.println("静态代码");
    }

    {
        System.out.println("构造代码");
    }

    public Xoo() {
        System.out.println("构造器代码");
    }
}


