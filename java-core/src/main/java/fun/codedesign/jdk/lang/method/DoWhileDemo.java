package fun.codedesign.jdk.lang.method;

public class DoWhileDemo {
    public static void main(String[] args) {
        new DoWhileDemo().f(10);
    }

    void f(int x) {
        do {
            System.out.println("运行:" + x);
            x--;
        } while (x > 0);
    }

}
