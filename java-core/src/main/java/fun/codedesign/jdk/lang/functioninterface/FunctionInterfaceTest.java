package fun.codedesign.jdk.lang.functioninterface;

public class FunctionInterfaceTest {
    public static void main(String[] args) {
        GreetService greetService = hello -> System.out.println(hello);
        greetService.sayHello("hello world");
    }
}
