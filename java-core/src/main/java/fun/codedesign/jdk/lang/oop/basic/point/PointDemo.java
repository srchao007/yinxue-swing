package fun.codedesign.jdk.lang.oop.basic.point;

public class PointDemo {
    public static void main(String[] args) {
        Point p = new Point(3, 4);
        double d = p.distance();
        d = p.distance(-3, -4);
        System.out.println(d);//10.0

        Point p1 = new Point(6, 8);
        System.out.println(p.distance(p1));//5.0
    }
}
