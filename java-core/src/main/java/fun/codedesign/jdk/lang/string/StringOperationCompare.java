package fun.codedesign.jdk.lang.string;

public class StringOperationCompare {
    public static void main(String[] args) {
        String s = "abcdef";
        String s1 = s + "";
        System.out.println(s == s1);

        String s2 = "abc" + "def";
        System.out.println(s == s2);   //true

        String s3 = "a" + "b" + "c" + "def";
        System.out.println(s3 == s2);  //true

        s = "123abc";
        String s5 = '1' + '2' + '3' + "abc";
        System.out.println(s5);
        System.out.println(s == s5);  //false   '1' '2' '3'

//		byte b1=1;
//		byte b2=2;
//		byte b3=b1+b2;
        String s6 = 1 + 2 + "3abc";
        System.out.println(s == s6);  //false

        String s7 = "123abc";
        System.out.println(s == s7);


    }
}
