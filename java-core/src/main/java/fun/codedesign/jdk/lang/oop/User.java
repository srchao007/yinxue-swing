package fun.codedesign.jdk.lang.oop;

public class User {

    private String name;
    private String pwdString;
    private int age;

    public User() {
    }

    public User(String name, String pwdString, int age) {
        this.name = name;
        this.pwdString = pwdString;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwdString() {
        return pwdString;
    }

    public void setPwdString(String pwdString) {
        this.pwdString = pwdString;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return this.name + ":" + this.pwdString;
    }
}
