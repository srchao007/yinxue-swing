package fun.codedesign.jdk.lang.oop;


public class Card {
    public static final int DIAMOND = 0;
    public static final int CLUB = 1;
    public static final int HEART = 2;
    public static final int SPADE = 3;
    public static final int JOKER = 4;
    public static final int THEREE = 0;  //3
    public static final int FOUR = 1;  //4
    public static final int FIVE = 2;   //5
    public static final int SIX = 3;  //6
    public static final int SEVEN = 4;   //7
    public static final int EIGHT = 5;   //8
    public static final int NINE = 6;   //9
    public static final int TEN = 7;   //10
    public static final int JACK = 8;   //J
    public static final int QUEEN = 9;    //Q
    public static final int KING = 10;   //K
    public static final int ACE = 11;   //A
    public static final int TWO = 12;    //2
    public static final int BLACK = 13;
    public static final int COLOR = 14;
    public static final String[] SUIT_NAME = {"红桃", "黑桃", "梅花", "红心", ""};
    public static final String[] RANK_NAME = {"3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "2", "小王", "大王"};
    private int suit;
    private int rank;

    public Card() {
    }

    public Card(int suit, int rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public int getSuit() {
        return suit;
    }

    public void setSuit(int suit) {
        this.suit = suit;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String toString() {
        return SUIT_NAME[suit] + RANK_NAME[rank];
    }

//	public int hashCode() {
//		return suit*33+rank*22;
//	}

    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof Card) {
            Card other = (Card) obj;
            System.out.println(this.suit == other.suit);
            return this.suit == other.suit && this.rank == other.rank;
        }
        return false;
    }
}
