package fun.codedesign.jdk.lang.oop.newclass.abstractclass;

public class Circle extends Shape {
    int r;

    public Circle() {
    }

    public Circle(int x, int y, int r) {
        this.location = new Point(x, y);
        this.r = r;
    }

    public double area() {
        return Math.PI * r * r;
    }


    public boolean contains(int x, int y) {
        return this.location.distance(x, y) <= r;
    }

}
