package fun.codedesign.jdk.lang.method;

import java.math.BigInteger;

/*
 * sum = 9 +99 + 999+... n*9  i<=19
 */
public class ForDemo4 {
    public static void main(String[] args) {
        BigInteger n = new BigInteger("0");
        BigInteger nine = new BigInteger("9");
        BigInteger ten = new BigInteger("10");
        BigInteger sum = new BigInteger("0");

        for (int i = 1; i <= 19; i++) {
            n = n.multiply(ten).add(nine); //n=n*10+9
            sum = sum.add(n);
            System.out.println(i + "," + n + "," + sum);
        }
        System.out.println(sum);
    }
}
