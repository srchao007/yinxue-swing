package fun.codedesign.jdk.lang.string;

public class StringBasicDemo2 {
    public static void main(String[] args) {
        String s = "  wuhc@njrst.com.cn  ";

        int index = s.indexOf("@");
        System.out.println(index);

        char c = s.charAt(index);
        System.out.println(c);

        int length = s.length();
        System.out.println(length);
        s = s.trim();
        System.out.println(s.length());

        System.out.println(s.toCharArray());
        System.out.println(s.toLowerCase());
        System.out.println(s.toUpperCase());

        String s1 = s.substring(0, s.indexOf("@"));
        System.out.println(s1);

        s1 = s.substring(s.indexOf("@") + 1, s.indexOf("."));
        System.out.println(s1);
        String url = "localhost:8080/web01/add.do";
        String s2 = url.substring(url.lastIndexOf("/"), url.lastIndexOf("."));
        System.out.println(s2);

        s = "-123";
        System.out.println(s.substring(1));
        System.out.println(s.startsWith("-"));

        String filename = "card.jpg";    //
        System.out.println(filename.endsWith(".jpg"));


    }
}
