package fun.codedesign.jdk.lang.string;

public class WhyStringFinal {

    // (1) 安全性：不会被继承
    // (2) 效率：使用常量池更加效率

    public static void main(String[] args) {
        String s = "name";
        String s1 = "name";
        // 将name放入常量池，返回常量池中name的引用
        s1.intern();
        System.out.println(s == s1); // false
        s1 = s1.intern();
        System.out.println(s == s1); // true
    }
}
