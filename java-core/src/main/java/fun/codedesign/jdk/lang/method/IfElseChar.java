package fun.codedesign.jdk.lang.method;

public class IfElseChar {
    public static void main(String[] args) {
        int a = 100;//
        if (a <= 100) {
            System.out.println("100以内");
        } else {
            System.out.println("100以外");
        }
        char c = 'A';
        if (c != 'A') {
            System.out.println("不是A");
        } else {
            System.out.println(c);
        }

    }
}
