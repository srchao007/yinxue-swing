package fun.codedesign.jdk.lang.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectDemo {
    public static void main(String[] args) {
        Employee e = new Employee("zhangsan", 120, 4200);
        reflect(e);
    }

    public static void reflect(Object obj) {
        Class<?> cls = obj.getClass();
        System.out.println("类名" + cls.getName());

        Field[] fields = cls.getDeclaredFields();
        System.out.println("变量名");
        for (Field f : fields) {
            System.out.println(f.getType() + "" + f.getName());

            Method[] methods = cls.getDeclaredMethods();
            System.out.println("方法名");
            for (Method m : methods) {
                System.out.println(m.getReturnType() + "\t");
                System.out.println(m.getName() + "\t");
                System.out.println(Arrays.toString(m.getParameterTypes()));
            }

            Constructor<?>[] constructors = cls.getDeclaredConstructors();
            System.out.println("构造器");
            for (Constructor<?> constructor : constructors) {
                System.out.println(constructor.getName() + "\t");
                System.out
                        .println(Arrays.toString(constructor.getParameterTypes()));
            }
        }
    }
}

class Employee {
    String name;
    int age;
    double salary;

    public Employee() {
    }

    public Employee(String name, int age, int salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}




