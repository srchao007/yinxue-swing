package fun.codedesign.jdk.lang.oop.basic.point;

public class Point2 {
    private int x;
    private int y;

    public Point2() {

    }

    public Point2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point2(int x) {
        this(x, x);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double distance(int x, int y) {
        double d1 = this.x - x;
        double d2 = this.y - y;
        double d = Math.sqrt(d1 * d1 + d2 * d2);
        return d;
    }

    public double distance(Point2 p) {
        return this.distance(p.x, p.y);
    }

    public double distance(Point2 p1, Point2 p2) {  //��һ�������������������ľ��룿 ��ͬ����p1�����p2�ľ���
        return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
    }

    public String toString() {
        return "Point2 [x=" + x + ", y=" + y + "]";
    }
}

