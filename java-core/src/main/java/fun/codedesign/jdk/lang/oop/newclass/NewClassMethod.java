package fun.codedesign.jdk.lang.oop.newclass;

import java.util.Arrays;

public class NewClassMethod {
    public static void main(String[] args) {
        Circle c2 = new Circle(20);
        System.out.println(Arrays.toString(c2.center));
        System.out.println(c2.getArea());

        c2.center = new int[]{18, 10};
        c2.point = new int[]{30, 10};
        System.out.println(c2.contains(c2.point));
        System.out.println(c2.contains(100, 10));

        System.out.println(c2.getDistance());
        c2.point1 = new int[]{30, 20};
        System.out.println(c2.getDistance(c2.point1));
    }
}

class Circle {
    double r;
    int[] center;
    int[] point;
    int[] point1;

    Circle() {
        this.r = 5;
        this.center = new int[]{0, 0};
    }

    Circle(double r) {
        this.r = r;
        this.center = new int[]{10, 1};
//		Random rd=newclass Random();
//		this.center[0]= rd.nextInt(10);
//		this.center[1]= rd.nextInt(10);
    }

    double getArea() {
        return r * r * Math.PI;
    }


    boolean contains(int[] point) {

        int d1 = point[0] - center[0];
        int d2 = point[1] - center[1];
        return (Math.sqrt(d1 * d1 + d2 * d2) <= r);

    }

    boolean contains(int x, int y) {
        int d1 = x - center[0];
        int d2 = y - center[1];

        return (Math.sqrt(d1 * d1 + d2 * d2) <= r);
    }

    double getDistance() {
        return Math.sqrt((point[0] - center[0]) * (point[0] - center[0]) + (point[1] - center[1]) * (point[1] - center[1]));
    }

    double getDistance(int[] point1) {
        return Math.sqrt((point1[0] - point[0]) * (point1[0] - point[0]) + (point1[1] - point[1]) * (point1[1] - point[1]));
    }

}

