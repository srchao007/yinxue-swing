package fun.codedesign.jdk.lang.oop.basic;

public class PointDemo {
    public static void main(String[] args) {
        Point p = new Point();
        Point p1 = new Point(2, 3);
        Point p2 = new Point(6);

        System.out.println(p.x);
        System.out.println(p1.x + p1.y);
        System.out.println(p2.x + " " + p2.y);
    }
}
