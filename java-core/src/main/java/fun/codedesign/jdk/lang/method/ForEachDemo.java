package fun.codedesign.jdk.lang.method;

public class ForEachDemo {
    public static void main(String[] args) {
        int[] x = new int[10];
        for (int y : x) {
            System.out.print(y + " ");
        }
    }
}
