package fun.codedesign.jdk.lang.method;

/**
 * print 9*9乘法表
 */
public class NestedForDemo {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + "*" + i + "=" + i * j + " ");
            }
            System.out.println();
        }
    }
}
