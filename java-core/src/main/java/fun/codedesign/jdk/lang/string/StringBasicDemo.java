package fun.codedesign.jdk.lang.string;


public class StringBasicDemo {
    public static void main(String[] args) {
        String x = "  hello world 中国  abc  ";
        System.out.println(x.charAt(3));
        System.out.println(x.length());
        System.out.println(x.trim());
        System.out.println(x.toLowerCase());
        System.out.println(x.toUpperCase());
        System.out.println(x.indexOf("中国"));
        System.out.println(x.lastIndexOf(" "));
        System.out.println(x.endsWith("c"));
        System.out.println(x.endsWith(" "));
        System.out.println(x.startsWith(" "));
        System.out.println(x.substring(3));
        System.out.println(x.substring(x.indexOf("中国") + 1));
    }
}
