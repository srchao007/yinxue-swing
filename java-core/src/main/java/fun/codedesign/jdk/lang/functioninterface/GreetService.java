package fun.codedesign.jdk.lang.functioninterface;

@FunctionalInterface
public interface GreetService {
    void sayHello(String hello);
}
