package fun.codedesign.jdk.lang.method;

import java.math.BigInteger;

//BigInteger  ������
/*
 * sum=1*2*3....20
 */
public class ForDemo3 {
    public static void main(String[] args) {
        long sum = 1;
        for (int i = 1; i < 20; i++) {
            sum *= i;
        }
        System.out.println(sum);
	/*
	
	/*
	BigInteger n1=newclass BigInteger("6234234543455");
	BigInteger n2=newclass BigInteger("5");
	//BigInteger n3=n1.divide(n2);//n1����n2
	BigInteger n3=n1.multiply(n2);//n1����n2
	//n3=n1.subtract(n2);//n1��ȥn2
	//n3=n1.add(n2);//n1����n2
	System.out.println(n3);*/

        BigInteger sum1 = new BigInteger("1");
        for (int i = 1; i <= 20; i++) {
            BigInteger n = new BigInteger(i + "");//+һ���ַ�������ɲ���
            sum1 = sum1.multiply(n); //sum1=sum1*n
            System.out.println(n + "," + sum1);
        }


        /*
         * i=1  sum1=1*1=1   i=2
         *        true   i=2   sum1=2
         */
    }
}
