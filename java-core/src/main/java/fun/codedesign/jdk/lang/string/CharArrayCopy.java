package fun.codedesign.jdk.lang.string;

import java.util.Arrays;

public class CharArrayCopy {
    public static void main(String[] args) {
        char[] chs1 = {'a', 'b'};
        char[] chs2 = {'a', 'b', 'c'};
        char[] chs3 = Arrays.copyOf(chs1, chs1.length + chs2.length); //
        System.out.println(Arrays.toString(chs3));

        System.arraycopy(chs2, 0, chs3, chs1.length, chs2.length);
        System.out.println(chs3);

        String s1 = "hello";
        String s2 = " world";
        String st = s1 + s2;
        System.out.println("st:"+ st);

        String s3 = s1.concat(s2);
        System.out.println("s3:" + s3);
    }
}
