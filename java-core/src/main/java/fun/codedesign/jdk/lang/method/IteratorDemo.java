package fun.codedesign.jdk.lang.method;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class IteratorDemo {
    public static void main(String[] args) {
        Collection<String> names = new HashSet<>();
        names.add("zhangsan");
        names.add("lisi");
        names.add("wanger");
        names.add("mazi");
        names.add("liuxing");

        Iterator<String> ite = names.iterator();
        while (ite.hasNext()) { //hasNext()
            // String name =ite.next();
            System.out.println(ite.next());//
        }
//		System.out.println("---------------------");
//		for(String name:names){
//			System.out.println(name);
//		}
    }
}
