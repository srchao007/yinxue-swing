package fun.codedesign.jdk.lang.oop.newclass;

public class NewClassMethodDemo {
    public static void main(String[] args) {
        FooTwo f = new FooTwo();
        System.out.println(f.add('0', 1));
    }
}

class FooTwo {
    long add(long a, long b) {
        System.out.println("long add long a + long b");
        return a + b;
    }

    long add(int a, int b) {
        for (int i = 0; i < 5; i++) {
            String s = "long add int a int b ";
            System.out.println(s);
            System.out.println("1");
            System.out.println("2");
            System.out.println("3");
        }
        return a + b;
    }

    long add(int a) {
        System.out.println("long add int a");
        return a + a;
    }
}

