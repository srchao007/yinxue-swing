package fun.codedesign.jdk.lang.oop.basic;

public class Demo4 {
    public static void main(String[] args) {
        new Hoo();
    }
}

class Joo {
    Joo() {
        System.out.println("Joo()");
    }

    Joo(int x) {
        System.out.println("youcan");
    }
}

class Hoo extends Joo {
    Hoo() {
        super(10);
        System.out.println("Hoo()");
    }
}
