package fun.codedesign.jdk.lang.method;

/**
 * PI
 */
public class ForDemo2 {
    public static void main(String[] args) {
        double sum = 0;
        double pi = 0;
        for (int i = 1; i <= 1000000000; i += 4) {
            double n = 1.0 / i - 1.0 / (i + 2);
            sum += n;
            pi = 4 * sum;
        }
        System.out.println(pi);
    }
}
