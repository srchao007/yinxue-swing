package fun.codedesign.jdk.lang.oop.basic.ball;

import javax.swing.*;

public class BallFrame extends JFrame {


    public void showMe() {
        this.setTitle("小球运动");
        this.setSize(1024, 768);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

    }

    public void addPanel() {
        BallPanel panel = new BallPanel();
        this.add(panel);
        panel.startRun();

    }
}
