package fun.codedesign.jdk.lang.oop.basic.ball;

import java.util.Arrays;

public class FlyingObject {

    public static void main(String[] args) {

        Flying[] enemy = new Flying[5];
        new Bomb(244, 1, 0);
        System.out.println(new Bomb(244, 1, 0));
        // System.out.println(Arrays.toString(enemy));

        enemy[3] = new AirPlane(54, 38, 76, 83);
        enemy[1] = new AirPlane(202, 120, 76, 83);
        enemy[2] = new Bomb(58, 196, 20);
        enemy[0] = new Bomb(104, 281, 20);
        enemy[4] = new Bomb(255, 289, 20);

        System.out.println(new Bomb(58, 196, 20));

        int x = 64;
        int y = 204;


        int index = -1;
        for (int i = 0; i < enemy.length; i++) {
            if (enemy[i].shootBy(x, y)) {
                index = i;
                System.out.println(i + "击中");
                // break;
            }
        }
        System.out.println(Arrays.toString(enemy));

        if (index != -1) {
            Flying deleteOne = enemy[index];
            enemy[enemy.length - 1] = enemy[index];
            enemy = Arrays.copyOf(enemy, enemy.length - 1);
            System.out.println(Arrays.toString(enemy));
            System.out.println("删除:" + deleteOne);
        }
    }
}

class Flying {
    int x, y;

    void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    boolean shootBy(int x, int y) {
        return false;
    }
}


class AirPlane extends Flying {
    int w, h;

    AirPlane(int x, int y, int w, int h) {
        this.x = x;
        super.y = y;
        this.w = w;
        this.h = h;
    }

    public boolean shootBy(int x, int y) {
        int d1 = x - this.x;
        int d2 = y - this.y;
        return (d1 > 0 && d1 < w && d2 > 0 && d2 < h);
    }

    public String toString() {
        return "point" + x + "," + y + "," + w + "," + h + ")";
    }


}

class Bomb extends Flying {
    int r;

    Bomb(int x, int y, int r) {
        this.r = r;
        super.x = x;
    }

    public boolean shootBy(int x, int y) {
        return (Math.sqrt((x - this.x) * (x - this.x) + (y - this.y) * (y - this.y))) <= r;
    }

    public String toString() {
        return "Bomb(" + x + "," + y + "," + r + ")";
    }

}
