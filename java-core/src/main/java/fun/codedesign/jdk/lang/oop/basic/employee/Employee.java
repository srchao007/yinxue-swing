package fun.codedesign.jdk.lang.oop.basic.employee;

//HashCodeֵ   equals  double boolean   

public class Employee {
    String name;
    int age;
    double salary;

    public Employee() {
    }

    public Employee(String name, int age, double salary) {
        super();
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;

    }

    public String toString() {

        return name + ":" + salary;  //��д���෽��
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + age;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        long temp;
        temp = Double.doubleToLongBits(salary);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        //1.�Ƚ��Ƿ�Ϊͬһ������
        if (this == obj) {
            return true;
        }

        //2.�ж�obj�Ƿ�Ϊ��
        if (obj == null) {
            return false;
        }

        //3.�ж�object�Ƿ�ΪEmployeeʵ��
        if (obj instanceof Employee) {

            Employee other = (Employee) obj; //����ת��
            System.out.println(this.salary == other.salary);
            return this.name.equals(other.name) && this.age == other.age &&
                    this.salary == other.salary;

        }
        return false;
    }


//	public int hashCode() {
//		return (int)(salary*33+age*33);
//	}
}
