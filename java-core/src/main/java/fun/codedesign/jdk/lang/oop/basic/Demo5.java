package fun.codedesign.jdk.lang.oop.basic;

public class Demo5 {
    public static void main(String[] args) {
        new Yoo();
    }
}

class Xoo {
    Xoo(int a) {
        // super();// Object��Xoo�ĸ��࣬java��һ�нԶ���ֻҪ����class��һ�оͻ����Ĭ�ϵ�super();
        System.out.println("Xoo(int)");
    }
}
// class Yoo extends Xoo{}    //������󣬸���û���޲ι�����

class Yoo extends Xoo {
    Yoo() {
        //super();//������󣬸���û���޲ι�����
        super(88);
        //super();//ֻ��д�����๹�����У�Ĭ���ڵ�һ��
        System.out.println("Yoo()");
    }
}

