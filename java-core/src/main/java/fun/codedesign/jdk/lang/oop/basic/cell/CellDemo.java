package fun.codedesign.jdk.lang.oop.basic.cell;

public class CellDemo {
    public static void main(String[] args) {
        Cell c1;
        c1 = new Cell();
        Cell c2 = new Cell();
        c1.row = 2;
        c1.col = 3;
        c2.row = 2;
        c2.col = 4;
        System.out.println(c1.row);//2
        c1.drop();
        c2.drop();
        System.out.println(c1.row);//3
        System.out.println(c2.row);//3

        c1.drop(5);
        System.out.println(c1.row); //3

        c1.moveleft(2);
        System.out.println(c1.col);

    }
}
