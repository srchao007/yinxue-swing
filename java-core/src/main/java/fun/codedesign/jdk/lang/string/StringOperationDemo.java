package fun.codedesign.jdk.lang.string;

public class StringOperationDemo {
    public static void main(String[] args) {
        String s1 = "abc";
        String s2 = s1;
        s2 += "def";
        System.out.println(s2);
        System.out.println(s1);
        System.out.println("abcdefg".substring(2, 4));
    }
}
