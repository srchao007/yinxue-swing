package fun.codedesign.jdk.lang.method;

/**
 * 作用域
 */
public class ScopeDemo {
    public static void main(String[] args) {
        int age = 11;
        System.out.println(age);
        age = 18;
        System.out.println(age);
        {
            int score = 78;
            System.out.println(score);
        }
    }
}
