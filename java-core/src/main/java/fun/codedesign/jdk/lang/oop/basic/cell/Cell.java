package fun.codedesign.jdk.lang.oop.basic.cell;

public class Cell {
    int row;
    int col;

    public void drop() {
        row++;
    }

    public void drop(/*Cell.this*/int step) {
        row = 8;
        row += step;
        //this.row+=step;
    }


    public void moveRight(int step) {
        col += step;
    }

    public void moveleft(int step) {
        col -= step;
    }

}
