package fun.codedesign.jdk.lang.reflect;

/**
 * 反射样式方法
 *
 * @author zengjian
 * @create 2018-03-13 17:36
 * @since 1.0.0
 */
public class Template {

    private String name;

    private String doString(String[] str) {
        this.name = str[0];
        System.out.println("name被设置为：" + name);
        return "ok";
    }

    private void helloWorld() {
        System.out.println("hello");
    }

    @Deprecated
    private void hi(String str, Integer x) {
        System.out.println("hi");
    }

    public static void main(String[] args) {
        Template template = new Template();
        template.doString(new String[1]);
        template.helloWorld();
        template.hi(null, null);
    }

}
