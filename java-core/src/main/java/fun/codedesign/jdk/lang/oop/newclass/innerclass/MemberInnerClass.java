package fun.codedesign.jdk.lang.oop.newclass.innerclass;


/**
 * 成员内部类：与成员变量同级内部类
 */
public class MemberInnerClass {
    public static void main(String[] args) {
        Aoo a = new Aoo();
        Aoo.Coo c = a.new Coo();
        System.out.println("��Ա�ڲ��������ⲿ�������:" + c.add3());
    }
}

class Aoo {
    int a = 9;

    class Coo {
        int add3() {
            return a + 1;
        }
    }
}