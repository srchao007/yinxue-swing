package fun.codedesign.jdk.lang.method;

/**
 * y=f(x)=3*x+5;
 */
public class MethodDemo {
    public static void main(String[] args) {
        // int x=5;
        int y = f(5);
        System.out.println(y); // = 3*5+5=20
    }

    public static int f(int i) {
        return 3 * i + 5;
    }

}
