package fun.codedesign.jdk.lang.methodref;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class MethodRefTest {
    public static void main(String[] args) {
        // 构造器引用  Class::new
        Car car = Car.create(Car::new);
        final List<Car> cars = Arrays.asList(car);
        // 静态方法引用 Class::static_method
        cars.forEach(Car::collide);
        // 特定类的任意对象方法引用 Class::metho
        cars.forEach(Car::repair);
        // 特定对象的方法引用 instance::method，重写该方法时候会调用重写逻辑
        final Car police = new Car() {
            @Override
            public void follow(Car another) {
                System.out.println("follow new car" + this);
            }
        };
        cars.forEach(police::follow);
        
        printlnMethodRef();
    }

    public static void printlnMethodRef() {
        List<String> names = new ArrayList<>();
        names.add("Google");
        names.add("Runoob");
        names.add("Taobao");
        names.add("Baidu");
        names.add("Sina");
        names.forEach(System.out::println);
    }
}

class Car {
    //Supplier是jdk1.8的接口，这里和lamda一起使用了
    public static Car create(final Supplier<Car> supplier) {
        return supplier.get();
    }

    public static void collide(final Car car) {
        System.out.println("Collided " + car.toString());
    }

    public void follow(final Car another) {
        System.out.println("Following the " + another.toString());
    }

    public void repair() {
        System.out.println("Repaired " + this.toString());
    }
}
