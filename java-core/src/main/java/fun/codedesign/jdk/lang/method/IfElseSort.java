package fun.codedesign.jdk.lang.method;

import java.util.Scanner;

public class IfElseSort {
    public static void main(String[] args) {
        try( Scanner sc = new Scanner(System.in)) {
            System.out.println("输入两个数:");
            int a, b;
            a = sc.nextInt();
            b = sc.nextInt();
    
            if (a > b) {
            /*int z = a;
            a = b;
            b = z;*/
                System.out.println(b + "," + a);
            } else {
                System.out.println(a + "," + b);
            }
        }
    }
}
