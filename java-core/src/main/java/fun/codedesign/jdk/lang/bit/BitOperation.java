package fun.codedesign.jdk.lang.bit;

import org.junit.Test;

/**
 * 与或运算 位运算
 */
public class BitOperation {
    public static void main(String[] args) {
        int high = 150;
        char sex = '男';
        // 第一个为否后面的不执行
        if (sex == '女' && high++ >= 162) {
            System.out.println("复合规定");
        } else {
            System.out.println("不符合规定");
        }
        System.out.println(high);

        // 两边条件都要执行
        if (sex == '女' & high++ >= 162) {
            System.out.println("符合规定");
        } else {
            System.out.println("不符合规定");
        }
        System.out.println(high);//151
        //
        sex = '女';
        high = 150;

        // 第一个肯定，后面不执行
        if (sex == '女' || high++ >= 162) {
            System.out.println("符合规定");
        } else {
            System.out.println("不符合规定");
        }
        System.out.println(high);//150

        // 均要执行
        if (sex == '女' | high++ >= 162) {
            System.out.println("符合规定");
        } else {
            System.out.println("不符合规定");
        }
        System.out.println(high);//151

    }

    @Test
    public void testName() throws Exception {
        int i = 2 >> 2; //  0010 右移两位 0000 即0
        System.out.println(i); //0
        i = 1 << 2; // 1 * 2的2次方  0001 左移两位 0100  即4
        System.out.println(i);
    }
}