package fun.codedesign.jdk.lang.oop.basic.car;

public class QQDemo {
    public static void main(String[] args) {
        QQ qq = new QQ();
        Product p;  //�ӿڲ���ֱ�Ӵ������󣬵��ǿ�����Ϊ�������ͣ�ָ��ʵ����Ķ���

        p = qq;
        System.out.println(p.getPrice());
        // System.out.println(p.stop());  //�����ȴӸ���product���ң�Ȼ��������

        Car c = qq;
        c.run();
        c.stop();

        c = (Car) p;  //����ת��
        if (p instanceof Car) {   //��ʾp�Ƿ�Ϊcar���͵ĵ�ʵ��
            c = (Car) p;
            c.run();
        } else {
            System.out.println("����ǿת");
        }
    }
}
