package fun.codedesign.jdk.lang.oop.basic.person;

public class EmployeeDemo {
    public static void main(String[] args) {
        Employee e = new Employee("zhangsan", 23, 2000);
        System.out.println(e.getName());
        e.setName("lisi");
        System.out.println(e.getName());
        e.setAge(26);
        System.out.println(e.getAge());
    }
}
