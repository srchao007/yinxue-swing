package fun.codedesign.jdk.lang.method;

import java.util.Random;

public class DoWhileDemo2 {
    public static void main(String[] args) {
        Random rd = new Random();
        int x, y;
        int count = 0;
        do {
            x = rd.nextInt(10);
            y = rd.nextInt(10);
            count++;

        } while (x * x + y * y != 50);
        System.out.println(x + "," + y);
        System.out.println("循环次数:" + count);
    }
}
