package fun.codedesign.jdk.lang.method;

import java.util.Scanner;

public class IfElseScore {
    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            System.out.println("输入数字");
            int score = sc.nextInt();
            if (score < 0 || score > 100) {
                System.out.println("输入不符合规范");
                return;
            }
            if (score < 60) {
                System.out.println("不及格");
            } else if (score >= 60 && score < 70) {
                System.out.println("合格");
            } else if (score >= 70 && score < 90) {
                System.out.println("良好");
            } else if (score >= 90 && score <= 100) {
                System.out.println("优秀");
            }
        }
    }
}
