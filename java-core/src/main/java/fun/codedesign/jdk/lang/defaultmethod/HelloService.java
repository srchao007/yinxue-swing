package fun.codedesign.jdk.lang.defaultmethod;

public interface HelloService {
    // 可设置默认方法
    default void sayHello() {
        System.out.println("hello");
    }
}
