package fun.codedesign.jdk.lang.method;

import java.util.Scanner;

public class DoWhileDemo1 {

    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            int score;
            do {
                System.out.print("输入数字:");
                score = sc.nextInt();
            } while (score < 0 || score > 100);
            System.out.println(score + "%");
        }
    }
}
