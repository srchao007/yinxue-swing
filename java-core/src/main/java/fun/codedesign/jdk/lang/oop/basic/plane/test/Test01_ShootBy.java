package fun.codedesign.jdk.lang.oop.basic.plane.test;

import fun.codedesign.jdk.lang.oop.basic.plane.dao.Flying;
import fun.codedesign.jdk.lang.oop.basic.plane.dao.impl.Bomb;
import fun.codedesign.jdk.lang.oop.basic.plane.dao.impl.Plane;
import fun.codedesign.jdk.lang.oop.basic.plane.entity.Point;

public class Test01_ShootBy {
    public static void main(String[] args) {
        Flying[] enemy = new Flying[5];

        enemy[0] = new Bomb(new Point(3, 4), 5);
        enemy[1] = new Bomb(new Point(4, 5), 6);
        enemy[2] = new Bomb(new Point(6, 7), 8);
        enemy[3] = new Plane(new Point(6, 7), 8, 16);
        enemy[4] = new Plane(new Point(10, 6), 10, 16);


        Point point1 = new Point(9, 5);
        int index = -1;
        for (int i = 0; i < enemy.length; i++) {
            if (enemy[i].shootBy(point1)) {
                index = i;
                System.out.println("enemy " +i +" :" + enemy[i].toString());
            }
        }
        System.out.println("index:" + index);
    }
}
