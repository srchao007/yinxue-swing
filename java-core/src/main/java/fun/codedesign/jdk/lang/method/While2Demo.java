package fun.codedesign.jdk.lang.method;

public class While2Demo {
    public static void main(String[] args) {
        //new abc2().run();

        int x = 102;
        System.out.println(x);

        char y = (char) 35;
        System.out.println((int) 'f');
        System.out.println(y);
    }
}
