package fun.codedesign.jdk.lang.oop.basic.employee;

public class EmployeeDemo {
    public static void main(String[] args) {
        Employee e = new Employee("123", 77, 33);
        Employee e1 = new Employee("1323", 33, 77);
        System.out.println(e);
        System.out.println(e.toString());
        System.out.println(Integer.toHexString(e.hashCode()));  //

        System.out.println(e.hashCode());
        System.out.println(e1.hashCode());
        System.out.println(e.equals(e1));

    }
}
