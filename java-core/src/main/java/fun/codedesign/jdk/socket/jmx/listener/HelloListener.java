package fun.codedesign.jdk.socket.jmx.listener;


import javax.management.Notification;
import javax.management.NotificationListener;

import fun.codedesign.jdk.socket.jmx.javabean.Hello;

/**
 * @author zengjian
 * @create 2018-06-12 11:17
 * @since 1.0.0
 */
public class HelloListener implements NotificationListener {
    @Override
    public void handleNotification(Notification notification, Object handback) {
        if (handback instanceof Hello) {
            Hello hello = (Hello) handback;
            hello.helloWorld(notification.getMessage());
        }
    }
}
