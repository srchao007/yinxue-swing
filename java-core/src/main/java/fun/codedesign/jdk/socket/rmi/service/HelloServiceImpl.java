package fun.codedesign.jdk.socket.rmi.service;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * 服务端实现类
 *
 * @author zengjian
 * @create 2018-03-23 10:08
 * @since 1.0.0
 */
public class HelloServiceImpl extends UnicastRemoteObject implements HelloService {

    private static final long serialVersionUID = -7914021734418633337L;

    public HelloServiceImpl() throws RemoteException {
        super();
    }

    @Override
    public String sayHello(String someOne) throws RemoteException {
        return "hello," + someOne;
    }
}
