package fun.codedesign.jdk.socket.jmx.rmi;


import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import fun.codedesign.jdk.socket.jmx.javabean.Hello;

import java.lang.management.ManagementFactory;
import java.rmi.registry.LocateRegistry;

/**
 * 以服务器的方式提供远程调用
 *
 * @author zengjian
 * @create 2018-06-12 10:28
 * @since 1.0.0
 */
public class HelloAgentServer {

    public static void main(String[] args) throws Exception {
        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        ObjectName hello = new ObjectName("jmxBean:name=hello");
        server.registerMBean(new Hello(), hello);

        LocateRegistry.createRegistry(9999);
        JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:9999/jmxrmi");
        JMXConnectorServer jcs = JMXConnectorServerFactory.newJMXConnectorServer(url, null, server);

        System.out.println("begin rmi start");
        jcs.start();
        System.out.println("rmi start");

    }
}
