package fun.codedesign.jdk.socket.rmi;


import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.RMISocketFactory;

import fun.codedesign.jdk.socket.rmi.service.HelloServiceImpl;

/**
 * @author zengjian
 * @create 2018-03-23 10:12
 * @since 1.0.0
 */
public class ServerTest {
    public static void main(String[] args) throws Exception {
        // 创建注册对象
        LocateRegistry.createRegistry(8801);
        // 指定通讯端口防止被防火墙拦截，负责网络通讯的工厂类
        // 用来基于以上端口来创建连接对象
        RMISocketFactory.setSocketFactory(new CustomSocketFactory());
        // 获得注册对象之前注册的对象，绑定接口名称及路径
        Naming.bind("rmi://localhost:8801/helloService", new HelloServiceImpl());
        LogUtil.info(ServerTest.class, "服务器启动，对外端口{0}:,服务接口:{1}", 8801, "helloService");
    }
}
