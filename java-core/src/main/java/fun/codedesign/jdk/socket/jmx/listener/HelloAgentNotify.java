package fun.codedesign.jdk.socket.jmx.listener;


import javax.management.MBeanServer;
import javax.management.ObjectName;

import fun.codedesign.jdk.socket.jmx.javabean.Hello;
import fun.codedesign.jdk.socket.jmx.javabean.Jack;

import java.lang.management.ManagementFactory;

/**
 * MBean之间的通信
 *
 * @author zengjian
 * @create 2018-06-12 11:19
 * @since 1.0.0
 */
public class HelloAgentNotify {

    public static void main(String[] args) throws Exception {
        MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
        ObjectName helloBean = new ObjectName("jmxBean:name=hello");
        ObjectName jackBean = new ObjectName("jackBean:name=jack");
        Hello hello = new Hello();
        Jack jack = new Jack();
        mBeanServer.registerMBean(jack, jackBean);
        mBeanServer.registerMBean(hello, helloBean);
        // 观察者模式
        jack.addNotificationListener(new HelloListener(), null, hello);
        Thread.sleep(60 * 1000 * 1000);
    }
}
