package fun.codedesign.jdk.socket.rmi.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 服务端
 *
 * @author zengjian
 * @create 2018-03-23 10:06
 * @since 1.0.0
 */
public interface HelloService extends Remote {
    String sayHello(String someOne) throws RemoteException;
}
