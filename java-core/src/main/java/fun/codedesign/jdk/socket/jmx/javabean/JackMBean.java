package fun.codedesign.jdk.socket.jmx.javabean;

/**
 * @author zengjian
 * @create 2018-06-12 11:14
 * @since 1.0.0
 */
public interface JackMBean {

    void hi();
}
