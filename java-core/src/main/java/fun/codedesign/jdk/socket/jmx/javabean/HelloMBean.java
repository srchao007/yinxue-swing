package fun.codedesign.jdk.socket.jmx.javabean;

/**
 * @author zengjian
 * @create 2018-06-12 9:49
 * @since 1.0.0
 */
public interface HelloMBean {

    int getAge();

    void setAge(int age);

    String getName();

    void setName(String name);

    void helloWorld();

    void helloWorld(String str);

    void getTelephone();

}
