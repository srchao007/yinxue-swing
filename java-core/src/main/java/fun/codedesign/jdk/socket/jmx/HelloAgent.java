package fun.codedesign.jdk.socket.jmx;


import javax.management.ObjectName;

import fun.codedesign.jdk.socket.jmx.javabean.Hello;

import java.lang.management.ManagementFactory;

/**
 * jconsole连接查看
 *
 * @author zengjian
 * @create 2018-06-12 9:51
 * @since 1.0.0
 */
public class HelloAgent {
    public static void main(String[] args) throws Exception {
        // 采用jconsole可以对mbean进行控制和变更
        // jmx注册mbean, 注册类需要实现接口
        ObjectName helloName = new ObjectName("jmxBean:name=hello");
        ManagementFactory.getPlatformMBeanServer().registerMBean(new Hello(), helloName);
        Thread.sleep(60 * 1000 * 1000);
    }
}
