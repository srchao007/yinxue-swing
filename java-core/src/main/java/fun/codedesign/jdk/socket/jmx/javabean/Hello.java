package fun.codedesign.jdk.socket.jmx.javabean;

/**
 * @author zengjian
 * @create 2018-06-12 9:50
 * @since 1.0.0
 */
public class Hello implements HelloMBean {

    private int age;
    private String name;

    public int getAge() {
        System.out.println("getAge" + age);
        return age;
    }

    public void setAge(int age) {
        System.out.println("setAge" + age);
        this.age = age;
    }

    public String getName() {
        System.out.println("getName" + name);
        return name;
    }

    public void setName(String name) {
        System.out.println("setName" + name);
        this.name = name;
    }

    @Override
    public void helloWorld() {
        System.out.println("helloworld default");
    }

    @Override
    public void helloWorld(String str) {
        System.out.println("hello123 " + str);
    }

    @Override
    public void getTelephone() {
        System.out.println("123455667");
    }

}
