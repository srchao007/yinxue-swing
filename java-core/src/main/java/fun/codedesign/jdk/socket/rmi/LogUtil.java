/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: LogUtil
 * Author:   zengjian
 * Date:     2018/7/31 10:41
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.jdk.socket.rmi;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 〈日志记录工具类〉<br>
 * 〈INFO:打印到控制台，ERROR:打印到控制台并且记录文件日志〉
 *
 * @author zengjian
 * @create 2018/7/31 10:41
 */
public class LogUtil {

    private static final String SEPARATOR = System.getProperty("line.separator");

    /**
     * 打印到控制台开关
     */
    public static boolean console = true;
    /**
     * 记录到文件开关
     */
    public static boolean record = true;

    public static void info(Object src, String format, Object... message) {
        if (console == false) {
            return;
        }
        StringBuilder builder = new StringBuilder();
        builder.append("INFO:");
        builder.append(getTimestamp());
        builder.append(":");
        builder.append(getClassName(src));
        builder.append(":");
        builder.append(MessageFormat.format(format, message));
        console(builder);
    }

    public static void info(Object src, Object message) {
        StringBuilder builder = new StringBuilder();
        builder.append("INFO:");
        builder.append(getTimestamp());
        builder.append(":");
        builder.append(getClassName(src));
        builder.append(":");
        builder.append(message);
        console(builder);
    }

    private static void console(StringBuilder builder) {
        System.out.println(builder.toString());
    }

    private static String getClassName(Object src) {
        if (src instanceof Class) {
            return ((Class<?>) src).getName();
        }
        return src.getClass().getName();
    }

    public static void error(Object src, String format, Exception e, String... message) {
        if (console == false && record == false) {
            return;
        }
        StringBuilder builder = new StringBuilder();
        builder.append("ERROR:");
        builder.append(getTimestamp());
        builder.append(":");
        builder.append(getClassName(src));
        builder.append(":");
        builder.append(MessageFormat.format(format, (Object)message));
        builder.append(SEPARATOR);
        console(builder);
        e.printStackTrace();
        if (record == true) {
            record(src, format, e, message);
        }
    }

    public static void error(Object src, String message, Exception e) {
        if (console == false && record == false) {
            return;
        }
        StringBuilder builder = new StringBuilder();
        builder.append("ERROR:");
        builder.append(getTimestamp());
        builder.append(":");
        builder.append(getClassName(src));
        builder.append(":");
        builder.append(message);
        builder.append(SEPARATOR);
        console(builder);
        e.printStackTrace(); //打印堆栈信息
        if (record == true) {
            record(src, e, message);
        }
    }

    private static void record(Object src, Exception e, String... message) {

    }

    private static void record(Object src, String format, Exception e, String... message) {

    }


    public static String getTimestamp() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
        return format.format(new Date());
    }
}