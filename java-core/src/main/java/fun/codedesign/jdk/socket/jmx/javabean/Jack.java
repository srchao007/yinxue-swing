package fun.codedesign.jdk.socket.jmx.javabean;

import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

/**
 * MBean之间的通信
 *
 * @author zengjian
 * @create 2018-06-12 11:14
 * @since 1.0.0
 */
public class Jack extends NotificationBroadcasterSupport implements JackMBean {

    @Override
    public void hi() {
        Notification notification = new Notification("jack.hi", this, System.currentTimeMillis(), "jack");
        sendNotification(notification);
    }
}
