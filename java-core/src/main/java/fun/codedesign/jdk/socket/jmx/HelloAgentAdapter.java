package fun.codedesign.jdk.socket.jmx;

//import com.sun.jdmk.comm.HtmlAdaptorServer;


import javax.management.ObjectName;

import fun.codedesign.jdk.socket.jmx.javabean.Hello;

import java.lang.management.ManagementFactory;

/**
 * 用浏览器管理MBean,增加依赖
 * <dependency>
 * <groupId>com.sun.jdmk</groupId>
 * <artifactId>jmxtools</artifactId>
 * <version>1.2.1</version>
 * </dependency>
 *
 * @author zengjian
 * @create 2018-06-12 10:11
 * @since 1.0.0
 */
public class HelloAgentAdapter {

    public static void main(String[] args) throws Exception {
        // 采用浏览器进行mBean管理
        ObjectName helloName = new ObjectName("jmxBean:name=hello");
        ManagementFactory.getPlatformMBeanServer().registerMBean(new Hello(), helloName);
        // 注册适配器<!-- 浏览器可视化javaMbean管理 -->
//        ObjectName adapterName = new ObjectName("html:name=htmlAdapter,port=8082");
//        HtmlAdaptorServer adaptorServer = new HtmlAdaptorServer();
//        mBeanServer.registerMBean(adaptorServer, adapterName);
//        adaptorServer.start();
//        Thread.sleep(60*1000*1000);
    }
}
