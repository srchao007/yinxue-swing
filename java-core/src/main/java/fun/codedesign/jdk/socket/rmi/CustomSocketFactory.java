package fun.codedesign.jdk.socket.rmi;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.RMISocketFactory;

/**
 * RMI端口随机，防止被拦截需要强制指定
 *
 * @author zengjian
 * @create 2018-03-23 10:22
 * @since 1.0.0
 */
public class CustomSocketFactory extends RMISocketFactory {
    @Override
    public Socket createSocket(String host, int port) throws IOException {
        Socket socket = new Socket(host, port);
        LogUtil.info(this, "创建socket连接,host:{0},port:{1}", host, port);
        return socket;
    }

    @Override
    public ServerSocket createServerSocket(int port) throws IOException {
        if (port == 0) {
            port = 8501;
        }
        LogUtil.info(this, "服务器监听端口:{0}" + port);
        return new ServerSocket(port);
    }
}
