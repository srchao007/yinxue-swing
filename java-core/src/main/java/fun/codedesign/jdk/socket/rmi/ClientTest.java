package fun.codedesign.jdk.socket.rmi;


import java.rmi.Naming;

import fun.codedesign.jdk.socket.rmi.service.HelloService;


/**
 * 客户端RMI调用
 *
 * @author zengjian
 * @create 2018-03-23 10:15
 * @since 1.0.0
 */
public class ClientTest {
    public static void main(String[] args) throws Exception {
        // 服务引入
        HelloService helloService = (HelloService) Naming.lookup("rmi://localhost:8801/helloService");
        LogUtil.info(ClientTest.class, "找到服务接口:{0}", helloService.toString());
        // 调用远程方法
        System.out.println("RMI 服务器返回的结果是:" + helloService.sayHello("zengjian"));
    }
}
