package fun.codedesign.jdk.thread;

public class ThreadMain {
    public static void main(String[] args) throws Exception {
        Thread t1 = new Numbers();
        Thread t2 = new English();
        t1.start();
        t2.start();
    }
}

class Numbers extends Thread {
    @Override
    public void run() {
        Object o = new Numbers();
        for (int i = 1; i <= 52; i += 2) {
            System.out.print(i);
            System.out.print(i + 1);
            o.notify();
            //o.wait(5000);
        }
    }
}

class English extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 26; i += 2) {
            System.out.print((char) ('A' + i));
            System.out.print((char) ('A' + i + 1));

        }
    }
}
