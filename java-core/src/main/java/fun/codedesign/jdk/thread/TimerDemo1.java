package fun.codedesign.jdk.thread;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerDemo1 {


    public static void main(String[] args) throws Exception {
        Timer timer = new Timer();

        String s = "2017/06/7 20:10:40";
        SimpleDateFormat sdf
                = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        Date date = sdf.parse(s);

        timer.schedule(new TimerTask() {
            public void run() {
                System.out.println("定时执行");
            }
        }, date);
    }

}
