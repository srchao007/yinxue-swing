package fun.codedesign.jdk.thread.juc.future;

import java.util.concurrent.CompletableFuture;

/**
 * 组合future
 */
public class CompleteFutureTest {

    public static void main(String[] args) throws Exception {
        final CompletableFuture<Object> future1 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName());
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 123;
        });
        final CompletableFuture<Object> future2 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName());
            return 321;
        });
        // 取先执行完成获得结果的
        System.out.println(CompletableFuture.anyOf(future1, future2).get());
    }
}
