package fun.codedesign.jdk.thread;


/**
 * wait持有锁
 *
 * @author zengjian
 * @create 2018-03-19 20:18
 * @since 1.0.0
 */
public class WaitLock {
    public static void main(String[] args) {
        try {
            System.out.println("11111");
            synchronized ("A") {
                System.out.println("AAAAAA");
                "A".wait();
                System.out.println("bbbbbb");
            }
            System.out.println("2222");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
