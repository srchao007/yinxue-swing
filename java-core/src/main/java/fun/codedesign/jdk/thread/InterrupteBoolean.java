package fun.codedesign.jdk.thread;

/**
 * 测试 中断的状态
 *
 * @author zengjian
 * @create 2018-03-20 9:57
 * @since 1.0.0
 */
public class InterrupteBoolean {

    public static void main(String[] args) {
        Thread t1 = new MyThread();
        t1.start();
        t1.interrupt();
    }

    static class MyThread extends Thread {
        @Override
        public void run() {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                boolean b1 = Thread.currentThread().isInterrupted();
                System.out.println("异常抛出后:" + b1);
                Thread.currentThread().interrupt();
                b1 = Thread.currentThread().isInterrupted();
                System.out.println("在异常中执行中断处理:" + b1);
                interrupted();
                b1 = Thread.currentThread().isInterrupted();
                System.out.println("在异常后再次执行清除中断操作" + b1);
                interrupted();
                b1 = Thread.currentThread().isInterrupted();
                System.out.println("在异常后再次执行第二次清除中断操作" + b1);
                Thread.currentThread().interrupt();
                b1 = Thread.currentThread().isInterrupted();
                System.out.println("在异常后再次执行第二次清除后再次中断" + b1);
            }
            System.out.println("中断后还能运行吗？能");
        }
    }
}
