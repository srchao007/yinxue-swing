package fun.codedesign.jdk.thread;

/**
 * @author zengjian
 * @create 2018-03-19 19:18
 * @since 1.0.0
 */
public class Mythread extends Thread {
    @Override
    public void run() {
        System.out.println("hello world");
    }
}
