package fun.codedesign.jdk.thread;

public class MultiRunnableDemo {
    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new Thread(new Worker()).start();
        }
    }
}

class Worker implements Runnable {
    static int id = 0;
    int count = id++;

    public Worker() {
        System.out.println(" " + id + " ");
    }

    public void run() {
        for (int i = 0; i < 3; i++) {
            System.out.println(" " + id + "  " + (i + 1));
            Thread.yield();
        }
        System.out.println("运行线程:" + id + "-----run-----");
    }

}
