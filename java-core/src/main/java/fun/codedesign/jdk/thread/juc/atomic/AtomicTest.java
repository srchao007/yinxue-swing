package fun.codedesign.jdk.thread.juc.atomic;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * 原子变量
 * @zengjian
 */
public class AtomicTest {

    static class State {
        volatile int count;
    }

    public static void main(String[] args) {
        // 只能是实例变量，并且采用volatile修饰
        AtomicIntegerFieldUpdater<State> atomicField = AtomicIntegerFieldUpdater.newUpdater(State.class, "count");
        State state = new State();
        state.count = 123;
        // expect为本来值
        atomicField.compareAndSet(state, 123, 256);
        System.out.println(state.count);
    }
}
