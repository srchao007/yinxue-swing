package fun.codedesign.jdk.thread;

/**
 * 三个线程一次打印1-75 各5个数字
 *
 * @author zengjian
 * @create 2018-03-20 9:22
 * @since 1.0.0
 */
public class Print1_75_ThreeThread {

    private static Object lock = new Object();
    private static int num = 1;
    private static int end = 75;

    public static void main(String[] args) {
        new Thread1(0).start();
        new Thread1(1).start();
        new Thread1(2).start();
    }

    static class Thread1 extends Thread {

        private int id;

        public Thread1(int id) {
            this.id = id;
        }

        @Override
        public void run() {
            synchronized (lock) {
                while (num <= end) {
                    if (num / 5 % 3 == id) {
                        for (int i = 0; i < 5; i++) {
                            System.out.print(Thread.currentThread().getName() + ":" + num++ + ",");
                        }
                        System.out.println();
                        lock.notifyAll();
                    } else {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
