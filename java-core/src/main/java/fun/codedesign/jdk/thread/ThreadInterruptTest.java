package fun.codedesign.jdk.thread;
/**
 * 线程中断测试
 *
 * @author zengjian
 * @create 2018-03-19 19:03
 * @since 1.0.0
 */
public class ThreadInterruptTest {

    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                int count = 1;
                while (true) {
                    System.out.println(count++);
                }
            }
        });
        t1.start();

        Thread myThread = new Mythread();
        myThread.start();
        /*try {
            myThread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        // interrupted()表是当前线程，当前线程是main方法，而不是myThread
        // 源码 currentThread.isInterrupted(true)
        // 是静态方法
        // System.out.println(myThread.interrupted());
        // System.out.println(myThread.interrupted());

        myThread.interrupt();
        System.out.println(myThread.isInterrupted());

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(myThread.isInterrupted());
    }
}
