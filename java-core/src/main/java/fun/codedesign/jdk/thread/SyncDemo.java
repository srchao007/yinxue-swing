package fun.codedesign.jdk.thread;

public class SyncDemo {
    public static void main(String[] args) {
        Table table = new Table();
        Thread t1 = table.new Person();
        Thread t2 = table.new Person();
        t1.start();
        t2.start();

    }
}

class Table {
    int beans = 20;

    public int getBeans() {
        synchronized (this) {
            if (beans == 0) {
                throw new RuntimeException("空了");
            }
            Thread.yield();
            return beans--;
        }
    }

    class Person extends Thread {
        @Override
        public void run() {
            while (true) {
                int bean = getBeans();
                System.out.println(getName() + "," + bean);
                Thread.yield();
            }
        }
    }

}