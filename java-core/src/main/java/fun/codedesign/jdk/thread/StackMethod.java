package fun.codedesign.jdk.thread;

import java.util.Arrays;

/**
 * 栈方法
 *
 * @author zengjian
 * @create 2018-03-20 9:29
 * @since 1.0.0
 */
public class StackMethod {
    public static void main(String[] args) {
        print();
    }

    static void print() {
        StackTraceElement[] ss = Thread.currentThread().getStackTrace();
        System.out.println(Arrays.toString(ss));
    }
}
