package fun.codedesign.jdk.thread;

/**
 * 三个线程循环打印ABC
 *
 * @author zengjian
 * @create 2018-03-20 9:40
 * @since 1.0.0
 */
public class ABCprintThreeThread {

    private static int num = 0;
    private static int count = 10;
    private static Object lock = new Object();

    public static void main(String[] args) {
        new ABCThread(0).start();
        new ABCThread(1).start();
        new ABCThread(2).start();
    }

    static class ABCThread extends Thread {

        private int modId;

        public ABCThread(int modId) {
            this.modId = modId;
        }

        @Override
        public void run() {
            synchronized (lock) {
                while (num < count) {
                    if (num % 3 == modId) {
                        for (char i = 'A'; i <= 'C'; i++) {
                            System.out.println(Thread.currentThread().getName() + i);
                        }
                        lock.notifyAll();
                        num++;
                    } else {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
