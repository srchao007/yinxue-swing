package fun.codedesign.jdk.thread;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerDemo2 {
    public static void main(String[] args) {
        int min = 699;
        long start = System.currentTimeMillis();
        final long end = start + min * 60 * 1000;
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                long now = System.currentTimeMillis();
                long show = end - now;
                long hour = show / 1000 / 60 / 60;
                long m = show / 1000 / 60 % 60;
                long s = show / 1000 % 60;
                System.out.println(hour + "时" + m + "分" + s + "秒");
            }
        }, 0, 2000); //2s执行一次


        timer.schedule(new TimerTask() {
            public void run() {
                timer.cancel();
            }
        }, new Date(end));
    }
}
