package fun.codedesign.jdk.thread;

public class RunnableDemo implements Runnable {
    static int count = 0;
    final int id = count++;
    int x = 10;

    public RunnableDemo() {
    }

    public RunnableDemo(int count) {
        RunnableDemo.count = count;
    }

    public static void main(String[] args) {
        RunnableDemo t = new RunnableDemo();
        t.run();
    }

    public String state() {
        return "#" + "(" + id + ")" + (x > 0 ? x + " " : ": bingo");
    }

    public void run() {
        while (x-- > 0) {
            System.out.print(state());
        }
    }

}
