package fun.codedesign.jdk.thread;

import java.util.Arrays;

public class FibonacciRunnable implements Runnable {
    int f1, f2, fn;

    public FibonacciRunnable() {
    }

    public FibonacciRunnable(int count) {
        f1 = 1;
        f2 = 1;
        if (count == 1 || count == 2) {
            fn = 1;
        } else {
            for (int i = 3; i <= count; i++) {
                fn = f1 + f2;
                f1 = f2;
                f2 = fn;
            }
        }
        System.out.println("Fibonacci值:" + fn);
    }

    public static void main(String[] args) {
        new FibonacciRunnable(64).toString(20);
    }

    public String toString(int count) {
        int[] m = new int[count];
        for (int i = 1; i <= count; i++) {
            FibonacciRunnable f = new FibonacciRunnable(i);
            int n = f.fn;
            m[i - 1] = n;
        }
        return "Fibonaci数组：" + Arrays.toString(m);

    }

    public String toString() {
        return "FibonacciRunnable [f1=" + f1 + ", f2=" + f2 + ", fn=" + fn + "]";
    }

    @Override
    public void run() {
        int count = (int) (Math.random() * 20);
        new FibonacciRunnable(count);
        Thread.yield();
    }
}

