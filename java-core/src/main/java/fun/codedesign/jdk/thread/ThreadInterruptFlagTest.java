package fun.codedesign.jdk.thread;

/**
 * 测试利用中断标志进行中断运行
 *
 * @author zengjian
 * @create 2018-03-19 19:32
 * @since 1.0.0
 */
public class ThreadInterruptFlagTest {

    public static void main(String[] args) throws InterruptedException {
        Thread t2 = new MyThread2();
        t2.start();

        Thread.sleep(1000);
        t2.interrupt();
    }

    private static class MyThread2 extends Thread {
        @Override
        public void run() {
            super.run();
            int count = 0;
            long l1 = System.currentTimeMillis();
            try {
                while (true) {
                    if (interrupted()) {
                        throw new Exception("线程中止不中止");
                    }
                    System.out.println(count++);
                }
            } catch (Exception e) {
                System.out.println("异常");
                System.out.println("end");
                System.out.println(System.currentTimeMillis() - l1);
            }

        }
    }
}


