/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: SemaphoreTest2
 * Author:   zengjian
 * Date:     2018/8/7 9:44
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */package fun.codedesign.jdk.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/7 9:44
 */
public class SemaphoreTest2 {

    public static void main(String[] args) {
        final Semaphore semaphore = new Semaphore(100);
        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                semaphore.release(50);
            }
        }, 1000, 500, TimeUnit.MILLISECONDS);

        ExecutorService executorService = Executors.newFixedThreadPool(200);
        for (int i = 0; i < 100; i++) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        for (int j = 0; j < 1000; j++) {
                            semaphore.acquireUninterruptibly(1);
                        }

                        semaphore.acquire();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {

                    }
                }
            });
        }
    }

}