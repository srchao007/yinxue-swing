package fun.codedesign.jdk.thread;

/**
 * 当前线程中断
 *
 * @author zengjian
 * @create 2018-03-19 19:26
 * @since 1.0.0
 */
public class ThreadCurrentInterruptTest {
    public static void main(String[] args) {
        Thread.currentThread().interrupt();
        // 第一次返回的是已中断的标志，并且清除该状态，第二次就是未中断的标志
        System.out.println(Thread.interrupted());
        // Thread #interrupted 有清除中断标志的功能
        System.out.println(Thread.interrupted());

        Thread.currentThread().interrupt();
        System.out.println(Thread.currentThread().isInterrupted());
    }
}
