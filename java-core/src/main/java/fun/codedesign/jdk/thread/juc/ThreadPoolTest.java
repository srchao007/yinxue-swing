package fun.codedesign.jdk.thread.juc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zengjian
 * @create 2018-06-13 11:43
 * @since 1.0.0
 */
public class ThreadPoolTest {

    public static void main(String[] args) {
        ExecutorService service1 = Executors.newFixedThreadPool(10);
        ExecutorService service2 = Executors.newSingleThreadExecutor();
        ExecutorService service3 = Executors.newCachedThreadPool();
        System.out.println(service1);
        System.out.println(service2);
        System.out.println(service3);
    }
}
