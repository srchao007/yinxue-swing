package fun.codedesign.jdk.thread;

/**
 * sleep下中断
 *
 * @author zengjian
 * @create 2018-03-19 19:47
 * @since 1.0.0
 */
public class ThreadSleepPratice {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new MyThread();
        t1.start();
        System.out.println(t1.isInterrupted());
        t1.interrupt();
        System.out.println(t1.isInterrupted());
    }

    private static class MyThread extends Thread {
        @Override
        public void run() {
            System.out.println("start");
            try {
                for (int i = 0; i < 10000; i++) {
                    System.out.println(i);
                }
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("异常后的，中断标志是:" + this.isInterrupted());
            }
            System.out.println("end");
        }
    }
}

