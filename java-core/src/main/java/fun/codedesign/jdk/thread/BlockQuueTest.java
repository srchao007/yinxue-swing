package fun.codedesign.jdk.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

public class BlockQuueTest {

    public static void main(String[] args) {
        LinkedBlockingDeque<String> queue = new LinkedBlockingDeque<>();
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.submit(new PutRunnable(queue));
        service.submit(new TakeRunable(queue));
    }

}

class PutRunnable implements Runnable {

    private LinkedBlockingDeque<String> queue;

    public PutRunnable(LinkedBlockingDeque<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
            System.out.println("等待5s put 123");
            queue.put("123");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class TakeRunable implements Runnable {

    private LinkedBlockingDeque<String> queue;

    public TakeRunable(LinkedBlockingDeque<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            System.out.println(queue.take());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
