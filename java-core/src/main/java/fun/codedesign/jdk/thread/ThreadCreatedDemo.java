package fun.codedesign.jdk.thread;

public class ThreadCreatedDemo {
    public static void main(String[] args) {
        Thread t1 = new Person1();
        t1.start();

        Thread t2 = new Thread(new Person2());
        t2.start();

        Thread t3 = new Thread() {
            public void run() {
                System.out.println("111");
            }
        };
        t3.start();

        Thread t4 = new Thread(new Runnable() {
            public void run() {
                System.out.println("22");
            }
        });
        t4.start();
    }
}

class Person1 extends Thread {
    public void run() {
        System.out.println("Hello, person1");
    }
}

class Person2 implements Runnable {
    public void run() {
        System.out.println("Hi，person2");
    }
}