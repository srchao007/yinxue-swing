package fun.codedesign.jdk.thread;

public class ThreadDemo1 {
    public static void main(String[] args) {
        Thread t1 = new Bear1();
        Thread t2 = new Bear2();
        Thread t3 = new Person();
//	    t3.setPriority(Thread.MAX_PRIORITY);
//  	t2.setPriority(Thread.MIN_PRIORITY);
        t2.setDaemon(true);
        t1.start();
        t2.start();
        t3.start();
        System.out.println("main over");
    }
}

class Bear1 extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("bear1");
            Thread.yield();
        }
        System.out.println("######################");
    }
}

class Bear2 extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("bear2");
            Thread.yield();
        }
        System.out.println("--------------------------------");
    }
}

class Person extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("person");
            Thread.yield();
        }
        System.out.println("***************************");
    }
}




