package fun.codedesign.jdk.thread;

public class ThreadSleepDemo {
    public static void main(String[] args) {
        Thread t = new Thread() {
            public void run() {
                long start = System.currentTimeMillis();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                long end = System.currentTimeMillis();
                System.out.println("耗时：" + (end - start));
                System.out.println();
            }
        };
        t.start();
        System.out.println("main结束");
        try {
            Thread.sleep(3000);
            t.interrupt();
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
    }
}

