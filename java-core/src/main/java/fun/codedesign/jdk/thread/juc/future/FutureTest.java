/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: FutureTest
 * Author:   zengjian
 * Date:     2018/9/20 16:04
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.jdk.thread.juc.future;

import java.util.concurrent.*;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/20 16:04
 */
public class FutureTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(1);
        Future<Integer> future = service.submit(() -> {
            throw new RuntimeException("123");
        });
        // 不get不会有异常抛出
        System.out.println(future.get());
    }
}