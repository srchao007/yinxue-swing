package fun.codedesign.jvm;

public class ClassForNameAndClassLoaderLoad {

    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> clz = Class.forName("core.a.basic.StaticClass");
        // ClassLoader.getSystemClassLoader().loadClass("org.javacollection.basic.subject.java.StaticClass");
        System.out.println(clz);
    }
}
