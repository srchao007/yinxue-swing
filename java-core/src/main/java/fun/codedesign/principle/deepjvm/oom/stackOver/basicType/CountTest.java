package fun.codedesign.principle.deepjvm.oom.stackOver.basicType;

public class CountTest {
    public static void main(String[] args) {
        // 虚拟机栈溢出,异常是java.lang.StackOverflowError
        Count count = new Count();
        try {
            count.count();
        } catch (Error e) {
            e.printStackTrace();
            System.out.println("自增次数：" + count.getA());
            //throw e;
        }
    }
}
