package fun.codedesign.principle.deepjvm;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-05 19:33
 * @since 1.0.0
 */
public class TestGC2 {
    public static void main(String[] args) throws InterruptedException {
        User user = new User("my name is zhangsan");
        WeakReference<User> userRef = new WeakReference<>(user);

        Map<String, WeakReference<User>> map = new HashMap<>();
        map.put("hello", userRef);

        System.out.println(map.get("hello").get().getName());
        user = null;
        System.gc();
        // Thread.sleep(5000);
        // 上面的user置空后，取的的弱引用对象也将为null
        System.out.println(map.get("hello"));
    }
}
