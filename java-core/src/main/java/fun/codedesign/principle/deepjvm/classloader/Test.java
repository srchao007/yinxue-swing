package fun.codedesign.principle.deepjvm.classloader;

public class Test {

    @SuppressWarnings("all")
    public static void main(String[] args) {
        // 被动使用字段(本类不初始化)
        // 子类调用父类静态字段，初始化父类，但不初始化子类
        System.out.println(SubClass.a);
        // new数组不触发类的初始化
        SubClass[] subs = new SubClass[3];
        // 引用静态常量类，即final修饰的常量，不初始化
        System.out.println(SubClass.CONSTANT_VALUE);

    }
}
