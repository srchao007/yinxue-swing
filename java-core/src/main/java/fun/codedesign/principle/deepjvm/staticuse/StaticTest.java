package fun.codedesign.principle.deepjvm.staticuse;

public class StaticTest {
    public static int k = 0;
    public static StaticTest t1 = new StaticTest("t1");
    public static StaticTest t2 = new StaticTest("t2");
    public static int n = 99;
    public static int i = print("i");

    static {
        print("静态块");
    }

    public int j = print("j");

    {
        print("构造块"); //第二步，在构造方法后，在this构造方法体执行前
    }

    public StaticTest(String str) {
        System.out.println((++k) + ":" + str + " i=" + i + " n=" + n); // 第一步  第四步
        ++n;
        ++i;
    }

    public static int print(String str) {
        System.out.println((++k) + ":" + str + " i=" + i + " n=" + n); // 第三步
        ++i;
        return ++n;
    }

    public static void main(String[] args) {
        StaticTest t = new StaticTest("init");
        System.out.println(t);
    }
}
