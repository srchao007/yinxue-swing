package fun.codedesign.principle.deepjvm.oom.stackOver.newProject;

public class ProjectTest {
    public static void main(String[] args) {
        Project prj = new Project();
        // Project 加上静态修饰符static，则得到的类是相同的类，否则会不同的类并且造成栈溢出
        System.out.println(prj.getProject());
        System.out.println(prj.getProject().getProject());
        System.out.println(prj.getProject().getProject().getProject());
    }
}
