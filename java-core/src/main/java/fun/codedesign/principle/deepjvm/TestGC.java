package fun.codedesign.principle.deepjvm;

import java.util.HashMap;
import java.util.Map;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-05 19:20
 * @see TestGC2
 * @since 1.0.0
 */
public class TestGC {
    public static void main(String[] args) throws InterruptedException {
        Map<String, User> map = new HashMap<>();
        User user = new User("zhangsan");
        map.put("me", user);
        user = null;
        // user=null了，但map.get出来的还是正确的名称 zhangsan，原因是因为user对象被作为内部的new Entry存储在内部了
        // 所以该对象的引用始终存在
        System.out.println(map.get("me").getName()); // zhangsan
        System.gc();
        Thread.sleep(50000);
        System.out.println(map.get("me").getName());
        System.out.println(user);
        System.out.println(map.get("me"));
    }
}


class User {

    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }
}