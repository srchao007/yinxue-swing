package fun.codedesign.principle.deepjvm.oom.permGemOOM.string;

import java.util.ArrayList;
import java.util.List;

public class RuntimeConstantPoolOOM {
    // 运行时常量池内存溢出  jdk1.6 有效，1.7之后无效
    public static void main(String[] args) {
        int i = 1;
        List<String> list = new ArrayList<>();
        while (true) {
            i++;
            list.add(String.valueOf(i).intern());
            System.out.println(i);
        }
    }

}
