/*
 * Copyright (C), 2002-2018, 苏宁易购电子商务有限公司
 * FileName: Finalized.java
 * Author:   88383079
 * Date:     2018年1月13日 下午3:24:46
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package fun.codedesign.principle.deepjvm.finalized;

/**
 * 〈一句话功能简述〉<br>
 * GC前的最后一次挽救机会，只能执行一次
 *
 * @author 88383079
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class Finalized {
    private static Finalized hook = null;

    @SuppressWarnings("all")
    public static void main(String[] args) throws Throwable {
        hook = new Finalized();
        hook = null;
        System.gc();
        // finalized的执行级别很低，等待0.5s(等finalized)执行
        Thread.sleep(500);
        if (hook != null) {
            System.out.println("我活着，没有被GC回收");
        } else {
            System.out.println("i'm dead");
        }

        // finalized只能被执行一次
        hook = null;
        System.gc();
        Thread.sleep(500);
        if (hook != null) {
            System.out.println("我活着，没有被GC回收");
        } else {
            System.out.println("i'm dead");
        }


    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        hook = this;
        System.out.println("执行finalize方法");
    }
}
