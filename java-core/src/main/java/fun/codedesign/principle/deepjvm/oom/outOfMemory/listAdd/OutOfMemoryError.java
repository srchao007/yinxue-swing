package fun.codedesign.principle.deepjvm.oom.outOfMemory.listAdd;

import java.util.ArrayList;
import java.util.List;

public class OutOfMemoryError {

    // 常见的内存溢出
    public static void main(String[] args) {

        class OOM {

        }

        List<OOM> list = new ArrayList<>();

        try {
            // 不断产生对象，使得堆内存溢出
            while (true) {
                list.add(new OOM());
            }
        } catch (Error e) {
            System.out.println("创建对象：" + list.size());
            throw e;
        }

    }
}
