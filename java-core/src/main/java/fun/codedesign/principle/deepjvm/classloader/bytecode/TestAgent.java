package fun.codedesign.principle.deepjvm.classloader.bytecode;

import java.lang.instrument.Instrumentation;

public class TestAgent {

    public static void agentmain(String args, Instrumentation inst) {
        //指定我们自己定义的Transformer，在其中利用Javassist做字节码替换
        inst.addTransformer(new DemoClassFileTransformer(), true);
        try {
            //重定义类并载入新的字节码
            inst.retransformClasses(ServerMain.class);
            System.out.println("Agent Load Done.");
        } catch (Exception e) {
            System.out.println("agent load failed!");
        }
    }

    // 随着java程序启动，指定-javaagent时默认扫描
    public static void premain(String args, Instrumentation inst) {
        System.out.println("TestAgent#premain(String agentArgs, Instrumentation instrumentation)");
    }
}
