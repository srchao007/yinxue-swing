package fun.codedesign.principle.deepjvm;

public class StringIntern {
    public static void main(String[] args) {

        // 1.6 两个false  1.7 前者true 后者false
        // 常量池中的String引用和堆内的String引用是不同的引用
        // intern 在1.6中是添加到
        String str1 = new StringBuilder("计算机").append("软件").toString();
        String str2 = new StringBuilder("ja").append("va").toString();
        //
        System.out.println(str1.intern() == str1);
        // 常量池中的java引用(默认就有这个string)与stringbuffer建立在堆中的java引用是不一样的
        System.out.println(str2.intern() == str2);
        System.out.println(str1.intern().equals(str1));
        System.out.println(str2.intern().equals(str2));
        // 1.7版本的常量池 intern 将会记录首次出现的String的堆引用地址，== 为true
        // 如果常量池中有该数据，返回的就是常量池中的引用，此时与堆内的必然不同

        String str3 = "123";
        // 添加到常量池了
        System.out.println(str3.intern() == str3);

        // 常量池与堆内分别有一个String对象,str4的地址是堆内的引用
        String str4 = new String("234");
        System.out.println(str4.intern() == str4);
        System.out.println(str4.hashCode());
        System.out.println(str4.intern().hashCode());
        String str5 = new StringBuilder("234").toString();
        System.out.println(str4 == str5);
        // 常量池中返回的地址肯定是相同的
        System.out.println(str4.intern() == str5.intern());
    }
}
