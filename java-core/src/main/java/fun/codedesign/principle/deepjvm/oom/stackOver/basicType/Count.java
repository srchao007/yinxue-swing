package fun.codedesign.principle.deepjvm.oom.stackOver.basicType;

public class Count {

    // 基本类型存储在虚拟机栈内
    private int a = 1;

    // 递归调用
    public void count() {
        a++;
        count();
    }

    public int getA() {
        return a;
    }

}
