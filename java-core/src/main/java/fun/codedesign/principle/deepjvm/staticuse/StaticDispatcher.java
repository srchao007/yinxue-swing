package fun.codedesign.principle.deepjvm.staticuse;

public class StaticDispatcher {

    public static void main(String[] args) {
        Human lady = new Woman();
        Human man = new Man();

        StaticDispatcher sd = new StaticDispatcher();
        // 输出均为human，为静态分派，虚拟机通过编译时期的static Human确定确定重载方法
        sd.sayHello(lady);
        sd.sayHello(man);
    }

    public void sayHello(Man man) {
        System.out.println("hi,man");
    }

    public void sayHello(Woman woman) {
        System.out.println("hi,woman");
    }

    public void sayHello(Human human) {
        System.out.println("hi,human");
    }

    static abstract class Human {
    }

    static class Woman extends Human {
    }

    static class Man extends Human {
    }
}	
