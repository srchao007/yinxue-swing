package fun.codedesign.principle.princple6.a002.liskvo_replace;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.io.Serializable;
import java.util.Date;

/**
 * 里氏替换原则 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 14:05
 * @since 1.0.0
 */
@Notice("基类可以出现的地方，那么子类一定可以出现")
public class LiskovSubstitutionPrinciple {

    @SuppressWarnings("all")
    @Test
    public void test() {
        Base base = new Base();
        // 以上的Base()出现的问题可以替换为子类
        Base base1 = new Req();
    }

}

class Base implements Serializable {
    private static final long serialVersionUID = 4677994749668601455L;
    private long id;
    private Date createTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}

class Req extends Base {

    private String question;
    private String description;
    private String name;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
