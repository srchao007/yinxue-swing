package fun.codedesign.principle.princple6.a001.open_and_close;

import fun.codedesign.principle.advice151.Notice;

/**
 * 抽象出来的接口
 */
interface Hello {
    void hello();
}

/**
 * 开闭原则 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 13:59
 * @since 1.0.0
 */
@Notice("开闭原则:对修改关闭，对扩展开放，意味着需要开放抽象类和接口用于扩展")
public class OpenClosePrincple {


}

/**
 * 以下扩展
 */

class ChineseHello implements Hello {
    @Override
    public void hello() {
        System.out.println("你好！");
    }
}


class EnglishHello implements Hello {
    @Override
    public void hello() {
        System.out.println("Hello!");
    }
}

class KoreaHello implements Hello {

    @Override
    public void hello() {
        System.out.println("simida！");
    }
}
