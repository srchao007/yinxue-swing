package fun.codedesign.principle.princple6.a005.demeter_know_least;

import fun.codedesign.principle.advice151.Notice;

/**
 * 迪米特法则 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 15:30
 * @since 1.0.0
 */
@Notice("最少知道原则，一个实体类对其他的实体类尽量少的知道其信息")
public class DemeterPrinciple {
}
