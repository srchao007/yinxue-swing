package fun.codedesign.principle.princple6.a006.composite_first;

import fun.codedesign.principle.advice151.Notice;

/**
 * 尽量使用组合、聚合的方式，而不采用继承 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 15:31
 * @since 1.0.0
 */
@Notice("合成的原则尽量采用组合和聚合的方式，因为这样，对该类的污染最小")
public class CompositeReusePrinciple {
    // 否则会有很多的无效方法
    /**
     * 比如工具类，采用聚合的方式来调用，比采用继承的方式更好，因为不会出现过多冗余的
     * 方法，并且之后好替换
     */
}
