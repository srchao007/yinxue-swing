package fun.codedesign.principle.princple6.a004.interface_segeration;

import java.util.List;
import java.util.Map;

import fun.codedesign.principle.advice151.Notice;

/**
 * 比如查询接口和更新接口分开
 */
interface Query {

    <T> List<T> queryList(T t);

    <T> T queryOne(String primaryKey);

    <T> List<Map<String, Object>> queryMap4List(T t);

}

interface Update {

    int updateByPrimaryKey(String primaryKey);

    int updateBySelectKey(String selectKey);

    <T> int update(T t);

    <T> int delete(T t);
}

/**
 * 接口隔离原则 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 15:23
 * @since 1.0.0
 */
@Notice("即根据接口功能的不同，尽量的分离开，不要耦合在一起，需要的时候在多接口使用")
public class InterfaceSegregationPrinciple {
}
