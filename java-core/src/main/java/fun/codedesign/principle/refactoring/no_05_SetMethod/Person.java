package fun.codedesign.principle.refactoring.no_05_SetMethod;

public class Person {

    private final String name;

    public Person() {
        name = "123";
    }

    @SuppressWarnings("all")
    public static void main(String[] args) {
        Person p1 = new Person();
        Person p2 = new Person();
    }

    // final类型不能被this标记
//	public void setName(String _name) {
//		name = _name;
//	} 

    public String getName() {
        return name;
    }

}
