package fun.codedesign.principle.refactoring.no_04_Person.version01;

public class Female extends Person {

    @Override
    boolean isMale() {
        return false;
    }

    @Override
    String getCode() {
        return "F";
    }

}
