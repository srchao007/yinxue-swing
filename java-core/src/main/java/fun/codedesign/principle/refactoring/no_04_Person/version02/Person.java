package fun.codedesign.principle.refactoring.no_04_Person.version02;


public class Person {

    private final boolean isMale;
    private final String code;

    protected Person(boolean isMale, String code) {
        this.isMale = isMale;
        this.code = code;
    }

    static Person createMale() {
        return new Person(true, "M");
    }

    static Person createFemale() {
        return new Person(false, "F");
    }

    public static void main(String[] args) {
        Person male = Person.createMale();
        Person female = Person.createFemale();
        System.out.println(male);
        System.out.println(female);
    }

    public String getCode() {
        return code;
    }

    public boolean getIsMale() {
        return isMale;
    }

    @Override
    public String toString() {
        return "Person [isMale=" + isMale + ", code=" + code + "]";
    }
}
