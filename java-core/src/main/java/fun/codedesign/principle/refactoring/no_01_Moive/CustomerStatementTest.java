package fun.codedesign.principle.refactoring.no_01_Moive;

import fun.codedesign.principle.refactoring.no_01_Moive.entity.Customer_rb_01;
import fun.codedesign.principle.refactoring.no_01_Moive.entity.Movie;
import fun.codedesign.principle.refactoring.no_01_Moive.entity.Rental;

public class CustomerStatementTest {
    public static void main(String[] args) {

        Movie m1 = new Movie("美丽人生", Movie.REGULAR);
        Movie m2 = new Movie("天堂电影院", Movie.NEW_RELEASE);
        Movie m3 = new Movie("萌娃满天飞", Movie.CHINDREN);

        Rental r1 = new Rental(m1, 3);
        Rental r2 = new Rental(m2, 4);
        Rental r3 = new Rental(m3, 5);

        Customer_rb_01 custom = new Customer_rb_01("张三");
        custom.addRental(r1);
        custom.addRental(r2);
        custom.addRental(r3);

        String result = custom.statement();
        System.out.println(result);

    }
}
