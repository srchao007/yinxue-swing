package fun.codedesign.principle.refactoring.no_02_Customer;

import java.util.Dictionary;
import java.util.Hashtable;


public class Customer {

    private static Dictionary<String, Customer> instances = new Hashtable<>();
    private final String name;

    private Customer(String name) {
        this.name = name;
    }

    public static Customer getCustomer(String name) {
        return instances.get(name);
    }

    public static void loadCustomers() {
        new Customer("张三").store();
        new Customer("李四").store();
        new Customer("王五").store();
    }

    private void store() {
        instances.put(this.getName(), this);
    }

    public String getName() {
        return name;
    }
}
