package fun.codedesign.principle.refactoring.no_01_Moive.entity;

// 表示一个顾客租了一部影片
public class Rental_rb_04 {

    private Movie movie;
    private int daysRented;

    public Rental_rb_04(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    // 将计算的函数放在数据引用的类中
    public double getCharge() {
        double result = 0;
        switch (movie.getPriceCode()) {
            case Movie.REGULAR:
                result += 2;
                if (getDaysRented() > 2) {
                    result += (getDaysRented() - 2) * 1.5;
                }
                break;
            case Movie.CHINDREN:
                result += 1.5;
                if (getDaysRented() > 3) {
                    result += (getDaysRented() - 3) * 1.5;
                }
                break;
            case Movie.NEW_RELEASE:
                result += getDaysRented() * 3;
                break;
        }
        return result;
    }

}
