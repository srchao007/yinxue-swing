package fun.codedesign.principle.refactoring.no_02_Customer;

public class Order {

    private Customer customer;

    public Order(String name) {
        customer = Customer.getCustomer(name);
    }

    public static void main(String[] args) {
        Customer.loadCustomers();
        Order od1 = new Order("张三");
        Order od2 = new Order("张三");
        // true 说明获得的是一个Customer实例
        System.out.println(od1.getCustomer() == od2.getCustomer());
    }

    public Customer getCustomer() {
        return customer;
    }
}
