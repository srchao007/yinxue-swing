package fun.codedesign.principle.refactoring.no_04_Person.version01;

public abstract class Person {
    abstract boolean isMale();

    abstract String getCode();
}
