package fun.codedesign.principle.refactoring.no_03_Currency;

public class Currency {
    private String code;

    public Currency(String code) {
        this.code = code;
    }

    public static void main(String[] args) {
        System.out.println(new Currency("USD").equals(new Currency("USD")));
        System.out.println(new Currency("USB"));
        System.out.println(new Currency("123"));
        System.out.println(new Currency("123"));

        Integer a = 123;
        System.out.println(a.hashCode());
        System.out.println(a.toString());
        System.out.println(a.toString().getClass());

    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Currency)) {
            return false;
        }
        Currency other = (Currency) obj;
        return code.equals(other.getCode());
    }

    public String getCode() {
        return code;
    }
}	
