package fun.codedesign.principle.refactoring.no_04_Person.version01;

public class Male extends Person {

    @Override
    boolean isMale() {
        return true;
    }

    @Override
    String getCode() {
        return "M";
    }

}
