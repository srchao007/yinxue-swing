package fun.codedesign.principle.refactoring.no_01_Moive.entity;

import java.util.Enumeration;
import java.util.Vector;

/**
 * 〈一句话功能简述〉<br>
 * 顾客，表示名字及拥有相应的数据和访问权限
 *
 * @author 88383079
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class Customer_rb_01 {
    private String name;
    private Vector<Rental> rentals = new Vector<Rental>();

    public Customer_rb_01(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;
        Enumeration<Rental> curRentals = rentals.elements();
        String result = "Rental Record for " + getName() + "\n";
        while (curRentals.hasMoreElements()) {
            double thisAmout = 0;
            Rental each = (Rental) curRentals.nextElement();
            thisAmout = amoutFor(each);

            frequentRenterPoints++;
            totalAmount += thisAmout;
        }

        result += "amout is " + totalAmount + "\n";
        result += "points is " + frequentRenterPoints + "\n";
        return result;
    }

    // 将不变量当作传参，返回变化量
    private double amoutFor(Rental each) {
        double thisAmout = 0;
        switch (each.getMovie().getPriceCode()) {
            case Movie.REGULAR:
                thisAmout += 2;
                if (each.getDaysRented() > 2) {
                    thisAmout += (each.getDaysRented() - 2) * 1.5;
                }
                break;
            case Movie.CHINDREN:
                thisAmout += 1.5;
                if (each.getDaysRented() > 3) {
                    thisAmout += (each.getDaysRented() - 3) * 1.5;
                }
                break;
            case Movie.NEW_RELEASE:
                thisAmout += each.getDaysRented() * 3;
                break;
        }
        return thisAmout;
    }

}
