package fun.codedesign.principle.pattern33.a031.servicelocator;

public class CreatedService implements Service {
    @Override
    public String getName() {
        return "created";
    }

    @Override
    public void execute() {
        System.out.println("创建服务运行中....");
    }
}
