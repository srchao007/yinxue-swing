package fun.codedesign.principle.pattern33.a030.filter;

import java.util.ArrayList;
import java.util.List;

public class FilterChain {

    private static ThreadLocal<List<Filter>> local = new ThreadLocal<>();

    static {
        local.set(new ArrayList<Filter>());
    }

    public void setFilter(Filter filter) {
        local.get().add(filter);
    }

    public List<Filter> getFilters() {
        return local.get();
    }
}
