/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Test
 * Author:   zengjian
 * Date:     2018/7/27 10:47
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a020.observer;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:47
 */
public class Test {
    public static void main(String[] args) {
        Listener listener = new Listener() {
            @Override
            public void receive(String state) {
                System.out.println("监听器收到信息:" + state);
            }
        };
        Listener listener1 = new Listener() {
            @Override
            public void receive(String state) {
                System.out.println("监听器处理信息:" + state);
            }
        };

        Subject subject = new Subject();
        subject.addListener(listener);
        subject.addListener(listener1);

        subject.setState("hello");
    }
}