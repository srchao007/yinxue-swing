package fun.codedesign.principle.pattern33.a032.transferobject;

public class Client {

    public static void main(String[] args) {
        StudentBO bussiness = new StudentBO();
        bussiness.add(new StudentVO("lisa", 2));
        System.out.println(bussiness.getAllStudents());
    }
}
