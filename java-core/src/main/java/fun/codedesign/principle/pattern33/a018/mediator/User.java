/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: User
 * Author:   zengjian
 * Date:     2018/7/27 10:26
 * Description: 用户类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a018.mediator;

/**
 * 〈用户类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:26
 */
public class User {

    private String name;

    public User(String name) {
        this.name = name;
    }

    public void sendMessage(String msg) {
        ChatRoom.showMessage(name, msg);
    }
}