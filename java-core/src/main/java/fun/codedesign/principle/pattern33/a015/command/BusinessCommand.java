/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: BusinessCommand
 * Author:   zengjian
 * Date:     2018/7/27 9:17
 * Description: 业务命令
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a015.command;

/**
 * 〈业务命令〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:17
 */
public class BusinessCommand implements Command {

    private Recevier recevier;

    @Override
    public void execute() {
        recevier.doSomething();
    }
}