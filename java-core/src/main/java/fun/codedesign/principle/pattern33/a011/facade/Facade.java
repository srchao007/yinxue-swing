package fun.codedesign.principle.pattern33.a011.facade;

public class Facade {

    private Handler integerHandler;
    private Handler stringHandler;

    public Facade(Handler integerHandler, Handler stringHandler) {
        this.integerHandler = integerHandler;
        this.stringHandler = stringHandler;
    }

    public void handleString() {
        stringHandler.handle();
    }

    public void handleInteger() {
        integerHandler.handle();
    }
}
