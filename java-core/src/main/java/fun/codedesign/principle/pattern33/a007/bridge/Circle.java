package fun.codedesign.principle.pattern33.a007.bridge;

public abstract class Circle {

    protected Draw draw;

    protected Circle(Draw draw) {
        this.draw = draw;
    }

    protected abstract void drawCircle();

}
