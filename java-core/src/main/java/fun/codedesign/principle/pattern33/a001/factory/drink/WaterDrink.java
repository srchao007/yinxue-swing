package fun.codedesign.principle.pattern33.a001.factory.drink;


public class WaterDrink implements Drink {

    @Override
    public void prices() {
        System.out.println("喝水都塞牙，3块钱一瓶");
    }

}
