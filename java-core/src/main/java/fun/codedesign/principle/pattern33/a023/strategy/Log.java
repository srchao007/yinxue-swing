package fun.codedesign.principle.pattern33.a023.strategy;

public interface Log {
    void printLog(String message);
}
