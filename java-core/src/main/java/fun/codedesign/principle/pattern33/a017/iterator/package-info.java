package fun.codedesign.principle.pattern33.a017.iterator;

// 优点：迭代器不需要暴露聚合对象的内部结构
// 缺点：每增加一个类都需要增加对应的迭代器，类的个数成对增加，增加了系统的复杂性