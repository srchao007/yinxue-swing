package fun.codedesign.principle.pattern33.a012.flyweight;

import org.junit.Test;

public class Fly {
    @Test
    public void testName() throws Exception {
        final Integer a = Integer.valueOf(56);
        final Integer b = Integer.valueOf(56);
        System.out.println(a == b); //true

        final Integer m = Integer.valueOf(129);
        final Integer n = Integer.valueOf(129);
        System.out.println(m == n); //false

        String ss = "123";
        ss.intern();
        String sd = "123";
        System.out.println(ss == sd); //true

        // 返回的是新的String
        final String c = String.valueOf(123);
        final String d = String.valueOf(123);
        System.out.println(c == d); //false
    }
}
