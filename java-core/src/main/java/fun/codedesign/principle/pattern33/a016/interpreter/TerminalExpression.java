/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: TerminalExpression
 * Author:   zengjian
 * Date:     2018/7/27 9:43
 * Description: 终结符
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a016.interpreter;

/**
 * 〈终结符〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:43
 */
public class TerminalExpression implements Expression {

    private String data;

    public TerminalExpression(String data) {
        this.data = data;
    }

    @Override
    public boolean interpreter(String context) {
        return data.contains(context);
    }
}