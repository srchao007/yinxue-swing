/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: BFilter
 * Author:   zengjian
 * Date:     2018/7/19 10:43
 * Description: B过滤器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a014.chain_of_responsibility;

/**
 * 〈B过滤器〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/19 10:43
 */
public class BFilter extends AbstractFilter {

    public BFilter(Filter next) {
        super(next);
    }

    @Override
    public void doFilter(String request) {

    }

    @Override
    public boolean match(String request) {
        return false;
    }
}