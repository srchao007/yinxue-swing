package fun.codedesign.principle.pattern33.a030.filter;

public class Target {

    public void execute(Request request) {
        System.out.println("执行目标方法，得到的request是:" + request.getRequest());
    }
}
