package fun.codedesign.principle.pattern33.a010.decorator;

import org.junit.Test;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Fos {
    public static void main(String[] args) {
        System.out.println(args.toString());
        System.out.println(args);
    }

    @Test
    public void testName() throws Exception {

        // 写入一个文件里
        File file = new File(".", "code.txt");
        FileOutputStream fos = new FileOutputStream(file);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.write(1);
        oos.writeBoolean(true);
        oos.writeObject(new ArrayList<String>());
        oos.flush();
        oos.close();
        bos.close();
        fos.close();
    }
}
