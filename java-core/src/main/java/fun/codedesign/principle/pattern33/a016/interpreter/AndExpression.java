/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: AndExpression
 * Author:   zengjian
 * Date:     2018/7/27 9:45
 * Description: 与运算
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a016.interpreter;

/**
 * 〈与运算〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:45
 */
public class AndExpression implements Expression {

    private Expression expression1;
    private Expression expression2;

    public AndExpression(Expression expression1, Expression expression2) {
        this.expression1 = expression1;
        this.expression2 = expression2;
    }

    @Override
    public boolean interpreter(String context) {
        return expression1.interpreter(context) && expression2.interpreter(context);
    }
}