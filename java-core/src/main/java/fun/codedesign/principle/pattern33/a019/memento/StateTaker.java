/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StateTaker
 * Author:   zengjian
 * Date:     2018/7/27 10:38
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a019.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈状态保存类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:38
 */
public class StateTaker {

    private List<StateWrapper> list = new ArrayList<>();

    public boolean add(StateWrapper state) {
        return list.add(state);
    }

    public StateWrapper roll(int version) {
        return list.get(version);
    }


}