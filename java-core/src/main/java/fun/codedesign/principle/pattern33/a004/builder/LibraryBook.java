package fun.codedesign.principle.pattern33.a004.builder;

enum BookType {
    FICTION,
    OTHER;
}


public class LibraryBook {
    private BookType type;
    private String bookName;

    /**
     * 封装，私有构造
     *
     * @param type
     * @param bookName
     */
    private LibraryBook(BookType type, String bookName) {
        super();
        this.type = type;
        this.bookName = bookName;
    }

    

    /**
     * 用于分散的属性build
     *
     * @param args
     */
    @SuppressWarnings("all")
    public static void main(String[] args) {
        LibraryBook.Builder build = new LibraryBook.Builder();
        // 默认不需要使用withType 来指定书籍类型，默认是小说类型
        LibraryBook book = build.withBookName("war and peace").build();
        Builder builder = new Builder();
        builder.withBookName("hello").withType(BookType.FICTION).build();
    }

    public static class Builder {
        private BookType type;
        private String bookName;

        public Builder withType(BookType type) {
            this.type = type;
            return this;
        }

        public Builder withBookName(String bookName) {
            this.bookName = bookName;
            return this;
        }

        public LibraryBook build() {
            return new LibraryBook(type, bookName);
        }
    }

    public BookType getType() {
        return type;
    }



    public String getBookName() {
        return bookName;
    }
}
