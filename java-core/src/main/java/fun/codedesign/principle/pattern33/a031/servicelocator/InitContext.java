package fun.codedesign.principle.pattern33.a031.servicelocator;

public class InitContext {

    public Object lookup(String jndiname) {
        if (jndiname.equalsIgnoreCase("createdservice")) {
            return new CreatedService();
        } else if (jndiname.equalsIgnoreCase("updateService")) {
            return new UpdateService();
        }
        return null;
    }
}
