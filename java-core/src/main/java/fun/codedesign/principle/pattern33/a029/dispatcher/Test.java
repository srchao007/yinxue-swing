/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Test
 * Author:   zengjian
 * Date:     2018/7/31 14:49
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a029.dispatcher;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/31 14:49
 */
public class Test {
    public static void main(String[] args) {
        FrontController controller = new FrontController();
        //  调度student页面
        controller.dispatchRequest("student");
        //  调度teacher页面
        controller.dispatchRequest("teacher");
        controller.dispatchRequest("");
    }
}