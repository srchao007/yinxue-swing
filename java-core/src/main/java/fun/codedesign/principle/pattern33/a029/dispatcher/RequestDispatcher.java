package fun.codedesign.principle.pattern33.a029.dispatcher;

public class RequestDispatcher {
    private View studentView;
    private View teacherView;

    public RequestDispatcher() {
        this.studentView = new StudentView();
        this.teacherView = new TeacherView();
    }

    public void dispatch(String request) {
        System.out.println("开始调度");
        if ("STUDENT".equalsIgnoreCase(request)) {
            studentView.show();
        }
        if ("Teacher".equalsIgnoreCase(request)) {
            teacherView.show();
        }
        System.out.println("调度执行ok");
    }
}
