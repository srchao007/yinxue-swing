package fun.codedesign.principle.pattern33.a001.factory;

import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;

public class ReflectNoodlesFactory {

    @SuppressWarnings("all")
    public static <T extends Noodles> T getNoodles(Class<T> clz) {
        T result = null;
        try {

            result = (T) Class.forName(clz.getName()).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
