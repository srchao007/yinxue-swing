package fun.codedesign.principle.pattern33.a001.factory;

import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;

public interface NoodlesFactory {
    Noodles getNoodles();
}
