/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Test
 * Author:   zengjian
 * Date:     2018/7/27 9:48
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a016.interpreter;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:48
 */
public class Test {
    public static void main(String[] args) {
        Expression expression1 = new TerminalExpression("hello");
        Expression expression2 = new TerminalExpression("world");
        Expression expression3 = new AndExpression(expression1, expression2);
        boolean result = expression3.interpreter("o");
        System.out.println(result);
    }
}