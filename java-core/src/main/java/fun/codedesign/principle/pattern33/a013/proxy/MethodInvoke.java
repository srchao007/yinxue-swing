package fun.codedesign.principle.pattern33.a013.proxy;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class MethodInvoke implements MethodInterceptor {

    @Override
    public Object intercept(Object arg0, Method arg1, Object[] arg2,
                            MethodProxy arg3) throws Throwable {
        System.out.println(arg1.getName());
        // 此处执行父类的方法invokeSuper
        Object obj = arg3.invokeSuper(arg0, arg2);
        System.out.println(arg3.getSuperName());
        return obj;
    }
}
