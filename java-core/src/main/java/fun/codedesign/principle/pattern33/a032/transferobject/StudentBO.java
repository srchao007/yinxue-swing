package fun.codedesign.principle.pattern33.a032.transferobject;

import java.util.ArrayList;
import java.util.List;

public class StudentBO {
    private List<StudentVO> list;

    public StudentBO() {
        list = new ArrayList<>();
        list.add(new StudentVO("jack", 0));
        list.add(new StudentVO("kitty", 1));
    }

    public List<StudentVO> getAllStudents() {
        return list;
    }

    public void remove(int index) {
        StudentVO studentVo = list.remove(index);
        System.out.println(studentVo.getName() + "被移除");
    }

    public void add(StudentVO student) {
        list.add(student);
        System.out.println(student.getName() + "被添加");
    }

    public void update(StudentVO student) {
        list.get(student.getIndex()).setName(student.getName());
        System.out.println(student.getIndex() + "号同学被修改名称");
    }
}
