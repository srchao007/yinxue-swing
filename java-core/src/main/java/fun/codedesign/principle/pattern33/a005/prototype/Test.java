package fun.codedesign.principle.pattern33.a005.prototype;

public class Test {
    public static void main(String[] args) {
        Product pro = new Product();
        pro.setName("123");
        pro.setAgeName(new Name());
        Product pro2 = (Product) pro.clone();
        System.out.println(pro == pro2);
        pro2.setName("321");
        pro.setName("abc");
        System.out.println(pro.getName());
        System.out.println(pro2.getName());
        System.out.println("----");
        pro.setAgeName(new Name());
        System.out.println(pro.getAgeName().hashCode());
        System.out.println(pro2.getAgeName().hashCode());

    }
}
