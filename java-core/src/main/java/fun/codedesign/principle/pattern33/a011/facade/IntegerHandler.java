/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: IntegerHandler
 * Author:   zengjian
 * Date:     2018/7/18 16:41
 * Description: 整型处理类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a011.facade;

/**
 * 〈整型处理类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/18 16:41
 */
public class IntegerHandler implements Handler {
    @Override
    public void handle() {
        System.out.println("12344正在被处理");
    }
}