package fun.codedesign.principle.pattern33.a023.strategy;

// 策略模式最主要的特征就是，就是实现了同样的接口，但是逻辑不同，可以进行替换
// 一般可以作为传参或者是构造器入参