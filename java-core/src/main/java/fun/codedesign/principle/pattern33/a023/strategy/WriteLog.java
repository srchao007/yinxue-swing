package fun.codedesign.principle.pattern33.a023.strategy;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteLog implements Log {
    @Override
    public void printLog(String message) {
        try {
            /*
             * 路径说明，如果是  path是根路径则根据系统转换，如果是相对路径是就从项目工程目录family-admin-web往下
             * . 当前目录，..当前目录往上
             */
            FileOutputStream fos = new FileOutputStream("../file4.txt");
            char[] array = message.toCharArray();
            for (int i = 0; i < array.length; i++) {
                try {
                    fos.write(array[i]);
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
