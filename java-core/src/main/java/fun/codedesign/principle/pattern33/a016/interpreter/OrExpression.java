/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: OrExpression
 * Author:   zengjian
 * Date:     2018/7/27 9:46
 * Description: 或运算解释器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a016.interpreter;

/**
 * 〈或运算解释器〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:46
 */
public class OrExpression implements Expression {

    private Expression expression1;
    private Expression expression2;

    @Override
    public boolean interpreter(String context) {
        return expression1.interpreter(context) || expression2.interpreter(context);
    }
}