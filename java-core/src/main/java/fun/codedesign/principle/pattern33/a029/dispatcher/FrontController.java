package fun.codedesign.principle.pattern33.a029.dispatcher;

public class FrontController {
    private RequestDispatcher dispatcher;

    public FrontController() {
        this.dispatcher = new RequestDispatcher();
    }

    private void trackRequet(String request) {
        System.out.println("记录每一次请求");
    }

    private boolean isAuthentic(String request) {
        return true;
    }

    public void dispatchRequest(String request) {
        trackRequet(request);
        if (isAuthentic(request)) {
            dispatcher.dispatch(request);
        }
    }
}
