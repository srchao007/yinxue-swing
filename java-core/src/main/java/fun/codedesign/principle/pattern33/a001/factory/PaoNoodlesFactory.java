package fun.codedesign.principle.pattern33.a001.factory;

import fun.codedesign.principle.pattern33.a001.factory.noodles.GanKouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;

public class PaoNoodlesFactory implements NoodlesFactory {

    @Override
    public Noodles getNoodles() {
        return new GanKouNoodles();
    }

}
