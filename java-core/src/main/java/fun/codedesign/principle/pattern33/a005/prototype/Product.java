package fun.codedesign.principle.pattern33.a005.prototype;

public class Product implements Cloneable {

    private String id;
    private String name;
    private Name ageName;

    public Name getAgeName() {
        return ageName;
    }

    public void setAgeName(Name ageName) {
        this.ageName = ageName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public Object clone() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
}
