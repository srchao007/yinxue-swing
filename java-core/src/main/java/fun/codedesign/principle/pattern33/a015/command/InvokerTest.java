/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: InvokerTest
 * Author:   zengjian
 * Date:     2018/7/27 9:19
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a015.command;

/**
 * 〈调用者逻辑〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:19
 */
public class InvokerTest {
    public static void main(String[] args) {
        // 回调
        Invoker invoker = new Invoker() {
            @Override
            public void action(Command command) {
                command.execute();
            }
        };

        final Recevier recevier = new Recevier() {
            @Override
            public void doSomething() {
                System.out.println("i want to do something");
            }
        };


        Command command = new Command() {
            @Override
            public void execute() {
                recevier.doSomething();
            }
        };

        invoker.action(command);
    }
}