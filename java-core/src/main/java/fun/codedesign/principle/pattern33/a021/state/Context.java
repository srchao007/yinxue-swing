/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Context
 * Author:   zengjian
 * Date:     2018/7/27 11:51
 * Description: 上下文
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a021.state;

/**
 * 〈上下文〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 11:51
 */
public class Context {

    private State state;

    public Context() {
    }

    public Context(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}