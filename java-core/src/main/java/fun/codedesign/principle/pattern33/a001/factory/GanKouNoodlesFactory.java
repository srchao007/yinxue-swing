package fun.codedesign.principle.pattern33.a001.factory;

import fun.codedesign.principle.pattern33.a001.factory.noodles.GanKouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 16:20
 * @since 1.0.0
 */
public class GanKouNoodlesFactory implements NoodlesFactory {


    @Override
    public Noodles getNoodles() {
        return new GanKouNoodles();
    }
}



