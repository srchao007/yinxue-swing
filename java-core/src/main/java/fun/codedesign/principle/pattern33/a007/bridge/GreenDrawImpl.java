package fun.codedesign.principle.pattern33.a007.bridge;

public class GreenDrawImpl implements Draw {

    @Override
    public void draw() {
        System.out.println("green circle");
    }
}
