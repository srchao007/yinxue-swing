/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Filter
 * Author:   zengjian
 * Date:     2018/7/18 16:50
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a014.chain_of_responsibility;

/**
 * 〈过滤器接口〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/18 16:50
 */
public interface Filter {

    void doFilter(String request);

    boolean match(String request);

    Filter next();

}