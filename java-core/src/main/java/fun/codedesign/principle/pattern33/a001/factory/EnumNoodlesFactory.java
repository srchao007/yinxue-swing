package fun.codedesign.principle.pattern33.a001.factory;

import fun.codedesign.principle.pattern33.a001.factory.noodles.GanKouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.LanZhouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.PaoNoodles;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 16:50
 * @since 1.0.0
 */
public enum EnumNoodlesFactory {
    GANKOU {
        @Override
        Noodles getNoodles() {
            return new GanKouNoodles();
        }
    }, LANZHOU {
        @Override
        Noodles getNoodles() {
            return new LanZhouNoodles();
        }
    }, PAO {
        @Override
        Noodles getNoodles() {
            return new PaoNoodles();
        }
    };

    abstract Noodles getNoodles();
}
