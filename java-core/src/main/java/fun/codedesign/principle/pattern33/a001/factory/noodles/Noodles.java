package fun.codedesign.principle.pattern33.a001.factory.noodles;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 16:09
 * @since 1.0.0
 */
public interface Noodles {
    // 面条描述方法
    void desc();
}

