/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Test
 * Author:   zengjian
 * Date:     2018/7/19 10:46
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a014.chain_of_responsibility;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/19 10:46
 */
public class Test {

    public static void main(String[] args) {
        // 组建过滤链
        Filter c = new CFilter(null);
        Filter b = new BFilter(c);
        Filter a = new AFilter(b);
        Chain chain = new Chain(a);

        // 将请求发送到过滤链中
        String request = "请求";
        chain.doFilter(request);

    }

}