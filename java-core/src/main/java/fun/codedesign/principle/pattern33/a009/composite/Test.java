package fun.codedesign.principle.pattern33.a009.composite;

public class Test {
    public static void main(String[] args) {
        Employee e1 = new Employee("张三", "技术部", 123);
        Employee e2 = new Employee("张四", "技术部", 123);
        Employee e3 = new Employee("李五", "技术部", 123);
        e1.add(e2);
        e1.add(e3);
        System.out.println(e1);
    }
}
