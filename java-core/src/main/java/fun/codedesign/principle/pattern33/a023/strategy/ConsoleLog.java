package fun.codedesign.principle.pattern33.a023.strategy;

public class ConsoleLog implements Log {
    @Override
    public void printLog(String message) {
        System.out.println(message);
    }
}
