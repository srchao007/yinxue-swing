package fun.codedesign.principle.pattern33.a007.bridge;

public class Test {
    public static void main(String[] args) {
        // 行为与名词类拆开
        Draw draw = new RedDrawImpl();
        Circle circle = new CircleImpl(draw, 1, 2, "red circle");
        circle.drawCircle();
        Circle circle2 = new CircleImpl(new GreenDrawImpl(), 1, 2, "green circle");
        circle2.drawCircle();
    }
}
