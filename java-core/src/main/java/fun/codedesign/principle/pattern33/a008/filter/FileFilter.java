/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: FileFilter
 * Author:   zengjian
 * Date:     2018/7/18 10:54
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a008.filter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author zengjian
 * @create 2018/7/18
 * @since 1.0.0
 */
public class FileFilter {

    public static void main(String[] args) {

        File file = new File(".");
        File[] files = file.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".md");
            }
        });
        System.out.println(Arrays.toString(files));
    }
}