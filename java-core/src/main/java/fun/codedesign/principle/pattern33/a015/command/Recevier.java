/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: BBBRecevier
 * Author:   zengjian
 * Date:     2018/7/27 9:15
 * Description: BBB接收者实现类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a015.command;

/**
 * 〈BBB接收者实现类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:15
 */
public interface Recevier {
    void doSomething();
}