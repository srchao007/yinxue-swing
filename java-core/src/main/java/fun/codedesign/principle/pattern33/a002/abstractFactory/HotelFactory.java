package fun.codedesign.principle.pattern33.a002.abstractFactory;

import fun.codedesign.principle.pattern33.a001.factory.drink.BearDrink;
import fun.codedesign.principle.pattern33.a001.factory.drink.Drink;
import fun.codedesign.principle.pattern33.a001.factory.noodles.LanZhouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;

// 具体工厂实现类-酒店
public class HotelFactory implements AbstractFoodFactory {

    @Override
    public Noodles getNoodles() {
        return new LanZhouNoodles();
    }

    @Override
    public Drink getDrinks() {
        return new BearDrink();
    }
}
