package fun.codedesign.principle.pattern33.a031.servicelocator;

public class UpdateService implements Service {
    @Override
    public String getName() {
        return "update";
    }

    @Override
    public void execute() {
        System.out.println("调用升级服务");
    }
}
