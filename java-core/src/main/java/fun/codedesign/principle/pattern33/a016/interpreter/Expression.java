/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Expression
 * Author:   zengjian
 * Date:     2018/7/27 9:41
 * Description: 解释器接口，所有终结符及非终结符都需要遵循它
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a016.interpreter;

/**
 * 〈解释器接口，所有终结符及非终结符都需要实现它〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:41
 */
public interface Expression {

    boolean interpreter(String context);

}