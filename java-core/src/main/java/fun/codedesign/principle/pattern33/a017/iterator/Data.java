/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Data
 * Author:   zengjian
 * Date:     2018/7/27 10:14
 * Description: 数据结构，采用迭代器进行遍历
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a017.iterator;

import java.util.Iterator;
import java.util.List;

/**
 * 〈数据结构，采用迭代器进行遍历〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:14
 */
public class Data {

    private List<String> list;

    public Data(List<String> list) {
        this.list = list;
    }

    public Iterator<String> iterator() {
        return new DataIterator();
    }

    class DataIterator implements Iterator<String> {

        volatile int index;

        @Override
        public boolean hasNext() {
            return index >= 0 && index < list.size();
        }

        @Override
        public String next() {
            return list.get(index++);
        }

        @Override
        public void remove() {

        }
    }


}