package fun.codedesign.principle.pattern33.a029.dispatcher;

public class StudentView implements View {
    @Override
    public void show() {
        System.out.println("Hello, this is person view");
    }
}
