package fun.codedesign.principle.pattern33.a030.filter;

public class Request {
    private String request = "hello";

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
