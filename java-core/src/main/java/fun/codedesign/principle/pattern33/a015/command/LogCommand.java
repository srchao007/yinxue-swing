/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: LogCommand
 * Author:   zengjian
 * Date:     2018/7/27 9:16
 * Description: 打印日志的指令
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a015.command;

/**
 * 〈打印日志的指令〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:16
 */
public class LogCommand implements Command {

    private Recevier recevier;

    public LogCommand(Recevier recevier) {
        this.recevier = recevier;
    }

    @Override
    public void execute() {
        recevier.doSomething();
    }
}