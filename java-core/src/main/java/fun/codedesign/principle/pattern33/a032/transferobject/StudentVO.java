package fun.codedesign.principle.pattern33.a032.transferobject;

import java.io.Serializable;

public class StudentVO implements Serializable {

    private static final long serialVersionUID = 1649744574202221390L;
    private String name;
    private int index;

    public StudentVO(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
