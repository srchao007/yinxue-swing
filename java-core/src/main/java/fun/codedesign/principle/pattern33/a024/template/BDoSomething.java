/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: BDoSomething
 * Author:   zengjian
 * Date:     2018/7/30 17:17
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a024.template;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/30 17:17
 */
public class BDoSomething extends AbstractDoSomething {

    @Override
    public void doPrivateThing() {
        System.out.println("do something about B");
    }
}