package fun.codedesign.principle.pattern33.a011.facade;

/**
 * 门面模式，用来屏蔽系统复杂的内部细节
 * 但是不利于扩展，不符合开闭原则
 */