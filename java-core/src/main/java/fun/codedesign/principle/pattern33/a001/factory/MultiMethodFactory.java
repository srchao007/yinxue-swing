package fun.codedesign.principle.pattern33.a001.factory;

import fun.codedesign.principle.pattern33.a001.factory.noodles.GanKouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.LanZhouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.PaoNoodles;

public class MultiMethodFactory {

    public static Noodles getLanZhouNoodles() {
        return new LanZhouNoodles();
    }

    public static Noodles getGanKouNoodles() {
        return new GanKouNoodles();
    }

    public static Noodles getPaoNoodles() {
        return new PaoNoodles();
    }

}
