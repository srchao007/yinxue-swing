package fun.codedesign.principle.pattern33.a007.bridge;

public interface Draw {
    void draw();
}
