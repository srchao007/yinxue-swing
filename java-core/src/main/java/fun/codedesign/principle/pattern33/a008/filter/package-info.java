package fun.codedesign.principle.pattern33.a008.filter;

// 过滤器模式，主要用来过滤出自己定义的符合要求的对象，比如File.listFile(filter) 的操作方法就是利用该模式