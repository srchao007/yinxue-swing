/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: AbstractFilter
 * Author:   zengjian
 * Date:     2018/7/18 16:53
 * Description: 过滤器公共类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a014.chain_of_responsibility;

/**
 * 〈过滤器公共类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/18 16:53
 */
public abstract class AbstractFilter implements Filter {

    private Filter next;

    public AbstractFilter(Filter next) {
        this.next = next;
    }

    @Override
    public Filter next() {
        return next;
    }
}