package fun.codedesign.principle.pattern33.a001.factory;

import fun.codedesign.principle.pattern33.a001.factory.noodles.GanKouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.LanZhouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.PaoNoodles;

public class SimpleNoodlesFactory {

    public static final int LAN_ZHOU_NOODLES = 1;
    public static final int PAO_NOODLES = 2;
    public static final int GAN_KOU_NOODLES = 3;

    public static Noodles getNoodles(int NoodlesType) {
        switch (NoodlesType) {
            case LAN_ZHOU_NOODLES:
                return new LanZhouNoodles();
            case PAO_NOODLES:
                return new PaoNoodles();
            case GAN_KOU_NOODLES:
                return new GanKouNoodles();
            default:
                return null;
        }
    }
}
