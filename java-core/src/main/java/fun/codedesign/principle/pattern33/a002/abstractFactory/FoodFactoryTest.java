package fun.codedesign.principle.pattern33.a002.abstractFactory;

public class FoodFactoryTest {
    public static void main(String[] args) {
        AbstractFoodFactory factory = new HotelFactory();
        factory = getFactory(KFCFactory.class);
        factory.getDrinks().prices();
        factory.getNoodles().desc();
    }

    @SuppressWarnings("all")
    public static <T extends AbstractFoodFactory> T getFactory(Class<T> clz) {
        T result = null;
        try {
            result = (T) Class.forName(clz.getName()).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
