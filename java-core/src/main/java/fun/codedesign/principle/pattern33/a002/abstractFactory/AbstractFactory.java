package fun.codedesign.principle.pattern33.a002.abstractFactory;

/**
 * 抽象工厂
 *
 * @author zengjian
 * @create 2018-03-28 16:42
 * @since 1.0.0
 */
public class AbstractFactory {

    interface Factory {
        Editor createEditor();

        PictureView createPictureView();

        VideoPlayer createVideoPlayer();
    }

    public static class Software {
    }

    public static class Editor extends Software {
    }

    public static class PictureView extends Software {
    }

    public static class VideoPlayer extends Software {
    }

    public class AndroidFactory implements Factory {

        @Override
        public Editor createEditor() {
            return null;
        }

        @Override
        public PictureView createPictureView() {
            return null;
        }

        @Override
        public VideoPlayer createVideoPlayer() {
            return null;
        }
    }

    public class WindowsFactory implements Factory {

        @Override
        public Editor createEditor() {
            return null;
        }

        @Override
        public PictureView createPictureView() {
            return null;
        }

        @Override
        public VideoPlayer createVideoPlayer() {
            return null;
        }
    }

    public class LinuxFactory implements Factory {

        @Override
        public Editor createEditor() {
            return null;
        }

        @Override
        public PictureView createPictureView() {
            return null;
        }

        @Override
        public VideoPlayer createVideoPlayer() {
            return null;
        }
    }
}
