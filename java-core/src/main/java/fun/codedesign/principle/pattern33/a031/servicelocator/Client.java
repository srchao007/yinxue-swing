package fun.codedesign.principle.pattern33.a031.servicelocator;

public class Client {
    public static void main(String[] args) {
        ServiceLocator locator = new ServiceLocator();
        locator.getService("createdservice").execute();
    }
}
