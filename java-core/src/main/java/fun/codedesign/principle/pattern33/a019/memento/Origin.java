/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Origin
 * Author:   zengjian
 * Date:     2018/7/27 10:37
 * Description: 原始类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a019.memento;

/**
 * 〈原始类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:37
 */
public class Origin {

    private StateTaker taker;
    private Object state;

    public Origin(StateTaker taker, Object state) {
        this.taker = taker;
        this.state = state;
    }

    public Origin(Object state) {
        this.state = state;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public boolean saveState() {
        return taker.add(new StateWrapper(state));
    }

    public void recoverState(int version) {
        this.state = taker.roll(version).getState();
    }

    @Override
    public String toString() {
        return "Origin{" +
                "state=" + state +
                '}';
    }
}