/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Vistor
 * Author:   zengjian
 * Date:     2018/7/30 16:58
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a025.visitor;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/30 16:58
 */
public interface Vistor {

    void handle(ASubject aSubject);

    void handle(BSubject bSubject);

    void handle(CSubject cSubject);

    void handle(DSubject dSubject);

}