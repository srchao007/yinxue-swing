package fun.codedesign.principle.pattern33.a001.factory.drink;


public class BearDrink implements Drink {

    @Override
    public void prices() {
        System.out.println("啤酒，8块钱一瓶");
    }

}
