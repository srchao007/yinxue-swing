package fun.codedesign.principle.pattern33.a004.builder;

/**
 * 建造者模式
 * 优点：便于控制细节
 * 缺点: 内部类关联，类膨胀
 * 场景: 比如KFC用不同的食品组合出不同的套餐
 */

