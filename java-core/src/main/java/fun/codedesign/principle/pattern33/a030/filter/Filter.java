package fun.codedesign.principle.pattern33.a030.filter;

public interface Filter {
    void filterExecute(Request request);
}
