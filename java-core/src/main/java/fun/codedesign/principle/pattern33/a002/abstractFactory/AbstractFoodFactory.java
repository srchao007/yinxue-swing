package fun.codedesign.principle.pattern33.a002.abstractFactory;

import fun.codedesign.principle.pattern33.a001.factory.drink.Drink;
import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 16:29
 * @since 1.0.0
 */
public interface AbstractFoodFactory {
    Noodles getNoodles();

    Drink getDrinks();
}
