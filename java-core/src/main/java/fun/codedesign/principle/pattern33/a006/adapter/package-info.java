package fun.codedesign.principle.pattern33.a006.adapter;

/**
 * 适配模式的作用，即通过组合的方式，将原来的老接口来适配新的接口
 * 过多的使用，会使得系统凌乱，此时建议重构
 */