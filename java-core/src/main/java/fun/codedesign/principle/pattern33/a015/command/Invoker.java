/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Invoker
 * Author:   zengjian
 * Date:     2018/7/27 9:11
 * Description: 实际的调用者
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a015.command;

/**
 * 〈实际的调用者〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:11
 */
public interface Invoker {

    // 此处也可以采用回调的方式来执行，直接用匿名实现的command进行业务执行
    void action(Command command);

}