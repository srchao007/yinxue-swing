package fun.codedesign.principle.pattern33.a005.prototype;


/**
 * 原型模式
 * 1. 用于取得与原来不同的实例，解耦实例关系
 * 2. 屏蔽构造器构造细节
 */
