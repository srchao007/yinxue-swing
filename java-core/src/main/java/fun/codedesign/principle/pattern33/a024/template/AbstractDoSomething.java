/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: AbstractDoSomething
 * Author:   zengjian
 * Date:     2018/7/30 17:13
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a024.template;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/30 17:13
 */
public abstract class AbstractDoSomething implements DoSomething {


    @Override
    public void doSomething() {
        doCommonThing();
        doPrivateThing();
    }

    @Override
    public void doCommonThing() {
        System.out.println("do common things first");
    }

}