package fun.codedesign.principle.pattern33.a023.strategy;

import org.junit.Test;


public class Client {

    @Test
    public void test() {
        Log log = new ConsoleLog();
        log.printLog("123");
    }

    @Test
    public void test01() throws Exception {
        Log log = new WriteLog();
        log.printLog("123 message");
    }
}
