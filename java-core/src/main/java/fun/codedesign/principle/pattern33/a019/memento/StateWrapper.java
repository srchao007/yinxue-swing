/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StateWrapper
 * Author:   zengjian
 * Date:     2018/7/27 10:36
 * Description: 状态的包装类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a019.memento;

/**
 * 〈状态的包装类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:36
 */
public class StateWrapper {
    private Object state;

    public StateWrapper(Object state) {
        this.state = state;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }
}