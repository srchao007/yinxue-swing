/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Handler
 * Author:   zengjian
 * Date:     2018/7/18 16:39
 * Description: 内部处理接口
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a011.facade;

/**
 * 〈内部处理接口〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/18 16:39
 */
public interface Handler {
    void handle();
}