package fun.codedesign.principle.pattern33.a029.dispatcher;

public interface View {
    void show();
}
