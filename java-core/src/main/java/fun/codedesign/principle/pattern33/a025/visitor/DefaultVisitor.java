/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: DefaultVisitor
 * Author:   zengjian
 * Date:     2018/7/30 17:01
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a025.visitor;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/30 17:01
 */
public class DefaultVisitor implements Vistor {

    @Override
    public void handle(ASubject aSubject) {
        System.out.println("handle aaa");
    }

    @Override
    public void handle(BSubject bSubject) {
        System.out.println("handle bbb");
    }

    @Override
    public void handle(CSubject cSubject) {
        System.out.println("handle ccc");
    }

    @Override
    public void handle(DSubject dSubject) {
        System.out.println("handle ddd");
    }
}