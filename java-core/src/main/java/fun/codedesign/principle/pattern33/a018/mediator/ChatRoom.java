/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: ChatRoom
 * Author:   zengjian
 * Date:     2018/7/27 10:28
 * Description: 中介者
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a018.mediator;

/**
 * 〈中介者〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:28
 */
public class ChatRoom {

    public static void showMessage(String sender, String msg) {
        System.out.println(sender + " say:\"" + msg + "\"");
    }
}