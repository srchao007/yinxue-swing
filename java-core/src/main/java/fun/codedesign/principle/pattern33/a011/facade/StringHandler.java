/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StringHandler
 * Author:   zengjian
 * Date:     2018/7/18 16:40
 * Description: 字符串处理类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a011.facade;

/**
 * 〈字符串处理类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/18 16:40
 */
public class StringHandler implements Handler {

    @Override
    public void handle() {
        System.out.println("字符串处理");
    }
}