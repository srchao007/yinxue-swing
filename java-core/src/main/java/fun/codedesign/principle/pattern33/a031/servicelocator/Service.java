package fun.codedesign.principle.pattern33.a031.servicelocator;

public interface Service {
    String getName();

    void execute();
}
