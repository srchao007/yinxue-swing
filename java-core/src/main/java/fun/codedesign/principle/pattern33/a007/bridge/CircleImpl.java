package fun.codedesign.principle.pattern33.a007.bridge;

public class CircleImpl extends Circle {

    private int x;
    private int y;
    private String desc;

    public CircleImpl(Draw draw, int x, int y, String desc) {
        super(draw);
        this.x = x;
        this.y = y;
        this.desc = desc;
    }

    @Override
    protected void drawCircle() {
        draw.draw();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
