package fun.codedesign.principle.pattern33.a031.servicelocator;

public class ServiceLocator {

    private Cache cache;

    public ServiceLocator() {
        cache = new Cache();
    }

    public Service getService(String jndiName) {
        Service service = cache.getService(jndiName);
        if (null != service) {
            return service;
        }
        InitContext context = new InitContext();
        service = (Service) context.lookup(jndiName);
        cache.addService(service);
        return service;
    }
}
