/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Test
 * Author:   zengjian
 * Date:     2018/7/27 10:43
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a019.memento;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:43
 */
public class Test {

    public static void main(String[] args) {
        StateTaker taker = new StateTaker();
        Origin origin = new Origin(taker, "hello");
        origin.saveState();
        origin.setState("lalaalalalala");
        System.out.println(origin.toString());
        origin.recoverState(0);
        System.out.println(origin.toString());
    }

}