/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: AFilter
 * Author:   zengjian
 * Date:     2018/7/19 10:43
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a014.chain_of_responsibility;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/19 10:43
 */
public class AFilter extends AbstractFilter {

    public AFilter(Filter next) {
        super(next);
    }

    @Override
    public void doFilter(String request) {

    }

    @Override
    public boolean match(String request) {
        return false;
    }

}