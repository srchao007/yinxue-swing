package fun.codedesign.principle.pattern33.a030.filter;

import java.util.List;

public class FilterManager {

    private FilterChain chain;
    private Target target;

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public FilterChain getChain() {
        return chain;
    }

    public void setChain(FilterChain chain) {
        this.chain = chain;
    }

    public void execute(Request request) {
        List<Filter> list = chain.getFilters();
        for (Filter filter : list) {
            filter.filterExecute(request);
        }
        target.execute(request);
    }
}
