package fun.codedesign.principle.pattern33.a018.mediator;

// 中介者模式，就是大家都去找中介，而不需要进行类与类之间的交互，两个类尽可能知道的少，迪米特法则

// 缺点：中介者容易变得庞大，不容易控制，建议将相似或者相同逻辑类型的归纳在其中