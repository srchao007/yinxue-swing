package fun.codedesign.principle.pattern33.a007.bridge;

public class RedDrawImpl implements Draw {

    @Override
    public void draw() {
        System.out.println("red circle");
    }
}
