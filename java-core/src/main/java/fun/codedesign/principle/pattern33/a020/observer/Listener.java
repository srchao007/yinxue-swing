/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Listener
 * Author:   zengjian
 * Date:     2018/7/27 10:46
 * Description: 监听器，即观察者
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a020.observer;

/**
 * 〈监听器，即观察者〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:46
 */
public interface Listener {

    void receive(String state);
}