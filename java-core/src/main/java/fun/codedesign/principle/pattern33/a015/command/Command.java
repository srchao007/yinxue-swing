/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Command
 * Author:   zengjian
 * Date:     2018/7/27 9:09
 * Description: 命令的请求类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a015.command;

/**
 * 〈命令的请求类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 9:09
 */
public interface Command {

    // 将实际的执行者注入到Command当中，然后委托执行
    // 也可以作为传参进行回调执行

    void execute();

}