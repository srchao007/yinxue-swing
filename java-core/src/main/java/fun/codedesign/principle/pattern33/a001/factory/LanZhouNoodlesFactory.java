package fun.codedesign.principle.pattern33.a001.factory;

import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.PaoNoodles;

public class LanZhouNoodlesFactory implements NoodlesFactory {

    @Override
    public Noodles getNoodles() {
        return new PaoNoodles();
    }

}
