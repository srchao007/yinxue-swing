package fun.codedesign.principle.pattern33.a016.interpreter;


// 用来构件语法树的设计模式
// 提供一个语法接口，定义终结符与非终结符
// 扩展只需要扩展非终结符即可
// 可以用来编写代码解析
// 复杂的文法比较难维护，建议用替代expression4J