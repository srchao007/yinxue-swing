/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Test
 * Author:   zengjian
 * Date:     2018/7/27 11:54
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a021.state;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 11:54
 */
public class Test {

    public static void main(String[] args) {
        // 通过状态来调用个上下文进行逻辑

        Context context = new Context();

        State start = new StartState();
        start.doAction(context);

        State stop = new StopState();
        stop.doAction(context);
    }
}