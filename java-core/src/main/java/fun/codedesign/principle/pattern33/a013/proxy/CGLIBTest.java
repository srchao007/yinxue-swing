package fun.codedesign.principle.pattern33.a013.proxy;

import net.sf.cglib.proxy.Enhancer;

public class CGLIBTest {
    public static void main(String[] args) {
        Enhancer en = new Enhancer();
        en.setSuperclass(Hello.class);
        en.setCallback(new MethodInvoke());
        // 创建代理类
        Hello hello = (Hello) en.create();
        hello.hello();
    }
}
