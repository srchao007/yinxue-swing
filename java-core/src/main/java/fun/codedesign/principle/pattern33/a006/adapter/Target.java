/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Target
 * Author:   zengjian
 * Date:     2018/7/18 10:38
 * Description: 需要适配的目标类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a006.adapter;

/**
 * 〈一句话功能简述〉<br>
 * 〈需要适配的目标类〉
 *
 * @author zengjian
 * @create 2018/7/18
 * @since 1.0.0
 */
public class Target implements ITarget {

    @Override
    public void printMessage(String msg) {
        System.out.println(msg);
    }
}