package fun.codedesign.principle.pattern33.a009.composite;

import java.util.ArrayList;
import java.util.List;

public class Employee {
    String name;
    String dept;
    double salary;
    List<Employee> list = new ArrayList<>();

    public Employee(String name, String dept, double salary) {
        this.name = name;
        this.dept = dept;
        this.salary = salary;
    }

    void add(Employee e) {
        list.add(e);
    }

    void remove(Employee e) {
        list.remove(e);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee [name=" + name + ", dept=" + dept + ", salary="
                + salary + ", collection=" + list + "]";
    }

}
