package fun.codedesign.principle.pattern33.a030.filter;

public class Client {
    public static void main(String[] args) {
        Filter filter1 = new DebugFilter();
        Filter filter2 = new LoginFilter();
        FilterChain chain = new FilterChain();
        chain.setFilter(filter1);
        chain.setFilter(filter2);

        FilterManager manager = new FilterManager();
        manager.setChain(chain);

        Target target = new Target();
        manager.setTarget(target);

        Request request = new Request();
        // 依次通过Debug和Login过滤
        manager.execute(request); //执行目标方法，得到的request是:hello:debug:login
    }
}
