/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: ITarget
 * Author:   zengjian
 * Date:     2018/7/18 10:39
 * Description: 目标类接口
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a006.adapter;

/**
 * 〈一句话功能简述〉<br>
 * 〈目标类接口〉
 *
 * @author zengjian
 * @create 2018/7/18
 * @since 1.0.0
 */
public interface ITarget {
    void printMessage(String msg);
}