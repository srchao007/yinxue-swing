/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Client
 * Author:   zengjian
 * Date:     2018/7/18 16:37
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a011.facade;

/**
 * 〈客户端只访问门面类，而不直接访问内部的类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/18 16:37
 */
public class Client {

    private Facade facade;

    public Client(Facade facade) {
        this.facade = facade;
    }

    public void execute() {
        facade.handleInteger();
        facade.handleString();
    }
}