package fun.codedesign.principle.pattern33.a007.bridge;


/**
 * 桥接模式
 * 主要用来分离主体以及其行为
 * 使得其行为能够单独的进行变化，即一个接口多个实现类，针对不同的主体传入不同的接口实现类
 */