/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Chain
 * Author:   zengjian
 * Date:     2018/7/18 16:50
 * Description: 职责链
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a014.chain_of_responsibility;

/**
 * 〈职责链〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/18 16:50
 */
public class Chain {

    private Filter filter;

    public Chain(Filter filter) {
        this.filter = filter;
    }

    public void doFilter(String request) {
        if (filter.match(request)) {
            filter.doFilter(request);
        } else {
            filter.next().doFilter(request);
        }
    }
}