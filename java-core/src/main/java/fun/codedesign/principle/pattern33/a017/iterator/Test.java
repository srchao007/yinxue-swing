/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Test
 * Author:   zengjian
 * Date:     2018/7/27 10:19
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a017.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 10:19
 */
public class Test {

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>() {
            {
                add("123");
                add("abc");
                add("xxx");
                add("fff");
            }
        };

        Data data = new Data(list);
        Iterator<String> iterator = data.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}