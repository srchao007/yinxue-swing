package fun.codedesign.principle.pattern33.a031.servicelocator;

import java.util.ArrayList;
import java.util.List;

public class Cache {

    private static List<Service> services;

    public Cache() {
        services = new ArrayList<>(256);
    }

    public void addService(Service service) {
        if (!services.isEmpty()) {
            for (Service oldService : services) {
                if (service.getName().equalsIgnoreCase(oldService.getName())) {
                    break;
                }
            }
        } else {
            services.add(service);
        }
    }

    public Service getService(String name) {
        if (!services.isEmpty()) {
            for (Service service : services) {
                if (service.getName().equalsIgnoreCase(name)) {
                    return service;
                }
            }
        }
        return null;
    }
}
