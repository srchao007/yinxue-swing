package fun.codedesign.principle.pattern33.a002.abstractFactory;

import fun.codedesign.principle.pattern33.a001.factory.drink.Drink;
import fun.codedesign.principle.pattern33.a001.factory.drink.WaterDrink;
import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.PaoNoodles;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 16:27
 * @since 1.0.0
 */
public class KFCFactory implements AbstractFoodFactory {
    @Override
    public Noodles getNoodles() {
        return new PaoNoodles();
    }

    @Override
    public Drink getDrinks() {
        return new WaterDrink();
    }
}


