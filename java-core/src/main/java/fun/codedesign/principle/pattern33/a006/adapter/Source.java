/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Source
 * Author:   zengjian
 * Date:     2018/7/18 10:38
 * Description: 原来的类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a006.adapter;

/**
 * 〈一句话功能简述〉<br>
 * 〈原来的类〉
 *
 * @author zengjian
 * @create 2018/7/18
 * @since 1.0.0
 */
public class Source implements ISource {

    @Override
    public void printOld(String msg) {
        System.out.println(msg);
    }
}