package fun.codedesign.principle.pattern33.a029.dispatcher;

public class TeacherView implements View {
    @Override
    public void show() {
        System.out.println("Hi, this is teacher view");
    }
}
