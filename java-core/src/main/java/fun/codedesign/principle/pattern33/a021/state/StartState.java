/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StartState
 * Author:   zengjian
 * Date:     2018/7/27 11:52
 * Description: 开启状态
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a021.state;

/**
 * 〈开启状态〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 11:52
 */
public class StartState implements State {

    @Override
    public void doAction(Context context) {
        System.out.println("开启状态");
        context.setState(this);
    }
}