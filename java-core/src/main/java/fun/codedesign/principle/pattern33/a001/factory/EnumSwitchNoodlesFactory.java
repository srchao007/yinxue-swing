package fun.codedesign.principle.pattern33.a001.factory;

import fun.codedesign.principle.pattern33.a001.factory.noodles.GanKouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.LanZhouNoodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.Noodles;
import fun.codedesign.principle.pattern33.a001.factory.noodles.PaoNoodles;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 16:53
 * @since 1.0.0
 */
public enum EnumSwitchNoodlesFactory {
    GANKOU, LANZHOU, PAO;

    public Noodles create() {
        switch (this) {
            case GANKOU:
                return new GanKouNoodles();
            case LANZHOU:
                return new LanZhouNoodles();
            case PAO:
                return new PaoNoodles();
            default:
                return null;
        }
    }
}
