package fun.codedesign.principle.pattern33.a021.state;

// 状态模式，主要将状态进行独立的抽象
// 将不同的状态放在上下文中，改变其行为
// 用于替代if else 等语句
// 缺点：修改状态类需要修改对应的转换类源码，对开闭原则的支持不太好
// 优点：可以多环境共享状态
