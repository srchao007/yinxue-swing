/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: State
 * Author:   zengjian
 * Date:     2018/7/27 11:51
 * Description: 状态接口
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.principle.pattern33.a021.state;

/**
 * 〈状态接口〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 11:51
 */
public interface State {
    void doAction(Context context);
}