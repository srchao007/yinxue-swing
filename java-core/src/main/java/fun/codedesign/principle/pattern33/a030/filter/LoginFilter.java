package fun.codedesign.principle.pattern33.a030.filter;

public class LoginFilter implements Filter {

    @Override
    public void filterExecute(Request request) {
        request.setRequest(request.getRequest() + ":login");
    }
}
