package fun.codedesign.principle.effectivejava;

import java.util.EnumSet;

public class Item_01_UseFactoryReplaceConstrutor {

    @SuppressWarnings("all")
    public static void main(String[] args) {
        // 用静态工厂类替代构造器，jdk下的EnumSet的静态方法枚举小于等于64返回RegularEnumSet大于返回JumboEnumSet
        EnumSet set = EnumSet.noneOf(food.class);
    }

    enum food {
        MILK, BANANA;
    }
}
