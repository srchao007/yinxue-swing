package fun.codedesign.principle.advice151.a066;

import java.util.Arrays;
import java.util.List;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 21:02
 * @since 1.0.0
 */
@Notice("数组转换的list是不可变的")
public class UnmodifiedList {
    public static void main(String[] args) {
        int[] arr = {1, 3, 3};
        List<?> list = Arrays.asList(arr); // 这是一个数组工具类的内部类
        System.out.println(list);
    }
}
