package fun.codedesign.principle.advice151.a031;

import fun.codedesign.principle.advice151.Bad;

/**
 * 接口中不该包含实现代码 <br>
 * <pre>
 *     这一点在jdk8中似乎有所改变，接口可以有默认实现
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:11
 * @since 1.0.0
 */
@Bad
public class InterfaceContainsImpl {

    interface S {
        X x = new X() {

            @Override
            public void doSomething() {
                System.out.println("匿名内部类");
            }
        };
    }

    interface X {
        public void doSomething();
    }
}
