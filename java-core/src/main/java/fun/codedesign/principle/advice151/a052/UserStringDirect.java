package fun.codedesign.principle.advice151.a052;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

/**
 * 直接使用String赋值，不需要new <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:40
 * @since 1.0.0
 */
@Notice("直接String赋值，有关String常量池问题")
public class UserStringDirect {

    @Test
    public void test52() {
        String s1 = "hello";
        String s2 = "hello";
        String s3 = new String("hello");
        System.out.println(s1 == s2);
        System.out.println(s2 == s3);
        System.out.println(s1 == s3.intern());
    }
}
