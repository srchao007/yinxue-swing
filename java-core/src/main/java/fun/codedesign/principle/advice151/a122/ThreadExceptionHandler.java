package fun.codedesign.principle.advice151.a122;

import org.junit.Test;

import fun.codedesign.principle.advice151.Good;

/**
 * 是哦那个线程异常处理器提升系统可靠性 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:31
 * @since 1.0.0
 */
@Good
public class ThreadExceptionHandler {

    // 建议122： 使用线程异常处理器提升系统可靠性
    @Test
    public void test122() throws Exception {
        // 可以实现以下的异常处理器接口 UncaughtExceptionHandler
        class UncaughtExceptionHandlerTest implements Thread.UncaughtExceptionHandler {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                // 可以在这里在服务中断之后重新启动服务
                // 保证业务不中断
                new Thread() {
                    @Override
                    public void run() {
                        System.out.println("重新开启一个线程" + Thread.currentThread().getName());
                    }
                }.start();


            }
        }

        Thread t = new Thread() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "线程开始服务");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                throw new RuntimeException("线程中止");
            }
        };
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandlerTest());
        t.start();

        // 阻塞下
        Thread.currentThread().join();
    }
}
