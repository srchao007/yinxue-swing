package fun.codedesign.principle.advice151.a119;

import org.junit.Test;

import fun.codedesign.principle.advice151.Bad;
import fun.codedesign.principle.advice151.Notice;

/**
 * 不用过期了的stop方法停止线程 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:19
 * @since 1.0.0
 */
@Notice
public class DontUseStopMethod4Thread {

    // 建议119： 启动线程前stop方法是不可靠的
    // 依然可能导致部分线程启动，由于其判断机制问题
    @SuppressWarnings("all")
    @Bad("此处可以写死循环增加线程stop, 然后启动，会有不可预测线程启动")
    @Test
    public void test() throws InterruptedException {
        Thread t = new Thread("hello thread");
        t.stop(); // 一般stop状态下的线程不会被启动，极端情况下可能会能启动，因为本地方法
        // start0在stop0状态检查前启动
        t.start();
        Thread.currentThread().join();
    }
}
