package fun.codedesign.principle.advice151.a023;

public class UnexpectedTypeConvert {
    public static void main(String[] args) {
        // 由于 long类型没有显示声明，那么一般都是先int类型，然后自动转换成long类型
        // 如果没有显示声明，那就可能存在先以int类型计算，超过了取值范围，再赋值给long，使得long得到一个非预期的数值
        // 解决的办法就是显示声明
        int i = 30 * 10000 * 1000;
        long l = i * 60 * 8;  // Q:次数加上l显示声明，则不会发生int的默认转换
        System.out.println(l);  // -2028888064

        long l1 = i * 60 * 8L;
        System.out.println(l1);  // 6561046528

    }
}
