package fun.codedesign.principle.advice151.a138;

import fun.codedesign.principle.advice151.Notice;

/**
 * 关于性能的问题 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 11:52
 * @since 1.0.0
 */
@Notice("只要用心肯投入都可以解决")
public class Performance {

    //建议138： 性能是个大咕咚
    /*
     * 1. 没有慢的系统，只有没满足业务要求的系统
     * 2. 没有慢的系统，只有架构不良的系统
     * 3. 没有慢的系统，只有懒惰的技术人员
     * 4. 没有慢的系统，只有不愿意投入的系统
     *
     */
}
