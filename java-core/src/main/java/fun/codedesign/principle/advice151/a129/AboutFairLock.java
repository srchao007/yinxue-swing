package fun.codedesign.principle.advice151.a129;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@SuppressWarnings("all")
public class AboutFairLock {
    public static void main(String[] args) {
        // Lock可以实现非公平锁或者公平锁 ，sync是非公平锁，公平锁即先到的(等待时间最长的先持有锁)
        ReadWriteLock lock = new ReentrantReadWriteLock(true); // 传入true得到的是公平锁, 默认为false，非公平
        Lock read = lock.readLock();
        Lock write = lock.writeLock();
    }
}
