package fun.codedesign.principle.advice151.a146;

/**
 * 好的注释 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 14:15
 * @since 1.0.0
 */
public class GoodNote {
    // 建议146： 让注释正确、清晰、简洁
    /* 1. 废话
     * 2. 故事
     * 3. 不必要
     * 4. 过时
     * 5. 大块
     * 6. 流水账
     * 7. javadoc注释
     *
     * 良好的：
     * 1. 法律版权
     * 2. 解释意图注释
     * 3. 警示性注释
     * 4. TODO注释
     *
     * 注释是催化剂而不是美化剂
     */
}
