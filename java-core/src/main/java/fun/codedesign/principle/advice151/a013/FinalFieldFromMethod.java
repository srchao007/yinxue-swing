package fun.codedesign.principle.advice151.a013;

import java.io.Serializable;

import fun.codedesign.principle.advice151.Bad;
import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 15:58
 * @since 1.0.0
 */
@Bad("用方法返回的final值也不会被序列化")
@Notice("final修饰的不是基本类型也不会参与序列化")
public class FinalFieldFromMethod implements Serializable {
    private static final long serialVersionUID = -3920859957598320376L;
    private final String name = initValue();

    private String initValue() {
        return "zhangsan";
    }
}
