package fun.codedesign.principle.advice151.a051;

import fun.codedesign.principle.advice151.Bad;
import fun.codedesign.principle.advice151.Notice;

/**
 * 不要用系统GC ,会扫描整个包<br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:28
 * @since 1.0.0
 */
@Notice
@Bad("不要显示的进行垃圾回收，会stop world，导致非预期效果，阻塞、无响应等等")
public class NoUserSystemGC {
    public static void main(String[] args) {

        StringBuilder sb = new StringBuilder("helloworld");
        System.out.println(sb);
        System.gc();

    }
}
