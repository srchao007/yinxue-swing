package fun.codedesign.principle.advice151.a005;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

public class NoDirectNullForVarsargs {

    @Test
    @Notice("变长参数方法时候，需要指定null的类型，否则会冲突，编译不通过")
    public void testNull() {
        /**
         * complie no match, method conflict, if only getMessage(String name, Integer... msg)
         * all can match
         */
        // getMessage("张三",null);
    }

    @Test
    public void testNullRight() {
        String[] ss = null;
        getMessage("张三", ss);

        Integer[] ii = null;
        getMessage("张三", ii);
    }


    public String getMessage(String name, String... msg) {
        return name + "string msg";
    }

    public String getMessage(String name, Integer... msg) {
        return name + "integer msg";
    }

    public String getMessage(String name, Object... msg) {
        return name + "object msg";
    }
}
