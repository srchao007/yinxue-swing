package fun.codedesign.principle.advice151.a025;

import java.math.BigDecimal;

public class Rounding {
    // 四舍五入问题
    public static void main(String[] args) {
        // Math.round(..) 采用了正无穷方向舍入规则
        System.out.println("10.5的四舍五入值:" + Math.round(10.5)); // 11
        System.out.println("-10.5的四舍五入值:" + Math.round(-10.5)); // -10

        // 以下示例中需要用字符串构造
        // BigDecimal对应的规则
        // 远离0的规则，不分大于还是小于5，即使后面是0也按远离0的规则
        BigDecimal a1 = new BigDecimal("0.451").setScale(2, BigDecimal.ROUND_UP);
        BigDecimal a2 = new BigDecimal("-0.453").setScale(2, BigDecimal.ROUND_UP);
        System.out.println(a1); // 0.46 远离0的方向
        System.out.println(a2); // -0.46

        // 接近0的规则，即使后面是9也按此
        BigDecimal a3 = new BigDecimal("0.459").setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal a4 = new BigDecimal("-0.455").setScale(2, BigDecimal.ROUND_DOWN);
        System.out.println(a3); // 0.45
        System.out.println(a4); // -0.45

        // 最近邻居法，如果是0.5(即两边临近相等)则按照round_up来
        BigDecimal a5 = new BigDecimal("0.455").setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal a6 = new BigDecimal("-0.455").setScale(2, BigDecimal.ROUND_HALF_UP);
        System.out.println(a5); // 0.46
        System.out.println(a6); // -0.46

        // 最近邻居法，如果是0.5则按照round_down来
        BigDecimal a7 = new BigDecimal("0.455").setScale(2, BigDecimal.ROUND_HALF_DOWN);
        BigDecimal a8 = new BigDecimal("-0.455").setScale(2, BigDecimal.ROUND_HALF_DOWN);
        System.out.println(a7); // 0.45
        System.out.println(a8); // -0.45

        BigDecimal a9 = new BigDecimal("-0.455").setScale(2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal a10 = new BigDecimal("-0.465").setScale(2, BigDecimal.ROUND_HALF_EVEN);
        System.out.println(a9); // -0.46
        System.out.println(a10); // -0.46
    }
}
