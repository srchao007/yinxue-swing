package fun.codedesign.principle.advice151.a049;

import fun.codedesign.principle.advice151.Good;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:21
 * @since 1.0.0
 */
@Good("推荐覆写toString()方法")
public class RecommandOverwriteToStringMethod {

    private String name;

    @Override
    public String toString() {
        return "RecommandOverwriteToStringMethod{" +
                "name='" + name + '\'' +
                '}';
    }
}
