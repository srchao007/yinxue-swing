package fun.codedesign.principle.advice151.a114;

import fun.codedesign.principle.advice151.Notice;

/**
 * 在构造器中尽量不要抛出异常 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:02
 * @since 1.0.0
 */
@Notice("构造器中的异常难以处理和预期")
public class ConstructorException {

    // 建议114： 不要在构造函数中抛出异常
    /*
     * 1. error类及子类表示错误，不需要储蓄员处理也不能处理的异常
     * 2. runtimeException 表示非受检异常，可处理可不处理，最典型的是空指针异常和越界异常
     * 3. Exception表示受检异常，必须处理的异常，不处理无法通过
     *
     * 构造函数异常：
     * 1. 如果造成虚拟机错误，无法预知错误发生，及捕捉处理
     * 2. 构造函数不该抛出运行时异常，抛出后无法执行
     *
     * 抛运行时异常：
     * 1. 加重上层负担，不知道该异常是啥，不捕捉对整个系统造成影响
     * 2. 后续代码不会执行
     *
     * 抛普通异常(受检异常)：
     * 1. 导致子类代码膨胀
     * 2. 违反里氏替换原则，子类能替换父类，因为子类继承抛异常中止
     * 3. 子类扩展受限
     */
}
