package fun.codedesign.principle.advice151.a009;

import java.text.NumberFormat;

import fun.codedesign.principle.advice151.Bad;
import fun.codedesign.principle.advice151.Notice;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.Math.PI;
import static java.text.NumberFormat.getInstance;

/**
 * getInstance() 会导致理解混乱与本地方法类似
 * debug
 * 好的用法是 JUnit断言静态导入
 */
public class UseStaticImportCareFully {

    @Bad("注意静态导入包的冲突问题，如下可能不知道是来自于什么方法")
    @Notice
    public static void main(String[] args) {
        double s = PI * parseDouble(args[0]);
        NumberFormat nf = getInstance();
        nf.setMaximumFractionDigits(parseInt(args[1]));
        formatMessage(nf.format(s));
    }

    public static void formatMessage(String s) {
        System.out.println("圆面积是:" + s);
    }
}
