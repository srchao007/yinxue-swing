package fun.codedesign.principle.advice151.a041;

import fun.codedesign.principle.advice151.Notice;

interface Father {
    int strong();
}

interface Mother {
    int kind();
}

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 17:22
 * @since 1.0.0
 */
@Notice("成员内部类的使用，因为java是单继承语言，这里用另外一种途径来实现多类继承，起始有点乱")
public class PrivateInnerClassApply {
    // 建议41：让多继承成为现实，通过私有化成员内部类来实现
}

class FatherImpl implements Father {
    @Override
    public int strong() {
        return 8;
    }
}

class MotherImpl implements Mother {

    @Override
    public int kind() {
        return 8;
    }
}

class Son extends FatherImpl implements Mother {

    @Override
    public int strong() {
        return super.strong() + 1;
    }

    @Override
    public int kind() {
        return new MotherSpecial().kind() - 1;
    }

    private class MotherSpecial extends MotherImpl {
        @Override
        public int kind() {
            return super.kind();
        }
    }
}


class Daugther extends MotherImpl implements Father {

    @Override
    public int kind() {
        return super.kind() + 1;
    }

    @Override
    public int strong() {
        return (new Daugther.FatherSpecial()).strong() - 1;
    }

    private class FatherSpecial extends FatherImpl {
        @Override
        public int strong() {
            return super.strong();
        }
    }
}


