package fun.codedesign.principle.advice151.a053;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:41
 * @since 1.0.0
 */
@Notice("原生的replaceAll方法第一个参数是正则表达式，注意")
public class ReplaceAllIsRegex {
    public static void main(String[] args) {
        String abc = "abc";
        abc.replace("a", "c");
        System.out.println(abc.replaceAll("^a.{0,}", ""));
    }
}
