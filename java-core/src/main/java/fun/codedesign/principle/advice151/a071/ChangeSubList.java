package fun.codedesign.principle.advice151.a071;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.util.ArrayList;
import java.util.List;

/**
 * 子列表不要进行修改 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 14:10
 * @since 1.0.0
 */
@Notice("推荐使用子列表操作列表范围数据,此处有待商榷")
public class ChangeSubList {

    // 建议71： 推荐使用subList处理局部列表
    @Test
    public void test71() {
        // 删除索引位置上的20-30号内容
        List<Integer> list = new ArrayList<>(100);
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }

        // 方法1：直接删
        /*
         * System.out.println(collection.toString()); for (int i = 0; i < 11; i++) {
         * collection.remove(19); } System.out.println(collection.toString());
         */

        // 方法2：sublist直接清空
        list.subList(20, 30).clear();
        System.out.println(list.toString());
    }
}
