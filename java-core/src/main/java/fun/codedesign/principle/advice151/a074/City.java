package fun.codedesign.principle.advice151.a074;

import org.apache.commons.lang3.builder.CompareToBuilder;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 14:20
 * @since 1.0.0
 */
public class City implements Comparable<City> {

    private String name;

    public City(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(City o) {
        return new CompareToBuilder().append(name, o.name).toComparison();
    }
}
