package fun.codedesign.principle.advice151.a115;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

/**
 * 通过异常得到栈信息 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:04
 * @since 1.0.0
 */
@Notice("可以通过堆栈来获得方法调用信息")
public class ThrowableInfomation {

    // 建议115： 使用Throwable获得栈信息,看调用的方法名是什么
    @Test
    public void test115() throws Exception {
        // 调用方法是m1 与 Xoo中的方法一致
        Invoker.m1();
        Invoker.m2();
    }
}


class Xoo {
    public static boolean m() {
        StackTraceElement[] array = new Throwable().getStackTrace();
        for (StackTraceElement stackTraceElement : array) {
            if ("m1".equals(stackTraceElement.getMethodName())) {
                return true;
            }
        }
        throw new RuntimeException("只允许m1方法进行本方法调用");
    }
}

class Invoker {
    public static void m1() {
        System.out.println(Xoo.m());
    }

    public static void m2() {
        System.out.println(Xoo.m());
    }
}
