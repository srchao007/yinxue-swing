package fun.codedesign.principle.advice151.a062;

import java.util.Arrays;

import fun.codedesign.principle.advice151.Notice;

/**
 * 注意数组浅拷贝 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 20:06
 * @since 1.0.0
 */
@Notice("和其他浅拷贝一样，注意数组的浅拷贝情况")
public class NoticeArraySimpleClone {

    public static void main(String[] args) {
        // Arrays.copyOf
        // clone 方法
        Person[] person = new Person[]{new Person("zhangsan"), new Person("lisi")};
        Person[] person2 = person.clone();
        person2[0].setName("lisi");
        System.out.println(Arrays.toString(person));

        Person[] person3 = Arrays.copyOf(person, person.length);
        person3[0].setName("ssssssssssss");
        System.out.println(Arrays.toString(person));
    }
}


class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
