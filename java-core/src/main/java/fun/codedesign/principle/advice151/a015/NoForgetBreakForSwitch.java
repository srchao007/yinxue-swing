package fun.codedesign.principle.advice151.a015;

public class NoForgetBreakForSwitch {

    public static void main(String[] args) {
        System.out.println(toChineseNumberCase(1));
    }

    /**
     * 不加break; 输出的始终都是最后一个default的值 输入0-9
     *
     * @param n
     * @return
     */
    public static String toChineseNumberCase(int n) {
        String chineseNum = "";
        switch (n) {
            case 0:
                chineseNum = "零";
                break;
            case 1:
                chineseNum = "壹";
                break;
            case 2:
                chineseNum = "贰";
                break;
            case 3:
                chineseNum = "叁";
                break;
            case 4:
                chineseNum = "肆";
                break;
            case 5:
                chineseNum = "伍";
                break;
            case 6:
                chineseNum = "陆";
                break;
            case 7:
                chineseNum = "柒";
                break;
            case 8:
                chineseNum = "捌";
                break;
            case 9:
                chineseNum = "玖";
                break;
            default:
                chineseNum = "输入0-9";
                break;
        }
        return chineseNum;
    }
}
