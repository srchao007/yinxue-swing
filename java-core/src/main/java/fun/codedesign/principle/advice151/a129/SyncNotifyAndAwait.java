package fun.codedesign.principle.advice151.a129;

public class SyncNotifyAndAwait {
    public static void main(String[] args) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized ("A") {
                    for (int i = 0; i < 5; i++) {
                        System.out.println(i);
                    }
                    try {
                        "A".wait(2000);  // 释放锁后等2s再重新运行，WAIT加时间值的含义，就是多少时间之后自己再重新被唤醒运行
                        System.out.println("再次被唤醒");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized ("A") {
                    for (int i = 6; i < 11; i++) {
                        System.out.println(i);
                    }
                    //"A".notify();
                }
            }
        }).start();

    }
}
