package fun.codedesign.principle.advice151.a129;

import java.util.Calendar;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockReadAndWrite {

    public static void main(String[] args) {
        ReadWriteLock lock = new ReentrantReadWriteLock();
        // final Lock read = lock.readLock();
        final Lock write = lock.writeLock();
        // 读锁可以并发，写锁不可以

        // 匿名类
        for (int i = 0; i < 3; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    write.lock();
                    try {
                        Thread.sleep(3000);
                        System.out.println(Calendar.getInstance().get(13));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        write.unlock();
                    }
                }
            }).start();
        }
    }
}


