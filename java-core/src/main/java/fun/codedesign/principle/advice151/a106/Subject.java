package fun.codedesign.principle.advice151.a106;

/**
 * 实际对象接口 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-08 14:59
 * @since 1.0.0
 */
public interface Subject {
    void request();
}
