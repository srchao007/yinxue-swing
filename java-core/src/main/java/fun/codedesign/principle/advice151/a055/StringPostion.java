package fun.codedesign.principle.advice151.a055;

import java.util.ArrayList;

import fun.codedesign.principle.advice151.Notice;

/**
 * 注意String字符串运算位置，它拥有最高优先级 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:51
 * @since 1.0.0
 */
@Notice("看示例")
public class StringPostion {
    public static void main(String[] args) {
        String a1 = 1 + 2 + "hello";
        System.out.println(a1);

        String b1 = "hello" + 1 + 2;
        System.out.println(b1);
        
        String c1 = "hello" + new ArrayList<>();
        System.out.println(c1);
    }
}
