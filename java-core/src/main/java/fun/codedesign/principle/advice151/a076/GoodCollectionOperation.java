package fun.codedesign.principle.advice151.a076;

import java.util.ArrayList;

import fun.codedesign.principle.advice151.Good;
import fun.codedesign.principle.advice151.Notice;

/**
 * 优雅的集合运算方式 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 15:43
 * @since 1.0.0
 */
@Good
@Notice("起始还有Guava等工具类提供的方法")
public class GoodCollectionOperation {
    // 建议76： 集合运算时使用更优雅的方式
    public static void main(String[] args) {
        City c1 = new City("广州");
        City c2 = new City("西安");
        City c3 = new City("南京");
        City c4 = new City("北京");
        City c5 = new City("西安");

        ArrayList<City> list1 = new ArrayList<>(4);
        list1.add(c1);
        list1.add(c2);
        list1.add(c3);

        ArrayList<City> list2 = new ArrayList<>(4);
        list2.add(c4);
        list2.add(c5);
        // 并集
        list1.addAll(list2);
        // 交集
        list1.retainAll(list2);
        // 差集
        list1.removeAll(list2);
        // 无重复并集
        list1.removeAll(list2);
        list2.addAll(list1);
    }
}
