package fun.codedesign.principle.advice151.a077;

import java.util.ArrayList;
import java.util.Collections;

import fun.codedesign.principle.advice151.Good;

@Good("集合乱序")
public class CollectionsShuffle {

    // 建议77： 使用shuffle打乱列表

    public static void main(String[] args) {
        City c1 = new City("广州");
        City c2 = new City("西安");
        City c3 = new City("南京");
        City c4 = new City("北京");
        City c5 = new City("西安");

        ArrayList<City> list1 = new ArrayList<>(4);
        list1.add(c1);
        list1.add(c2);
        list1.add(c3);
        list1.add(c4);
        list1.add(c5);
        // 乱序
        Collections.shuffle(list1);
    }
}
