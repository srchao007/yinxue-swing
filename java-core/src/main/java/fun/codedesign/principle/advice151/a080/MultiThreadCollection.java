package fun.codedesign.principle.advice151.a080;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:19
 * @since 1.0.0
 */
@Notice("多线程要求线程安全的采用vector或者hashTable，其实一般用ConcurrentHashMap")
public class MultiThreadCollection {
    // 建议80： 多线程使用Vector或者HashTable
    // 考虑使用ArrayList线程安全的Vector 和 hashMap线程安全的HashTable
}
