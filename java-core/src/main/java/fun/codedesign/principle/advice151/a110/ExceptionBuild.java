package fun.codedesign.principle.advice151.a110;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * 提倡异常封装 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 19:34
 * @since 1.0.0
 */
@Notice("为更好的展示异常信息，建议对异常进行封装")
public class ExceptionBuild {

    // 第8章 异常
    // 建议110： 提倡异常封装
    /*
     * 1. 自定义异常进行抛出，可以由终端用户看明白相关含义
     * 2. 自定义一个异常容器，可以再catch到过个异常之后统一的抛出
     * 	     不能一下子就在catch到一个异常之后就抛出相关异常，否则程序会中止，可以放在一个容器中统一抛出
     */
    @Test
    public void test110() throws IOException {
        InputStream is = null;
        try {
            is = new FileInputStream("stting.xxx");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // 抛出自定义异常
            throw new MyBussinessException(e);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
}
