package fun.codedesign.principle.advice151.a058;

import java.io.UnsupportedEncodingException;

import fun.codedesign.principle.advice151.Good;

/**
 * 使用UTF-8编码格式 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:55
 * @since 1.0.0
 */
@Good("使用UTF-8编码格式")
public class UseUtf_8 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        byte[] bytes = "abc".getBytes("utf-8");
        String abc = new String(bytes);
        System.out.println(abc);
    }
}
