package fun.codedesign.principle.advice151.a129;

import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockSetting {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Lock lock = new ReentrantLock();
        for (int i = 0; i < 3; i++) {
            executorService.submit(new LockJob(lock));
        }
        executorService.shutdown();
    }
}

class LockJob implements Runnable {

    private Lock lock;

    public LockJob(Lock lock) {
        this.lock = lock;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            Thread.sleep(3000);
            System.out.println("当前时间:" + Calendar.getInstance().get(13));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}