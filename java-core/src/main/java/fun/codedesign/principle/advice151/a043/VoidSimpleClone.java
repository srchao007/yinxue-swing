package fun.codedesign.principle.advice151.a043;

import fun.codedesign.principle.advice151.Notice;

/**
 * 避免浅拷贝 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 17:28
 * @since 1.0.0
 */
@Notice("浅拷贝是引用共享的问题，应该注意引起对象变化，好的方式是改写clone方法,避免共享引用变量")
public class VoidSimpleClone {

}

@SuppressWarnings("unused")
class Template implements Cloneable {

    private Aoo aoo;

    /**
     * 全部深拷贝
     *
     * @return
     */
    @Override
    protected Template clone() {
        Template template = new Template();
        return template;
    }
}

@SuppressWarnings("unused")
class Template2 implements Cloneable {

    private Aoo aoo;

    @Override
    protected Template2 clone() throws CloneNotSupportedException {
        Template2 template2 = (Template2) super.clone(); // 拷贝基础类型
        template2.aoo = new Aoo();  // 改变引用地址
        return template2;
    }
}


class Aoo {

}
