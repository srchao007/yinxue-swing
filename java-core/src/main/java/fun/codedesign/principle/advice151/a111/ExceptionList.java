package fun.codedesign.principle.advice151.a111;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 异常链 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 19:38
 * @since 1.0.0
 */
@Notice("可以将异常放入容器然后再统一抛出")
public class ExceptionList {

    @Test
    public void test111() throws GroupException, IOException {
        List<Throwable> list = new ArrayList<Throwable>();
        InputStream is = null;
        try {
            is = new FileInputStream("stting.xxx");
        } catch (FileNotFoundException e) {
            list.add(e);
        } catch (Exception e) {
            list.add(e);
        } finally {
            if (is != null) {
                is.close();
            }
        }
        // 假设异常不为空，则可以逐个抛出该异常
        if (list.size() > 0) {
            throw new GroupException(list);
        }
    }
}
