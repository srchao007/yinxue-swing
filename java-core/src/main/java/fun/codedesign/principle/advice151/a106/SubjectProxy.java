package fun.codedesign.principle.advice151.a106;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-08 15:01
 * @since 1.0.0
 */
public class SubjectProxy implements Subject {
    // 代理类
    private Subject subject = null;

    public SubjectProxy() {
        subject = new SubjectImpl();
    }

    @Override
    public void request() {
        before();
        subject.request();
        after();
    }

    private void after() {
        System.out.println("houhouhouhouohoohohohohoho");
    }

    private void before() {
        System.out.println("qianqianqianiqqianiqaiqnainq");
    }
}
