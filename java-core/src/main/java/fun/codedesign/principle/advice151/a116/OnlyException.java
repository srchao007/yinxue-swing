package fun.codedesign.principle.advice151.a116;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:07
 * @since 1.0.0
 */
@Notice("可以通过判断，避免一些异常，比如空指针，文件不存在等异常")
public class OnlyException {
    // 建议116： 异常只为异常服务
    @Test
    public void test116() throws Exception {
        // 比如某些不清楚的非受检异常，catch runtimeException来捕捉异常，但是这样不如采用比如 返回true或者false来判断
        // 比如会产生路径或者文件找不到的情况，可以先用file.isExist这样先判断存在，然后再处理，而不应该以找不到该类的异常进行处理
    }
}
