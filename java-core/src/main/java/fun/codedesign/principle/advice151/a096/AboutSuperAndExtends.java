package fun.codedesign.principle.advice151.a096;

import java.util.List;

import fun.codedesign.principle.advice151.Bad;
import fun.codedesign.principle.advice151.Good;
import fun.codedesign.principle.advice151.Notice;

/**
 * 关于super 和 extends的用法 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 19:16
 * @since 1.0.0
 */
@Notice("super是定义上界，extends定义下界")
public class AboutSuperAndExtends {

    @SuppressWarnings("all")
    @Bad("不知道对象o有什么方法能够执行")
    public static <E> void read(List<? super E> src) {
        for (Object o : src) {
            // 此处逻辑无法继续写下去
        }
    }

    public static <E> void read0(List<? extends E> src) {

    }

    @Good
    public static <E> void read1(List<E> src) {

    }

    @Good
    public static void write(List<? super Number> list) {
        list.add(1);
        list.add(2.33);
        // 圈定了必须是Number的派生类，其他不可以，起始extends和super不是相反的含义
        // 是两种不同的泛型限定
        //list.add("123");
    }

    @Bad("编译不通过")
    @Notice("一般写操作用super，读操作用extends")
    public static void write0(List<? extends Number> list) {
        //list.add(1);
        //list.add(2.33);
    }
}
