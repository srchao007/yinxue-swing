package fun.codedesign.principle.advice151;

import java.lang.annotation.*;

/**
 * 错误的示例不好的示例 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 14:06
 * @since 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,
        ElementType.ANNOTATION_TYPE,
        ElementType.CONSTRUCTOR,
        ElementType.LOCAL_VARIABLE,
        ElementType.TYPE,
        ElementType.METHOD,
        ElementType.PARAMETER,
        ElementType.PACKAGE})
@Documented
public @interface Bad {
    String value() default "";
}
