package fun.codedesign.principle.advice151.a098;

import fun.codedesign.principle.advice151.Good;
import fun.codedesign.principle.advice151.Notice;

/**
 * 使用泛型的推荐顺序 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 19:46
 * @since 1.0.0
 */
@Notice
public class GenericOrder {

    @Good("依次使用这三种泛型写法")
    public static void main(String[] args) {
        // List<T>
        // List<?>
        // List<Object>
    }
}
