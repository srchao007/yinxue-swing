package fun.codedesign.principle.advice151.a072;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 保持原来的表不变 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 15:17
 * @since 1.0.0
 */

@Notice("size()方法会导致原列表进行修改后会发生并发修改异常，所以不要修改原列表")
public class KeepOriginList {

    // 建议72： 生成子列表后不要再操作原列表
    // subList操作后对原列表进行锁定操作，生成了多个子列表后也不应该进行修改
    @Test
    public void test72() {
        List<Integer> list = new ArrayList<>(100);
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }

        List<Integer> subList = list.subList(0, 3);
        // 锁定原列表，只读，不能修改
        Collections.unmodifiableList(list);
        // 可对subList进行操作
        subList.add(111);
        System.out.println(subList.toString());
        System.out.println(list.toString());
        // 对原子表进行操作将会导致 一下list.size时候出现异常，因为subList的时候size发生了变化
        // add时候再次发生变化就会抛出异常
        // collection.add(122);
        System.out.println(list.toString());
        System.out.println(list.size());
        System.out.println(subList.size());
        System.out.println(subList.toString());
    }
}
