package fun.codedesign.principle.advice151.a012;

import java.io.Serializable;

import fun.codedesign.principle.advice151.Bad;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 15:56
 * @since 1.0.0
 */
public class FinalFieldFromConstrutor implements Serializable {
    private static final long serialVersionUID = 5307575448651467525L;
    private final String name;

    @Bad("序列化时，构造函数赋的值不会被序列化,应该避免这种方式")
    public FinalFieldFromConstrutor(String name) {
        this.name = "zhangsan";
    }
}
