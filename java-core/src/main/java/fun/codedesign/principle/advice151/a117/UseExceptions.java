package fun.codedesign.principle.advice151.a117;

import fun.codedesign.principle.advice151.Notice;

/**
 * 使用分层次的异常 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:08
 * @since 1.0.0
 */
@Notice("可以使用一层一层的异常，用来进行不同状况的捕捉，比如密码异常，用户名异常，其他异常等")
public class UseExceptions {
    // 建议117： 多使用异常，把性能问题放一边
    // 定义多个异常来处理情况，虽然异常的性能高于平常基础类型5倍，但是可以增加明确性
    // 比如 一级 用户名错误异常  二级 密码错误异常  三级 多次登录失败异常
}
