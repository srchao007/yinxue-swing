package fun.codedesign.principle.advice151.a063;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fun.codedesign.principle.advice151.Good;

/**
 * 指定初始集合大小 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 20:19
 * @since 1.0.0
 */
@Good("避免频繁扩容")
public class SpecifyCapacityForCollection {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>(3); // 小于10的取10容量，大于的取大于的
        System.out.println(list);

        Map<String, String> map = new HashMap<>(3); // 最小容量为接近初始容量的2的n次方的值
        map.put("123", "123");

        int num = Integer.highestOneBit((3 - 1) << 1);
        System.out.println(num);

    }

}
