package fun.codedesign.principle.advice151.a059;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.text.Collator;
import java.util.Arrays;
import java.util.Locale;

/**
 * 字符串排序 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:57
 * @since 1.0.0
 */
@Notice("字符串排序")
public class StringSort {
    @Test
    public void test59() {
        // 更细致的排序需要用到开源包 比如 pingyin4j
        String[] strArray = {"zhangsan", "李四", "王五"};
        Arrays.sort(strArray);
        System.out.println(Arrays.toString(strArray));
        Collator c = Collator.getInstance(Locale.CHINA);
        Arrays.sort(strArray, c);
        System.out.println(Arrays.toString(strArray));
    }

}
