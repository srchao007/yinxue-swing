package fun.codedesign.principle.advice151.a118;

import fun.codedesign.principle.advice151.Bad;
import fun.codedesign.principle.advice151.Notice;

/**
 * 不要覆写线程的启动方法 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:11
 * @since 1.0.0
 */
@Notice
public class DontOverideStartMethod {
    // 第9章 多线程和并发
    // 建议118： 不推荐覆写start方法
    // 由本地方法管控start方法，一定要写加上super.start() 才执行，目前没有什么场景需要用这个，如果需要可以用别的取代
}

@Bad
class Task extends Thread {
    @Override
    public synchronized void start() {
        super.start();
    }
}
