package fun.codedesign.principle.advice151.a032;

import org.junit.Test;

import fun.codedesign.principle.advice151.Bad;

/**
 * 静态变量顺序 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:15
 * @since 1.0.0
 */
@Bad("先声明后赋值，否则先赋的值无效")
public class StaticOrder {
    /**
     * 建议32：静态变量一定要先声明后赋值,以下要交换下位置，JVM的原理是先初始化静态的地址，然后赋值
     */
    @SuppressWarnings("all")
    @Test
    public void test() {
        CCC c = new CCC();
        System.out.println(c.i);  // 此处i的值为100
    }

    @Test
    public void aaa() {
        AAA a = new AAA();
        System.out.println(a.a); // 此处a值为100
    }

}

class CCC {
    public static int i = 100;

    static {
        i = 1;
    }
}


class AAA {
    int a = 100;

    {
        a = 1;
    }
}
