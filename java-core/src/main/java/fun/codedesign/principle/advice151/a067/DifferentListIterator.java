package fun.codedesign.principle.advice151.a067;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import fun.codedesign.principle.advice151.Notice;

/**
 * 不同的迭代方法 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 21:03
 * @since 1.0.0
 */
@Notice("数据结构的不同要采用不同的迭代方式")
public class DifferentListIterator {
    public static void main(String[] args) {
        // 注意迭代器for each就是采用iterator的方法
        List<Integer> list = new ArrayList<>();
        for (int i = 0, size = list.size(); i < size; i++) {
            // 迭代操作效率最高 因为这个list实现了随机存储接口 RandomAccess
        }

        List<Integer> list2 = new LinkedList<>();
        // 采用for each的方式
        for (Integer a : list2) {
            // 有序存取就用这个
            System.out.println(a);
        }


    }
}
