package fun.codedesign.principle.advice151.a129;

import java.util.Calendar;

public class SyncTemplate {

    public static void main(String[] args) {
        int i = 3;
        while (i > 0) {
            new Thread(new SyncJob()).start();
            i--;
        }
    }
}

class SyncJob implements Runnable {
    @Override
    public void run() {
        // sync中的如果是.class, 常量或者引用地址相同，则是互斥的
        // 否则就是不互斥的
        synchronized (Integer.valueOf(10)) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Calendar.getInstance().get(13));
        }
    }
}
