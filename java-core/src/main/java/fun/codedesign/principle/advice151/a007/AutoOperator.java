package fun.codedesign.principle.advice151.a007;

import org.junit.Test;

import fun.codedesign.principle.advice151.Bad;

public class AutoOperator {

    @Test
    @Bad("这里的结果始终会0，因为i++会始终为0值")
    public void testOperator01() {
        int i = 0;
        for (int j = 0; j < 5; j++) {
            // i=1 i++=0  i=i++=0
            i = i++;
        }
        System.out.println(i); // 0
    }

    @Test
    public void testRightOperator02() {
        int i = 0;
        for (int j = 0; j < 5; j++) {
            i++;
        }
        System.out.println(i);
    }

}
