package fun.codedesign.principle.advice151.a079;

import fun.codedesign.principle.advice151.Notice;

/**
 * 通过哈希定位 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:03
 * @since 1.0.0
 */
@Notice("hashCode尽量不要相同")
public class HashCodeLocation {
    // 建议79：集合中的哈希码不要重复
    // 通过hashCode定位数组下标，如果hashCode一样就采用链表进行处理，这样它的查找与ArrayList相仿了
    // 如果是hashCode不同，则效率比ArrayList快大概40倍
}
