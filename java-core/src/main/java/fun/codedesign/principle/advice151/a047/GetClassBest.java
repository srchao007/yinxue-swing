package fun.codedesign.principle.advice151.a047;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:07
 * @since 1.0.0
 */
@Notice("用getClass()进行比较是最好的，instanceof 可能出现判断父类的情况")
public class GetClassBest {
    public static void main(String[] args) {
        Son s = new Son();
        Person ps = new Son();
        if (s instanceof Person) {
            System.out.println("s is Person");
        }

        if (s instanceof Son) {
            System.out.println("s is Son");
        }

        if (s.getClass() == ps.getClass()) {
            System.out.println("s and ps is the same");
        }

        System.out.println("what is s ???");
    }
}

class Person {

}


class Son extends Person {

}