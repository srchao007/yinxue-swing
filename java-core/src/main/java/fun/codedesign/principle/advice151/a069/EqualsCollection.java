package fun.codedesign.principle.advice151.a069;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import fun.codedesign.principle.advice151.Notice;

/**
 * 集合判断只比较元素内容 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 11:41
 * @since 1.0.0
 */
@Notice("在集合判断时，只会判断其内容是否相等, map等容器均如此")
public class EqualsCollection {
    public static void main(String[] args) {
        Vector<String> vector = new Vector<>();
        vector.add("123");
        vector.add("456");
        vector.add("789");

        List<String> list = new ArrayList<>();
        list.add("123");
        list.add("456");
        list.add("789");

        System.out.println(list.equals(vector)); // true

    }
}
