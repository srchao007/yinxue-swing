package fun.codedesign.principle.advice151.a105;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * 数组的类加载 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-08 13:29
 * @since 1.0.0
 */
@Notice("数组对象无法直接通过ClassForName的方式构建，但是可以通过工具类Array来实现")
public class ClassForArray {

    @Test
    public void test105() throws Exception {
        int[] array = (int[]) Array.newInstance(int.class, 3);
        int[][] array2 = (int[][]) Array.newInstance(int.class, 2, 3);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(array2));
    }

    @Test
    public void test() throws ClassNotFoundException {
        Class.forName("[Ljava.lang.String;"); //加载一个String数组
        Class.forName("[J"); // Long[]
        Class.forName("[B"); // byte[]
        Class.forName("[C"); // char[]
        Class.forName("[D"); // Double[]
        Class.forName("[F"); // Float[]
        Class.forName("[I"); // Int[]
        Class.forName("[S"); // Short[]
        Class.forName("[Z"); // Boolean[]
    }
}
