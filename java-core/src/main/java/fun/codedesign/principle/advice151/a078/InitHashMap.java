package fun.codedesign.principle.advice151.a078;

import java.util.ArrayList;
import java.util.List;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 15:47
 * @since 1.0.0
 */
@Notice("hashMap需要注意其中的元素数量")
public class InitHashMap {
    /**
     * 建议78： 减少HashMap中元素的数量
     * HashMap按2倍长度进行扩展，比ArrayList的1.5倍更耗内存,其中多一个entry键值对，将更多占用内存
     * 模拟程序
     */
    public static void main(String[] args) {
        final Runtime runtime = Runtime.getRuntime();
        runtime.addShutdownHook(new Thread() {
            @Override
            public void run() {
                StringBuilder builder = new StringBuilder();
                long heapMaxSize = runtime.maxMemory() >> 20;
                long totalSize = runtime.totalMemory() >> 20;
                long free = runtime.freeMemory() >> 20;
                builder.append("最大内存大小:" + heapMaxSize + "M\n")
                        .append("对内存大小:" + totalSize + "M\n")
                        .append("空闲内存大小:" + free + "M\n");
                System.out.println(builder.toString());
            }
        });
        // Map<String, String> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 393217; i++) {
            // map.put("key" + i, "value" + i);
            list.add("key" + i);
        }

        /**
         *  map 数组
         *  最大内存大小:1751M
         *  堆内存大小:149M
         *  空闲内存大小:85M
         */


        /**
         *  list 数组
         *  最大内存大小:1751M
         *  堆内存大小:118M
         *  空闲内存大小:89M
         */
    }
}
