package fun.codedesign.principle.advice151.a077;

import java.io.Serializable;

public class City implements Serializable, Comparable<City> {

    private static final long serialVersionUID = -7254380365278898491L;

    private String name;

    public City(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(City o) {
        return name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        return "City [name=" + name + "]";
    }

}
