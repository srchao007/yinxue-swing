package fun.codedesign.principle.advice151.a075;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 15:33
 * @since 1.0.0
 */
public class City implements Comparable<City> {

    private String code;
    private String name;

    public City(String code, String name) {
        this.code = code;
        this.name = name;
    }


    @Override
    public int compareTo(City o) {
        return new CompareToBuilder().append(code, o.code).toComparison();
        //return new CompareToBuilder().append(name, o.name).toComparison();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return new EqualsBuilder().append(code, city.code).isEquals();
    }

    @Override
    public int hashCode() {
        int result = code.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
