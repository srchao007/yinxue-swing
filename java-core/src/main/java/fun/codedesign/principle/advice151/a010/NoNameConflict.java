package fun.codedesign.principle.advice151.a010;

import fun.codedesign.principle.advice151.Bad;

public class NoNameConflict {

    @Bad("与静态导入的math冲突")
    private static final String PI = "HELLO"; // 覆盖了静态导入的Math.PI

    public static void main(String[] args) {
        System.out.println(PI); // HELLO  编译器最短路径查找，在本类中能找到，就不会在其他包或者父类中查找
    }
}
