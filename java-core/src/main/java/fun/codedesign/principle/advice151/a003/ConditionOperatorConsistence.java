package fun.codedesign.principle.advice151.a003;

import org.junit.Assert;
import org.junit.Test;

import fun.codedesign.principle.advice151.Bad;

public class ConditionOperatorConsistence {

    @Test
    public void testOperator() {
        int i = 80;
        String right = String.valueOf(i > 60 ? 100 : 10);

        @Bad("在条件判断式中用同样的变量类型")
        String wrong = String.valueOf(i > 60 ? 100.0 : 10);

        Assert.assertEquals("100", right);
        Assert.assertEquals("100.0", wrong);
    }
}
