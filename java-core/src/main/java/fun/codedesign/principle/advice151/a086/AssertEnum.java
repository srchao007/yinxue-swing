package fun.codedesign.principle.advice151.a086;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:50
 * @since 1.0.0
 */
@Notice("可以在enum的switch最后加一个断言，表示逻辑不要走到那里去")
public class AssertEnum {

}
