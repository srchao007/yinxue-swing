package fun.codedesign.principle.advice151.a024;

import java.util.Scanner;

public class NoticeBound {

    public static void main(String[] args) {

        try(Scanner sc = new Scanner(System.in)){
            while (sc.hasNextInt()) {
                int order = sc.nextInt();
                if (order > 0 && order + 1000 < 2000) { // note:次数order如果输入为Integer的最大值2147483647，依然会订购成功
                    System.out.println("新订购" + order + "件");
                } else {
                    System.out.println("超过范围，订购失败");
                }
            }
        }
    }
}
