package fun.codedesign.principle.advice151.a056;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:53
 * @since 1.0.0
 */
@Notice("三种拼接方法")
public class StringConcat {
    public static void main(String[] args) {
        String a = "123" + "35555hello";
        String b = a.concat("123123213");
        System.out.println(b);

        StringBuilder sb = new StringBuilder("123");
        sb.append("hello1111hello").toString();
    }
}
