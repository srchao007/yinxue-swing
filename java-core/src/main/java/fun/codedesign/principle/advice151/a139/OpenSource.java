package fun.codedesign.principle.advice151.a139;

/**
 * 开源代码 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 11:54
 * @since 1.0.0
 */
public class OpenSource {

    // 建议139： 大胆采用开源工具
    /*
     * MVC: structs2,springMVC,WebWorker
     * IoC容器：spring googleguice
     * ORM: Mybatis hibernate
     * 工具类：Apache Commons
     */
}
