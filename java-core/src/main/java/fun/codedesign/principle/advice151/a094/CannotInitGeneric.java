package fun.codedesign.principle.advice151.a094;

import java.util.ArrayList;
import java.util.List;

import fun.codedesign.principle.advice151.Notice;

/**
 * 不能初始化泛型 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 17:40
 * @since 1.0.0
 */
@Notice("无法new初始化泛型，因为编译器会被擦除")
public class CannotInitGeneric {


}

@SuppressWarnings("all")
class Generic<T> {
    // 无法new初始化，因为编译器会被擦除
//    private T t = new T();
//    private T[] tArray = new T[5];
    private List<T> list = new ArrayList<T>();
}
