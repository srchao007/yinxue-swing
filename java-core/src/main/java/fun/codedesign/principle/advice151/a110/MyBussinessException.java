package fun.codedesign.principle.advice151.a110;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 19:35
 * @since 1.0.0
 */
public class MyBussinessException extends RuntimeException {
    private static final long serialVersionUID = -371853109139390380L;

    public MyBussinessException(Throwable cause) {
        super(cause);
    }
}
