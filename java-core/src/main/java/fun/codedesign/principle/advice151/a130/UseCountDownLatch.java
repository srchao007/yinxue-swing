package fun.codedesign.principle.advice151.a130;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.util.concurrent.CountDownLatch;

/**
 * 使用CountDownLatch来协调子程序 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 21:01
 * @since 1.0.0
 */
@Notice
public class UseCountDownLatch {

    @Test
    public void test() throws InterruptedException {

        final CountDownLatch latch = new CountDownLatch(2);
        new Thread() {
            @Override
            public void run() {
                System.out.println("线程1");
                latch.countDown();
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                System.out.println("线程2");
                latch.countDown();
            }
        }.start();
        latch.await();
    }
}
