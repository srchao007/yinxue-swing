package fun.codedesign.principle.advice151.a002;

import fun.codedesign.principle.advice151.Bad;

public class NoFinalVariable {

    @Bad("不要让final值不再final")
    private static final double wrong = Math.random();

}
