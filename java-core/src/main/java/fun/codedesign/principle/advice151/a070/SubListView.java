package fun.codedesign.principle.advice151.a070;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.util.ArrayList;
import java.util.List;

/**
 * 子列表只是原列表的一个视图 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 11:55
 * @since 1.0.0
 */
@Notice("关联的是同样的对象")
public class SubListView {

    // 建议70： 子列表只是原列表的一个视图
    @Test
    public void test70() {
        List<String> c = new ArrayList<>(3);
        c.add("A");
        c.add("B");

        List<String> c1 = new ArrayList<>(c);
        System.out.println(c == c1); // false
        System.out.println(c.equals(c1)); // true

        List<String> c2 = c.subList(0, c.size());

        System.out.println(c2.equals(c)); // true
        System.out.println(c2 == c);  // false
        System.out.println(c2.toString());

        c2.add("B"); // 修改会反映在原列表上，本列表也会变化
        System.out.println(c.toString()); // A B B
        System.out.println(c2.toString()); // A B B
    }
}
