package fun.codedesign.principle.advice151.a035;

import fun.codedesign.principle.advice151.Bad;

/**
 * 不要循环引用 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:37
 * @since 1.0.0
 */
@Bad("不要在构造函数中初始化其他类，造成比如循环引用，内存溢出")
public class NoReferenceCircle {
    
    @SuppressWarnings("all")
    public static void main(String[] args) {
        Aoo aoo = new Aoo(); // StackOverflowError 栈溢出
    }
}

class Aoo {

    Boo b;

    public Aoo() {
        b = new Boo();
    }
}

class Boo {
    Coo c;

    public Boo() {
        c = new Coo();
    }
}

class Coo {
    Aoo a;

    public Coo() {
        a = new Aoo();
    }
}