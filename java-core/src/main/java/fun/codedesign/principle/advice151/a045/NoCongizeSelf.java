package fun.codedesign.principle.advice151.a045;

import fun.codedesign.principle.advice151.Notice;

/**
 * 避免equals方法不能识别出自己 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:02
 * @since 1.0.0
 */
@Notice("避免写equals方法无法识别出自己的情况")
public class NoCongizeSelf {
}

/**
 * 建议45：覆写equals方法时候不要识别不出自己，以下的equals方法就会出现识别不出自己的问题。去掉trim()即可
 */
class Person {

    private String name;

    public Person() {
    }

    public Person(String name) {
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Person) {
            Person p = (Person) obj;
            return this.name.equalsIgnoreCase(p.getName().trim());
        }
        return false;
    }


}
