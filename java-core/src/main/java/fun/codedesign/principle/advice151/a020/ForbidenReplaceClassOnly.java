package fun.codedesign.principle.advice151.a020;

public class ForbidenReplaceClassOnly {
    // 在web的发布过程中，如果一个 constant类文件中的常量发生了改变，此时如果只是替换constant类文件，
    // 那么实际应用中的 final static 可能不会发生变化，因为编译器不会重新编译该值
    // 所以最好重新打一个war包然后再发布
}
