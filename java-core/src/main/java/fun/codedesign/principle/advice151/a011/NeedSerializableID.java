package fun.codedesign.principle.advice151.a011;

import java.io.Serializable;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 15:53
 * @since 1.0.0
 */
@Notice("显示声明UUID，避免版本问题")
public class NeedSerializableID implements Serializable {
    private static final long serialVersionUID = 8847813139254397050L;
}
