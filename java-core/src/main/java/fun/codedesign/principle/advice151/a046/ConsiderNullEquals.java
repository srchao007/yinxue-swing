package fun.codedesign.principle.advice151.a046;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:05
 * @since 1.0.0
 */
@Notice("equals的时候可能会出现null的情况，所以需要先进行判空")
public class ConsiderNullEquals {
    public static void main(String[] args) {
        String s = null;
        // 先判空，在对比字符串
        if (s == null || s.equalsIgnoreCase("hello")) {
            System.out.println("true");
        }
    }
}
