package fun.codedesign.principle.advice151.a129;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 设置阻塞队列防止请求堆积 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:54
 * @since 1.0.0
 */
@Notice
public class SetBlockingQueue {
    // 建议129： 适当设置阻塞队列长度
    @Test
    public void test129() throws Exception {
        // 阻塞队列不能设置长度，一下会出现非法状态一场Queue full
        BlockingQueue<String> bq = new ArrayBlockingQueue<String>(5);
        int count = 0;
        //
        while (true) {
            bq.add("123"); // add满了会抛出异常，如果是采用offer方法会在满了后返回false
            // 而如果采用put则会一致阻塞，直到有空位置进行put,对应的方法是remove poll 和take方法
            count++;
            if (count == 6) {
                break;
            }
        }
    }
}
