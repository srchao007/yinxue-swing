package fun.codedesign.principle.advice151.a001;


import fun.codedesign.principle.advice151.Bad;
import fun.codedesign.principle.advice151.Good;

public class NoUseConfusingCharacter {

    @Good
    private long right = 26L;

    @Bad("不用用容易混淆的缩写 l")
    private long wrong = 26l;

}
