package fun.codedesign.principle.advice151.a101;

import fun.codedesign.principle.advice151.Notice;

/**
 * 取得class类 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-05 20:23
 * @since 1.0.0
 */
@SuppressWarnings("all")
@Notice("三种方式")
public class AllocateClass {
    public void main(String[] args) throws ClassNotFoundException {
        Class clazz = AllocateClass.class;
        Class clazz2 = this.getClass();
        Class clazz3 = Class.forName("principle.advice151.a101.AllocateClass");
    }
}
