package fun.codedesign.principle.advice151.a018;

import java.util.Date;

public class UnexpectedInstanceOf {

    @SuppressWarnings("all")
    public static void main(String[] args) {
        // true
        boolean b1 = "String" instanceof Object;
        // true
        boolean b2 = new String() instanceof Object;
        // true
        boolean b3 = new String() instanceof String;
        // false
        boolean b4 = new Object() instanceof String;
        // no compile
        // boolean b5 = 'A' instanceof Character;
        // false
        boolean b6 = null instanceof String;
        // note: false  null没有类型，转换后还是null
        boolean b7 = (String) null instanceof String;
        // no compile
        // boolean b8 = new Date() instanceof String;
        // note:compile but false  T在编译期因为是泛型所以表面类型是Object,
        // 运行期实际类型为String，String和Date没有继承关系
        boolean b9 = new Generic<String>().isDate("");
    }
}

class Generic<T> {
    public boolean isDate(T t) {
        return t instanceof Date;
    }
}
