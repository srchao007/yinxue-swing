package fun.codedesign.principle.advice151.a106;

import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 反射及代理类 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-08 14:57
 * @since 1.0.0
 */
public class ReflectProxy {

    @Test
    public void test() {
        Subject subject = new SubjectImpl();
        SubjectHandler hanlder = new SubjectHandler(subject);
        ClassLoader c1 = subject.getClass().getClassLoader();
        Subject proxy = (Subject) Proxy.newProxyInstance(c1, subject.getClass().getInterfaces(), hanlder);
        proxy.request();
    }
}


class SubjectHandler implements InvocationHandler {

    private Subject subject;

    public SubjectHandler(Subject subject) {
        this.subject = subject;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {
        // 代理方法前
        System.out.println("qianqianqinqin");
        Object obj = method.invoke(subject, args);
        // 方法实现后
        System.out.println("houhohouojo");
        return obj;
    }
}
