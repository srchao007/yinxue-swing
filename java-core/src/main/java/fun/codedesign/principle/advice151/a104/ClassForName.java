package fun.codedesign.principle.advice151.a104;

import org.junit.Test;

import fun.codedesign.principle.advice151.Bad;
import fun.codedesign.principle.advice151.Notice;

/**
 * 需要引入Reflection包 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-08 12:04
 * @since 1.0.0
 */
@Notice("典型的加载数据库驱动，驱动的静态代码块执行加入到DriverManager当中")
public class ClassForName {

    public static final String className = "org.javacollection.high.advice151.a104.ClassForName";

    @Test
    public void test() throws ClassNotFoundException {
        Class.forName(className);
        // System.out.println(Reflection.getCallerClass());
    }

    @Bad
    @Test(expected = ClassNotFoundException.class)
    public void test2() throws ClassNotFoundException {
        // classLoader不能为null
        Class.forName(className, false, null);
    }

    @Test
    public void test3() throws ClassNotFoundException {
        Class.forName(className, false, Thread.currentThread().getContextClassLoader());
    }

    @Test
    public void test4() throws ClassNotFoundException {
        Class.forName(className, true, this.getClass().getClassLoader());
    }
}
