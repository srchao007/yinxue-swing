package fun.codedesign.principle.advice151.a081;

import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:27
 * @since 1.0.0
 */
@Notice("排序要注意其内部原理，可能的话采用list")
public class MayBeList {
    // 建议81： 非稳定排序推荐使用List
    public static void main(String[] args) {
        PersonEntity p1 = new PersonEntity("张三", 123);
        PersonEntity p2 = new PersonEntity("张五", 176);
        // TreeSet添加时默认升序排列,如果修改了不会再重新排序，需要重新new一个TreeSet
        SortedSet<PersonEntity> set = new TreeSet<>();
        set.add(p1);
        set.add(p2);
        p2.setHeight(100);
        for (PersonEntity entity : set) {
            System.out.println("高度" + entity.getHeight());
        }
        // set2是set的浅拷贝，不会重新排序，需要new一个arrayList进去重新排序
        SortedSet<PersonEntity> set2 = new TreeSet<>(set);
        for (PersonEntity entity : set2) {
            System.out.println("高度" + entity.getHeight());
        }
        // 方法1： newclass ArrayList重排set
        SortedSet<PersonEntity> set3 = new TreeSet<>(new ArrayList<>(set));
        for (PersonEntity entity : set3) {
            System.out.println("高度" + entity.getHeight());
        }

        // 方法2：Collectons.sort(collection)即可
        // 即要保持集合中的元素单独，又需要排序，可以先转set 然后再转回list排序
    }
}
