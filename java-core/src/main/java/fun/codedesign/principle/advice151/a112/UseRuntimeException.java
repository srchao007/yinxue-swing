package fun.codedesign.principle.advice151.a112;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 19:41
 * @since 1.0.0
 */
@Notice("尽量将受检异常转换为非受检")
public class UseRuntimeException {

    @Test
    public void test112() throws IOException {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(".");
        } catch (FileNotFoundException e) {
            throw new RuntimeException("找不到该文件");
        } finally {
            if (fileInputStream != null) {
                fileInputStream.close();
            }
        }
    }
}
