package fun.codedesign.principle.advice151.a106;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-08 15:00
 * @since 1.0.0
 */
public class SubjectImpl implements Subject {
    @Override
    public void request() {
        System.out.println("我是一个粉刷匠");
    }
}
