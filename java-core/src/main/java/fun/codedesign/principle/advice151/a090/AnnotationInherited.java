package fun.codedesign.principle.advice151.a090;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.lang.annotation.*;

enum BirdProducer {
    BirdX;

    public Bird birdProducer() {
        Desc bd = BirdX.class.getAnnotation(Desc.class);
        return bd == null ? new BirdX() : new BirdX(bd.c());
    }
}


enum Color {
    WHITE, GREEN;
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
        // 继承特性
@interface Desc {
    Color c() default Color.WHITE;
}

/**
 * 注解的继承特性 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 17:09
 * @since 1.0.0
 */
@Notice("注意注解可以自动继承的情况")
public class AnnotationInherited {

    @Test
    public void test90() throws Exception {
        Bird bird = BirdProducer.BirdX.birdProducer();
        System.out.println(bird.getColor());
    }
}

@Desc(c = Color.GREEN)
abstract class Bird {
    public abstract Color getColor();
}

class BirdX extends Bird {
    private Color color;

    public BirdX() {
        color = Color.GREEN;
    }

    public BirdX(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }
}
