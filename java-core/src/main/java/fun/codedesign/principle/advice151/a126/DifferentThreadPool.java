package fun.codedesign.principle.advice151.a126;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 不同线程池的作用 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:45
 * @since 1.0.0
 */
public class DifferentThreadPool {

    // 建议126: 适时选择不同的线程池来实现
    @SuppressWarnings("all")
    @Test
    public void test126() throws Exception {
        ExecutorService es1 = Executors.newSingleThreadExecutor();
        ExecutorService es2 = Executors.newFixedThreadPool(10);
        ExecutorService es3 = Executors.newCachedThreadPool();

        // 旗舰版
        // ThreadPoolExecutor tpe = new ThreadPoolExecutor(
        // corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue)
    }
}
