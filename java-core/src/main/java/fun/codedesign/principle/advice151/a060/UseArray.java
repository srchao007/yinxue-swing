package fun.codedesign.principle.advice151.a060;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:59
 * @since 1.0.0
 */
@Notice("使用数组，它的性能最高")
public class UseArray {
    /**
     * 建议60：性能考虑，数组是首选
     * 同样的int数组元素相加，数组效率大概是list的10倍，因为list有拆箱装箱的过程
     * 性能要求高的地方请使用数组
     */
}
