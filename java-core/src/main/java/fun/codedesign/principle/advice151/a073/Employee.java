package fun.codedesign.principle.advice151.a073;


import org.apache.commons.lang3.builder.CompareToBuilder;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 14:22
 * @since 1.0.0
 */
public class Employee implements Comparable<Employee> {
    private int num;
    private String name;
    private Position position;

    public Employee(int num, String name, Position position) {
        this.num = num;
        this.name = name;
        this.position = position;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public int compareTo(Employee o) {
        return new CompareToBuilder().append(num, o.num).toComparison();
    }
}
