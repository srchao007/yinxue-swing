package fun.codedesign.principle.advice151.a084;

import fun.codedesign.principle.advice151.Good;

enum Season {
    SPRING("春"), SUMMER("夏"), AUTUMN("秋"), WINTER("冬");

    String desc;

    Season(String desc) {
        this.desc = desc;
    }
}

/**
 * 使用构造器构造枚举类 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:35
 * @since 1.0.0
 */
@Good
public class UseEnumConstrutor {
}


