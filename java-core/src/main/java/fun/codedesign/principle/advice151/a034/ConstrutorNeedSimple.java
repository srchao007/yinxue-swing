package fun.codedesign.principle.advice151.a034;

import fun.codedesign.principle.advice151.Bad;
import fun.codedesign.principle.advice151.Notice;

/**
 * 构造函数尽量简单 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:26
 * @since 1.0.0
 */
@Bad("构造函数应该尽量简单")
public class ConstrutorNeedSimple {
    public static void main(String[] args) {
        Server server = new SimpleServer(); // 这里的端口号是0
        System.out.println(server.getPort());
        Server server1 = new SimpleServer(1234);
        System.out.println(server1.getPort());
    }
}

/**
 * 建议34：构造函数尽量简化，见_test类, 由于实例时候是先初始化父类变量和构造，再初始子类变量，在还没有初始赋值子类变量port的情况下，父类构造初始时候得到的port可能是0或者40000
 * 修改的办法就是如下，通过提取到一个相应的成员方法来，启动该服务
 */
abstract class Server {
    public final static int defaultPort = 1000;

    public Server() {
        int port = getPort();
        System.out.println("端口号是" + port);
    }

    protected abstract int getPort();

    public void start() {
        int port = getPort();
        System.out.println("端口号是" + port);
    }
}

class SimpleServer extends Server {

    @Notice("这个100值是在构造函数结束之后才赋值的，默认是0")
    private int port = 100;

    public SimpleServer() {
    }

    @Notice("这里会隐式的调用父类无参构造函数")
    public SimpleServer(int _port) {
        port = _port;
    }

    @Override
    protected int getPort() {
        return Math.random() > 0.5 ? port : 40000;
    }
}
