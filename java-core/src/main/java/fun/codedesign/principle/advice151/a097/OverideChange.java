package fun.codedesign.principle.advice151.a097;

import fun.codedesign.principle.advice151.Notice;

/**
 * 协变和逆变 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 19:38
 * @since 1.0.0
 */
@Notice("协变：宽类型变窄类型，逆变：窄类型变宽类型")
public class OverideChange {

    // 重写方法可以协变，但不可以逆变，否则就不是重写，而是重载
    // 数组可以协变，但泛型不可以
    @SuppressWarnings("all")
    public static void main(String[] args) {

        Number[] numbers = new Integer[3];

        // List<Number> list = new ArrayList<Integer>(); // 编译不通过
    }


}

class Parent {
    public Number hello(Number num) {
        System.out.println(num);
        return num;
    }
}

class son extends Parent {

    // 返回值变窄，返回协变
    @Override
    public Integer hello(Number num) {
        return (Integer) num;
    }

    // 重载
    public Double hello(Double num) {
        return num;
    }
}
