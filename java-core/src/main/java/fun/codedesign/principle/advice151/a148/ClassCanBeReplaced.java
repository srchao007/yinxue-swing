package fun.codedesign.principle.advice151.a148;

/**
 * 类可以被替换 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 14:16
 * @since 1.0.0
 */
public class ClassCanBeReplaced {
    // 建议148： 增强类的可替换性
}
