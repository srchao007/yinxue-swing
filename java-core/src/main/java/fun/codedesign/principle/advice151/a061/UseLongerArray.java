package fun.codedesign.principle.advice151.a061;

import org.junit.Test;

import fun.codedesign.principle.advice151.Good;

import java.util.Arrays;

/**
 * 使用更长的数组 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 20:01
 * @since 1.0.0
 */
@Good("使用更长数组")
public class UseLongerArray {

    /**
     * 建议61： 若有必要，使用变长数组
     */
    @Test
    public void test61() {
        String[] arr = new String[60];
        String[] arr2 = Arrays.copyOf(arr, 80);
        System.out.println(Arrays.toString(arr2));
        System.out.println(arr2.length);
        System.out.println(arr2.getClass() == arr.getClass());
        System.out.println(arr2 == arr);

        // 系统级拷贝
        System.arraycopy(arr, 0, arr2, 0, 100);
        // 越界异常，因为需要先新建arr2为更长数组然后记性填充
        System.out.println(arr2.length);
    }
}
