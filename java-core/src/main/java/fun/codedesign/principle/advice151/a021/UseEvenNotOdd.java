package fun.codedesign.principle.advice151.a021;

public class UseEvenNotOdd {
    // 用偶数作结果判断，而不要用奇数
    public static void main(String[] args) {

        // wrong: 余数1 判断奇数，如果是负数会有问题
        for (int i = -2; i < 3; i++) {
            String str = i % 2 == 1 ? "奇数" : "偶数";
            System.out.println(i + "是" + str);
        }

        System.out.println("-------");

        // right：余数0 判断为偶数，其他为奇数
        for (int i = -2; i < 3; i++) {
            String str = i % 2 == 0 ? "偶数" : "奇数";
            System.out.println(i + "是" + str);
        }

        System.out.println(-1 / 2); //0
        System.out.println(mod(-1, 2)); //-1 != 1 所以不能用奇数判断
    }


    /**
     * 模拟java取余过程
     *
     * @param dividend 被除数
     * @param divisor  除数
     * @return 余数
     */
    public static int mod(int dividend, int divisor) {
        return dividend - dividend / divisor * divisor;
    }


}
