package fun.codedesign.principle.advice151.a027;

public class NoticeCompareOfBox {

    public static void main(String[] args) {
        Integer a = new Integer(100);
        Integer b = new Integer(200);
        System.out.println(a < b); // 此处比较的是Integer的intValue方法返回的值，<>只能比较两端大小的值

        // 正确的比较方式应该是用CompareTo，相等返回0 大于返回1 小于返回-1
        System.out.println(b.compareTo(a));
    }
}
