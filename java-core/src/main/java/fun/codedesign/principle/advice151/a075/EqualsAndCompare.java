package fun.codedesign.principle.advice151.a075;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fun.codedesign.principle.advice151.Notice;

/**
 * 比较区别 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 15:30
 * @since 1.0.0
 */
@Notice("compareTo方法和equals的比较条件需要保持一致")
public class EqualsAndCompare {
    // 建议75： 集合中的元素必须做到compareTo和equals同步
    // indexOf采用的是 equals比较
    // 二分查找使用的是compareTo比较，

    public static void main(String[] args) {
        List<City> list = new ArrayList<>();
        list.add(new City("021", "上海"));
        list.add(new City("021", "沪"));
        Collections.sort(list);

        City city = new City("021", "沪");
        int index = list.indexOf(city);
        int binaryIndex = Collections.binarySearch(list, city);
        System.out.println(index == binaryIndex);

    }
}
