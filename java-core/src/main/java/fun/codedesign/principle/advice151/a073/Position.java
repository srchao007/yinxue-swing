package fun.codedesign.principle.advice151.a073;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 14:23
 * @since 1.0.0
 */
public enum Position {
    Boss, Manager, Staff;
}
