package fun.codedesign.principle.advice151.a149;

/**
 * 依赖抽象而非具体实现 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 14:16
 * @since 1.0.0
 */
public class DependAbstract {
    // 建议149： 依赖抽象而不是事先
}
