package fun.codedesign.principle.advice151.a087;

enum Season {

    Spring, Summer, Autumn, Winter;

    public static boolean contain(String name) {
        for (Season s : Season.values()) {
            if (s.name() == name) {
                return true;
            }
        }
        return false;
    }
}


