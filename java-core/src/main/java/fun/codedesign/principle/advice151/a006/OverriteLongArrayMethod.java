package fun.codedesign.principle.advice151.a006;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 15:47
 * @since 1.0.0
 */
public class OverriteLongArrayMethod {

    @Test
    @Notice("继承的方法有所不同")
    public void test4() {
        Base base = new Sub();
        // base对sub对象进行了向上转型
        base.fun(100, 80); //调用子类方法
        Sub sub = new Sub();
        // sub未做向上转型，传入的80 就是int类型，无法转换成数组引用
        // sub.fun(100, 80);
        Base sub2 = sub;
        sub2.fun(100, 80); // 调用子类方法
    }

    class Base {
        public void fun(int i, int... j) {
            System.out.println("父类方法");
        }
    }

    class Sub extends Base {
        // 覆写方法时候包括显式形式也应该一样，才不会报警惕
        @Override
        public void fun(int i, int[] j) {
            System.out.println("子类方法");
        }
    }
}
