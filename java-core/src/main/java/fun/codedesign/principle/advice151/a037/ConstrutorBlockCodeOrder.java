package fun.codedesign.principle.advice151.a037;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:44
 * @since 1.0.0
 */
@Notice("注意代码块的执行顺序")
public class ConstrutorBlockCodeOrder {
    public static void main(String[] args) {
        Parent parent = new Parent();
        System.out.println("------------" + parent);

        Parent pSon = new Son();
        System.out.println("----------" + pSon);

        Son sonName = new Son("name"); // 构造代码块在super执行完成后再执行
        System.out.println("----------" + sonName);

        Son sonAge = new Son(123);
        System.out.println("----------" + sonAge);
    }
}

/**
 * 建议37：构造代码会想你所想，如果有参构造器用this调用无参构造，则构造代码不会执行两遍，只会在无参中执行一遍
 * 而如果是super，则构造代码将在super()后面进行执行，和this() 稍有不同
 */
class Parent {
    {
        System.out.println("*** 父类构造代码块 ***");
    }

    public Parent() {
        System.out.println("*** 父类构造器 ***");
    }
}

class Son extends Parent {
    {
        System.out.println("--- 子类构造代码块 ---");
    }

    public Son() {
        System.out.println("--- 子类构造器 ---");
    }

    public Son(String name) {
        super();
    }

    public Son(int age) {
        this();
    }
}

