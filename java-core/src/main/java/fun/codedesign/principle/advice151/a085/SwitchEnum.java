package fun.codedesign.principle.advice151.a085;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

/**
 * 注意Switch时候的null情况 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:47
 * @since 1.0.0
 */
@Notice
public class SwitchEnum {

    @Test
    public void test85() {
        class InnerClass2 {
            public void getComfortableSeason(Season s) {
                switch (s) {
                    case Spring:
                        System.out.println(Season.Spring);
                        break;
                    case Summer:
                        System.out.println(Season.Summer);
                        break;
                    case Autumn:
                        System.out.println(Season.Autumn);
                        break;
                    case Winter:
                        System.out.println(Season.Winter);
                        break;
                    default:
                        System.out.println("输入错误");
                        break;
                }
            }
        }
        new InnerClass2().getComfortableSeason(Season.Spring);
        new InnerClass2().getComfortableSeason(null); // 空指针异常，enum的判断是先转换为整型
    }

}
