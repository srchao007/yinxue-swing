package fun.codedesign.principle.advice151.a081;

import org.apache.commons.lang3.builder.CompareToBuilder;


public class PersonEntity implements Comparable<PersonEntity> {

    private String name;
    private int height;

    public PersonEntity(String name, int height) {
        super();
        this.name = name;
        this.height = height;
    }


    @Override
    public int compareTo(PersonEntity o) {
        return new CompareToBuilder().append(height, o.getHeight()).
                toComparison();
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public int getHeight() {
        return height;
    }


    public void setHeight(int height) {
        this.height = height;
    }

}
