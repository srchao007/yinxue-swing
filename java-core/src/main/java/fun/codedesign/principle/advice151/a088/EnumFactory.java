package fun.codedesign.principle.advice151.a088;

import org.junit.Test;

import fun.codedesign.principle.advice151.Good;

enum CarEnum {
    FordCar, BuckerCar;

    public Car createCar() {
        switch (this) {
            case FordCar:
                return new FordCar();
            case BuckerCar:
                return new BuckerCar();
            default:
                throw new AssertionError("不要到这儿来");
        }
    }
}

/**
 * 每一个枚举可以对抽象类方法进行单独的实现
 */
enum CarEnumFactory {
    FordCar {
        @Override
        Car create() {
            return new FordCar();
        }
    },
    BuckerCar {
        @Override
        Car create() {
            return new BuckerCar();
        }
    };

    abstract Car create();
}

interface Car {
    String getName();
}

/**
 * 枚举工厂 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:59
 * @since 1.0.0
 */
@Good
public class EnumFactory {

    
    @Test
    @SuppressWarnings("all")
    public void test88_01() throws Exception {
        Car car = CarFactory.create(FordCar.class);
        System.out.println(car.getName());

        Car car1 = CarEnum.BuckerCar.createCar();
        Car car2 = CarEnum.FordCar.createCar();

        Car car3 = CarEnumFactory.BuckerCar.create();
        Car car4 = CarEnumFactory.FordCar.create();
    }

    static class CarFactory {
        static Car create(Class<? extends Car> car) {
            try {
                return (Car) car.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

class BuckerCar implements Car {
    @Override
    public String getName() {
        return "BuckerCar";
    }
}

class FordCar implements Car {
    @Override
    public String getName() {
        return "FordCar";
    }
}
