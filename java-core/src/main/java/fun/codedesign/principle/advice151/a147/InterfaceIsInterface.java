package fun.codedesign.principle.advice151.a147;

/**
 * 接口功能保持单一 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 14:16
 * @since 1.0.0
 */
public class InterfaceIsInterface {
    // 建议147： 让接口的职责保持单一
}
