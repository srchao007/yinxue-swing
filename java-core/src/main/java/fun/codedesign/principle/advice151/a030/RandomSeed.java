package fun.codedesign.principle.advice151.a030;

import org.junit.Test;

import fun.codedesign.principle.advice151.Bad;

import java.util.Random;

/**
 * 随机种子问题 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:09
 * @since 1.0.0
 */
@Bad("随机种子设定后在固定机器上生成的随机数是固定的，所以需要慎重使用")
public class RandomSeed {
    // 建议30： 不要随便设计随机种子
    @Test
    public void test30() {
        // 每次都会出现相同的随机数
        Random rd = new Random(1000);
        for (int i = 0; i < 4; i++) {
            System.out.println(rd.nextInt());
        }
    }
}
