package fun.codedesign.principle.advice151.a033;

import fun.codedesign.principle.advice151.Bad;

/**
 * 不要覆写静态方法 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:19
 * @since 1.0.0
 */
@Bad("不要覆写静态方法")
public class NotOverrideStatic {
    
    @SuppressWarnings("all")
    public static void main(String[] args) {
        Parent son = new Son();
        son.doAnyThing();
        son.doSomething();

        Son son2 = (Son) son;
        son2.doAnyThing();
        son2.doSomething();
    }
}

/**
 * 建议33： 不要覆写父类的静态方法，实际上是可以覆写的，但不会有覆写标志，会隐藏，
 * 这样父类引用new 子对象时，静态方法会父类的，而普通方法会调子类的，但用子类引用来new时就可以都调用子类的
 * 测试代码 见以上main方法
 */
class Parent {
    public static void doSomething() {
        System.out.println("---父类静态方法---");
    }

    public void doAnyThing() {
        System.out.println("---父类普通方法---");
    }
}

class Son extends Parent {
    public static void doSomething() {
        System.out.println("***子类静态方法***");
    }

    @Override
    public void doAnyThing() {
        System.out.println("***子类普通方法***");
    }
}
