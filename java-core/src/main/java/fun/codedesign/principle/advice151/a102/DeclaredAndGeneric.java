package fun.codedesign.principle.advice151.a102;

import fun.codedesign.principle.advice151.Notice;

/**
 * 反射取得的普通方法和Declared方法 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-05 20:29
 * @since 1.0.0
 */
@SuppressWarnings("all")
@Notice("Declared可以取得所有不论公开还是私有的方法，普通的只能取到public方法")
public class DeclaredAndGeneric {

    public static void main(String[] args) {
        Class parent = Deprecated.class.getSuperclass(); // 取得父类
    }
}
