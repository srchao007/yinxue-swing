package fun.codedesign.principle.advice151.a082;

import fun.codedesign.principle.advice151.Notice;

/**
 * 注意java的各个集合类 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:29
 * @since 1.0.0
 */
@Notice
public class AllCollection {

    // 建议82： 由点及面，一叶知秋————集合大家族
    /*
     * 1. List: ArrayList(动态数组)、Vector(安全动态数组)、linkedList(双向链表)、stack(对象栈，先进后出)
     * 2. Set：EnumSet(枚举类)、HashSet(hashCode定下标)、TreeSet(实现SortedSet接口自动排序)
     * 3.Map:TreeMap(自动key排序Map)、HashMap(常规)、HashTable(线程安全)、Properties(hashTable子类
     * )、EnumMap(key为枚举)
     * 4.Queue：ArrayBlockingQueue(有界阻塞队列数组实现)、PriorityBlockingQueue
     * (优先级组建队列)、LinkedBlockingQueue(链表实现阻塞队列) PriorityQueue(无界非阻塞类队列)、ArrayDeque
     * 、LinkedBlockingDeque、LinkedList(双端队列实现类，支持头尾两端插入和移除)
     * 5.数组:数组与集合最大区别是能容纳基本类型，集合底层就是数组
     * 6.工具类： Arrays、Conllections
     * 7.扩展类：commons-collections google-collections
     */
}
