package fun.codedesign.principle.advice151.a135;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

/**
 * 定义标准 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 11:38
 * @since 1.0.0
 */
@Notice("比如QPS,TPS等数据值")
public class Standard {
    // 建议135： 必须定义性能优化的衡量标准
    /*
     * 1. 性能衡量 --> 业务目标，即技术与业务的之间的契约
     * 2. 技术优化的目标，可读性与优化的指标平衡
     */
    @SuppressWarnings("all")
    @Test
    public void testName() throws Exception {

        long time = System.nanoTime();
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            int a = 10 * 16;
        }
        long time1 = System.nanoTime();

        // 此处位运算特别的快
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            int b = 10 << 4;
        }
        long time2 = System.nanoTime();

        System.out.println(time);
        System.out.println(time1);
        System.out.println(time2);
        System.out.println(time1 - time);
        System.out.println(time2 - time1);
    }
}
