package fun.codedesign.principle.advice151.a091;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.lang.annotation.*;

enum CommonIdentifier implements Identifier {
    Reader, Author, Admin;

    @Override
    public boolean indentify() {
        return false;
    }
}


interface Identifier {
    String REFUSE_WORD = "没有权限";

    boolean indentify();
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
@interface Access {
    CommonIdentifier level() default CommonIdentifier.Admin;
}

/**
 * 注解和枚举的结合，比如鉴权系统 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 17:28
 * @since 1.0.0
 */
@Notice
public class EnumAndAnnotation {

    @Test
    @SuppressWarnings("all")
    public void test91() throws Exception {
        Foo foo = new Foo();
        Access access = foo.getClass().getAnnotation(Access.class);
        if (access == null || !access.level().indentify()) {
            System.out.println(access.level().REFUSE_WORD);
        }
        System.out.println(access);
    }
}

@Access(level = CommonIdentifier.Admin)
class Foo {

}
