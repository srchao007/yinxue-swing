package fun.codedesign.principle.advice151.a022;

import java.math.BigDecimal;

public class UseIntForCurrency {
    /**
     * <p>浮点数计算会存在精度问题，所以货币运算应该用整型</p>
     * {@link BigDecimal}
     */
    public static void main(String[] args) {
        // Q: 存在精度问题，无限接近结果，但不等于结果
        System.out.println(10.00 - 9.60); //0.40000000000000036

        // A1: 使用 BigDecimal
        BigDecimal a1 = new BigDecimal("10.00");
        BigDecimal a2 = new BigDecimal("9.60");
        BigDecimal result = a1.subtract(a2);
        System.out.println(result);

        // A2: 使用100倍乘法,然后再对结果除以100
        System.out.println((10.00 * 100 - 9.60 * 100) / 100);

    }
}
