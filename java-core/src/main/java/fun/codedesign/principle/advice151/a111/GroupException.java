package fun.codedesign.principle.advice151.a111;

import java.util.List;

public class GroupException extends Exception {
    private static final long serialVersionUID = 7633550430645185751L;
    private List<Throwable> list;

    public GroupException(List<Throwable> list) {
        this.list.addAll(list);
    }

    public List<Throwable> getList() {
        return list;
    }
}
