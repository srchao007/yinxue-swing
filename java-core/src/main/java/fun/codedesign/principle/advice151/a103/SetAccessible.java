package fun.codedesign.principle.advice151.a103;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * 设置可访问提供性能 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-05 20:48
 * @since 1.0.0
 */
@Notice("将accessible设置为true,提高访问速度，实际上如果是公开可以访问的方法就不需要设置")
public class SetAccessible {


    @Test
    public void test() throws Exception {

        @SuppressWarnings("all")
        class Demo {
            private String name;
        }


        Demo demo = new Demo();
        Field field = demo.getClass().getDeclaredField("name");
        System.out.println(Modifier.isPrivate(field.getModifiers()));
        field.setAccessible(true);
        field.set(demo, "zhangsan");
    }

}
