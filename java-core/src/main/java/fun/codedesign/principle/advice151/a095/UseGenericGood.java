package fun.codedesign.principle.advice151.a095;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fun.codedesign.principle.advice151.Good;
import fun.codedesign.principle.advice151.Notice;

/**
 * 使用好泛型 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 17:46
 * @since 1.0.0
 */
@Notice("泛型有助于显示的确定集合中的元素类型")
public class UseGenericGood {

    @SuppressWarnings("all")
    @Good
    public static void main(String[] args) {
        // 如果不使用Number可能就直接向上转型为Object对象
        List<Number> list1 = ArrayListUtil.<Number>asList(12, 12.5);
    }
}

abstract class ArrayListUtil {

    @SuppressWarnings("all")
    public static <T> List<T> asList(T... t) {
        List<T> list = new ArrayList<>();
        Collections.addAll(list, t);
        return list;
    }
}
