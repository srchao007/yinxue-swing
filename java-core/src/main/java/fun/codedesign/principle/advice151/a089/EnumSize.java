package fun.codedesign.principle.advice151.a089;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.util.EnumSet;

/**
 * Enum最好控制在64个元素以内 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 17:04
 * @since 1.0.0
 */
@Notice
public class EnumSize {
    @Test
    public void test89() throws Exception {
        EnumSet<Constant> set = EnumSet.allOf(Constant.class);
        // class java.util.RegularEnumSet
        System.out.println(set.getClass());
        EnumSet<Constant2> set2 = EnumSet.allOf(Constant2.class);
        // class java.util.JumboEnumSet
        System.out.println(set2.getClass());
    }

    enum Constant {
        A1, A2, A3, A4, A5, A6, A7, A8, A9, A10,
        A11, A12, A13, A14, A15, A16, A17, A18, A19, A20,
        A21, A22, A23, A24, A25, A26, A27, A28, A29, A30,
        A31, A32, A33, A34, A35, A36, A37, A38, A39, A40,
        A41, A42, A43, A44, A45, A46, A47, A48, A49, A50,
        A51, A52, A53, A54, A55, A56, A57, A58, A59, A60,
        A61, A62, A63, A64;
    }


    enum Constant2 {
        A1, A2, A3, A4, A5, A6, A7, A8, A9, A10,
        A11, A12, A13, A14, A15, A16, A17, A18, A19, A20,
        A21, A22, A23, A24, A25, A26, A27, A28, A29, A30,
        A31, A32, A33, A34, A35, A36, A37, A38, A39, A40,
        A41, A42, A43, A44, A45, A46, A47, A48, A49, A50,
        A51, A52, A53, A54, A55, A56, A57, A58, A59, A60,
        A61, A62, A63, A64, A65;
    }
}
