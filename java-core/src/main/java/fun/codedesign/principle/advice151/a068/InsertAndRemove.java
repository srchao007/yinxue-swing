package fun.codedesign.principle.advice151.a068;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 21:08
 * @since 1.0.0
 */
@Notice("频繁在固定位置插入和删除用Linkeklist，其他用数组，包括后面加参数也用数组更快")
public class InsertAndRemove {
}
