package fun.codedesign.principle.advice151.a128;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

/**
 * 谨防死锁 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:53
 * @since 1.0.0
 */
@Notice("在实际应用中要注意锁使用的位置和场景，注意不要出现相互请求锁的情况")
public class ForbidenDeadLock {

    // 建议128： 预防线程死锁
    @Test
    public void test128() throws Exception {

    }
}
