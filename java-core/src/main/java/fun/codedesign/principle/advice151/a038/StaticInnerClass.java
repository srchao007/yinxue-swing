package fun.codedesign.principle.advice151.a038;

import fun.codedesign.principle.advice151.Good;

/**
 * 静态内部类提高封装性 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:51
 * @since 1.0.0
 */
@Good("采用静态内部类的单例模式是一种，，满足懒加载和单例的需求")
public class StaticInnerClass {
    public static StaticInnerClass singleton() {
        return Proxy.singleton;
    }

    /**
     * 建议38：使用静态内部类提高封装性，public 类中带一个public static 类
     */
    private static class Proxy {
        private volatile static StaticInnerClass singleton = new StaticInnerClass();
    }
}
