package fun.codedesign.principle.advice151.a050;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:22
 * @since 1.0.0
 */
@Notice("在需要的时候使用包服务")
public class UsePackageInfo {
}
