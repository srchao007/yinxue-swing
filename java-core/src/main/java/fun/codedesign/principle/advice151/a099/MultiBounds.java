package fun.codedesign.principle.advice151.a099;

import fun.codedesign.principle.advice151.Notice;

interface Salary {
    boolean isLowerSalary();
}

interface Marry {
    boolean isMarried();
}

/**
 * 多重边界限定 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 20:24
 * @since 1.0.0
 */
@Notice("泛型可以多重界定")
public class MultiBounds {
    public static <T extends Salary & Marry> void canStand(T t) {
        if (t.isLowerSalary() && t.isMarried()) {
            System.out.println("可以坐椅子了");
        }
    }
}
