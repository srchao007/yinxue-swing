package fun.codedesign.principle.advice151.a028;

public class ValueCacheFirst {

    // 在包装型设计的时候都会存有一个Cache用来存储一些缓存的值，这样可以减少不需要的对象，
    // 使用的方式 ValueOf
    public static void main(String[] args) {
        // -128~127是Integer的缓存数组
        Integer a = Integer.valueOf(100);
        Integer b = Integer.valueOf(100);
        System.out.println(a == b); // true
    }
}
