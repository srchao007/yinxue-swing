package fun.codedesign.principle.advice151.a064;

import org.junit.Test;

import fun.codedesign.principle.advice151.Good;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

/**
 * 最值算法 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 20:41
 * @since 1.0.0
 */
@Good("不同集合的最值算法")
public class MaxSort {
    // 建议64： 多种最值算法，适时选择
    @Test
    public void test64() {
        // 条件表达式
        int[] arr = new int[]{1, 3, 4, 19, 3, 6, 3};
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            max = max > arr[i] ? max : arr[i];
        }
        System.out.println(max + "-----");

        // 排序后取末尾数
        int[] arr2 = arr.clone();
        Arrays.sort(arr2);
        System.out.println(arr2[arr2.length - 1] + "-----");

        // 数组转list
        Integer[] arr3 = new Integer[]{1, 3, 4, 19, 3, 6, 3};
        List<Integer> list = Arrays.asList(arr3);
        System.out.println(list.size());
        // list再转二叉树
        TreeSet<Integer> set = new TreeSet<>(list);
        System.out.println(set.first()); // 取首位小的值
        System.out.println(set.lower(set.last()) + "----"); // 取比最大值小一位的值
    }

}
