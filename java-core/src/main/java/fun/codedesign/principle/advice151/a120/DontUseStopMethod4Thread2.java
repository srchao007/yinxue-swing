package fun.codedesign.principle.advice151.a120;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:27
 * @since 1.0.0
 */
@Notice("不要用来停止线程")
public class DontUseStopMethod4Thread2 {

    // 建议120：不适用stop方法停止线程
    // stop已经过时，是比较粗暴的中断方法，会破坏线程的原子特性，可以用线程池的shutdown方法逐步关闭线程
}
