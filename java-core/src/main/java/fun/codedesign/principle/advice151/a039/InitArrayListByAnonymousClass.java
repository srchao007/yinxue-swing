package fun.codedesign.principle.advice151.a039;

import java.util.ArrayList;
import java.util.List;

import fun.codedesign.principle.advice151.Good;

/**
 * 通过匿名内部类来初始化集合 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:57
 * @since 1.0.0
 */
@Good("集合类都可以这样使用，常见list或者map")
public class InitArrayListByAnonymousClass {

    public static void main(String[] args) {

        List<String> list = new ArrayList<String>() {
            // 构造代码块1
            {
                System.out.println("---构造块1---");
                add("123");
                add("321");
                remove("123");
            }

            // 构造代码块2
            {
                System.out.println("---构造块2---");
                add("zhangsan");
                add("lisi");
            }

            // 匿名内部类的方法重写
            @Override
            public String toString() {
                return "nothing";
            }
        };
        System.out.println(list.toString());
    }
}
