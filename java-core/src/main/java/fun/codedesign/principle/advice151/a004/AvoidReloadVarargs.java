package fun.codedesign.principle.advice151.a004;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.util.Arrays;

public class AvoidReloadVarargs {

    @Test
    @Notice("注意变长参数数组和单个参数的区别")
    public void testReload() {
        final String s1 = getMessage("张三");
        final String s2 = getMessage("张三", "男");
        System.out.println(s1);
        System.out.println(s2);
    }

    public String getMessage(String name) {
        return name;
    }

    /**
     * reload
     */
    public String getMessage(String... msg) {
        return Arrays.toString(msg);
    }

}
