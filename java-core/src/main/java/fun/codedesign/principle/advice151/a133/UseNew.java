package fun.codedesign.principle.advice151.a133;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 11:30
 * @since 1.0.0
 */
@Notice("根据实际的情况来使用，new的情况，必须要确定类的类型，在某些情况也无法做到")
public class UseNew {
    // 建议133：若非必要不要克隆对象clone() 宁愿用new ，java对new的优化更多

}
