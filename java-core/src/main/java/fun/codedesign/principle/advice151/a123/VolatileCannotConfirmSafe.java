package fun.codedesign.principle.advice151.a123;

import fun.codedesign.principle.advice151.Notice;

/**
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:40
 * @since 1.0.0
 */
@Notice("volatile只能保证可见性，不能保证修改的时候的同步性，加sync")
public class VolatileCannotConfirmSafe {

    // 建议123： volatile不能保证数据同步
    class VolatileTest {
        private volatile int count;
        // 以上只能保证取得主内存中最新得count值，但是不能保证多线程的时候同步

        public int getCount() {
            return this.count;
        }
    }
}


