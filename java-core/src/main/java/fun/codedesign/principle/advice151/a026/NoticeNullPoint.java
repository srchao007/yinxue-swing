package fun.codedesign.principle.advice151.a026;

import java.util.ArrayList;
import java.util.List;

public class NoticeNullPoint {

    @SuppressWarnings("all")
    public static void main(String[] args) {
        // 常见包装类与基础类的转换
        List<Integer> list = new ArrayList<>();
        list.add(11);
        list.add(12);
        list.add(null);
        int b = 0;
        for (int a : list) {
            b += a; // null因为无法转换出现空指针异常
        }
    }
}
