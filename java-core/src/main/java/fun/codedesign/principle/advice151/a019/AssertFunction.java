package fun.codedesign.principle.advice151.a019;

public class AssertFunction {
    // 实际作用中，assert是有作用的，看框架源码就知道了
    // 可以在开发时使用，生产环境下assert是关闭的  --此处assert是java自带
    // 实际单元测试可以用 JUnit的断言
    public static void main(String[] args) {
        checkNull(null);
        checkNull2(null);
    }

    public static void checkNull(String str) {
        assert str != null : "str不能为null"; // 前者条件为false时，弹出:后的提示，显示在java.lang.AssertionError中
    }

    public static void checkNull2(String str) {
        assert str != null;
    }
}
