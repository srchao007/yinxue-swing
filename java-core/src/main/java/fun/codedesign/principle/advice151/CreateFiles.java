package fun.codedesign.principle.advice151;

import java.io.File;
import java.io.IOException;

/**
 * 创建下001-151的文件夹 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 14:26
 * @since 1.0.0
 */
public class CreateFiles {

    public static final String PATH = "D:\\WorkJian\\code\\javacollection\\java-high-performance\\src\\main\\java\\org\\javacollection\\high\\pattern33";
    public static final int NUM = 33;

    public static void main(String[] args) throws IOException {
        File parent = new File(PATH);
        String fileName = "";
        // String token = "0";
        for (int i = 1; i <= NUM; i++) {
            // 10位 100位如果不不能再就补0
            fileName = String.valueOf(i);
            if (fileName.length() == 1) {
                fileName = "00" + fileName;
            } else if (fileName.length() == 2) {
                fileName = "0" + fileName;
            } else {
                // donothing
            }
            fileName = "a" + fileName;
            File child = new File(parent, fileName);
            if (!child.exists()) {
                child.mkdir();
                System.out.println(fileName + "创建成功");
            }
        }
    }

}
