package fun.codedesign.principle.advice151.a092;

import fun.codedesign.principle.advice151.Notice;

/**
 * 注意注解版本 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 17:34
 * @since 1.0.0
 */
@Notice("主要是1.5版本前后，@Override注解在1.5版本前无效，需要删除")
public class AnnotationVersion {
}
