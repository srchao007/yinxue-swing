package fun.codedesign.principle.advice151.a121;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

/**
 * 三种优先级对线程来说 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-09 20:29
 * @since 1.0.0
 */
@Notice
public class ThreePriorities4Thread {
    // 建议121： 线程优先级只使用三个线程
    // 优先级：Priority 决定了线程取得cpu的运行机会大小，有10个级别，越大，越优先理论上，jvm是0级(加上11级)
    //
    @Test
    public void test121() throws Exception {
        Thread t1 = new Thread();
        // 主要使用以下三个优先级，只是表示抢占cpu的概率，但不是绝对的
        t1.setPriority(Thread.MAX_PRIORITY);
        t1.setPriority(Thread.MIN_PRIORITY);
        t1.setPriority(Thread.NORM_PRIORITY);
        t1.start();
    }
}
