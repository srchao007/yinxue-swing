package fun.codedesign.principle.advice151.a008;

import org.junit.Test;

import fun.codedesign.principle.advice151.Bad;

public class NoGoTo {


    @Test
    @Bad("goto是保留字，以前的老语言，不要次用这种写法，有隐患，用break和continue代替")
    public void testGoto() {
        final int fee = 10;
        saveDefault:
        saveFee(fee); // 原始的goto语句,用continue及break替代
    }

    public void saveDefault() {

    }

    public void saveFee(int fee) {
        System.out.println("费用:" + fee);
    }
}
