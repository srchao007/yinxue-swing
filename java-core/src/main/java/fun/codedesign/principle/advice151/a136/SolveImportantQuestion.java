package fun.codedesign.principle.advice151.a136;

import fun.codedesign.principle.advice151.Notice;

/**
 * 解决最重要的问题 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 11:43
 * @since 1.0.0
 */
@Notice("先解决最重要和最紧急的事情")
public class SolveImportantQuestion {

    // 建议136：枪打出头鸟，解决首要性能问题
    // 2 8 原则，找出要解决的前三个问题，然后集中力量解决第一个
}
