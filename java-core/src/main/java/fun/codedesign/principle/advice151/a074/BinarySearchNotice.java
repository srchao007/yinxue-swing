package fun.codedesign.principle.advice151.a074;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.util.ArrayList;
import java.util.Collections;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 15:29
 * @since 1.0.0
 */
@Notice("二分查找需要的是排好序的集合进行查找，否则用indexOf")
public class BinarySearchNotice {
    // 建议74： 不推荐使用binarySearch对列表进行检索
    // 二分查找需要使用排序后的数据进行查找，虽然高效 ，但意味着会改变原来的数据列表，但是也可以先进行排序然后二分查找
    // indexOf只能检索到第一个，但是可以通过subList的方式再继续查找，然后找到所有的元素
    @Test
    public void test74() {
        City c1 = new City("广州");
        City c2 = new City("西安");
        City c3 = new City("南京");
        City c4 = new City("北京");
        City c5 = new City("西安");

        ArrayList<City> list = new ArrayList<>(4);
        list.add(c1);
        list.add(c2);
        list.add(c3);
        list.add(c4);
        list.add(c5);
        // 排序后正常
        Collections.sort(list);
        System.out.println("indexOf:" + list.indexOf(c1));
        System.out.println("二分查找：" + Collections.binarySearch(list, c1));
        System.out.println(list.toString());

        // @SuppressWarnings("unchecked")
        // List<City> list2 = (List<City>) collection.clone();
        // Collections.sort(list2);
        // System.out.println("二分查找："+ Collections.binarySearch(list2, c5));
        // System.out.println(collection.toString());
        // System.out.println(list2.toString());
        // Collections.binarySearch(collection, "西安");
    }
}
