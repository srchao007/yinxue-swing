package fun.codedesign.principle.advice151.a073;

import org.junit.Test;

import fun.codedesign.principle.advice151.Good;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 对比排序 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 15:24
 * @since 1.0.0
 */
@Good("可以实现compare方法进行排序，也可以扩展comparator接口进行排序")
public class ComparatorSort {

    // 建议73： 使用Comparator进行排序
    @Test
    public void test73() {
        Employee e1 = new Employee(1001, "张三", Position.Boss);
        Employee e2 = new Employee(1003, "李四", Position.Manager);
        Employee e3 = new Employee(1002, "王五", Position.Staff);
        Employee e4 = new Employee(1004, "赵六", Position.Staff);

        List<Employee> list = new ArrayList<>(4);
        list.add(e1);
        list.add(e2);
        list.add(e3);
        list.add(e4);
        System.out.println(list.toString());
        Collections.sort(list);
        System.out.println(list.toString());
        Collections.reverse(list);
        System.out.println(list.toString());
        Collections.sort(list, new EmployeeComparator());
        System.out.println(list.toString());

        // 如果是需要先按职位排序然后再按工序排序可以继续用apache自带的工具方法，写在实体内

    }
}
