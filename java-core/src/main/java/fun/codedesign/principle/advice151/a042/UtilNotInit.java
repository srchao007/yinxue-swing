package fun.codedesign.principle.advice151.a042;

import fun.codedesign.principle.advice151.Good;

/**
 * 让工具类不能实例化 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 17:26
 * @since 1.0.0
 */
@Good("让工具不能实例化，也不会被反射，但是以下这种示例其实不是最好的，最好的是在class前加abstract即可")
public class UtilNotInit {
}

// 建议42：让工具类不可实例化
class Util {
    private Util() {
        throw new Error("不能实例化本类");
    }
}

abstract class StringUtil {
    // like this
}
