package fun.codedesign.principle.advice151.a134;

import fun.codedesign.principle.advice151.Notice;

/**
 * 定位错误 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 11:32
 * @since 1.0.0
 */
@Notice("分析问题是很重要的")
public class ConfirmError {

    // 建议134： 望闻问切来诊断系统性能问题
    /*
     * 1. 望，(1)偶发性问题，无法重现，分析日志 (2) 重现问题，用多组数据进行对比，至少3组，来对比效果
     * 2. 闻， 主动检查代码质量，性能问题，
     * 3. 问： 与业务员进行业务讨论
     * 4. 切： 直接分析大量记录的日志，进行分析，定位问题点，同1.(1)
     *
     *  分析问题，不要过度急躁
     *
     */
    /**
     * 一些常见的手段：
     * 1. 问题是否能复现
     * 2. 日志记录怎么样
     * 3. 审视代码的边界问题
     * 4. 考虑jvm回收的影响
     */


}
