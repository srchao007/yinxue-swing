package fun.codedesign.principle.advice151.a087;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:53
 * @since 1.0.0
 */
@Notice
public class EnumValueOf {

    @Test
    public void test87() throws Exception {
        // 如果name是小写spring, 并取消contain判断，会直接报错，因为枚举定位时，除了需要传入name，还需要枚举class类型，此时没有class类型，即报错
        String name = "Spring";
        if (Season.contain(name)) {
            Season s = Season.valueOf(name); // 查找名称与该输入值相同的美剧类
            System.out.println(s);
        } else {
            System.out.println("---");
        }
    }


    // 抛出异常
    @Test(expected = IllegalArgumentException.class)
    public void test() {
        String name = "spring";
        Season s = Season.valueOf(name); // 查找名称与该输入值相同的美剧类
        if (s != null) {
            System.out.println("nothing");
        } else {
            System.out.println(s);
        }
    }
}
