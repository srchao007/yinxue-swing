package fun.codedesign.principle.advice151.a093;

import org.junit.Test;

import fun.codedesign.principle.advice151.Notice;

import java.util.ArrayList;
import java.util.List;

/**
 * 泛型擦除 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 17:38
 * @since 1.0.0
 */
@Notice("泛型在编译后会自动擦除，还原为原始类")
public class GenericDeletion {

    @Test
    public void test93() throws Exception {
        List<String> list = new ArrayList<String>();
        List<Integer> list2 = new ArrayList<Integer>();
        System.out.println(list.getClass() == list2.getClass());
        System.out.println(list.getClass());
    }
}
