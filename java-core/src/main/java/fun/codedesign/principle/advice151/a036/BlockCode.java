package fun.codedesign.principle.advice151.a036;

import fun.codedesign.principle.advice151.Good;

/**
 * 用块代码块精炼程序 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 16:41
 * @since 1.0.0
 */
@Good("用块代码提取初始化逻辑")
public class BlockCode {
}

class Code {
    // 静态代码块：用于静态变量的初始化或者对象创建前的环境初始化
    static {

    }

    // 普通代码块，必须通过方法名来调用，new 对象时候先执行本代码然后执行构造函数
    {

    }

    // 同一时间只能有一个线程进入该方法，是一种多线程保护机制
    //synchronized {

    //}

    // 构造代码块的执行时在构造函数内最上端先执行,构造代码块依托于构造函数执行
    // 应用环境：1. 初始化实例变量  2.初始化实例环境 ，可以将同样的实现逻辑从构造函数中提取出来作为代码块
}
