package fun.codedesign.principle.advice151.a083;

import fun.codedesign.principle.advice151.Notice;

enum Season {

    Spring, Summer, Autumn, Winter;

    public static boolean contain(String name) {
        for (Season s : Season.values()) {
            if (s.name() == name) {
                return true;
            }
        }
        return false;
    }
}

/**
 * 推荐使用Enum替代接口常量 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-03 16:33
 * @since 1.0.0
 */
@Notice("推荐枚举代替接口常量")
public class UseEnumConstant {
}


