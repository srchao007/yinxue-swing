package fun.codedesign.principle.advice151.a048;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import fun.codedesign.principle.advice151.Notice;

/**
 * 改变equals和hashCode代表 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 19:12
 * @since 1.0.0
 */
@Notice("重写equals方法的同时必须重写hashCode方法，因为HashMap需要使用HashCode进行下标判断")
public class ChangeEqualsAndHashCode {

    private String name;

    /**
     * 可以使用{@link org.apache.commons.lang3.builder.HashCodeBuilder}
     *
     * @return
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).toHashCode();
    }
}
