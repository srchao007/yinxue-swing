package fun.codedesign.principle.advice151.a065;

import java.util.Arrays;
import java.util.List;

import fun.codedesign.principle.advice151.Notice;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-02 20:59
 * @since 1.0.0
 */
@Notice("避免数组转换陷阱")
public class VoidArrayConvertError {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4};
        List<?> list = Arrays.asList(arr); // 本方法是一个变长数组参数
        System.out.println(list.size()); // 1
    }

}
