package org.yinxueframework;

import org.junit.Test;
import org.yinxue.framework.rpc.tansport.HessianEncoder;

import java.io.IOException;
import java.util.Arrays;

public class HessianEncoderTest {

    @Test
    public void encode() throws IOException {
        HessianEncoder encoder = new HessianEncoder();
        byte[] bytes = encoder.encode(new Example());
        System.out.println(Arrays.toString(bytes));
    }
}