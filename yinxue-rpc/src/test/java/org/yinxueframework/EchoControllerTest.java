package org.yinxueframework;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.yinxue.framework.rpc.example.EchoController;
import org.yinxue.framework.rpc.spring.RpcInjectPostProcessor;

public class EchoControllerTest {

    @Test
    public void send() {
        AnnotationConfigApplicationContext configApplicationContext = new AnnotationConfigApplicationContext();
        configApplicationContext.register(RpcInjectPostProcessor.class);
        configApplicationContext.scan("org.yinxue.framework.rpc.example");
        configApplicationContext.refresh();
        EchoController echoController = configApplicationContext.getBean(EchoController.class);
        String resp = echoController.send("xxx");
        System.out.println(resp);
    }

    public static void main(String[] args) {
        AnnotationConfigApplicationContext configApplicationContext = new AnnotationConfigApplicationContext();
        configApplicationContext.register(RpcInjectPostProcessor.class);
        configApplicationContext.scan("org.yinxue.framework.rpc.example");
        configApplicationContext.refresh();
        EchoController echoController = configApplicationContext.getBean(EchoController.class);
        String resp = echoController.send("xxx");
        System.out.println(resp);
    }
}