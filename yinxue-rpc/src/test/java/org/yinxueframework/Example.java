/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Example
 * Author:   zengjian
 * Date:     2018/10/9 9:20
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxueframework;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 〈123〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/9 9:20
 */
public class Example implements Serializable{
    private  String ss;
    private int ff;
    private BigDecimal bigDecimal;

    public String getSs() {
        return ss;
    }

    public void setSs(String ss) {
        this.ss = ss;
    }

    public int getFf() {
        return ff;
    }

    public void setFf(int ff) {
        this.ff = ff;
    }

    public BigDecimal getBigDecimal() {
        return bigDecimal;
    }

    public void setBigDecimal(BigDecimal bigDecimal) {
        this.bigDecimal = bigDecimal;
    }
}