/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: RpcResponse
 * Author:   zengjian
 * Date:     2018/10/8 16:57
 * Description: rpc请求响应类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc;


import java.io.Serializable;

import org.yinxue.framework.rpc.utils.JsonTools;

/**
 * 〈rpc请求响应类〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 16:57
 */
public class RpcResponse implements Serializable {

    private static final long serialVersionUID = 1160738556752453856L;

    private String requestId;

    /**
     * 返回值
     */
    private Object value;

    /**
     * 返回类型
     */
    private Class<?> type;

    /**
     * 错误信息
     */
    private String errorMessage;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return JsonTools.toJsonString(this);
    }
}