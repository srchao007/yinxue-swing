/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Register
 * Author:   zengjian
 * Date:     2018/10/8 14:31
 * Description: 注册中心
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.register;

/**
 * 〈注册中心〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 14:31
 */
public interface Register {

}