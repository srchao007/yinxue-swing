/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: EchoController
 * Author:   zengjian
 * Date:     2018/10/8 14:47
 * Description: echo调用类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.example;

import org.springframework.stereotype.Component;
import org.yinxue.framework.rpc.annotation.RpcConsumer;

/**
 * 〈echo调用类〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 14:47
 */
@Component
public class EchoController {

    @RpcConsumer
    private EchoService echoService;

    public String send(String request){
        return echoService.echo(request);
    }
}