/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: RpcProvider
 * Author:   zengjian
 * Date:     2018/10/8 14:36
 * Description: 服务端暴露
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 〈服务端暴露〉<br> 
 * 〈一句话描述〉〉
 *
 * @author zengjian
 * @create 2018/10/8 14:36
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Component
public @interface RpcProvider {

}