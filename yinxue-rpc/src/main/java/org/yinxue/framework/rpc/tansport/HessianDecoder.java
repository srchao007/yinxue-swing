/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: HessianDecoder
 * Author:   zengjian
 * Date:     2018/10/8 17:04
 * Description: HessianDecoder
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.tansport;

import com.caucho.hessian.io.Hessian2Input;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * 〈HessianDecoder〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 17:04
 */
public class HessianDecoder implements Decoder {

    public static final HessianDecoder INSTANCE = new HessianDecoder();

    @Override
    public Object decode(byte[] source) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(source);
        Hessian2Input hessian2Input = new Hessian2Input(bis);
        Object resultObject = hessian2Input.readObject();
        hessian2Input.close();
        return resultObject;
    }
}