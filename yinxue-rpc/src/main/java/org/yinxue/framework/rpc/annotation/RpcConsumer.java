/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: RpcConsumer
 * Author:   zengjian
 * Date:     2018/10/8 14:38
 * Description: 客户端引用
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.annotation;

import java.lang.annotation.*;

/**
 * 〈客户端引用〉<br> 
 * 〈一句话描述〉〉
 *
 * @author zengjian
 * @create 2018/10/8 14:38
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RpcConsumer {

}