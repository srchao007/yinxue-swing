/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: EchoService
 * Author:   zengjian
 * Date:     2018/10/8 14:42
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.example;

/**
 * 〈响应服务接口〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 14:42
 */
public interface EchoService {

    String echo(String request);

}