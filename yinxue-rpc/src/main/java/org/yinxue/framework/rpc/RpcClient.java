/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: RpcClient
 * Author:   zengjian
 * Date:     2018/10/8 14:33
 * Description: Rpc客户端
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc;

/**
 * 〈Rpc客户端〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 14:33
 */
public interface RpcClient {

}