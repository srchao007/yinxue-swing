/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: HessianEncoder
 * Author:   zengjian
 * Date:     2018/10/8 17:08
 * Description: HessianEncoder
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.tansport;

import com.caucho.hessian.io.Hessian2Output;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 〈HessianEncoder〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 17:08
 */
public class HessianEncoder implements Encoder {

    public static final HessianEncoder INSTANCE = new HessianEncoder();

    @Override
    public byte[] encode(Object source) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Hessian2Output hessian2Output = new Hessian2Output(bos);
        hessian2Output.writeObject(source);
        // 必须close，否则无法到bos
        hessian2Output.close();
        return bos.toByteArray();
    }
}