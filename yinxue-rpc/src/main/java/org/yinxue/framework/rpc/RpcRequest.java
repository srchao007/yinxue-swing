/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: RpcRequest
 * Author:   zengjian
 * Date:     2018/10/8 16:56
 * Description: rpc请求实体类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;
import java.util.UUID;

/**
 * 〈rpc请求实体类〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 16:56
 */
public class RpcRequest implements Serializable {

    private static final long serialVersionUID = -8248778908080065549L;

    private String requestId;
    private String serviceName;
    private Object[] args;

    public RpcRequest() {
        requestId = UUID.randomUUID().toString();
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}