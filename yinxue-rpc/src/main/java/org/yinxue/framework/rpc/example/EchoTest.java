/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: EchoTest
 * Author:   zengjian
 * Date:     2018/10/8 20:32
 * Description: EchoTest
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.example;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.yinxue.framework.rpc.spring.RpcInjectPostProcessor;

/**
 * 〈EchoTest〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 20:32
 */
public class EchoTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext configApplicationContext = new AnnotationConfigApplicationContext();
        configApplicationContext.register(RpcInjectPostProcessor.class);
        configApplicationContext.scan("org.yinxueframework.core.rpc.example");
        configApplicationContext.refresh();
        EchoController echoController = configApplicationContext.getBean(EchoController.class);
        String resp = echoController.send("xxx");
        System.out.println(resp);
    }

}