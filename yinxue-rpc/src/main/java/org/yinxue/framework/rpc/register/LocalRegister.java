/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: LocalRegister
 * Author:   zengjian
 * Date:     2018/10/8 14:44
 * Description: 以本地内存作为注册中心
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.register;


import java.util.concurrent.ConcurrentHashMap;

import org.yinxue.framework.rpc.utils.JsonTools;

/**
 * 〈以本地内存作为注册中心〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 14:44
 */
public class LocalRegister implements Register {

    private final ConcurrentHashMap<String, ServiceExporter> serviceMap = new ConcurrentHashMap<>(32);

    public ServiceExporter register(ServiceExporter exporter) {
        return serviceMap.putIfAbsent(exporter.getServiceName(), exporter);
    }

    public ServiceExporter getServiceExporter(String serviceName) {
        return serviceMap.get(serviceName);
    }

    @Override
    public String toString() {
        return JsonTools.toJsonString(this);
    }
}