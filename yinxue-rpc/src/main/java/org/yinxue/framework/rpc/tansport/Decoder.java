/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Decoder
 * Author:   zengjian
 * Date:     2018/10/8 17:00
 * Description: 解码器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.tansport;

import java.io.IOException;

/**
 * 〈解码器〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 17:00
 */
public interface Decoder {

    Object decode(byte[] source) throws IOException;

}