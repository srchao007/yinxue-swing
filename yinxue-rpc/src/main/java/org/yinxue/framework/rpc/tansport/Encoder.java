/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Encoder
 * Author:   zengjian
 * Date:     2018/10/8 16:59
 * Description: 编码器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.tansport;

import java.io.IOException;

/**
 * 〈编码器〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 16:59
 */
public interface Encoder {

    byte[] encode(Object source) throws IOException;

}