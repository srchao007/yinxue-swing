/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: EchoServiceImpl
 * Author:   zengjian
 * Date:     2018/10/8 14:43
 * Description: EchoServiceImpl服务实现暴露
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.example;

import org.yinxue.framework.rpc.annotation.RpcProvider;

import fun.codedesign.yinxue.util.DateUtil;


/**
 * 〈EchoServiceImpl服务实现暴露〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 14:43
 */
@RpcProvider
public class EchoServiceImpl implements EchoService {

    @Override
    public String echo(String request) {
        return request+":" + DateUtil.getCurrentFormatMilliTime();
    }
}