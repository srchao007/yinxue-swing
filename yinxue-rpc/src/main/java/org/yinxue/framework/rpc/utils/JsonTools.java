/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: JsonTools
 * Author:   zengjian
 * Date:     2018/10/9 14:21
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.rpc.utils;

import com.alibaba.fastjson.JSON;

/**
 * 〈JsonTools〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/9 14:21
 */
public final class JsonTools {

    private JsonTools() {
    }

    public static String toJsonString(Object source){
        return JSON.toJSONString(source);
    }

}