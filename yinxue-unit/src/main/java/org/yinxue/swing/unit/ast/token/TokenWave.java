/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: TokenWave
 * Author:   zengjian
 * Date:     2018/9/6 10:38
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.token;

import org.yinxue.swing.core.util.StringUtil;
import org.yinxue.swing.unit.ast.node.Node;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 〈词法解析〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/6 10:38
 */
public class TokenWave {

    private String javaCode;
    private char[] chars;
    private int length;
    private List<String> tokens;
    private Set<Integer> quotationScope = new HashSet<>(512);
    private Set<Integer> noteScope = new HashSet<>(512);
    /**
     * 字符游标，供分词用
     */
    private int charCursor;

    /**
     * 分词游标，供语句分析用
     */
    private int wordCursor;

    /**
     * 类似于监听器的作用
     */
    private StatementWave statementWave = new StatementWave();

    private List<Node> nodes;

    public TokenWave(String javaCode) {
        validate(javaCode);
        this.javaCode = javaCode;
        this.chars = javaCode.toCharArray();
        this.length = chars.length;
        this.tokens = new ArrayList<>();
        splitTokens();
        fireStatementEvent();
    }


    private void validate(String javaCode) {
        if (StringUtil.isEmpty(javaCode)) {
            throw new IllegalArgumentException("入参javaCode不能为空");
        }
    }


    public void splitTokens() {
        // 扫描所有带引号的范围，避免注释符号在该区间
        scanQuotationScope();
        // 扫描所有注释的范围
        scanNoteScope();
        // 分词起始符
        start:
        for (int i = 0; i < length; i++) {
            // 注释、空格  \n \r \t开始的char跳过
            if (noteScope.contains(i) || chars[i] == ' ' || chars[i] == '\n' || chars[i] == '\r' || chars[i] == '\t') {
                continue;
            }

            // 分词结束符, i赋值后会再 i++ 所以对i赋值j或k时注意少一位
            end:
            for (int j = i; j < length; j++) {
                if (noteScope.contains(j)) {
                    // 将开始位置置到注释区末尾
                    i = j;
                    continue;
                }

                // 对字面量进行分词
                if ((chars[j] == ' ' || chars[j] == '\n')) {
                    tokens.add(new String(chars, i, j - i));
                    // 跳过连续空格或者换行
                    for (int k = j + 1; k < length; k++) {
                        if (chars[k] != ' ' && chars[k] != '\n') {
                            i = k - 1;
                            break end;
                        }
                    }
                }

                // 对java符号进行分词
                if (SymbolTable.contains(chars[j])) {
                    char c = chars[j];

                    // 引号范围内的合成为一个词
                    if (c == '"') {
                        int[] offset = new int[]{i};
                        doStepScanQuotationEnd(offset);
                        tokens.add(new String(chars, i, offset[0] - i + 1));
                        i = offset[0];
                        break;
                    }

                    // 检查是否存在组合运算符号
                    if (c == '+' || c == '-' || c == '*' || c == '/'
                            || c == '%' || c == '!' || c == '=' || c == '|'
                            || c == '&' || c == '<' || c == '>') {
                        if (chars[j + 1] == '=') {
                            // 符号左表达式
                            tokens.add(new String(chars, i, j - i));
                            // 两位符号
                            tokens.add(new String(chars, j, 2));
                            i = j + 1;
                            break;
                        } else if (chars[i + 1] == '+' || chars[i + 1] == '-' || chars[i + 1] == '&' || chars[i + 1] == '|') {
                            tokens.add(new String(chars, i, j - i));
                            tokens.add(new String(chars, j, 2));
                            i = j + 1;
                            break;
                        } else if (chars[i + 1] == '<' || chars[i + 1] == '>') {
                            if (chars[i + 2] == '<' || chars[i + 2] == '>') {
                                tokens.add(new String(chars, i, j - i));
                                tokens.add(new String(chars, j, 3));
                                i = j + 2;
                                break;
                            }
                            tokens.add(new String(chars, i, j - i));
                            tokens.add(new String(chars, j, 2));
                            i = j + 1;
                            break;
                        }
                    }
                    // 如果j-i为0不截取前部分，否则会出现空("")分词
                    if ((j - i) != 0) {
                        tokens.add(new String(chars, i, j - i));
                    }
                    // 截取符号本身
                    tokens.add(new String(chars, j, 1));
                    // 将起始i赋值到j
                    i = j;
                    break;
                }
            }
        }
    }

    /**
     * 扫描引号下标区
     */
    private void scanQuotationScope() {
        int[] offset = new int[1];
        for (int i = 0; i < length; i++) {
            if (chars[0] == '"' || (chars[i] == '"' && chars[i - 1] != '\\')) {
                offset[0] = i;
                doStepScanQuotationEnd(offset);
            }
            int val = offset[0];
            if (val != 0 && val > i && val < length) {
                i = val;
            }
        }
    }

    private void doStepScanQuotationEnd(int[] offset) {
        quotationScope.add(offset[0]);
        // 从 i 开始往后查找，直到遇见下一个非转义的引号
        for (int j = offset[0] + 1; j < length; j++) {
            if (chars[j] == '"' && chars[j - 1] != '\\') {
                quotationScope.add(j);
                offset[0] = j;
                break;
            }
            quotationScope.add(j);
        }
    }

    /**
     * 扫描注释下标区
     */
    private void scanNoteScope() {
        for (int i = 0; i < length; i++) {
            if (chars[i] == '/' && chars[i + 1] == '*' && !quotationScope.contains(i)) {
                noteScope.add(i);
                noteScope.add(i + 1);
                int[] offset = new int[]{i + 2};
                doScanNoteEndOne(offset);
                i = offset[0];
            } else if (chars[i] == '/' && chars[i + 1] == '/' && !quotationScope.contains(i)) {
                noteScope.add(i);
                noteScope.add(i + 1);
                int[] offset = new int[]{i + 2};
                doScanNoteEndTwo(offset);
                i = offset[0];
            }
        }
    }

    private void doScanNoteEndOne(int[] offset) {
        for (int j = offset[0]; j < length; j++) {
            if (chars[j] == '*' && chars[j + 1] == '/' && !quotationScope.contains(j)) {
                offset[0] = j + 1;
                noteScope.add(j);
                noteScope.add(j + 1);
                break;
            }
            noteScope.add(j);
        }
    }

    private void doScanNoteEndTwo(int[] offset) {
        for (int j = offset[0] + 2; j < length; j++) {
            if (chars[j] == '\n' && !quotationScope.contains(j)) {
                offset[0] = j;
                noteScope.add(j);
                break;
            }
            noteScope.add(j);
        }
    }

    public List<String> getTokens() {
        return tokens;
    }

    /**
     * 发布解析事件
     */
    private void fireStatementEvent() {
        if (!tokens.isEmpty()) {
            statementWave.fireStatementEvent(new TokenEvent(this, tokens));
        }
    }
}