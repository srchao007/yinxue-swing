/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Node
 * Author:   zengjian
 * Date:     2018/9/2 0:08
 * Description: 节点基类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.node;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈节点基类〉
 *
 * @author zengjian
 * @create 2018/9/2
 * @since 1.0.0
 */
public abstract class Node {

    private List<String> tokens;
    private int braceLevel;
    private int start;
    private int end;
    private Node parent;
    private LinkedList<Node> childs = new LinkedList<>();

    public Node braceLevel(int braceLevel) {
        this.braceLevel = braceLevel;
        return this;
    }

    public Node end(int end) {
        this.end = end;
        return this;
    }

    public Node start(int start) {
        this.start = start;
        return this;
    }

    public Node tokens(List<String> tokens) {
        this.tokens = tokens;
        return this;
    }

    public Node parent(Node parent){
        this.parent = parent;
        this.parent.addChild(this);
        return this;
    }

    public Node addChild(Node child){
        this.childs.addFirst(child);
        return this;
    }

    @Override
    public String toString() {
        List<String> subList = tokens.subList(start, end);
        StringBuilder builder = new StringBuilder(8192);
        Iterator<String> iterator = subList.iterator();
        while (iterator.hasNext()){
            builder.append(iterator.next());
            if (iterator.hasNext()){
                builder.append(" ");
            }
        }
        return builder.toString();
    }
}