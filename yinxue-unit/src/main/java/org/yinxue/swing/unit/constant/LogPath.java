package org.yinxue.swing.unit.constant;

/**
 * LogPath {@link org.yinxue.swing.unit.constant} <br>
 *
 * @author zengjian
 * @date 2019/2/19 18:49
 * @since 1.0.0
 */
public interface LogPath {

    String PARENT_PATH = "E:/log";
    String LIST_FILE = "list.txt";

    String RECORD_PATH = "E:/unit";
    String SETTING_FILE = "setting.properties";

}