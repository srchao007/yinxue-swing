//package org.yinxue.swing.unit.ast.javacc;
//
//import com.github.javaparser.JavaParser;
//import com.github.javaparser.ParseResult;
//import com.github.javaparser.ParserConfiguration;
//import com.github.javaparser.ast.CompilationUnit;
//import com.github.javaparser.ast.visitor.VoidVisitor;
//import com.github.javaparser.printer.PrettyPrinterConfiguration;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.nio.charset.Charset;
//
//public class JavaCCTest {
//    public static void main(String[] args) throws FileNotFoundException {
//        ParserConfiguration config = new ParserConfiguration();
//        config.setLanguageLevel(ParserConfiguration.LanguageLevel.JAVA_8);
//        config.setCharacterEncoding(Charset.forName("utf-8"));
//        JavaParser parser = new JavaParser(config);
//        ParseResult<CompilationUnit> result = parser.parse(new File("F:\\me\\java-learning\\java-swing\\yinxue-unit\\src\\main\\java\\org\\yinxue\\swing\\unit\\model\\ClassDesc.java"));
//        System.out.println(result);
//        PrettyPrinterConfiguration prettyConfig = new PrettyPrinterConfiguration();
//        prettyConfig.setPrintComments(false);
//        VoidVisitor visitor = new UnitTestVisitor(prettyConfig);
//        result.getResult().get().accept(visitor,null);
//        System.out.println(visitor.toString());
//    }
//}
