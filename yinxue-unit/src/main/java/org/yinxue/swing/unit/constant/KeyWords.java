/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: KeyWords
 * Author:   zengjian
 * Date:     2018/8/23 13:42
 * Description: java关键词
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.constant;

/**
 * 〈java关键词〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/23 13:42
 */
public interface KeyWords {
    // 访问权限
    String PUBLIC = "public";
    String PROTECTED = "protected";
    String PRIVATE = "private";
    String DEFAULT = "default";

    // 限定符
    String FINAL = "final";
    String STATIC = "static";

    // 类类型
    String INTERFACE = "interface";
    String ANNOTATION = "@annotation";
    String ABSTRACT = "abstract";
    String ENUM = "enum";

    // 基础类型
    String PRI_INT = "int";
    String PRI_SHORT = "short";
    String PRI_LONG = "long";
    String PRI_DOUBLE = "double";
    String PRI_CHAR = "char";
    String PRI_BYTE = "byte";
    String PRI_FLOAT = "float";
    String PRI_BOOLEAN = "boolean";

    // 引用类型
    String STRING = "String";
    String OBJECT = "Object";
    String CLASS = "Class";

    String INTEGER = "Integer";
    String SHORT = "Short";
    String LONG = "Long";
    String FLOAT = "Float";
    String DOUBLE = "Double";
    String BYTE = "Byte";
    String BOOLEAN = "Boolean";
    String CHARACTER = "Character";

    // API
    String BIG_INTEGER = "BigInteger";
    String BIG_DECIMAL = "BigDecimal";

    String HASH_MAP = "HashMap";
    String MAP = "Map";
    String LIST = "List";

    // 分隔符号
    char LPAREN = '(';
    char RPAREN = ')';
    char LBRACE = '{';
    char RBRACE = '}';
    char LBRACKET = '[';
    char RBRACKET = ']';
    char COMMA  = ',';
    char COLON  = ':';
    char LANGLE = '<';
    char RANGLE = '>';
    char SEMICOLON = ';';
    char STRING_MARK = '"';

    char WHITE_SPACE = ' ';
    char LINE_FEED = '\n';
}