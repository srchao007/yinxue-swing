/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: NodeEvent
 * Author:   zengjian
 * Date:     2018/9/28 17:52
 * Description: Node事件对象
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.token;


import org.yinxue.swing.unit.ast.node.Node;

/**
 * 〈Node事件对象〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/28 17:52
 */
public class NodeEvent {

    /**
     * 事件发生源
     */
    private StatementWave statementWave;

    /**
     * 变动值
     */
    private Node classNode;

    public NodeEvent(StatementWave statementWave, Node classNode) {
        this.statementWave = statementWave;
        this.classNode = classNode;
    }

    public StatementWave getStatementWave() {
        return statementWave;
    }

    public void setStatementWave(StatementWave statementWave) {
        this.statementWave = statementWave;
    }

    public Node getClassNode() {
        return classNode;
    }

    public void setClassNode(Node classNode) {
        this.classNode = classNode;
    }
}