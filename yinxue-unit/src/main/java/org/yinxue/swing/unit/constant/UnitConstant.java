package org.yinxue.swing.unit.constant;

/**
 * 正则表达式匹配
 *
 * @author zengjian
 * @create 2018-03-29 10:55
 * @since 1.0.0
 */
public interface UnitConstant {


    String S = " ";
    String SSSS = "    ";
    String SSSSSSSS = SSSS + SSSS;
    String EMPTY = "";
    String PLACE_HOLDER = "?"; //占位符

    // 截取类名称, 去除泛型粘连的情况
    String RETAIN_CLASS_NAME = "(private|public|abstract|protected|static|final|class|enum|interface|@interface|<[\\S\\s]*>|\\{)";

    // 内部类匹配行
    String INNER_CLASSES_MATCH = "(private|public|protected)? *(static|final)? *(final|static)?\\s*class [\\s\\S]*";

    // 注解类导入包过滤
    String FILTER_IMPORT_ANNOTATION = "import[\\S\\s]*(validation|annotation|stereotype)[\\S\\s]*;";

    String IMPORT_JAVA_MATH = "import java.math.*;";
    String IMPORT_JAVA_UTIL = "import java.util.*;";

    String IMPORT_TESTNG = "import org.testng.annotations.Test;";
    String IMPORT_TESTNG_BEFORE_METHOD = "import org.testng.annotations.BeforeMethod;";
    String IMPORT_TESTNG_BEFORE_CLASS = "import org.testng.annotations.BeforeClass;";

    String IMPORT_MOCKITO_MATCHER_STATIC = "import static org.mockito.Matchers.*;";
    String IMPORT_MOCKITO_MOCKITO_STATIC = "import static org.mockito.Mockito.*;";
    String IMPORT_MOCKITO = "import org.mockito.*;";

    // class追加注解
    String PREPARE4TEST = "@PrepareForTest({})\n";
    String INJECTMOCKS = "@InjectMocks\n";

    // 属性追加工厂初始
    String BEFORE_METHOD_STATEMENT =
            SSSS + "@BeforeMethod\n" +
                    SSSS + "public void beforeMethod() {\n" +
                    SSSSSSSS + "MockitoAnnotations.initMocks(this);\n" +
                    SSSS + "}\n\n";


    // 需要Mock的变量类型，以这些字符结尾的type
    String NEED_MOCK_CLASSDESC = "[\\s\\S]*" +
            "(Request|BindingResult|ResultSet|MultipartFile|JDBC|Binder|Service|" +
            "Process|Processor|Support|Dao|Session|Response|Model|Executor|Impl|Manager|" +
            "ProceedingJoinPoint|Document|HierarchicalStreamWriter|MarshallingContext|" +
            "HierarchicalStreamReader|UnmarshallingContext|JspTag|JspContext|JspFragment" +
            ")$";

    // 需要Mock的方法, 针对有换行情况的，需要再加上\n 做匹配判断
    // 可能会匹配这种类型  entity.setSeqNo(couponBatchDao.querySeqNo(paramMap)); --> couponBatchDao.querySeqNo(paramMap))
    // 解决办法： 比较下该短语中(与)的数量，如果多了，就删除， 先不判断 dalClient.组件 暂时不使用 [\"\"]
    String METHOD_NEED_MOCK = "[a-zA-Z0-9]*(Service|Process|Processor|Support|Dao|Impl|Manager|JDBC)\\.[a-zA-Z0-9_ ]+\\([a-zA-Z0-9 ,\\.\\(\\)\n]*\\);";

    // 过滤掉入参中的注解类
    String VARS_ANNOTATION = "@[a-zA-Z0-9_@ ]+\\([\", a-zA-Z_=]+\\)";

    // 注解中没有值，需要再替换一次
    String VARS_ANNATATION_NOPARAM = "@[a-zA-Z]* ";

    // 取构造方法入参
    String VARS_CONSTRUTOR = "\\([a-zA-Z0-9, <>\n]*\\) *\\{";

    // 取主方法入参，增加数组匹配
    String VARS_METHOD = "\\([a-zA-Z0-9_ ,@<>?\\.\"=\\(\\)\n\\[\\]]*\\)[\\s\\S]*\\{";

    // 取抽象方法入参
    String VARS_ABSTRACT_METHOD = "\\([a-zA-Z0-9_ ,@<>?\\.\"=\\(\\)\n\\[\\]]*\\)[\\s\\S]*;";

    // 取mock方法入参
    String VARS_METHOD_LOCAL = "\\([a-zA-Z_0-9]*\\)";

}
