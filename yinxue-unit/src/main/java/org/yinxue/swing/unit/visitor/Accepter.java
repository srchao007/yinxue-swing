/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Accepter
 * Author:   zengjian
 * Date:     2018/8/16 14:00
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.visitor;

/**
 * 〈接收者接口〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/16 14:00
 */
public interface Accepter {

    String accept(Visitor visitor);

}