/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: ConfigFrame
 * Author:   zengjian
 * Date:     2018/8/21 9:53
 * Description: 用户自定义的类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.main;

import org.yinxue.swing.core.panel.YxTextArea;
import org.yinxue.swing.core.standard.YxStructure;
import org.yinxue.swing.core.util.SwingUtils;
import org.yinxue.swing.unit.context.Context;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 〈用户自定义的类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/21 9:53
 */
public class ConfigFrame extends JFrame implements YxStructure {

    public static ConfigFrame singleton = new ConfigFrame();

    JPanel jPanel_mainPanel = new JPanel(new GridBagLayout());

    JPanel jPanel_top = new JPanel(new GridBagLayout());
    JLabel jLabel_specClass = new JLabel("指定生成类(类名,一行一类):");
    YxTextArea jText_specClass = new YxTextArea();

    JPanel jPanel_middle = new JPanel(new GridBagLayout());
    JLabel jLabel_specCondition = new JLabel("指定Mock方法(一般指二方后者三方库里的):");
    YxTextArea jText_specCondition = new YxTextArea();

    JPanel jPanel_middle2 = new JPanel(new GridBagLayout());
    JLabel jLabel_resolveType = new JLabel("解析方式:");
    JCheckBox jCheckBox_array = new JCheckBox("数组解析");
    JCheckBox jCheckBox_ast = new JCheckBox("抽象语法树解析");

    JLabel jLabel_outputOption = new JLabel("输出方式:");
    JCheckBox jCheckBox_emptyMethod = new JCheckBox("空方法体输出");
    JCheckBox jCheckBox_override = new JCheckBox("覆盖输出");
    JCheckBox jCheckBox_supply = new JCheckBox("增量输出");
    JCheckBox jCheckBox_addVerify = new JCheckBox("增加验证");
    JCheckBox jCheckBox_addAssert = new JCheckBox("增加断言");
    JCheckBox jCheckBox_lineParam = new JCheckBox("边界入参");

    JLabel jLabel_setting = new JLabel("系统设置:");
    JCheckBox jCheckBox_saveSetting = new JCheckBox("持久化设置参数");
    JCheckBox jCheckBox_modelTrain = new JCheckBox("模型训练");

    JPanel jPanel_button = new JPanel(new FlowLayout(FlowLayout.RIGHT,15,10));
    JButton jButton_cancel = SwingUtils.ofJButton("取消", 80, 30);
    JButton jButton_confirm = SwingUtils.ofJButton("保存", 80, 30);

    public ConfigFrame() {
        init();
        position();
        action();
    }

    @Override
    public void init() {
        this.getContentPane().add(jPanel_mainPanel);
        this.setSize(new Dimension(800, 600));
        this.setLocationRelativeTo(null);
        this.setTitle("自定义参数");

        jPanel_top.setBorder(BorderFactory.createEtchedBorder());
        jPanel_middle.setBorder(BorderFactory.createEtchedBorder());
        jPanel_middle2.setBorder(BorderFactory.createEtchedBorder());
        jPanel_button.setBorder(BorderFactory.createEmptyBorder());

        GridBagConstraints g = new GridBagConstraints();
        g.insets = new Insets(5,5,5,5);
        g.gridx = 0;
        g.gridy = 0;
        g.fill = GridBagConstraints.BOTH;
        g.weightx = 1;
        g.weighty = 1;
        jPanel_mainPanel.add(jPanel_top, g);

        g.gridy = 1;
        jPanel_mainPanel.add(jPanel_middle, g);

        g.gridy = 2;
        g.weighty = 0;
        jPanel_mainPanel.add(jPanel_middle2, g);

        g.gridy = 3;
        g.weighty = 0;
        jPanel_mainPanel.add(jPanel_button, g);
    }

    @Override
    public void position() {
        GridBagConstraints g = new GridBagConstraints();
        g.gridx = 0;
        g.gridy = 0;
        g.anchor = GridBagConstraints.WEST;
        g.fill = GridBagConstraints.BOTH;
        g.weightx = 1;
        g.insets = new Insets(5, 5, 5, 5);
        jPanel_top.add(jLabel_specClass, g);

        g.gridy = 1;
        g.weighty = 1;
        jPanel_top.add(jText_specClass, g);


        GridBagConstraints g1 = new GridBagConstraints();
        g1.gridx = 0;
        g1.gridy = 0;
        g1.anchor = GridBagConstraints.WEST;
        g1.fill = GridBagConstraints.BOTH;
        g1.weightx = 1;
        g1.insets = new Insets(5, 5, 5, 5);

        jPanel_middle.add(jLabel_specCondition, g1);
        g1.weighty = 1;
        g1.gridy = 1;
        jPanel_middle.add(jText_specCondition, g1);

        GridBagConstraints g2 = new GridBagConstraints();
        g2.gridx = 0;
        g2.gridy = 0;
        g2.anchor = GridBagConstraints.WEST;
        g2.weightx = 1;
        g2.insets = new Insets(5, 5, 5, 5);
        jPanel_middle2.add(jLabel_resolveType, g2);
        g2.gridx = 1;
        jPanel_middle2.add(jCheckBox_array, g2);
        g2.gridx= 2;
        jPanel_middle2.add(jCheckBox_ast, g2);

        g2.gridy = 1;
        g2.gridx = 0;
        jPanel_middle2.add(jLabel_outputOption, g2);
        g2.gridx = 1;
        jPanel_middle2.add(jCheckBox_emptyMethod, g2);
        g2.gridx = 2;
        jPanel_middle2.add(jCheckBox_override, g2);
        g2.gridx = 3;
        jPanel_middle2.add(jCheckBox_supply, g2);
        g2.gridx = 4;
        jPanel_middle2.add(jCheckBox_addVerify, g2);
        g2.gridx =5;
        jPanel_middle2.add(jCheckBox_addAssert, g2);
        g2.gridx = 6;
        jPanel_middle2.add(jCheckBox_lineParam, g2);

        g2.gridy = 2;
        g2.gridx = 0;
        jPanel_middle2.add(jLabel_setting, g2);
        g2.gridx = 1;
        jPanel_middle2.add(jCheckBox_saveSetting, g2);
        g2.gridx = 2;
        jPanel_middle2.add(jCheckBox_modelTrain, g2);

        jPanel_button.add(jButton_cancel);
        jPanel_button.add(jButton_confirm);
    }

    @Override
    public void action() {
        jButton_cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ConfigFrame.this.setVisible(false);
            }
        });

        jButton_confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String specClass = jText_specClass.getText();
                Context.config().setSpecClassDesc(specClass);
                ConfigFrame.this.setVisible(false);
            }
        });
    }

    @Override
    public void destory() {

    }

    public void setSpecClassDesc(String specClassDesc){
        jText_specClass.setText(specClassDesc);
    }

    public static void main(String[] args) {
        SwingUtils.initDefaultUI();
        new ConfigFrame().setVisible(true);
    }
}