package org.yinxue.swing.unit.filter;

/**
 * @author zengjian
 * @create 2018-05-07 16:04
 * @since 1.0.0
 */
public class JavaFilter implements Filter {

    private static String suffix = ".java";
    private static String middle = "src\\main\\java";


    @Override
    public boolean isFile(String fileName) {

        if (fileName.endsWith(suffix) && fileName.contains(middle)) {
            if (fileName.endsWith("package-info.java")){
                return false;
            }
            return true;
        }
        return false;
    }
}
