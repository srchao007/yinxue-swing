/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: NodeWave
 * Author:   zengjian
 * Date:     2018/9/28 17:52
 * Description: node细化处理类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.token;

import org.yinxue.swing.unit.ast.node.ClassNode;
import org.yinxue.swing.unit.ast.node.Node;


/**
 * 〈Node细化处理类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/28 17:52
 */
public class NodeWave {

    private Node classNode;

    public void fireNodeEvent(NodeEvent nodeEvent) {
        classNode = nodeEvent.getClassNode();
        if (classNode instanceof ClassNode){
            ClassNode node = (ClassNode) classNode;
            resolveClassNode(node);
        }
    }

    /**
     * 细化解析ClassNode，抽象出更多细致的类
     * @param node
     */
    private void resolveClassNode(ClassNode node) {



    }
}