/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: SymbolTable
 * Author:   zengjian
 * Date:     2018/8/23 9:35
 * Description: 符号表
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.token;

import org.yinxue.swing.core.util.LogUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * 〈符号表〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/23 9:35
 */
public class SymbolTable {

    private final static Set<Character> SYMBOLS = new HashSet<>();

    static {
        InputStream is = SymbolTable.class.getResourceAsStream("/symbol");
        try {
            int a = -1;
            while ((a = is.read()) != -1) {
                if (a=='\r'||a=='\n'){
                    continue;
                }
                SYMBOLS.add((char) a);
            }
        } catch (IOException e) {
            LogUtil.error(SymbolTable.class, "初始化符号类文件失败", e);
        }
    }

    public static boolean contains(char c) {
        return SYMBOLS.contains(c);
    }

}