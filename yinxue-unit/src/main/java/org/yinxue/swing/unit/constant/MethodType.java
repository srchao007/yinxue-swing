package org.yinxue.swing.unit.constant;

/**
 * @author zengjian
 * @create 2018-05-14 9:04
 * @since 1.0.0
 */
public enum MethodType {
    ABSTRACT, STATIC, CONSTRUCTOR, INTERFACE, NEED_MOCK
}
