package org.yinxue.swing.unit.context;


/**
 * 用于管理在swing程序运行时期，不被回收的实例 <br>
 *
 * @author zengjian
 * @create 2018-05-09 16:52
 * @since 1.0.0
 */
public class Context {

    private Context() {
    }

    private static Context context = new Context();
    private static Config config = new Config();
    private static ThreadPool threadPool = new ThreadPool();
    private static MailBox mailBox = new MailBox();
    private static Register register = new Register();

    public static Config config() {
        return config;
    }

    public static ThreadPool threadPool() {
        return threadPool;
    }

    public static Context open() {
        return context;
    }

    public static MailBox mailBox() {
        return mailBox;
    }

    public static Register register() {
        return register;
    }

}
