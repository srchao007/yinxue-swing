package org.yinxue.swing.unit.exception;

/**
 * @author zengjian
 * @create 2018-05-26 14:32
 * @since 1.0.0
 */
public class JavaFileException extends Exception {

    public JavaFileException(String message) {
        super(message);
    }

    public JavaFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
