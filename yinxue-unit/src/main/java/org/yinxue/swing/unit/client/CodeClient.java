package org.yinxue.swing.unit.client;


import java.util.List;

public interface CodeClient {

    /**
     * 批量检查代码
     *
     * @param path 文件夹路径
     */
    List<CodeReport> batchCheckCode(String path);

    /**
     * 检查当前代码
     *
     * @param codeText 代码内容
     */
    CodeReport checkCode(String codeText);
}
