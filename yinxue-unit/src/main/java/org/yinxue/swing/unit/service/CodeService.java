package org.yinxue.swing.unit.service;

public interface CodeService {


    /**
     * 批量检查代码
     *
     * @param path 文件夹路径
     */
    void batchCheckCode(String path);

    /**
     * 检查当前代码
     *
     * @param codeText 代码内容
     */
    void checkCode(String codeText);
}
