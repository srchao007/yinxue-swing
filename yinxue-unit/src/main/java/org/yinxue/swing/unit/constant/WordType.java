/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: WordType
 * Author:   zengjian
 * Date:     2018/8/23 13:40
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.constant;

import java.util.List;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/23 13:40
 */
public enum WordType {

    DECLARED(""), // 声明变量形式
    SCOPE("{"), // { } 作用域形式
    NOTE("/"),
    TRY("try"),
    CATCH("catch"),
    FIANLLY("finally"),
    IF("if") ,
    ELSE("else"),
    SWITCH("switch"),
    WHILE("while"),
    DO("do"),
    FOR("for");

    private static int skipStringMark(List<String> tokens, int start, int size, String endTag) {
        for (int i = start; i < size; i++) {
            String token = tokens.get(i);
            if (!endTag.equals(token) && "\"".equals(token)) {
                // 字符串中的内容进行忽略处理
                for (int j = i + 1; j < size; j++) {
                    String token2 = tokens.get(j);
                    if (token2.equals("\"")) {
                        i = j;
                        break;
                    }
                }
            } else if (endTag.equals(token)) {
                return i;
            } else {
                // doNothing
            }
        }
        return start;
    }

    String keyword;

    WordType(String keyword) {
        this.keyword = keyword;
    }

    public static WordType get(String token) {
        for (WordType wordType : WordType.values()) {
            if (wordType.keyword.equals("token")) {
                return wordType;
            }
        }
        return DECLARED;
    }

//    public abstract ExpressionNode getExpression(Lexer lexer, ExpressionNode parent);

}