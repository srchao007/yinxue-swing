/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: MethodNode
 * Author:   zengjian
 * Date:     2018/9/2 0:07
 * Description: 方法树
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.node;

/**
 * 〈一句话功能简述〉<br>
 * 〈方法树〉
 *
 * @author zengjian
 * @create 2018/9/2
 * @since 1.0.0
 */
public class MethodNode extends Node {

    private Node methodHeader;
    private Node methodBody;


}