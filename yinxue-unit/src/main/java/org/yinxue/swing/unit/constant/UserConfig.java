/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: UserConfig
 * Author:   zengjian
 * Date:     2018/8/15 21:09
 * Description: 用户自定义的数据
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.constant;

import org.yinxue.swing.core.util.EnvUtil;
import org.yinxue.swing.core.util.PropFile;

/**
 * 〈用户自定义的数据〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/15 21:09
 */
public class UserConfig {

    public static final String UNIT_CODE = "/*\n" +
            " * Copyright (C), 2002-2017, 苏宁易购电子商务有限公司\n" +
            " * FileName: QueryAccountBalanceForShoppingReq.java\n" +
            " * Author:   88383854\n" +
            " * Date:     2017年10月12日 下午7:01:52\n" +
            " * Description: //模块目的、功能描述      \n" +
            " * History: //修改记录\n" +
            " * <author>      <time>      <version>    <desc>\n" +
            " * 修改人姓名             修改时间            版本号                  描述\n" +
            " */\n" +
            "package com.suning.camp.rsf.input;\n" +
            "\n" +
            "import java.io.Serializable;\n" +
            "import java.math.BigDecimal;\n" +
            "import java.math.RoundingMode;\n" +
            "import java.util.List;\n" +
            "\n" +
            "/**\n" +
            " * 购物试算接口对应的请求参数部分数据<br> \n" +
            " * @author 88383854\n" +
            " * @see [相关类/方法]（可选）\n" +
            " * @since [产品/模块版本] （可选）\n" +
            " */\n" +
            "public class QueryAccountBalanceForShoppingReq extends BaseReq {\n" +
            "\n" +
            "    /**\n" +
            "     *用于实现对应的序列化相关操作\n" +
            "     */\n" +
            "    private static final long serialVersionUID = -2121707541282665075L;\n" +
            "    \n" +
            "    /**\n" +
            "     *业态类型\n" +
            "     */\n" +
            "    private String ecoType;\n" +
            "    /**\n" +
            "     *会员卡号\n" +
            "     */\n" +
            "    private String cardNo;\n" +
            "    /**\n" +
            "     *会员编号\n" +
            "     */\n" +
            "    private String custNum;\n" +
            "    /**\n" +
            "     *门店代码\n" +
            "     */\n" +
            "    private String register;\n" +
            "    /**\n" +
            "     *分公司代码\n" +
            "     */\n" +
            "    private String branch;\n" +
            "    /**\n" +
            "     *List账户类型实体\n" +
            "     */\n" +
            "    private List<valueStruct> valueStructList;\n" +
            "    \n" +
            "    /**\n" +
            "     * @return the ecoType\n" +
            "     */\n" +
            "    public String getEcoType() {\n" +
            "        return ecoType;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @param ecoType the ecoType to set\n" +
            "     */\n" +
            "    public void setEcoType(String ecoType) {\n" +
            "        this.ecoType = ecoType;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @return the cardNo\n" +
            "     */\n" +
            "    public String getCardNo() {\n" +
            "        return cardNo;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @param cardNo the cardNo to set\n" +
            "     */\n" +
            "    public void setCardNo(String cardNo) {\n" +
            "        this.cardNo = cardNo;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @return the custNum\n" +
            "     */\n" +
            "    public String getCustNum() {\n" +
            "        return custNum;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @param custNum the custNum to set\n" +
            "     */\n" +
            "    public void setCustNum(String custNum) {\n" +
            "        this.custNum = custNum;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @return the register\n" +
            "     */\n" +
            "    public String getStore() {\n" +
            "        return register;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @param register the register to set\n" +
            "     */\n" +
            "    public void setStore(String register) {\n" +
            "        this.register = register;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @return the branch\n" +
            "     */\n" +
            "    public String getBranch() {\n" +
            "        return branch;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @param branch the branch to set\n" +
            "     */\n" +
            "    public void setBranch(String branch) {\n" +
            "        this.branch = branch;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @return the valueStructList\n" +
            "     */\n" +
            "    public List<valueStruct> getValueStructList() {\n" +
            "        return valueStructList;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * @param valueStructList the valueStructList to set\n" +
            "     */\n" +
            "    public void setValueStructList(List<valueStruct> valueStructList) {\n" +
            "        this.valueStructList = valueStructList;\n" +
            "    }\n" +
            "\n" +
            "    public static class valueStruct implements Serializable{\n" +
            "        /**\n" +
            "         *\n" +
            "         */\n" +
            "        private static final long serialVersionUID = -7758087390744419491L;\n" +
            "        /**\n" +
            "         *基本/促销积分，礼金券/心意券\n" +
            "         */\n" +
            "        private String    accountType;\n" +
            "        /**\n" +
            "         *扣减的金额\n" +
            "         */\n" +
            "        private BigDecimal accountVarAmt;\n" +
            "        \n" +
            "        /**\n" +
            "         * 默认构造方法\n" +
            "         */\n" +
            "        public valueStruct() {\n" +
            "            super();\n" +
            "        }\n" +
            "        /**\n" +
            "         * @param accountType   账户类型\n" +
            "         * @param accountVarAmt 扣减额\n" +
            "         */\n" +
            "        public valueStruct(String accountType, BigDecimal accountVarAmt) {\n" +
            "            super();\n" +
            "            this.accountType = accountType;\n" +
            "            this.accountVarAmt = accountVarAmt;\n" +
            "        }\n" +
            "        /**\n" +
            "         * @return the accountType\n" +
            "         */\n" +
            "        public String getAccountType() {\n" +
            "            return accountType;\n" +
            "        }\n" +
            "        /**\n" +
            "         * @param accountType the accountType to set\n" +
            "         */\n" +
            "        public void setAccountType(String accountType) {\n" +
            "            this.accountType = accountType;\n" +
            "        }\n" +
            "        /**\n" +
            "         * @return the accountVarAmt\n" +
            "         */\n" +
            "        public BigDecimal getAccountVarAmt() {\n" +
            "            return accountVarAmt;\n" +
            "        }\n" +
            "        /**\n" +
            "         * @param accountVarAmt the accountVarAmt to set\n" +
            "         */\n" +
            "        public void setAccountVarAmt(BigDecimal accountVarAmt) {\n" +
            "            if(accountVarAmt!=null){\n" +
            "                this.accountVarAmt = accountVarAmt.setScale(2,RoundingMode.DOWN);\n" +
            "            }\n" +
            "        }\n" +
            "        /**\n" +
            "         * {@inheritDoc}\n" +
            "         */\n" +
            "        @Override\n" +
            "        public String toString() {\n" +
            "            return \"valueStruct [accountType=\" + accountType + \", accountVarAmt=\" + accountVarAmt + \"]\";\n" +
            "        }        \n" +
            "    }\n" +
            "    @Override\n" +
            "    public String toString() {\n" +
            "        return \"QueryAccountBalanceForShoppingReq [ecoType=\" + ecoType + \", cardNo=\" + cardNo + \", custNum=\" + custNum\n" +
            "                + \", register=\" + register + \", branch=\" + branch + \", valueStructList=\" + valueStructList + \"]\";\n" +
            "    }\n" +
            "}\n";

    // 默认项目路径
    public static final String DEFAULT_PROJECT_PATH = System.getProperty("user.dir");


    /*************
     * 配置用户参数
     *************/
    private static final String USER_PROJECT_PATH = "userProjectPath";

    private static int userIsPojo;

    /**
     * 读取生成的默认配置文件的参数值
     *
     * @return
     */
    public static String getUserProjectPath() {
        if (EnvUtil.getProperty(USER_PROJECT_PATH) != null) {
            return EnvUtil.getProperty(USER_PROJECT_PATH);
        }
        return DEFAULT_PROJECT_PATH;
    }

    public static void saveUserProjectPath(String userProjectPath) {
        EnvUtil.setProperty(USER_PROJECT_PATH, userProjectPath);
    }

    public static void main(String[] args) {
        saveUserProjectPath("123");
    }

    public static boolean getUserIsPojo() {
        return userIsPojo == 1;
    }
}