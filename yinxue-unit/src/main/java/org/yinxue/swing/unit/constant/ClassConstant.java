/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: ClassConstant
 * Author:   zengjian
 * Date:     2018/8/16 13:25
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.constant;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/16 13:25
 */
public interface ClassConstant {

    /**
     * 1~8 bit
     */
    int ABSTRACT = 1 << 0; // class methodNode
    int INTERFACE = 1 << 1; // class
    int ENUM = 1 << 2;  // class
    int ANNOTATION = 1 << 3; // class
    int GENERIC = 1 << 4; // class

    /**
     * 9~16bit
     */
    int POJO = 1 << 8; // class
    int POJO_PLUS = 1 << 9 | POJO; // class
    int UTIL = 1 << 10; // class 所有方法为静态方法
    int CONSTANT = 1 << 11; // class 常量类
    int NEST = 1 << 12; // class 内部类

    /**
     * 17~24bit
     */
    int STATIC = 1 << 16; // class methodNode field
    int FINAL = 1 << 17; // class methodNode field

    /**
     * 25~32bit
     */
    int PUBLIC = 1 << 24;
    int PROTECTED = 1 << 25;
    int PRIVATE = 1 << 26;
    int DEFAULT = 1 << 27;

}