/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: LiteralTable
 * Author:   zengjian
 * Date:     2018/9/28 12:02
 * Description: 字面量表
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.token;

import org.yinxue.swing.core.util.LogUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 〈字面量表〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/28 12:02
 */
public class LiteralTable {

    public static final Map<String,Object> LITERAL_MAP= new HashMap<>(32);

    static {
        try {
            FileReader fileReader = new FileReader(LiteralTable.class.getResource("/keyword").getFile());
            BufferedReader reader = new BufferedReader(fileReader);
            reader.readLine();



        } catch (IOException e) {
            LogUtil.error(SymbolTable.class, "初始化符号类文件失败", e);
        }
    }

}