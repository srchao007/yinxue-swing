/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: ExpressionNode
 * Author:   zengjian
 * Date:     2018/9/2 0:08
 * Description: 表达式基类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.node;

/**
 * 〈表达式节点〉<br>
 *
 * @author zengjian
 * @create 2018/9/2
 * @since 1.0.0
 */
public class ExpressionNode extends Node {


}