/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: TokenEvent
 * Author:   zengjian
 * Date:     2018/9/28 14:35
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.token;

import java.util.List;

/**
 * 〈〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/28 14:35
 */
public class TokenEvent {

    /**
     * 事件发生源
     */
    private TokenWave tokenWave;

    /**
     * 变动值
     */
    private List<String> tokens;

    public TokenEvent(TokenWave tokenWave, List<String> tokens) {
        this.tokenWave = tokenWave;
        this.tokens = tokens;
    }

    public TokenWave getTokenWave() {
        return tokenWave;
    }

    public void setTokenWave(TokenWave tokenWave) {
        this.tokenWave = tokenWave;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }
}