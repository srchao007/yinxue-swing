//package org.yinxue.swing.unit.ast.javacc;
//
//import com.github.javaparser.ast.Modifier;
//import com.github.javaparser.ast.NodeList;
//import com.github.javaparser.ast.body.BodyDeclaration;
//import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
//import com.github.javaparser.ast.body.FieldDeclaration;
//import com.github.javaparser.ast.body.MethodDeclaration;
//import com.github.javaparser.ast.type.ReferenceType;
//import com.github.javaparser.ast.type.TypeParameter;
//import com.github.javaparser.printer.PrettyPrintVisitor;
//import com.github.javaparser.printer.PrettyPrinterConfiguration;
//
//import java.util.Iterator;
//
//import static com.github.javaparser.utils.Utils.isNullOrEmpty;
//import static java.util.stream.Collectors.joining;
//
//public class UnitTestVisitor extends PrettyPrintVisitor {
//
//    public UnitTestVisitor(PrettyPrinterConfiguration prettyPrinterConfiguration) {
//        super(prettyPrinterConfiguration);
//    }
//
//    @Override
//    public void visit(ClassOrInterfaceDeclaration n, Void arg) {
//        // 如果lombok @Data注解了，则生成对应的GetSet方法
//        n.getAnnotationByName("Data").ifPresent(
//                annotationExpr -> {
//                    int i = n.getMembers().size()-1;
//                    for (;i > 0 ; i--) {
//                        if (n.getMembers().get(i).isFieldDeclaration()) {
//                            this.visit(n.getMembers().get(i).asFieldDeclaration(), arg);
//                        }
//                    }
//                    for (BodyDeclaration<?> member : n.getMembers()) {
//
//                    }
//                });
//        printModifiers(n.getModifiers());
//        if (n.isInterface()) {
//            printer.print("interface ");
//        } else {
//            printer.print("class ");
//        }
//        printer.print(n.getNameAsString() + "Test");
//        printTypeParameters(n.getTypeParameters(), arg);
//        printer.println(" {");
//        printer.indent();
//        if (!isNullOrEmpty(n.getMembers())) {
//            printMembers(n.getMembers(), arg);
//        }
//        printer.unindent();
//        printer.print("}");
//
//    }
//
//    @Override
//    public void visit(FieldDeclaration n, Void arg) {
//        ClassOrInterfaceDeclaration clazz = n.findAncestor(ClassOrInterfaceDeclaration.class).get();
//        clazz.addMember(n.createGetter());
//        clazz.addMember(n.createSetter());
//    }
//
//    private void printMembers(final NodeList<BodyDeclaration<?>> members, final Void arg) {
//        for (final BodyDeclaration<?> member : members) {
//            if (member.isMethodDeclaration() && member.asMethodDeclaration().isPublic()) {
//                printer.println();
//                member.accept(this, arg);
//                printer.println();
//            }
//        }
//    }
//
//    @Override
//    public void visit(MethodDeclaration n, Void arg) {
//        // 非公有方法不打印
//        if (!n.isPublic()) {
//            return;
//        }
//        // 打印注解
//        printer.print("@Test");
//        printer.println();
//        printModifiers(n.getModifiers());
//        printTypeParameters(n.getTypeParameters(), arg);
//        if (!isNullOrEmpty(n.getTypeParameters())) {
//            printer.print(" ");
//        }
//        // 打印返回值默认为void
//        printer.print("void");
//        printer.print(" ");
//        // 打印名称
//        String methodName = n.getName().getIdentifier();
//        methodName = methodName.substring(0, 1).toUpperCase() + methodName.substring(1);
//        printer.print("when" + methodName + "ThenOk");
//        printer.print("(");
//        printer.print(")");
//
//        if (!isNullOrEmpty(n.getThrownExceptions())) {
//            printer.print(" throws ");
//            for (final Iterator<ReferenceType> i = n.getThrownExceptions().iterator(); i.hasNext(); ) {
//                final ReferenceType name = i.next();
//                name.accept(this, arg);
//                if (i.hasNext()) {
//                    printer.print(", ");
//                }
//            }
//        }
//        if (!n.getBody().isPresent()) {
//            printer.print(";");
//        } else {
//            printer.print(" ");
//            printer.print("{");
//            printer.println();
//            printer.print("}");
//            //  TODO 生成对应的方法体
////            n.getBody().get().accept(this, arg);
//        }
//    }
//
//    private void printTypeParameters(final NodeList<TypeParameter> args, final Void arg) {
//        if (!isNullOrEmpty(args)) {
//            printer.print("<");
//            for (final Iterator<TypeParameter> i = args.iterator(); i.hasNext(); ) {
//                final TypeParameter t = i.next();
//                t.accept(this, arg);
//                if (i.hasNext()) {
//                    printer.print(", ");
//                }
//            }
//            printer.print(">");
//        }
//    }
//
//    private void printModifiers(final NodeList<Modifier> modifiers) {
//        if (modifiers.size() > 0) {
//            printer.print(modifiers.stream().map(Modifier::getKeyword).map(Modifier.Keyword::asString).collect(joining(" ")) + " ");
//        }
//    }
//}
