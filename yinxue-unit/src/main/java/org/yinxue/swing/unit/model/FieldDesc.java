package org.yinxue.swing.unit.model;


import org.yinxue.swing.unit.util.UnitUtil;

/**
 * 属性封装类
 *
 * @author zengjian
 * @create 2018-05-09 13:57
 * @since 1.0.0
 */
public class FieldDesc {

    public int modifiers;

    public String type;
    public String setMethod;
    public String getMethod;
    public String modifier;
    public String varName;
    public String upperVarName;
    public String line;
    public String context;
    public String annotation;
    public Object parent;

    public FieldDesc(String line, String annotation) {
        this.line = line;
        this.annotation = annotation;
        initField();
    }

    private void initField() {
        String key = this.line.trim().replaceAll("(( *=[\\s\\S]*;)|transient|volatile|;)", "");
        // [0]修饰符 [1]类名 [2]变量名
        String[] words = key.split("\\s+");
        // 兼容没有修饰符的情况  [0]类名 [1]变量名
        if (words.length==2){
            this.type = words[0];
            this.varName = words[1];
        } else {
            this.modifier = words[0];
            this.type = words[1];
            this.varName = words[2];
        }
        this.upperVarName = UnitUtil.toUpperCaseFirstChar(varName);
        this.setMethod = "set" + this.upperVarName;
        this.getMethod = "get" + this.upperVarName;
    }

    @Override
    public String toString() {
        return type + ' ' + varName + ' ' + annotation+"\n";
    }
}
