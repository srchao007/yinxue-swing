package org.yinxue.swing.unit.filter;

/**
 * 过滤器
 * @author zengjian
 * @create 2018-05-07 16:03
 * @since 1.0.0
 */
public interface Filter {
    /**
     * 是否为对应的Filter
     * @return
     */
    boolean isFile(String fileName);
}
