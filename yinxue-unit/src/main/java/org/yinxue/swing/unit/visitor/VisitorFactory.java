/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: VisitorFactory
 * Author:   zengjian
 * Date:     2018/9/27 11:41
 * Description: 访问者工厂
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.visitor;

import java.util.HashMap;
import java.util.Map;

/**
 * 〈访问者工厂〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/27 11:41
 */
public final class VisitorFactory {

    private static final Map<Class<? extends Visitor>, Visitor> VISITOR_CACHE_MAP = new HashMap<>(16);

    public static Visitor getInstance(Class<? extends Visitor> clazz) {
        if (!VISITOR_CACHE_MAP.containsKey(clazz)) {
            try {
                VISITOR_CACHE_MAP.put(clazz, clazz.newInstance());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return VISITOR_CACHE_MAP.get(clazz);
    }

}