/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: AnnotationNode
 * Author:   zengjian
 * Date:     2018/9/6 11:40
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.node;

/**
 * 〈注解节点〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/6 11:40
 */
public class AnnotationNode extends Node {

}