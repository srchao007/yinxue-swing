/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: ThreadPool
 * Author:   zengjian
 * Date:     2018/8/31 14:09
 * Description: 线程管理类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.context;

import java.util.concurrent.*;

/**
 * 〈线程管理类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/31 14:09
 */
public class ThreadPool {

    /**
     * 用于执行事件处理的单线程，形成队列效果
     */
    private ExecutorService singleExecutor = Executors.newFixedThreadPool(1);


    /**
     * 用于执行多个线程
     */
    private ExecutorService multiExecutor = new ThreadPoolExecutor(
            0,
            10,
            3000L,
            TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(1000),
            new ThreadPoolExecutor.CallerRunsPolicy());

    /**
     * 监听线程池，用于监听消息队列
     */
    private ExecutorService listenerExecutor = Executors.newFixedThreadPool(1);


    public void submitSingle(Runnable runnable) {
        singleExecutor.execute(runnable);
    }

    public <V> Future<V> submitCallable(Callable<V> callable) {
        return multiExecutor.submit(callable);
    }

    public void submitListener(Runnable runnable) {
        listenerExecutor.submit(runnable);
    }


}