/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Config
 * Author:   zengjian
 * Date:     2018/8/31 14:05
 * Description: 配置类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.context;

import org.yinxue.swing.core.util.LogUtil;

import java.io.IOException;
import java.io.InputStream;

/**
 * 〈界面配置类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/31 14:05
 */
public class Config {

    public String projectPath;
    public boolean isPojo;
    public String operateType;
    public String unitType;
    public String useAST; // 使用抽象树
    public String specClassDesc; // 生成指定的类
    public String defaultUnitCode;

    public Config() {
        init();
    }

    private void init() {
        InputStream is = this.getClass().getResourceAsStream("/defaultunitcode");
        try {
            byte[] bytes = new byte[is.available()];
            is.read(bytes);
            defaultUnitCode = new String(bytes,"utf-8");
            LogUtil.info(this, "初始化默认代码:\n"+defaultUnitCode);
        } catch (IOException e) {
            LogUtil.error(this, "加载资源文件defaultunitcode失败", e);
        }
    }

    public void setPojo(boolean pojo) {
        this.isPojo = pojo;
    }

    public boolean isPojo(){
        return isPojo;
    }

    public void setSpecClassDesc(String specClassDesc) {
        this.specClassDesc = specClassDesc;
    }

    public String getSpecClassDesc(){
        return this.specClassDesc;
    }
}