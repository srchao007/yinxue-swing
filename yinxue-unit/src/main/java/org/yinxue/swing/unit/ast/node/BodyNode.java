/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: BodyNode
 * Author:   zengjian
 * Date:     2018/9/6 10:15
 * Description: {} 作用域体
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.node;

/**
 * 〈{} 作用域节点〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/6 10:15
 */
public class BodyNode extends Node {

    private Node header;

    public Node header(Node header){
        this.header = header;
        return this;
    }

}