package org.yinxue.swing.unit.main;


import org.yinxue.swing.core.util.LogUtil;
import org.yinxue.swing.core.util.SwingUtils;
import org.yinxue.swing.unit.context.Context;

import javax.swing.*;
import java.awt.*;

public class MainTabPanel extends JPanel {

    private Context context = Context.open();
    private JTabbedPane jTabbedPane = new JTabbedPane();

    public MainTabPanel() {
        jTabbedPane.addTab("单元测试2", new UnitPanel2());
        setLayout(new GridLayout(1, 1, 5, 5));
        add(jTabbedPane);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                LogUtil.info(this, "初始化ui");
                SwingUtils.initDefaultUI();
                JFrame frame = SwingUtils.ofJFrameWithMenuBar("main");
                frame.add(new MainTabPanel());
                LogUtil.info(this, "初始化主窗口结束");
                frame.setVisible(true);
            }
        });
    }
}
