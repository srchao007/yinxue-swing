/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: PackageNode
 * Author:   zengjian
 * Date:     2018/9/6 10:24
 * Description: 导入包节点
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.node;

/**
 * 〈路径包节点〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/6 10:24
 */
public class PackageNode extends StatementNode {
    
    private String packageName;


}


