package org.yinxue.swing.unit.constant;

/**
 * @author zengjian
 * @create 2018-05-09 14:42
 * @since 1.0.0
 */
public enum ClassType {
    INTERFACE, ABSTRACT, ENUM, ANNOTATION, GENERIC;
}
