/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StatementNode
 * Author:   zengjian
 * Date:     2018/9/2 0:08
 * Description: 语句基类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit.ast.node;

/**
 * 〈以;结尾的语句部分，一般从 { 或者 }截取至; 或者是〉<br>
 *
 * @author zengjian
 * @create 2018/9/2
 * @since 1.0.0
 */
public class StatementNode extends Node {

}