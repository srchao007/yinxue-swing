package org.yinxue.swing.unit.client;

import java.util.Collections;
import java.util.List;

public class CheckCodeClient implements CodeClient {

    @Override
    public List<CodeReport> batchCheckCode(String path) {
        return Collections.singletonList(new CodeReport());
    }

    @Override
    public CodeReport checkCode(String codeText) {
        return new CodeReport();
    }
}
