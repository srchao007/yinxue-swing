package org.yinxue.swing.unit.constant;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.*;

/**
 * 常见类型
 *
 * @author zengjian
 * @create 2018-03-29 19:45
 * @since 1.0.0
 */
public enum CommonType {
    INT("int", "1", "a"),
    INTEGER("Integer", "1", "a"),
    BOOLEAN("boolean", "false", "flag"),
    BOOLEAN2("Boolean", "false", "flag"),
    STRING("String", "\"123456789\"", "str"),
    DOUBLE("Double", "1.0", "d"),
    DOUBLE2("Double", "1.0", "d"),
    LONG("long", "1L", "a"),
    LONG2("Long", "2L", "l"),
    SHORT("short", "(short) 1", "a"),
    SHORT2("Short", "(short) 1", "a"),
    BIGINTEGER("BigInteger", "new BigInteger(\"1\")", "value", BigInteger.class),
    DATE("Date", "new Date()", "date", Date.class),
    TIMESTAMP("Timestamp", "Timestamp.valueOf(\"9999-12-31 00:00:00\")", "time", Timestamp.class),
    LIST("List", "new ArrayList()", "list", List.class, ArrayList.class),
    LIST_T("List<T>", "new ArrayList()", "list", List.class, ArrayList.class),
    ARRAYLIST("ArrayList", "new ArrayList()", "list", ArrayList.class, ArrayList.class),
    LINKEDLIST("LinkedList", "new LinkedList()", "list", LinkedList.class),
    MAP("Map", "new HashMap()", "map", Map.class, HashMap.class),
    MAP_ARRAY("Map[]", "new HashMap[]{}", "mapArray", Map.class, HashMap.class),
    HASHMAP("HashMap", "new HashMap()", "map", HashMap.class),
    BIGDECIMAL("BigDecimal", "new BigDecimal(\"123\")", "value", BigDecimal.class),
    STRINGARRAY("String[]", "new String[]{}", "ss"),
    INT_ARRAY("int[]", "new int[]{}", "aa"),
    CLASS_ARRAY("Class[]", "new Class[]{}", "clzzArray"),
    INTEGER_ARRAY("Integer[]", "new Integer[]{}", "integerArray"),
    COLLECTION("Collection", "new ArrayList()", "cl", Collection.class, ArrayList.class),
    CLASS("Class", "Object.class", "declaredClazz"),
    SET("Set", "new HashSet()", "set", Set.class, HashSet.class),
    HASHSET("HashSet", "new HashSet()", "set", Set.class, HashSet.class);

    private String classType;
    private String initParamValue;
    private String initParamVariable;
    private Class declaredClazz;
    private Class realInitClazz;

    private static LinkedHashMap<String, String[]> map = new LinkedHashMap<>();
    private static Map<String, String> typeImportMap = new LinkedHashMap<>();
    private static Map<String, String> typeInitSimpleName = new LinkedHashMap<>();

    static {
        for (CommonType commonType : CommonType.values()) {
            map.put(commonType.classType, new String[]{commonType.initParamValue, commonType.initParamVariable});
            if (commonType.declaredClazz != null) {
                typeImportMap.put(commonType.classType, commonType.declaredClazz.getName());
            }
            if (commonType.realInitClazz != null) {
                typeInitSimpleName.put(commonType.classType, commonType.realInitClazz.getSimpleName());
            }
        }
    }

    CommonType(String classType, String initParamValue, String initParamVariable) {
        this.classType = classType;
        this.initParamValue = initParamValue;
        this.initParamVariable = initParamVariable;
    }

    CommonType(String classType, String initParamValue, String initParamVariable, Class declaredClazz) {
        this.classType = classType;
        this.initParamValue = initParamValue;
        this.initParamVariable = initParamVariable;
        this.declaredClazz = declaredClazz;
    }

    CommonType(String classType, String initParamValue, String initParamVariable, Class declaredClazz, Class realInitClazz) {
        this.classType = classType;
        this.initParamValue = initParamValue;
        this.initParamVariable = initParamVariable;
        this.declaredClazz = declaredClazz;
        this.realInitClazz = realInitClazz;
    }

    public static String getInitParamValue(String classType) {
        if (map.get(classType) != null) {
            return map.get(classType)[0];
        }
        return null;
    }

    public static String getInitParamVariable(String classType) {
        if (map.get(classType) != null) {
            return map.get(classType)[1];
        }
        return null;
    }

    public static String getDeclaredClassImportPath(String classType) {
        if (typeImportMap.get(classType) != null) {
            return typeImportMap.get(classType);
        }
        return null;
    }

    /**
     * 获得初始化对象的import包
     *
     * @param classType
     * @return
     */
    public static String getRealInitClassName(String classType) {
        if (typeInitSimpleName.get(classType) != null) {
            return typeInitSimpleName.get(classType);
        }
        return classType;
    }
}
