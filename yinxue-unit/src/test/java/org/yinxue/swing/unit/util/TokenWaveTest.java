package org.yinxue.swing.unit.util;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicInteger;

public class TokenWaveTest {

    @Test
    public void testInteger() {
        AtomicInteger b = new AtomicInteger(1);
        doSomething(b);
        System.out.println(b);
        Integer a = 1;
    }

    private void doSomething(AtomicInteger a) {
        a.incrementAndGet();
    }

    @Test
    public void testArr() {
        int[] abc = new int[1];
        count(abc);
        System.out.println(abc[0]);
    }

    private void count(int[] abc) {
        abc[0]++;
    }

    @Test
    public void testWhile() {
        int a = 1;
        while (a < 5) {
            // 死循环
            if (a > 2) {
//                continue; 死循环
                break;
            }
            System.out.println(a);
            a++;
        }
    }

    @Test
    public void TestFalse() {
        System.out.println(false || false);
    }

}