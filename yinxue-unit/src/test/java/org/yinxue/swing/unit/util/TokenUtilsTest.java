package org.yinxue.swing.unit.util;

import org.junit.Test;
import org.yinxue.swing.unit.ast.token.TokenWave;


import java.util.List;

public class TokenUtilsTest {

    @Test
    public void spiltTokens() {
        String ss = "private void buildMockMethod() {\n" +
                "        for (StatementDesc childMethod : this.childMethods) {\n" +
                "            // 根据方法行，内部获取准确的返回类型及变量\n" +
                "            childMethod.findReturnParam(childMethod, childMethod.methodLine);\n" +
                "        }\n" +
                "\n" +
                "        // 由于存在返回参数的相同，所以需要再开循环解决相同参数的问题\n" +
                "        // 采用Map来判断，如果map中包含了，那就将该变量进行修改，计数相加的方式，同时将子方法中的变量赋值\n" +
                "        Map<String, Integer> countMap = new HashMap<>();\n" +
                "        for (StatementDesc childMethod : this.childMethods) {\n" +
                "            if (!countMap.containsKey(childMethod.returnParam)) {\n" +
                "                countMap.put(childMethod.returnParam, 1);\n" +
                "            } else {\n" +
                "                int count = countMap.get(childMethod.returnParam);\n" +
                "                count++;\n" +
                "                countMap.put(childMethod.returnParam, (count));\n" +
                "                childMethod.returnParam = childMethod.returnParam + count;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "\n" +
                "        for (StatementDesc childMethod : this.childMethods) {\n" +
                "            // 此处的EIGHT_SPACE由方法 mockReturnVariable内部控制\n" +
                "            this.unitVariableLines.append(childMethod.mockReturnVariable(childMethod));\n" +
                "            this.unitMockMethodLines.append(SSSSSSSS).append(childMethod.mockMethod(childMethod));\n" +
                "        }\n" +
                "\n" +
                "    }";
        TokenWave tokenWave = new TokenWave(ss);
        //System.out.println(tokenWave.getTokens());
        List<String> list  = tokenWave.getTokens();
        for (String s : list) {
            System.out.println(s+"--->");
        }
    }


    @Test
    public void spiltTokens2() {
        String ss = "/**\n" +
                " * Copyright (C), 2017-2018, XXX有限公司\n" +
                " * FileName: MockitoVisitor\n" +
                " * Author:   zengjian\n" +
                " * Date:     2018/8/16 13:57\n" +
                " * Description: Mockito框架单元测试生成规则\n" +
                " * History:\n" +
                " * <author>          <time>          <version>          <desc>\n" +
                " * 作者姓名           修改时间           版本号              描述\n" +
                " */\n" +
                "package org.yinxueframework.swing.unit.visitor;\n" +
                "\n" +
                "import StringUtil;\n" +
                "import org.yinxueframework.swing.unit.constant.ClassConstant;\n" +
                "import org.yinxueframework.swing.unit.constant.CommonType;\n" +
                "import org.yinxueframework.swing.unit.context.Context;\n" +
                "import org.yinxueframework.swing.unit.model.ClassDesc;\n" +
                "import org.yinxueframework.swing.unit.model.FieldDesc;\n" +
                "import org.yinxueframework.swing.unit.model.MethodDesc;\n" +
                "import org.yinxueframework.swing.unit.model.StatementDesc;\n" +
                "import org.yinxueframework.swing.unit.util.UnitUtil;\n" +
                "\n" +
                "import java.util.*;\n" +
                "\n" +
                "import static org.yinxueframework.swing.unit.constant.UnitConstant.*;\n" +
                "\n" +
                "/**\n" +
                " * 〈Mockito框架单元测试生成规则〉<br>\n" +
                " * 〈一句话描述〉\n" +
                " *\n" +
                " * @author zengjian\n" +
                " * @create 2018/8/16 13:57\n" +
                " */\n" +
                "public class MockitoVisitor implements Visitor, ClassConstant {\n" +
                "\n" +
                "    /**\n" +
                "     * 同一线程复用同一ClassUnitContext对象\n" +
                "     */\n" +
                "    private volatile static ThreadLocal<ClassUnitContext> unitContextLocal = new ThreadLocal<>();\n" +
                "\n" +
                "\n" +
                "    @Override\n" +
                "    public String visit(ClassDesc classDesc) {\n" +
                "        String unitContext = null;\n" +
                "        if ((classDesc.modifier & ENUM) != 0) {\n" +
                "            unitContext = toBuildUnitContext(classDesc);\n" +
                "        } else if ((classDesc.modifier & GENERIC) != 0) {\n" +
                "            unitContext = toBuildUnitContext(classDesc);\n" +
                "        } else {\n" +
                "            // ignore 抽象类、注解、接口暂时忽略掉\n" +
                "        }\n" +
                "        return unitContext;\n" +
                "    }\n" +
                "\n" +
                "    private String toBuildUnitContext(ClassDesc classDesc) {\n" +
                "        ClassUnitContext unitContext = getThreadUnitContext(classDesc);\n" +
                "        // 导入包调整为最后添加\n" +
                "        this.doMakeUnitPackageLine(classDesc);\n" +
                "        this.doMakeUnitClassHeader(classDesc);\n" +
                "        this.doMakeUnitMethodLines(classDesc);\n" +
                "        this.doMakeUnitFieldLines(classDesc);\n" +
                "        this.doMakeUnitImportLines(classDesc);\n" +
                "        String unit = unitContext.unitPackageLine\n" +
                "                .append(unitContext.unitImportLines).append(\"\\n\")\n" +
                "                .append(unitContext.unitClassNameHeader)\n" +
                "                .append(unitContext.unitClassVariables)\n" +
                "                .append(unitContext.unitMethodLines)\n" +
                "                .append(unitContext.unitClassNameFooter)\n" +
                "                .toString();\n" +
                "        // 清理StringBuilder，保证容器实例存在时，重复使用时为空\n" +
                "        unitContext.reset();\n" +
                "        return unit;\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    private void doMakeUnitPackageLine(ClassDesc classDesc) {\n" +
                "        classDesc.tempUnitContext.unitPackageLine.append(classDesc.packageLine + \"\\n\\n\");\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    private void doMakeUnitImportLines(ClassDesc classDesc) {\n" +
                "        for (String dynamicImportLine : classDesc.tempUnitContext.targetImportLines) {\n" +
                "            classDesc.tempUnitContext.unitImportLines.append(dynamicImportLine + \"\\n\");\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    private void doMakeUnitClassHeader(ClassDesc classDesc) {\n" +
                "        classDesc.tempUnitContext.unitClassNameHeader.append(\"public class \" + classDesc.simpleName + \"Test\" + \" {\\n\\n\");\n" +
                "    }\n" +
                "\n" +
                "    private void doMakeUnitFieldLines(ClassDesc classDesc) {\n" +
                "        if ((classDesc.modifier & POJO) != 0 || ((classDesc.modifier & UTIL) != 0 )\n" +
                "                || (classDesc.modifier & ENUM) != 0 || classDesc.methodMap.isEmpty()) {\n" +
                "            return;\n" +
                "        }\n" +
                "\n" +
                "        ClassUnitContext unitContext = classDesc.tempUnitContext;\n" +
                "\n" +
                "\n" +
                "        // 注入待测实体(待测实体是以无参构造进行构造的)\n" +
                "        unitContext.unitClassNameHeader.append(SSSS).append(INJECTMOCKS)\n" +
                "                .append(SSSS).append(\"private \").append(classDesc.simpleName).append(S).append(classDesc.defaultVariable).append(\";\\n\\n\");\n" +
                "\n" +
                "        // 按注解mock\n" +
                "        for (Map.Entry<String, FieldDesc> entry : classDesc.fieldMap.entrySet()) {\n" +
                "            String fieldAnnotation = entry.getValue().annotation;\n" +
                "            String fieldType = entry.getValue().type;\n" +
                "            String fieldVariable = entry.getKey();\n" +
                "\n" +
                "            if (fieldAnnotation != null) {\n" +
                "                unitContext.unitClassVariables\n" +
                "                        .append(SSSS).append(\"@Mock\\n\")\n" +
                "                        .append(SSSS).append(\"private \").append(fieldType).append(S).append(fieldVariable).append(\";\\n\\n\");\n" +
                "                unitContext.targetFieldLines.add(BEFORE_METHOD_STATEMENT);\n" +
                "                refreshTargetImportLines(fieldType, classDesc, unitContext);\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        // 按需要添加beforemethod等项\n" +
                "        for (String targetFieldLine : unitContext.targetFieldLines) {\n" +
                "            unitContext.unitClassVariables.append(targetFieldLine);\n" +
                "        }\n" +
                "\n" +
                "        // mockito、testng导入包，可根据生成的单元测试不同进行置换\n" +
                "        unitContext.targetImportLines.add(IMPORT_TESTNG_BEFORE_METHOD); // @BeforeMethod\n" +
                "        unitContext.targetImportLines.add(IMPORT_MOCKITO); // @Mock @InjectMock MockitoAnnotations.initMocks(this)\n" +
                "\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    private static void refreshTargetImportLines(String paramType, ClassDesc classDesc, ClassUnitContext unitContext) {\n" +
                "        if (StringUtil.isEmpty(paramType) || unitContext == null) {\n" +
                "            return;\n" +
                "        }\n" +
                "        if (classDesc.sourceImportLines.containsKey(paramType)) {\n" +
                "            unitContext.targetImportLines.add(classDesc.sourceImportLines.get(paramType));\n" +
                "        }\n" +
                "        if (CommonType.getDeclaredClassImportPath(paramType) != null) {\n" +
                "            unitContext.targetImportLines.add(\"import \" + CommonType.getDeclaredClassImportPath(paramType) + \";\");\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    private void doMakeUnitMethodLines(ClassDesc classDesc) {\n" +
                "        ClassUnitContext unitContext = classDesc.tempUnitContext;\n" +
                "        if (!classDesc.methodMap.isEmpty()) {\n" +
                "            unitContext.targetImportLines.add(IMPORT_TESTNG);\n" +
                "        }\n" +
                "\n" +
                "        // 对pojo的单独处理\n" +
                "        if ((classDesc.modifier & POJO) != 0) {\n" +
                "            unitContext.unitMethodLines.append(SSSS + \"@Test\\n\").append(SSSS + \"public void testEntity() \");\n" +
                "            // 只要有一个方法抛异常就需要抛出异常\n" +
                "            appendExceptionIfExsit4Pojo(classDesc, unitContext);\n" +
                "            unitContext.unitMethodLines.append(appendConstrutorHeader4Pojo(classDesc, unitContext));\n" +
                "            // 说明有内部类，需要拼接header\n" +
                "            if (classDesc.hasNestClass()) {\n" +
                "                for (ClassDesc nestClassDesc : classDesc.nestClassList) {\n" +
                "                    unitContext.unitMethodLines.append(appendConstrutorHeader4Pojo(nestClassDesc, unitContext));\n" +
                "                }\n" +
                "                // 拼接内部类的get set等方法\n" +
                "                for (ClassDesc nestClassDesc : classDesc.nestClassList) {\n" +
                "                    unitContext.unitMethodLines.append(appendMethodBody4Pojo(nestClassDesc, unitContext));\n" +
                "                }\n" +
                "            }\n" +
                "            unitContext.unitMethodLines.append(appendMethodBody4Pojo(classDesc, unitContext));\n" +
                "            unitContext.unitMethodLines.append(SSSS + \"}\\n\");\n" +
                "            return;\n" +
                "        }\n" +
                "        // 测试方法去重复\n" +
                "        Map<String, Integer> uniqueMethodNameMap = new HashMap<>(48);\n" +
                "        for (Map.Entry<String, MethodDesc> methodEntry : classDesc.methodMap.entrySet()) {\n" +
                "            MethodDesc methodDesc = methodEntry.getValue();\n" +
                "            /* 由于可能存在方法重载的问题，所以需要对相同方法名称的情况，进行序号编号\n" +
                "               需要将对方法header的解析放在什么外部来解析 */\n" +
                "            String methodName = methodDesc.getMethodName();\n" +
                "            if (methodName == null) {\n" +
                "                continue;\n" +
                "            }\n" +
                "            String uniqueMethodName = methodNameMayChange(uniqueMethodNameMap, methodName);\n" +
                "            unitContext.unitMethodLines\n" +
                "                    .append(SSSS).append(\"@Test\\n\")\n" +
                "                    .append(SSSS).append(\"public void test\").append(StringUtil.toUpperCaseFirstChar(uniqueMethodName)).append(\"() \");\n" +
                "            appendExceptionIfExsit(unitContext.unitMethodLines, methodDesc.exceptions, methodDesc);\n" +
                "            unitContext.unitMethodLines.append(visit(methodDesc));\n" +
                "            unitContext.unitMethodLines.append(SSSS).append(\"}\\n\\n\");\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    // 方法名去重\n" +
                "    private String methodNameMayChange(Map<String, Integer> uniqueMethodNameMap, String methodName) {\n" +
                "        String uniqueMethodName = methodName;\n" +
                "        if (uniqueMethodNameMap.containsKey(methodName)) {\n" +
                "            Integer count = uniqueMethodNameMap.get(methodName);\n" +
                "            count++;\n" +
                "            uniqueMethodName += count;\n" +
                "            uniqueMethodNameMap.put(methodName, count);\n" +
                "        } else {\n" +
                "            uniqueMethodNameMap.put(methodName, 0);\n" +
                "        }\n" +
                "        return uniqueMethodName;\n" +
                "    }\n" +
                "\n" +
                "    private void appendExceptionIfExsit4Pojo(ClassDesc classDesc, ClassUnitContext unitContext) {\n" +
                "        boolean exsitException = false;\n" +
                "        for (MethodDesc methodDesc : classDesc.methodMap.values()) {\n" +
                "            if (methodDesc.exceptions.size() > 0) {\n" +
                "                exsitException = true;\n" +
                "                break;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        if (exsitException == false) {\n" +
                "            unitContext.unitMethodLines.append(\"{\\n\");\n" +
                "            return;\n" +
                "        }\n" +
                "        unitContext.unitMethodLines.append(\" throws \");\n" +
                "        for (MethodDesc methodDesc : classDesc.methodMap.values()) {\n" +
                "            if (!methodDesc.exceptions.isEmpty()) {\n" +
                "                for (String exception : methodDesc.exceptions) {\n" +
                "                    unitContext.unitMethodLines.append(exception).append(\",\");\n" +
                "                    refreshTargetImportLines(exception, classDesc, unitContext);\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        // 前面已判断有Exception，所以直接将,进行替换即可\n" +
                "        unitContext.unitMethodLines.setCharAt(unitContext.unitMethodLines.lastIndexOf(\",\"), ' ');\n" +
                "        unitContext.unitMethodLines.append(\"{\\n\");\n" +
                "    }\n" +
                "\n" +
                "    static void appendExceptionIfExsit(StringBuilder unitMethodLines, List<String> exceptions, MethodDesc methodDesc) {\n" +
                "        if (!exceptions.isEmpty()) {\n" +
                "            unitMethodLines.append(\" throws \");\n" +
                "            Iterator<String> iterator = exceptions.iterator();\n" +
                "            while (iterator.hasNext()) {\n" +
                "                String exception = iterator.next();\n" +
                "                unitMethodLines.append(exception);\n" +
                "                refreshTargetImportLines(exception, methodDesc.parent, methodDesc.parent.tempUnitContext);\n" +
                "                if (iterator.hasNext()) {\n" +
                "                    unitMethodLines.append(\",\");\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        unitMethodLines.append(\"{\\n\");\n" +
                "    }\n" +
                "\n" +
                "    private StringBuilder appendConstrutorHeader4Pojo(ClassDesc nestClassDesc, ClassUnitContext unitContext) {\n" +
                "        // 以下需要通过definition对象的parent来判断是否为子类还是主类，主类不加前缀，子类需要加主类前缀\n" +
                "        StringBuilder construtorBuilder = new StringBuilder(128);\n" +
                "        // 查看是否有有参构造器，将构造器按照\n" +
                "        Iterator<Map.Entry<String, MethodDesc>> iterator = nestClassDesc.constructrorMap.entrySet().iterator();\n" +
                "        int count = 0;\n" +
                "        while (iterator.hasNext()) {\n" +
                "            Map.Entry<String, MethodDesc> entry = iterator.next();\n" +
                "            MethodDesc constructor = entry.getValue();\n" +
                "            Map<String, String> params = UnitUtil.getMethodEntryParams(constructor.methodHeader);\n" +
                "            // 没入参默认为无参构造器，按默认变量拼接\n" +
                "            if (params == null || params.isEmpty()) {\n" +
                "                buildConstrutorPrefix(nestClassDesc, construtorBuilder, 0, params, unitContext);\n" +
                "                // 遍历取出属性，然后拼接一个有参对象\n" +
                "            } else {\n" +
                "                buildConstrutorPrefix(nestClassDesc, construtorBuilder, count, params, unitContext);\n" +
                "            }\n" +
                "            if (iterator.hasNext()) {\n" +
                "                count++;\n" +
                "            }\n" +
                "        }\n" +
                "        return construtorBuilder;\n" +
                "    }\n" +
                "\n" +
                "    private void buildConstrutorPrefix(ClassDesc nestClassDesc, StringBuilder builder, int i, Map<String, String> params, ClassUnitContext unitContext) {\n" +
                "        builder.append(SSSSSSSS + nestClassDesc.compositClassName + \" \" + nestClassDesc.defaultVariable + (i == 0 ? \"\" : i) + \" = \");\n" +
                "        if ((nestClassDesc.modifier & ENUM) != 0) {\n" +
                "            builder.append(nestClassDesc.enumList.get(0)).append(\";\\n\");\n" +
                "        } else {\n" +
                "            builder.append(\"new \" + nestClassDesc.compositClassName + \"(\");\n" +
                "            appendParamsAndEnd(builder, params, nestClassDesc, unitContext);\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    private StringBuilder appendMethodBody4Pojo(ClassDesc classDesc, ClassUnitContext unitContext) {\n" +
                "        StringBuilder testMethodBody = new StringBuilder(256);\n" +
                "\n" +
                "        StringBuilder setMethodBody = new StringBuilder(128);\n" +
                "        StringBuilder getMethodBody = new StringBuilder(128);\n" +
                "        StringBuilder otherMethodBody = new StringBuilder(128);\n" +
                "        StringBuilder setGetDateNullBody = new StringBuilder(128);\n" +
                "        List<MethodDesc> setDateMethods = new ArrayList<>(6);\n" +
                "\n" +
                "        for (MethodDesc methodDesc : classDesc.methodMap.values()) {\n" +
                "            String methodName = methodDesc.methodName;\n" +
                "            if (methodName.startsWith(\"set\") || methodName.startsWith(\"is\")) {\n" +
                "                setMethodBody.append(SSSSSSSS).append(methodDesc.getParentVariable()).append(\".\").append(methodName).append(\"(\");\n" +
                "                Iterator<Map.Entry<String, String>> iterator = methodDesc.params.entrySet().iterator();\n" +
                "                while (iterator.hasNext()) {\n" +
                "                    Map.Entry<String, String> entry = iterator.next();\n" +
                "                    String paramType = entry.getValue();\n" +
                "                    // 表示泛型\n" +
                "                    if (paramType.matches(\"^[A-Z][0-9]?$\")) {\n" +
                "                        paramType = \"Object\";\n" +
                "                    }\n" +
                "\n" +
                "                    String defaultParamValue = null;\n" +
                "                    // 处理内部类的情况\n" +
                "                    if (classDesc.hasNestClass()) {\n" +
                "                        // 有内部类先判断内部类的情况\n" +
                "                        for (int i = 0, size = classDesc.nestClassList.size(); i < size; i++) {\n" +
                "                            if (paramType.equals(classDesc.nestClassList.get(i).simpleName)) {\n" +
                "                                defaultParamValue = StringUtil.toLowerCaseFirstChar(paramType);\n" +
                "                                break;\n" +
                "                            }\n" +
                "                        }\n" +
                "                    }\n" +
                "\n" +
                "                    // 如果没有匹配的内部类再按一般的类判断\n" +
                "                    if (defaultParamValue == null) {\n" +
                "                        defaultParamValue = CommonType.getInitParamValue(paramType);\n" +
                "                    }\n" +
                "                    // 按照实际初始的类导入包\n" +
                "                    String initParamType = CommonType.getRealInitClassName(paramType);\n" +
                "                    // 配合进入的是内部类的情况\n" +
                "                    ClassDesc classDesc1 = classDesc;\n" +
                "                    for (; (classDesc1.modifier & NEST) != 0; classDesc1 = classDesc1.parent) {\n" +
                "                    }\n" +
                "                    refreshTargetImportLines(initParamType, classDesc1, classDesc1.tempUnitContext);\n" +
                "                    // 解决 java.util.Date 和 java.sql.Date冲突问题\n" +
                "                    if (\"new Date()\".equals(defaultParamValue)) {\n" +
                "                        if (classDesc.sourceImportLines.get(\"Date\").contains(\"java.sql.Date\")) {\n" +
                "                            defaultParamValue = \"new Date(System.currentTimeMillis())\";\n" +
                "                        }\n" +
                "                    }\n" +
                "                    setMethodBody.append(defaultParamValue);\n" +
                "\n" +
                "                    // 处理Date 和 Timestamp sonar覆盖的问题， 需要设置null然后再进行get，先缓存set的选项\n" +
                "                    if ((\"Date\".equals(paramType) || \"Timestamp\".equals(paramType)) && methodDesc.params.size() == 1) {\n" +
                "                        setDateMethods.add(methodDesc);\n" +
                "                    }\n" +
                "                    if (iterator.hasNext()) {\n" +
                "                        setMethodBody.append(\",\");\n" +
                "                    }\n" +
                "                }\n" +
                "                setMethodBody.append(\");\\n\");\n" +
                "            } else if (methodName.startsWith(\"get\")) {\n" +
                "                getMethodBody.append(SSSSSSSS + classDesc.defaultVariable).append(\".\")\n" +
                "                        .append(methodDesc.methodName).append(\"(\");\n" +
                "                appendParamsAndEnd(getMethodBody, methodDesc.params, classDesc, unitContext);\n" +
                "            } else {\n" +
                "                otherMethodBody.append(SSSSSSSS + classDesc.defaultVariable).append(\".\")\n" +
                "                        .append(methodDesc.methodName).append(\"(\");\n" +
                "                appendParamsAndEnd(otherMethodBody, methodDesc.params, classDesc, unitContext);\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        // 如果是包含了date 和 timestamp类型的值，需要对再set一次 null ，然后再get一次保证全覆盖\n" +
                "        /**\n" +
                "         *    public void setEndTime(Date endTime) {\n" +
                "         *        if (endTime != null) {\n" +
                "         *          this.endTime = new Date(endTime.getTime());\n" +
                "         *        } else {\n" +
                "         *           this.endTime = null;\n" +
                "         *       }\n" +
                "         *   }\n" +
                "         */\n" +
                "        if (!setDateMethods.isEmpty()) {\n" +
                "            for (MethodDesc setDateMethod : setDateMethods) {\n" +
                "                setGetDateNullBody.append(SSSSSSSS + classDesc.defaultVariable).append(\".\")\n" +
                "                        .append(setDateMethod.methodName).append(\"(\");\n" +
                "                setGetDateNullBody.append(\"null);\\n\");\n" +
                "                setGetDateNullBody.append(SSSSSSSS).append(classDesc.defaultVariable).append(\".\");\n" +
                "                setGetDateNullBody.append(\"get\" + setDateMethod.methodName.replace(\"set\", \"\"));\n" +
                "                setGetDateNullBody.append(\"();\\n\");\n" +
                "            }\n" +
                "        }\n" +
                "        return testMethodBody.append(setMethodBody).append(getMethodBody).append(otherMethodBody).append(setGetDateNullBody);\n" +
                "    }\n" +
                "\n" +
                "    private static void appendParamsAndEnd(StringBuilder body, Map<String, String> params, ClassDesc classDesc, ClassUnitContext unitContext) {\n" +
                "        if (params != null && !params.isEmpty()) {\n" +
                "            Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();\n" +
                "            while (iterator.hasNext()) {\n" +
                "                Map.Entry<String, String> entry = iterator.next();\n" +
                "                String paramType = entry.getValue();\n" +
                "                if (paramType != null) {\n" +
                "                    body.append(CommonType.getInitParamValue(paramType));\n" +
                "                    if (iterator.hasNext()) {\n" +
                "                        body.append(\", \");\n" +
                "                    }\n" +
                "                }\n" +
                "                refreshTargetImportLines(paramType, classDesc, unitContext);\n" +
                "            }\n" +
                "        }\n" +
                "        body.append(\");\\n\");\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public StringBuilder visit(MethodDesc methodDesc) {\n" +
                "        buildMethodInvokeLines(methodDesc);\n" +
                "        buildMockMethod(methodDesc);\n" +
                "        return methodDesc.unitVariableLines.append(methodDesc.unitMockMethodLines).append(methodDesc.unitInvokeLines);\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    private void buildMethodInvokeLines(MethodDesc methodDesc) {\n" +
                "        // 静态方法采用类本身，不用变量\n" +
                "        if ((methodDesc.modifier & STATIC) != 0) {\n" +
                "            methodDesc.unitInvokeLines.append(SSSSSSSS).append(methodDesc.getParentName()).append(\".\").append(methodDesc.methodName).append(\"(\");\n" +
                "            refreshTargetImportLines(methodDesc.getParentName(), methodDesc.parent, methodDesc.parent.tempUnitContext);\n" +
                "        } else if (methodDesc.isEnumMethod()) {\n" +
                "            methodDesc.unitInvokeLines.append(SSSSSSSS).append(methodDesc.getDefaultEnumName()).append(\".\").append(methodDesc.methodName).append(\"(\");\n" +
                "        } else {\n" +
                "            methodDesc.unitInvokeLines.append(SSSSSSSS).append(methodDesc.getParentVariable()).append(\".\").append(methodDesc.methodName).append(\"(\");\n" +
                "        }\n" +
                "        Iterator<Map.Entry<String, String>> iterator = methodDesc.params.entrySet().iterator();\n" +
                "        while (iterator.hasNext()) {\n" +
                "            Map.Entry<String, String> entry = iterator.next();\n" +
                "            String paramVariable = entry.getKey();\n" +
                "            String paramType = entry.getValue();\n" +
                "            String uniqueParamVar = uniqueParamVariable(methodDesc, paramVariable);\n" +
                "            methodDesc.unitInvokeLines.append(uniqueParamVar);\n" +
                "            // 如果是加长数组,将该参数类型转换标准的数组格式\n" +
                "            paramType = convertParamTypeIfNeed(paramType);\n" +
                "            String paramInitValue = getParamInitValue(paramType, methodDesc);\n" +
                "            methodDesc.unitVariableLines.append(SSSSSSSS).append(paramType + \" \" + uniqueParamVar + \" = \");\n" +
                "            // 内部已有后缀;\\n\n" +
                "            methodDesc.unitVariableLines.append(paramInitValue);\n" +
                "            if (iterator.hasNext()) {\n" +
                "                methodDesc.unitInvokeLines.append(\", \");\n" +
                "            }\n" +
                "        }\n" +
                "        methodDesc.unitInvokeLines.append(\");\\n\");\n" +
                "    }\n" +
                "\n" +
                "    private String uniqueParamVariable(MethodDesc methodDesc, String paramVar) {\n" +
                "        String uniqueParamVar = paramVar;\n" +
                "        if (methodDesc.unitVariableTable.containsKey(paramVar)) {\n" +
                "            int count = methodDesc.unitVariableTable.get(paramVar);\n" +
                "            uniqueParamVar = paramVar + (++count);\n" +
                "            methodDesc.unitVariableTable.put(paramVar, count);\n" +
                "        } else {\n" +
                "            methodDesc.unitVariableTable.put(paramVar, 0);\n" +
                "        }\n" +
                "        return uniqueParamVar;\n" +
                "    }\n" +
                "\n" +
                "    private void buildMockMethod(MethodDesc methodDesc) {\n" +
                "        // TODO 疑似重复判断 先刷新返回类型及返回值，及返回变量去重复\n" +
                "        methodDesc.refreshReturnParam();\n" +
                "        for (StatementDesc statement : methodDesc.statements) {\n" +
                "            StatementUnitContext statementUnitContext = this.visit(statement);\n" +
                "            methodDesc.unitVariableLines.append(statementUnitContext.variableBuilder);\n" +
                "            methodDesc.unitMockMethodLines.append(statementUnitContext.invokeBuilder);\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    private String convertParamTypeIfNeed(String paramType) {\n" +
                "        if (paramType.endsWith(\"...\")) {\n" +
                "            paramType = paramType.replaceAll(\"\\\\.\\\\.\\\\.\", \"[]\");\n" +
                "        }\n" +
                "        // 如果是泛型，参数类型转换为Object\n" +
                "        if (paramType.matches(\"^[A-Z][0-9]?$\")) {\n" +
                "            paramType = \"Object\";\n" +
                "        }\n" +
                "        return paramType;\n" +
                "    }\n" +
                "\n" +
                "    public static String getParamInitValue(String paramType, MethodDesc methodDesc) {\n" +
                "        String paramInitValue = \"\";\n" +
                "        String fullClassName = UnitUtil.fullClassName(paramType, methodDesc.parent);\n" +
                "        ClassUnitContext unitContext = methodDesc.parent.tempUnitContext;\n" +
                "        if (CommonType.getInitParamValue(paramType) != null) {\n" +
                "            // 此处ParamType和初始值类型可能不同，所以需要根据实际的初始值刷新下导入包\n" +
                "            paramInitValue = CommonType.getInitParamValue(paramType) + \";\\n\";\n" +
                "            String paramInitType = CommonType.getRealInitClassName(paramType);\n" +
                "            refreshTargetImportLines(paramInitType, methodDesc.parent, unitContext);\n" +
                "        } else if (paramType.matches(NEED_MOCK_CLASSDESC)) {\n" +
                "            paramInitValue = \"mock(\" + paramType + \".class);\\n\";\n" +
                "            unitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        } else if (getClassDescFromContext(fullClassName) != null) {\n" +
                "            // 加入容器判断\n" +
                "            ClassDesc cacheClassDesc = getClassDescFromContext(fullClassName);\n" +
                "            int modifier = cacheClassDesc.modifier;\n" +
                "            if ((modifier & ENUM) != 0) {\n" +
                "                if (cacheClassDesc.hasEnumList()) {\n" +
                "                    paramInitValue = cacheClassDesc.getDefaultEnum() + \";\\n\";\n" +
                "                }\n" +
                "            } else if ((modifier & INTERFACE) != 0 || (modifier & ABSTRACT) != 0 || (modifier & ANNOTATION) != 0) {\n" +
                "                paramInitValue = \"mock(\" + cacheClassDesc.simpleName + \".class)\";\n" +
                "                unitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "            } else {\n" +
                "                for (MethodDesc construct : cacheClassDesc.constructrorMap.values()) {\n" +
                "                    // 添加构造器参数\n" +
                "                    paramInitValue = buildNewInitValue(construct);\n" +
                "                    break;\n" +
                "                }\n" +
                "            }\n" +
                "        } else {\n" +
                "            paramInitValue = \"mock(\" + paramType + \".class);\\n\";\n" +
                "            unitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        }\n" +
                "        refreshTargetImportLines(paramType, methodDesc.parent, unitContext);\n" +
                "        return paramInitValue;\n" +
                "    }\n" +
                "\n" +
                "    private static ClassDesc getClassDescFromContext(String paramType) {\n" +
                "        return Context.register().getClassDesc(paramType);\n" +
                "    }\n" +
                "\n" +
                "    private static String buildNewInitValue(MethodDesc construct) {\n" +
                "        StringBuilder initNewValue = new StringBuilder(\" new \" + construct.methodName + \"(\");\n" +
                "        appendParamsAndEnd(initNewValue, construct.params, construct.parent, construct.parent.tempUnitContext);\n" +
                "        return initNewValue.toString();\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public StatementUnitContext visit(StatementDesc statementDesc) {\n" +
                "        StringBuilder variableBuilder = buildVariableMock(statementDesc, statementDesc.parent.tempUnitContext);\n" +
                "        StringBuilder invokeBuilder = buildInvokeMethodMock(statementDesc);\n" +
                "        return new StatementUnitContext(variableBuilder, invokeBuilder);\n" +
                "    }\n" +
                "\n" +
                "    private StringBuilder buildVariableMock(StatementDesc statement, ClassUnitContext unitContext) {\n" +
                "        StringBuilder builder = new StringBuilder(255);\n" +
                "\n" +
                "        // 声明返回变量\n" +
                "        if (StringUtil.isNotEmpty(statement.returnType) && !statement.returnType.equals(\"void\")) {\n" +
                "            String uniqueReturnParam = paramVarMayChange(statement.returnParam, statement);\n" +
                "            builder.append(SSSSSSSS).append(statement.returnType).append(\" \").append(uniqueReturnParam)\n" +
                "                    .append(\" = \").append(statement.returnInitValue);\n" +
                "        }\n" +
                "        return builder;\n" +
                "    }\n" +
                "\n" +
                "    private String paramVarMayChange(String returnParam, StatementDesc statementDesc) {\n" +
                "        String uniqueReturnParam = returnParam;\n" +
                "        if (statementDesc.parentMethodDesc.unitVariableTable.containsKey(returnParam)) {\n" +
                "            int count = statementDesc.parentMethodDesc.unitVariableTable.get(returnParam);\n" +
                "            count++;\n" +
                "            uniqueReturnParam += count;\n" +
                "            statementDesc.parentMethodDesc.unitVariableTable.put(returnParam, count);\n" +
                "        } else {\n" +
                "            statementDesc.parentMethodDesc.unitVariableTable.put(returnParam, 0);\n" +
                "        }\n" +
                "        statementDesc.returnParam = uniqueReturnParam;\n" +
                "        return uniqueReturnParam;\n" +
                "    }\n" +
                "\n" +
                "    private StringBuilder buildInvokeMethodMock(StatementDesc statementDesc) {\n" +
                "        // 如果为空，就进行两种可能性拼接，一种是doNothing，一种是return null，如果有确定的值，就进行一种拼接\n" +
                "        StringBuilder invokeMethodMock = new StringBuilder(256);\n" +
                "        String type = statementDesc.returnType;\n" +
                "        if (type != null) {\n" +
                "            invokeMethodMock.append(SSSSSSSS);\n" +
                "            if (type.equals(\"void\")) {\n" +
                "                buildDoNothingStatement(statementDesc, invokeMethodMock);\n" +
                "            } else {\n" +
                "                buildMockStatement(statementDesc, invokeMethodMock);\n" +
                "            }\n" +
                "        }\n" +
                "        return invokeMethodMock;\n" +
                "    }\n" +
                "\n" +
                "    private void buildDoNothingStatement(StatementDesc statementDesc, StringBuilder mockMethod) {\n" +
                "        mockMethod.append(\"doNothing().when(\")\n" +
                "                .append(statementDesc.methodVariable)\n" +
                "                .append(\").\")\n" +
                "                .append(statementDesc.methodName)\n" +
                "                .append(\"(\");\n" +
                "        addEntryParams(statementDesc, mockMethod);\n" +
                "        mockMethod.append(\";\\n\");\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MATCHER_STATIC);\n" +
                "    }\n" +
                "\n" +
                "    private void buildMockStatement(StatementDesc statementDesc, StringBuilder mockMethod) {\n" +
                "        mockMethod.append(\"when(\")\n" +
                "                .append(statementDesc.methodVariable)\n" +
                "                .append(\".\")\n" +
                "                .append(statementDesc.methodName)\n" +
                "                .append(\"(\");\n" +
                "        addEntryParams(statementDesc, mockMethod);\n" +
                "        mockMethod.append(\").thenReturn(\");\n" +
                "        mockMethod.append(statementDesc.returnParam);\n" +
                "        mockMethod.append(\");\\n\");\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MATCHER_STATIC);\n" +
                "    }\n" +
                "\n" +
                "    private void buildMockWordForTwoCases(StatementDesc statementDesc, StringBuilder mockMethod) {\n" +
                "        // 情况一: doNothing().when()\n" +
                "        mockMethod.append(\"doNothing().when(\")\n" +
                "                .append(statementDesc.methodVariable)\n" +
                "                .append(\").\")\n" +
                "                .append(statementDesc.methodName)\n" +
                "                .append(\"(\");\n" +
                "        addEntryParams(statementDesc, mockMethod);\n" +
                "        mockMethod.append(\";\\n\");\n" +
                "\n" +
                "        // 情况二: when() 默认返回null\n" +
                "        mockMethod.append(\"when(\")\n" +
                "                .append(statementDesc.methodVariable)\n" +
                "                .append(\".\")\n" +
                "                .append(statementDesc.methodName)\n" +
                "                .append(\"(\");\n" +
                "        addEntryParams(statementDesc, mockMethod);\n" +
                "        mockMethod.append(\").thenReturn(null);\\n\");\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MATCHER_STATIC);\n" +
                "    }\n" +
                "\n" +
                "    private void addEntryParams(StatementDesc statmentDesc, StringBuilder mockInvokeMethod) {\n" +
                "        // 增加参数前采用容器进行下检查\n" +
                "        for (Map.Entry<String, String> entry : statmentDesc.params.entrySet()) {\n" +
                "            // key 变量  value 类别\n" +
                "            if (entry.getValue().equals(PLACE_HOLDER)) {\n" +
                "                String fullClassName = UnitUtil.fullClassName(statmentDesc.methodClassName, statmentDesc.parent);\n" +
                "                ClassDesc cacheClassDesc = Context.register().getClassDesc(fullClassName);\n" +
                "                // 在本身类去找，再到继承类去找\n" +
                "                findParamTypeFromContext(statmentDesc, cacheClassDesc, statmentDesc.methodName);\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        Iterator<Map.Entry<String, String>> iterator = statmentDesc.params.entrySet().iterator();\n" +
                "        while (iterator.hasNext()) {\n" +
                "            Map.Entry<String, String> entryParam = iterator.next();\n" +
                "            mockInvokeMethod.append(\"any(\")\n" +
                "                    .append(entryParam.getValue())\n" +
                "                    .append(\".class\")\n" +
                "                    .append(\")\");\n" +
                "            if (iterator.hasNext()) {\n" +
                "                mockInvokeMethod.append(\",\");\n" +
                "            }\n" +
                "            if (CommonType.getInitParamValue(entryParam.getValue()) == null) {\n" +
                "                refreshTargetImportLines(entryParam.getValue(), statmentDesc.parent, statmentDesc.parent.tempUnitContext);\n" +
                "            }\n" +
                "        }\n" +
                "        mockInvokeMethod.append(\")\");\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 递归获取方法的入参 <br>\n" +
                "     *\n" +
                "     * @param statementDesc\n" +
                "     * @param classDesc\n" +
                "     * @param methodName\n" +
                "     */\n" +
                "    private void findParamTypeFromContext(StatementDesc statementDesc, ClassDesc classDesc, String methodName) {\n" +
                "        if (classDesc != null) {\n" +
                "            Map<String, MethodDesc> map = classDesc.methodMap;\n" +
                "            for (Map.Entry<String, MethodDesc> methodEntry : map.entrySet()) {\n" +
                "                if (methodName.equals(methodEntry.getValue().methodName)) {\n" +
                "                    statementDesc.params = new LinkedHashMap<>(methodEntry.getValue().params);\n" +
                "                    // 如果存在的是泛型，那么就从childMethod对应的definetion中的泛型替换为其中记录的真实泛型\n" +
                "                    for (Map.Entry<String, String> entry : statementDesc.params.entrySet()) {\n" +
                "                        // 如果类型是泛型，即大写的字母，则将该泛型更新为childMethodName中对应的该类的对应的泛型替换\n" +
                "                        int i = 0;\n" +
                "                        if (entry.getValue().matches(\"[A-Z]\")) {\n" +
                "                            ClassDesc cacheClassDesc = Context.register().getClassDesc(UnitUtil.fullClassName(statementDesc.methodClassName, statementDesc.parent));\n" +
                "                            List<String> list = cacheClassDesc.genericType.get(classDesc.simpleName);\n" +
                "                            statementDesc.params.put(entry.getKey(), list.get(i));\n" +
                "                            i++;\n" +
                "                        }\n" +
                "                    }\n" +
                "                    return;\n" +
                "                }\n" +
                "            }\n" +
                "            // 没找到再继续找它的父类\n" +
                "            if (classDesc.hasSuperClass()) {\n" +
                "                Map<String, ClassDesc> superMap = classDesc.superClassRef;\n" +
                "                for (Map.Entry<String, ClassDesc> entry : superMap.entrySet()) {\n" +
                "                    ClassDesc superClassDesc = entry.getValue();\n" +
                "                    if (superClassDesc == null) {\n" +
                "                        superClassDesc = Context.register().getClassDesc(entry.getKey());\n" +
                "                    }\n" +
                "                    if (superClassDesc == null) {\n" +
                "                        return;\n" +
                "                    }\n" +
                "                    findParamTypeFromContext(statementDesc, superClassDesc, methodName);\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public void visit(FieldDesc fieldDesc) {\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    ClassUnitContext getThreadUnitContext(ClassDesc classDesc) {\n" +
                "        if (unitContextLocal.get() == null) {\n" +
                "            unitContextLocal.set(new ClassUnitContext());\n" +
                "        }\n" +
                "        return unitContextLocal.get().setClassDesc(classDesc);\n" +
                "    }\n" +
                "}";
        TokenWave tokenWave = new TokenWave(ss);
        List<String> list  = tokenWave.getTokens();
        for (String s : list) {
            System.out.println(s+"-->");
        }
    }


    @Test
    public void spiltTokens3() {
        String ss = "package redis.clients.jedis;\n" +
                "\n" +
                "import static redis.clients.jedis.Protocol.Keyword.ALPHA;\n" +
                "import static redis.clients.jedis.Protocol.Keyword.ASC;\n" +
                "import static redis.clients.jedis.Protocol.Keyword.BY;\n" +
                "import static redis.clients.jedis.Protocol.Keyword.DESC;\n" +
                "import static redis.clients.jedis.Protocol.Keyword.GET;\n" +
                "import static redis.clients.jedis.Protocol.Keyword.LIMIT;\n" +
                "import static redis.clients.jedis.Protocol.Keyword.NOSORT;\n" +
                "\n" +
                "import java.util.ArrayList;\n" +
                "import java.util.Collection;\n" +
                "import java.util.Collections;\n" +
                "import java.util.List;\n" +
                "\n" +
                "import redis.clients.util.SafeEncoder;\n" +
                "\n" +
                "/**\n" +
                " * Builder Class for {@link Jedis#sort(String, SortingParams) SORT} Parameters.\n" +
                " */\n" +
                "public class SortingParams {\n" +
                "  private List<byte[]> params = new ArrayList<byte[]>();\n" +
                "\n" +
                "  /**\n" +
                "   * Sort by weight in keys.\n" +
                "   * <p>\n" +
                "   * Takes a pattern that is used in order to generate the key names of the weights used for\n" +
                "   * sorting. Weight key names are obtained substituting the first occurrence of * with the actual\n" +
                "   * value of the elements on the list.\n" +
                "   * <p>\n" +
                "   * The pattern for a normal key/value pair is \"keyname*\" and for a value in a hash\n" +
                "   * \"keyname*-&gt;fieldname\".\n" +
                "   * @param pattern\n" +
                "   * @return the SortingParams Object\n" +
                "   */\n" +
                "  public SortingParams by(final String pattern) {\n" +
                "    return by(SafeEncoder.encode(pattern));\n" +
                "  }\n" +
                "\n" +
                "  /**\n" +
                "   * Sort by weight in keys.\n" +
                "   * <p>\n" +
                "   * Takes a pattern that is used in order to generate the key names of the weights used for\n" +
                "   * sorting. Weight key names are obtained substituting the first occurrence of * with the actual\n" +
                "   * value of the elements on the list.\n" +
                "   * <p>\n" +
                "   * The pattern for a normal key/value pair is \"keyname*\" and for a value in a hash\n" +
                "   * \"keyname*-&gt;fieldname\".\n" +
                "   * @param pattern\n" +
                "   * @return the SortingParams Object\n" +
                "   */\n" +
                "  public SortingParams by(final byte[] pattern) {\n" +
                "    params.add(BY.raw);\n" +
                "    params.add(pattern);\n" +
                "    return this;\n" +
                "  }\n" +
                "\n" +
                "  /**\n" +
                "   * No sorting.\n" +
                "   * <p>\n" +
                "   * This is useful if you want to retrieve a external key (using {@link #get(String...) GET}) but\n" +
                "   * you don't want the sorting overhead.\n" +
                "   * @return the SortingParams Object\n" +
                "   */\n" +
                "  public SortingParams nosort() {\n" +
                "    params.add(BY.raw);\n" +
                "    params.add(NOSORT.raw);\n" +
                "    return this;\n" +
                "  }\n" +
                "\n" +
                "  public Collection<byte[]> getParams() {\n" +
                "    return Collections.unmodifiableCollection(params);\n" +
                "  }\n" +
                "\n" +
                "  /**\n" +
                "   * Get the Sorting in Descending Order.\n" +
                "   * @return the sortingParams Object\n" +
                "   */\n" +
                "  public SortingParams desc() {\n" +
                "    params.add(DESC.raw);\n" +
                "    return this;\n" +
                "  }\n" +
                "\n" +
                "  /**\n" +
                "   * Get the Sorting in Ascending Order. This is the default order.\n" +
                "   * @return the SortingParams Object\n" +
                "   */\n" +
                "  public SortingParams asc() {\n" +
                "    params.add(ASC.raw);\n" +
                "    return this;\n" +
                "  }\n" +
                "\n" +
                "  /**\n" +
                "   * Limit the Numbers of returned Elements.\n" +
                "   * @param start is zero based\n" +
                "   * @param count\n" +
                "   * @return the SortingParams Object\n" +
                "   */\n" +
                "  public SortingParams limit(final int start, final int count) {\n" +
                "    params.add(LIMIT.raw);\n" +
                "    params.add(Protocol.toByteArray(start));\n" +
                "    params.add(Protocol.toByteArray(count));\n" +
                "    return this;\n" +
                "  }\n" +
                "\n" +
                "  /**\n" +
                "   * Sort lexicographicaly. Note that Redis is utf-8 aware assuming you set the right value for the\n" +
                "   * LC_COLLATE environment variable.\n" +
                "   * @return the SortingParams Object\n" +
                "   */\n" +
                "  public SortingParams alpha() {\n" +
                "    params.add(ALPHA.raw);\n" +
                "    return this;\n" +
                "  }\n" +
                "\n" +
                "  /**\n" +
                "   * Retrieving external keys from the result of the search.\n" +
                "   * <p>\n" +
                "   * Takes a pattern that is used in order to generate the key names of the result of sorting. The\n" +
                "   * key names are obtained substituting the first occurrence of * with the actual value of the\n" +
                "   * elements on the list.\n" +
                "   * <p>\n" +
                "   * The pattern for a normal key/value pair is \"keyname*\" and for a value in a hash\n" +
                "   * \"keyname*-&gt;fieldname\".\n" +
                "   * <p>\n" +
                "   * To get the list itself use the char # as pattern.\n" +
                "   * @param patterns\n" +
                "   * @return the SortingParams Object\n" +
                "   */\n" +
                "  public SortingParams get(String... patterns) {\n" +
                "    for (final String pattern : patterns) {\n" +
                "      params.add(GET.raw);\n" +
                "      params.add(SafeEncoder.encode(pattern));\n" +
                "    }\n" +
                "    return this;\n" +
                "  }\n" +
                "\n" +
                "  /**\n" +
                "   * Retrieving external keys from the result of the search.\n" +
                "   * <p>\n" +
                "   * Takes a pattern that is used in order to generate the key names of the result of sorting. The\n" +
                "   * key names are obtained substituting the first occurrence of * with the actual value of the\n" +
                "   * elements on the list.\n" +
                "   * <p>\n" +
                "   * The pattern for a normal key/value pair is \"keyname*\" and for a value in a hash\n" +
                "   * \"keyname*-&gt;fieldname\".\n" +
                "   * <p>\n" +
                "   * To get the list itself use the char # as pattern.\n" +
                "   * @param patterns\n" +
                "   * @return the SortingParams Object\n" +
                "   */\n" +
                "  public SortingParams get(byte[]... patterns) {\n" +
                "    for (final byte[] pattern : patterns) {\n" +
                "      params.add(GET.raw);\n" +
                "      params.add(pattern);\n" +
                "    }\n" +
                "    return this;\n" +
                "  }\n" +
                "}\n";
        TokenWave tokenWave = new TokenWave(ss);
        List<String> list  = tokenWave.getTokens();
        for (String s : list) {
            System.out.println(s+"-->");
        }
    }


    @Test
    public void spiltTokens4() {
        String ss = "/**\n" +
                " * Copyright (C), 2017-2018, XXX有限公司\n" +
                " * FileName: MockitoVisitor\n" +
                " * Author:   zengjian\n" +
                " * Date:     2018/8/16 13:57\n" +
                " * Description: Mockito框架单元测试生成规则\n" +
                " * History:\n" +
                " * <author>          <time>          <version>          <desc>\n" +
                " * 作者姓名           修改时间           版本号              描述\n" +
                " */\n" +
                "package org.yinxueframework.swing.unit.visitor;\n" +
                "\n" +
                "import StringUtil;\n" +
                "import org.yinxueframework.swing.unit.constant.ClassConstant;\n" +
                "import org.yinxueframework.swing.unit.constant.CommonType;\n" +
                "import org.yinxueframework.swing.unit.context.Context;\n" +
                "import org.yinxueframework.swing.unit.model.ClassDesc;\n" +
                "import org.yinxueframework.swing.unit.model.FieldDesc;\n" +
                "import org.yinxueframework.swing.unit.model.MethodDesc;\n" +
                "import org.yinxueframework.swing.unit.model.StatementDesc;\n" +
                "import org.yinxueframework.swing.unit.util.UnitUtil;\n" +
                "\n" +
                "import java.util.*;\n" +
                "\n" +
                "import static org.yinxueframework.swing.unit.constant.UnitConstant.*;\n" +
                "\n" +
                "/**\n" +
                " * 〈Mockito框架单元测试生成规则〉<br>\n" +
                " * 〈一句话描述〉\n" +
                " *\n" +
                " * @author zengjian\n" +
                " * @create 2018/8/16 13:57\n" +
                " */\n" +
                "public class MockitoVisitor implements Visitor, ClassConstant {\n" +
                "\n" +
                "    /**\n" +
                "     * 同一线程复用同一ClassUnitContext对象\n" +
                "     */\n" +
                "    private volatile static ThreadLocal<ClassUnitContext> unitContextLocal = new ThreadLocal<>();\n" +
                "\n" +
                "\n" +
                "    @Override\n" +
                "    public String visit(ClassDesc classDesc) {\n" +
                "        String unitContext = null;\n" +
                "        if ((classDesc.modifier & ENUM) != 0) {\n" +
                "            unitContext = toBuildUnitContext(classDesc);\n" +
                "        } else if ((classDesc.modifier & GENERIC) != 0) {\n" +
                "            unitContext = toBuildUnitContext(classDesc);\n" +
                "        } else {\n" +
                "            // ignore 抽象类、注解、接口暂时忽略掉\n" +
                "        }\n" +
                "        return unitContext;\n" +
                "    }\n" +
                "\n" +
                "    private String toBuildUnitContext(ClassDesc classDesc) {\n" +
                "        ClassUnitContext unitContext = getThreadUnitContext(classDesc);\n" +
                "        // 导入包调整为最后添加\n" +
                "        this.doMakeUnitPackageLine(classDesc);\n" +
                "        this.doMakeUnitClassHeader(classDesc);\n" +
                "        this.doMakeUnitMethodLines(classDesc);\n" +
                "        this.doMakeUnitFieldLines(classDesc);\n" +
                "        this.doMakeUnitImportLines(classDesc);\n" +
                "        String unit = unitContext.unitPackageLine\n" +
                "                .append(unitContext.unitImportLines).append(\"\\n\")\n" +
                "                .append(unitContext.unitClassNameHeader)\n" +
                "                .append(unitContext.unitClassVariables)\n" +
                "                .append(unitContext.unitMethodLines)\n" +
                "                .append(unitContext.unitClassNameFooter)\n" +
                "                .toString();\n" +
                "        // 清理StringBuilder，保证容器实例存在时，重复使用时为空\n" +
                "        unitContext.reset();\n" +
                "        return unit;\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    private void doMakeUnitPackageLine(ClassDesc classDesc) {\n" +
                "        classDesc.tempUnitContext.unitPackageLine.append(classDesc.packageLine + \"\\n\\n\");\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    private void doMakeUnitImportLines(ClassDesc classDesc) {\n" +
                "        for (String dynamicImportLine : classDesc.tempUnitContext.targetImportLines) {\n" +
                "            classDesc.tempUnitContext.unitImportLines.append(dynamicImportLine + \"\\n\");\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    private void doMakeUnitClassHeader(ClassDesc classDesc) {\n" +
                "        classDesc.tempUnitContext.unitClassNameHeader.append(\"public class \" + classDesc.simpleName + \"Test\" + \" {\\n\\n\");\n" +
                "    }\n" +
                "\n" +
                "    private void doMakeUnitFieldLines(ClassDesc classDesc) {\n" +
                "        if ((classDesc.modifier & POJO) != 0 || ((classDesc.modifier & UTIL) != 0 )\n" +
                "                || (classDesc.modifier & ENUM) != 0 || classDesc.methodMap.isEmpty()) {\n" +
                "            return;\n" +
                "        }\n" +
                "\n" +
                "        ClassUnitContext unitContext = classDesc.tempUnitContext;\n" +
                "\n" +
                "\n" +
                "        // 注入待测实体(待测实体是以无参构造进行构造的)\n" +
                "        unitContext.unitClassNameHeader.append(SSSS).append(INJECTMOCKS)\n" +
                "                .append(SSSS).append(\"private \").append(classDesc.simpleName).append(S).append(classDesc.defaultVariable).append(\";\\n\\n\");\n" +
                "\n" +
                "        // 按注解mock\n" +
                "        for (Map.Entry<String, FieldDesc> entry : classDesc.fieldMap.entrySet()) {\n" +
                "            String fieldAnnotation = entry.getValue().annotation;\n" +
                "            String fieldType = entry.getValue().type;\n" +
                "            String fieldVariable = entry.getKey();\n" +
                "\n" +
                "            if (fieldAnnotation != null) {\n" +
                "                unitContext.unitClassVariables\n" +
                "                        .append(SSSS).append(\"@Mock\\n\")\n" +
                "                        .append(SSSS).append(\"private \").append(fieldType).append(S).append(fieldVariable).append(\";\\n\\n\");\n" +
                "                unitContext.targetFieldLines.add(BEFORE_METHOD_STATEMENT);\n" +
                "                refreshTargetImportLines(fieldType, classDesc, unitContext);\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        // 按需要添加beforemethod等项\n" +
                "        for (String targetFieldLine : unitContext.targetFieldLines) {\n" +
                "            unitContext.unitClassVariables.append(targetFieldLine);\n" +
                "        }\n" +
                "\n" +
                "        // mockito、testng导入包，可根据生成的单元测试不同进行置换\n" +
                "        unitContext.targetImportLines.add(IMPORT_TESTNG_BEFORE_METHOD); // @BeforeMethod\n" +
                "        unitContext.targetImportLines.add(IMPORT_MOCKITO); // @Mock @InjectMock MockitoAnnotations.initMocks(this)\n" +
                "\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    private static void refreshTargetImportLines(String paramType, ClassDesc classDesc, ClassUnitContext unitContext) {\n" +
                "        if (StringUtil.isEmpty(paramType) || unitContext == null) {\n" +
                "            return;\n" +
                "        }\n" +
                "        if (classDesc.sourceImportLines.containsKey(paramType)) {\n" +
                "            unitContext.targetImportLines.add(classDesc.sourceImportLines.get(paramType));\n" +
                "        }\n" +
                "        if (CommonType.getDeclaredClassImportPath(paramType) != null) {\n" +
                "            unitContext.targetImportLines.add(\"import \" + CommonType.getDeclaredClassImportPath(paramType) + \";\");\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    private void doMakeUnitMethodLines(ClassDesc classDesc) {\n" +
                "        ClassUnitContext unitContext = classDesc.tempUnitContext;\n" +
                "        if (!classDesc.methodMap.isEmpty()) {\n" +
                "            unitContext.targetImportLines.add(IMPORT_TESTNG);\n" +
                "        }\n" +
                "\n" +
                "        // 对pojo的单独处理\n" +
                "        if ((classDesc.modifier & POJO) != 0) {\n" +
                "            unitContext.unitMethodLines.append(SSSS + \"@Test\\n\").append(SSSS + \"public void testEntity() \");\n" +
                "            // 只要有一个方法抛异常就需要抛出异常\n" +
                "            appendExceptionIfExsit4Pojo(classDesc, unitContext);\n" +
                "            unitContext.unitMethodLines.append(appendConstrutorHeader4Pojo(classDesc, unitContext));\n" +
                "            // 说明有内部类，需要拼接header\n" +
                "            if (classDesc.hasNestClass()) {\n" +
                "                for (ClassDesc nestClassDesc : classDesc.nestClassList) {\n" +
                "                    unitContext.unitMethodLines.append(appendConstrutorHeader4Pojo(nestClassDesc, unitContext));\n" +
                "                }\n" +
                "                // 拼接内部类的get set等方法\n" +
                "                for (ClassDesc nestClassDesc : classDesc.nestClassList) {\n" +
                "                    unitContext.unitMethodLines.append(appendMethodBody4Pojo(nestClassDesc, unitContext));\n" +
                "                }\n" +
                "            }\n" +
                "            unitContext.unitMethodLines.append(appendMethodBody4Pojo(classDesc, unitContext));\n" +
                "            unitContext.unitMethodLines.append(SSSS + \"}\\n\");\n" +
                "            return;\n" +
                "        }\n" +
                "        // 测试方法去重复\n" +
                "        Map<String, Integer> uniqueMethodNameMap = new HashMap<>(48);\n" +
                "        for (Map.Entry<String, MethodDesc> methodEntry : classDesc.methodMap.entrySet()) {\n" +
                "            MethodDesc methodDesc = methodEntry.getValue();\n" +
                "            /* 由于可能存在方法重载的问题，所以需要对相同方法名称的情况，进行序号编号\n" +
                "               需要将对方法header的解析放在什么外部来解析 */\n" +
                "            String methodName = methodDesc.getMethodName();\n" +
                "            if (methodName == null) {\n" +
                "                continue;\n" +
                "            }\n" +
                "            String uniqueMethodName = methodNameMayChange(uniqueMethodNameMap, methodName);\n" +
                "            unitContext.unitMethodLines\n" +
                "                    .append(SSSS).append(\"@Test\\n\")\n" +
                "                    .append(SSSS).append(\"public void test\").append(StringUtil.toUpperCaseFirstChar(uniqueMethodName)).append(\"() \");\n" +
                "            appendExceptionIfExsit(unitContext.unitMethodLines, methodDesc.exceptions, methodDesc);\n" +
                "            unitContext.unitMethodLines.append(visit(methodDesc));\n" +
                "            unitContext.unitMethodLines.append(SSSS).append(\"}\\n\\n\");\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    // 方法名去重\n" +
                "    private String methodNameMayChange(Map<String, Integer> uniqueMethodNameMap, String methodName) {\n" +
                "        String uniqueMethodName = methodName;\n" +
                "        if (uniqueMethodNameMap.containsKey(methodName)) {\n" +
                "            Integer count = uniqueMethodNameMap.get(methodName);\n" +
                "            count++;\n" +
                "            uniqueMethodName += count;\n" +
                "            uniqueMethodNameMap.put(methodName, count);\n" +
                "        } else {\n" +
                "            uniqueMethodNameMap.put(methodName, 0);\n" +
                "        }\n" +
                "        return uniqueMethodName;\n" +
                "    }\n" +
                "\n" +
                "    private void appendExceptionIfExsit4Pojo(ClassDesc classDesc, ClassUnitContext unitContext) {\n" +
                "        boolean exsitException = false;\n" +
                "        for (MethodDesc methodDesc : classDesc.methodMap.values()) {\n" +
                "            if (methodDesc.exceptions.size() > 0) {\n" +
                "                exsitException = true;\n" +
                "                break;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        if (exsitException == false) {\n" +
                "            unitContext.unitMethodLines.append(\"{\\n\");\n" +
                "            return;\n" +
                "        }\n" +
                "        unitContext.unitMethodLines.append(\" throws \");\n" +
                "        for (MethodDesc methodDesc : classDesc.methodMap.values()) {\n" +
                "            if (!methodDesc.exceptions.isEmpty()) {\n" +
                "                for (String exception : methodDesc.exceptions) {\n" +
                "                    unitContext.unitMethodLines.append(exception).append(\",\");\n" +
                "                    refreshTargetImportLines(exception, classDesc, unitContext);\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        // 前面已判断有Exception，所以直接将,进行替换即可\n" +
                "        unitContext.unitMethodLines.setCharAt(unitContext.unitMethodLines.lastIndexOf(\",\"), ' ');\n" +
                "        unitContext.unitMethodLines.append(\"{\\n\");\n" +
                "    }\n" +
                "\n" +
                "    static void appendExceptionIfExsit(StringBuilder unitMethodLines, List<String> exceptions, MethodDesc methodDesc) {\n" +
                "        if (!exceptions.isEmpty()) {\n" +
                "            unitMethodLines.append(\" throws \");\n" +
                "            Iterator<String> iterator = exceptions.iterator();\n" +
                "            while (iterator.hasNext()) {\n" +
                "                String exception = iterator.next();\n" +
                "                unitMethodLines.append(exception);\n" +
                "                refreshTargetImportLines(exception, methodDesc.parent, methodDesc.parent.tempUnitContext);\n" +
                "                if (iterator.hasNext()) {\n" +
                "                    unitMethodLines.append(\",\");\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        unitMethodLines.append(\"{\\n\");\n" +
                "    }\n" +
                "\n" +
                "    private StringBuilder appendConstrutorHeader4Pojo(ClassDesc nestClassDesc, ClassUnitContext unitContext) {\n" +
                "        // 以下需要通过definition对象的parent来判断是否为子类还是主类，主类不加前缀，子类需要加主类前缀\n" +
                "        StringBuilder construtorBuilder = new StringBuilder(128);\n" +
                "        // 查看是否有有参构造器，将构造器按照\n" +
                "        Iterator<Map.Entry<String, MethodDesc>> iterator = nestClassDesc.constructrorMap.entrySet().iterator();\n" +
                "        int count = 0;\n" +
                "        while (iterator.hasNext()) {\n" +
                "            Map.Entry<String, MethodDesc> entry = iterator.next();\n" +
                "            MethodDesc constructor = entry.getValue();\n" +
                "            Map<String, String> params = UnitUtil.getMethodEntryParams(constructor.methodHeader);\n" +
                "            // 没入参默认为无参构造器，按默认变量拼接\n" +
                "            if (params == null || params.isEmpty()) {\n" +
                "                buildConstrutorPrefix(nestClassDesc, construtorBuilder, 0, params, unitContext);\n" +
                "                // 遍历取出属性，然后拼接一个有参对象\n" +
                "            } else {\n" +
                "                buildConstrutorPrefix(nestClassDesc, construtorBuilder, count, params, unitContext);\n" +
                "            }\n" +
                "            if (iterator.hasNext()) {\n" +
                "                count++;\n" +
                "            }\n" +
                "        }\n" +
                "        return construtorBuilder;\n" +
                "    }\n" +
                "\n" +
                "    private void buildConstrutorPrefix(ClassDesc nestClassDesc, StringBuilder builder, int i, Map<String, String> params, ClassUnitContext unitContext) {\n" +
                "        builder.append(SSSSSSSS + nestClassDesc.compositClassName + \" \" + nestClassDesc.defaultVariable + (i == 0 ? \"\" : i) + \" = \");\n" +
                "        if ((nestClassDesc.modifier & ENUM) != 0) {\n" +
                "            builder.append(nestClassDesc.enumList.get(0)).append(\";\\n\");\n" +
                "        } else {\n" +
                "            builder.append(\"new \" + nestClassDesc.compositClassName + \"(\");\n" +
                "            appendParamsAndEnd(builder, params, nestClassDesc, unitContext);\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    private StringBuilder appendMethodBody4Pojo(ClassDesc classDesc, ClassUnitContext unitContext) {\n" +
                "        StringBuilder testMethodBody = new StringBuilder(256);\n" +
                "\n" +
                "        StringBuilder setMethodBody = new StringBuilder(128);\n" +
                "        StringBuilder getMethodBody = new StringBuilder(128);\n" +
                "        StringBuilder otherMethodBody = new StringBuilder(128);\n" +
                "        StringBuilder setGetDateNullBody = new StringBuilder(128);\n" +
                "        List<MethodDesc> setDateMethods = new ArrayList<>(6);\n" +
                "\n" +
                "        for (MethodDesc methodDesc : classDesc.methodMap.values()) {\n" +
                "            String methodName = methodDesc.methodName;\n" +
                "            if (methodName.startsWith(\"set\") || methodName.startsWith(\"is\")) {\n" +
                "                setMethodBody.append(SSSSSSSS).append(methodDesc.getParentVariable()).append(\".\").append(methodName).append(\"(\");\n" +
                "                Iterator<Map.Entry<String, String>> iterator = methodDesc.params.entrySet().iterator();\n" +
                "                while (iterator.hasNext()) {\n" +
                "                    Map.Entry<String, String> entry = iterator.next();\n" +
                "                    String paramType = entry.getValue();\n" +
                "                    // 表示泛型\n" +
                "                    if (paramType.matches(\"^[A-Z][0-9]?$\")) {\n" +
                "                        paramType = \"Object\";\n" +
                "                    }\n" +
                "\n" +
                "                    String defaultParamValue = null;\n" +
                "                    // 处理内部类的情况\n" +
                "                    if (classDesc.hasNestClass()) {\n" +
                "                        // 有内部类先判断内部类的情况\n" +
                "                        for (int i = 0, size = classDesc.nestClassList.size(); i < size; i++) {\n" +
                "                            if (paramType.equals(classDesc.nestClassList.get(i).simpleName)) {\n" +
                "                                defaultParamValue = StringUtil.toLowerCaseFirstChar(paramType);\n" +
                "                                break;\n" +
                "                            }\n" +
                "                        }\n" +
                "                    }\n" +
                "\n" +
                "                    // 如果没有匹配的内部类再按一般的类判断\n" +
                "                    if (defaultParamValue == null) {\n" +
                "                        defaultParamValue = CommonType.getInitParamValue(paramType);\n" +
                "                    }\n" +
                "                    // 按照实际初始的类导入包\n" +
                "                    String initParamType = CommonType.getRealInitClassName(paramType);\n" +
                "                    // 配合进入的是内部类的情况\n" +
                "                    ClassDesc classDesc1 = classDesc;\n" +
                "                    for (; (classDesc1.modifier & NEST) != 0; classDesc1 = classDesc1.parent) {\n" +
                "                    }\n" +
                "                    refreshTargetImportLines(initParamType, classDesc1, classDesc1.tempUnitContext);\n" +
                "                    // 解决 java.util.Date 和 java.sql.Date冲突问题\n" +
                "                    if (\"new Date()\".equals(defaultParamValue)) {\n" +
                "                        if (classDesc.sourceImportLines.get(\"Date\").contains(\"java.sql.Date\")) {\n" +
                "                            defaultParamValue = \"new Date(System.currentTimeMillis())\";\n" +
                "                        }\n" +
                "                    }\n" +
                "                    setMethodBody.append(defaultParamValue);\n" +
                "\n" +
                "                    // 处理Date 和 Timestamp sonar覆盖的问题， 需要设置null然后再进行get，先缓存set的选项\n" +
                "                    if ((\"Date\".equals(paramType) || \"Timestamp\".equals(paramType)) && methodDesc.params.size() == 1) {\n" +
                "                        setDateMethods.add(methodDesc);\n" +
                "                    }\n" +
                "                    if (iterator.hasNext()) {\n" +
                "                        setMethodBody.append(\",\");\n" +
                "                    }\n" +
                "                }\n" +
                "                setMethodBody.append(\");\\n\");\n" +
                "            } else if (methodName.startsWith(\"get\")) {\n" +
                "                getMethodBody.append(SSSSSSSS + classDesc.defaultVariable).append(\".\")\n" +
                "                        .append(methodDesc.methodName).append(\"(\");\n" +
                "                appendParamsAndEnd(getMethodBody, methodDesc.params, classDesc, unitContext);\n" +
                "            } else {\n" +
                "                otherMethodBody.append(SSSSSSSS + classDesc.defaultVariable).append(\".\")\n" +
                "                        .append(methodDesc.methodName).append(\"(\");\n" +
                "                appendParamsAndEnd(otherMethodBody, methodDesc.params, classDesc, unitContext);\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        // 如果是包含了date 和 timestamp类型的值，需要对再set一次 null ，然后再get一次保证全覆盖\n" +
                "        /**\n" +
                "         *    public void setEndTime(Date endTime) {\n" +
                "         *        if (endTime != null) {\n" +
                "         *          this.endTime = new Date(endTime.getTime());\n" +
                "         *        } else {\n" +
                "         *           this.endTime = null;\n" +
                "         *       }\n" +
                "         *   }\n" +
                "         */\n" +
                "        if (!setDateMethods.isEmpty()) {\n" +
                "            for (MethodDesc setDateMethod : setDateMethods) {\n" +
                "                setGetDateNullBody.append(SSSSSSSS + classDesc.defaultVariable).append(\".\")\n" +
                "                        .append(setDateMethod.methodName).append(\"(\");\n" +
                "                setGetDateNullBody.append(\"null);\\n\");\n" +
                "                setGetDateNullBody.append(SSSSSSSS).append(classDesc.defaultVariable).append(\".\");\n" +
                "                setGetDateNullBody.append(\"get\" + setDateMethod.methodName.replace(\"set\", \"\"));\n" +
                "                setGetDateNullBody.append(\"();\\n\");\n" +
                "            }\n" +
                "        }\n" +
                "        return testMethodBody.append(setMethodBody).append(getMethodBody).append(otherMethodBody).append(setGetDateNullBody);\n" +
                "    }\n" +
                "\n" +
                "    private static void appendParamsAndEnd(StringBuilder body, Map<String, String> params, ClassDesc classDesc, ClassUnitContext unitContext) {\n" +
                "        if (params != null && !params.isEmpty()) {\n" +
                "            Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();\n" +
                "            while (iterator.hasNext()) {\n" +
                "                Map.Entry<String, String> entry = iterator.next();\n" +
                "                String paramType = entry.getValue();\n" +
                "                if (paramType != null) {\n" +
                "                    body.append(CommonType.getInitParamValue(paramType));\n" +
                "                    if (iterator.hasNext()) {\n" +
                "                        body.append(\", \");\n" +
                "                    }\n" +
                "                }\n" +
                "                refreshTargetImportLines(paramType, classDesc, unitContext);\n" +
                "            }\n" +
                "        }\n" +
                "        body.append(\");\\n\");\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public StringBuilder visit(MethodDesc methodDesc) {\n" +
                "        buildMethodInvokeLines(methodDesc);\n" +
                "        buildMockMethod(methodDesc);\n" +
                "        return methodDesc.unitVariableLines.append(methodDesc.unitMockMethodLines).append(methodDesc.unitInvokeLines);\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    private void buildMethodInvokeLines(MethodDesc methodDesc) {\n" +
                "        // 静态方法采用类本身，不用变量\n" +
                "        if ((methodDesc.modifier & STATIC) != 0) {\n" +
                "            methodDesc.unitInvokeLines.append(SSSSSSSS).append(methodDesc.getParentName()).append(\".\").append(methodDesc.methodName).append(\"(\");\n" +
                "            refreshTargetImportLines(methodDesc.getParentName(), methodDesc.parent, methodDesc.parent.tempUnitContext);\n" +
                "        } else if (methodDesc.isEnumMethod()) {\n" +
                "            methodDesc.unitInvokeLines.append(SSSSSSSS).append(methodDesc.getDefaultEnumName()).append(\".\").append(methodDesc.methodName).append(\"(\");\n" +
                "        } else {\n" +
                "            methodDesc.unitInvokeLines.append(SSSSSSSS).append(methodDesc.getParentVariable()).append(\".\").append(methodDesc.methodName).append(\"(\");\n" +
                "        }\n" +
                "        Iterator<Map.Entry<String, String>> iterator = methodDesc.params.entrySet().iterator();\n" +
                "        while (iterator.hasNext()) {\n" +
                "            Map.Entry<String, String> entry = iterator.next();\n" +
                "            String paramVariable = entry.getKey();\n" +
                "            String paramType = entry.getValue();\n" +
                "            String uniqueParamVar = uniqueParamVariable(methodDesc, paramVariable);\n" +
                "            methodDesc.unitInvokeLines.append(uniqueParamVar);\n" +
                "            // 如果是加长数组,将该参数类型转换标准的数组格式\n" +
                "            paramType = convertParamTypeIfNeed(paramType);\n" +
                "            String paramInitValue = getParamInitValue(paramType, methodDesc);\n" +
                "            methodDesc.unitVariableLines.append(SSSSSSSS).append(paramType + \" \" + uniqueParamVar + \" = \");\n" +
                "            // 内部已有后缀;\\n\n" +
                "            methodDesc.unitVariableLines.append(paramInitValue);\n" +
                "            if (iterator.hasNext()) {\n" +
                "                methodDesc.unitInvokeLines.append(\", \");\n" +
                "            }\n" +
                "        }\n" +
                "        methodDesc.unitInvokeLines.append(\");\\n\");\n" +
                "    }\n" +
                "\n" +
                "    private String uniqueParamVariable(MethodDesc methodDesc, String paramVar) {\n" +
                "        String uniqueParamVar = paramVar;\n" +
                "        if (methodDesc.unitVariableTable.containsKey(paramVar)) {\n" +
                "            int count = methodDesc.unitVariableTable.get(paramVar);\n" +
                "            uniqueParamVar = paramVar + (++count);\n" +
                "            methodDesc.unitVariableTable.put(paramVar, count);\n" +
                "        } else {\n" +
                "            methodDesc.unitVariableTable.put(paramVar, 0);\n" +
                "        }\n" +
                "        return uniqueParamVar;\n" +
                "    }\n" +
                "\n" +
                "    private void buildMockMethod(MethodDesc methodDesc) {\n" +
                "        // TODO 疑似重复判断 先刷新返回类型及返回值，及返回变量去重复\n" +
                "        methodDesc.refreshReturnParam();\n" +
                "        for (StatementDesc statement : methodDesc.statements) {\n" +
                "            StatementUnitContext statementUnitContext = this.visit(statement);\n" +
                "            methodDesc.unitVariableLines.append(statementUnitContext.variableBuilder);\n" +
                "            methodDesc.unitMockMethodLines.append(statementUnitContext.invokeBuilder);\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    private String convertParamTypeIfNeed(String paramType) {\n" +
                "        if (paramType.endsWith(\"...\")) {\n" +
                "            paramType = paramType.replaceAll(\"\\\\.\\\\.\\\\.\", \"[]\");\n" +
                "        }\n" +
                "        // 如果是泛型，参数类型转换为Object\n" +
                "        if (paramType.matches(\"^[A-Z][0-9]?$\")) {\n" +
                "            paramType = \"Object\";\n" +
                "        }\n" +
                "        return paramType;\n" +
                "    }\n" +
                "\n" +
                "    public static String getParamInitValue(String paramType, MethodDesc methodDesc) {\n" +
                "        String paramInitValue = \"\";\n" +
                "        String fullClassName = UnitUtil.fullClassName(paramType, methodDesc.parent);\n" +
                "        ClassUnitContext unitContext = methodDesc.parent.tempUnitContext;\n" +
                "        if (CommonType.getInitParamValue(paramType) != null) {\n" +
                "            // 此处ParamType和初始值类型可能不同，所以需要根据实际的初始值刷新下导入包\n" +
                "            paramInitValue = CommonType.getInitParamValue(paramType) + \";\\n\";\n" +
                "            String paramInitType = CommonType.getRealInitClassName(paramType);\n" +
                "            refreshTargetImportLines(paramInitType, methodDesc.parent, unitContext);\n" +
                "        } else if (paramType.matches(NEED_MOCK_CLASSDESC)) {\n" +
                "            paramInitValue = \"mock(\" + paramType + \".class);\\n\";\n" +
                "            unitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        } else if (getClassDescFromContext(fullClassName) != null) {\n" +
                "            // 加入容器判断\n" +
                "            ClassDesc cacheClassDesc = getClassDescFromContext(fullClassName);\n" +
                "            int modifier = cacheClassDesc.modifier;\n" +
                "            if ((modifier & ENUM) != 0) {\n" +
                "                if (cacheClassDesc.hasEnumList()) {\n" +
                "                    paramInitValue = cacheClassDesc.getDefaultEnum() + \";\\n\";\n" +
                "                }\n" +
                "            } else if ((modifier & INTERFACE) != 0 || (modifier & ABSTRACT) != 0 || (modifier & ANNOTATION) != 0) {\n" +
                "                paramInitValue = \"mock(\" + cacheClassDesc.simpleName + \".class)\";\n" +
                "                unitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "            } else {\n" +
                "                for (MethodDesc construct : cacheClassDesc.constructrorMap.values()) {\n" +
                "                    // 添加构造器参数\n" +
                "                    paramInitValue = buildNewInitValue(construct);\n" +
                "                    break;\n" +
                "                }\n" +
                "            }\n" +
                "        } else {\n" +
                "            paramInitValue = \"mock(\" + paramType + \".class);\\n\";\n" +
                "            unitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        }\n" +
                "        refreshTargetImportLines(paramType, methodDesc.parent, unitContext);\n" +
                "        return paramInitValue;\n" +
                "    }\n" +
                "\n" +
                "    private static ClassDesc getClassDescFromContext(String paramType) {\n" +
                "        return Context.register().getClassDesc(paramType);\n" +
                "    }\n" +
                "\n" +
                "    private static String buildNewInitValue(MethodDesc construct) {\n" +
                "        StringBuilder initNewValue = new StringBuilder(\" new \" + construct.methodName + \"(\");\n" +
                "        appendParamsAndEnd(initNewValue, construct.params, construct.parent, construct.parent.tempUnitContext);\n" +
                "        return initNewValue.toString();\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public StatementUnitContext visit(StatementDesc statementDesc) {\n" +
                "        StringBuilder variableBuilder = buildVariableMock(statementDesc, statementDesc.parent.tempUnitContext);\n" +
                "        StringBuilder invokeBuilder = buildInvokeMethodMock(statementDesc);\n" +
                "        return new StatementUnitContext(variableBuilder, invokeBuilder);\n" +
                "    }\n" +
                "\n" +
                "    private StringBuilder buildVariableMock(StatementDesc statement, ClassUnitContext unitContext) {\n" +
                "        StringBuilder builder = new StringBuilder(255);\n" +
                "\n" +
                "        // 声明返回变量\n" +
                "        if (StringUtil.isNotEmpty(statement.returnType) && !statement.returnType.equals(\"void\")) {\n" +
                "            String uniqueReturnParam = paramVarMayChange(statement.returnParam, statement);\n" +
                "            builder.append(SSSSSSSS).append(statement.returnType).append(\" \").append(uniqueReturnParam)\n" +
                "                    .append(\" = \").append(statement.returnInitValue);\n" +
                "        }\n" +
                "        return builder;\n" +
                "    }\n" +
                "\n" +
                "    private String paramVarMayChange(String returnParam, StatementDesc statementDesc) {\n" +
                "        String uniqueReturnParam = returnParam;\n" +
                "        if (statementDesc.parentMethodDesc.unitVariableTable.containsKey(returnParam)) {\n" +
                "            int count = statementDesc.parentMethodDesc.unitVariableTable.get(returnParam);\n" +
                "            count++;\n" +
                "            uniqueReturnParam += count;\n" +
                "            statementDesc.parentMethodDesc.unitVariableTable.put(returnParam, count);\n" +
                "        } else {\n" +
                "            statementDesc.parentMethodDesc.unitVariableTable.put(returnParam, 0);\n" +
                "        }\n" +
                "        statementDesc.returnParam = uniqueReturnParam;\n" +
                "        return uniqueReturnParam;\n" +
                "    }\n" +
                "\n" +
                "    private StringBuilder buildInvokeMethodMock(StatementDesc statementDesc) {\n" +
                "        // 如果为空，就进行两种可能性拼接，一种是doNothing，一种是return null，如果有确定的值，就进行一种拼接\n" +
                "        StringBuilder invokeMethodMock = new StringBuilder(256);\n" +
                "        String type = statementDesc.returnType;\n" +
                "        if (type != null) {\n" +
                "            invokeMethodMock.append(SSSSSSSS);\n" +
                "            if (type.equals(\"void\")) {\n" +
                "                buildDoNothingStatement(statementDesc, invokeMethodMock);\n" +
                "            } else {\n" +
                "                buildMockStatement(statementDesc, invokeMethodMock);\n" +
                "            }\n" +
                "        }\n" +
                "        return invokeMethodMock;\n" +
                "    }\n" +
                "\n" +
                "    private void buildDoNothingStatement(StatementDesc statementDesc, StringBuilder mockMethod) {\n" +
                "        mockMethod.append(\"doNothing().when(\")\n" +
                "                .append(statementDesc.methodVariable)\n" +
                "                .append(\").\")\n" +
                "                .append(statementDesc.methodName)\n" +
                "                .append(\"(\");\n" +
                "        addEntryParams(statementDesc, mockMethod);\n" +
                "        mockMethod.append(\";\\n\");\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MATCHER_STATIC);\n" +
                "    }\n" +
                "\n" +
                "    private void buildMockStatement(StatementDesc statementDesc, StringBuilder mockMethod) {\n" +
                "        mockMethod.append(\"when(\")\n" +
                "                .append(statementDesc.methodVariable)\n" +
                "                .append(\".\")\n" +
                "                .append(statementDesc.methodName)\n" +
                "                .append(\"(\");\n" +
                "        addEntryParams(statementDesc, mockMethod);\n" +
                "        mockMethod.append(\").thenReturn(\");\n" +
                "        mockMethod.append(statementDesc.returnParam);\n" +
                "        mockMethod.append(\");\\n\");\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MATCHER_STATIC);\n" +
                "    }\n" +
                "\n" +
                "    private void buildMockWordForTwoCases(StatementDesc statementDesc, StringBuilder mockMethod) {\n" +
                "        // 情况一: doNothing().when()\n" +
                "        mockMethod.append(\"doNothing().when(\")\n" +
                "                .append(statementDesc.methodVariable)\n" +
                "                .append(\").\")\n" +
                "                .append(statementDesc.methodName)\n" +
                "                .append(\"(\");\n" +
                "        addEntryParams(statementDesc, mockMethod);\n" +
                "        mockMethod.append(\";\\n\");\n" +
                "\n" +
                "        // 情况二: when() 默认返回null\n" +
                "        mockMethod.append(\"when(\")\n" +
                "                .append(statementDesc.methodVariable)\n" +
                "                .append(\".\")\n" +
                "                .append(statementDesc.methodName)\n" +
                "                .append(\"(\");\n" +
                "        addEntryParams(statementDesc, mockMethod);\n" +
                "        mockMethod.append(\").thenReturn(null);\\n\");\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MOCKITO_STATIC);\n" +
                "        statementDesc.parent.tempUnitContext.targetImportLines.add(IMPORT_MOCKITO_MATCHER_STATIC);\n" +
                "    }\n" +
                "\n" +
                "    private void addEntryParams(StatementDesc statmentDesc, StringBuilder mockInvokeMethod) {\n" +
                "        // 增加参数前采用容器进行下检查\n" +
                "        for (Map.Entry<String, String> entry : statmentDesc.params.entrySet()) {\n" +
                "            // key 变量  value 类别\n" +
                "            if (entry.getValue().equals(PLACE_HOLDER)) {\n" +
                "                String fullClassName = UnitUtil.fullClassName(statmentDesc.methodClassName, statmentDesc.parent);\n" +
                "                ClassDesc cacheClassDesc = Context.register().getClassDesc(fullClassName);\n" +
                "                // 在本身类去找，再到继承类去找\n" +
                "                findParamTypeFromContext(statmentDesc, cacheClassDesc, statmentDesc.methodName);\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        Iterator<Map.Entry<String, String>> iterator = statmentDesc.params.entrySet().iterator();\n" +
                "        while (iterator.hasNext()) {\n" +
                "            Map.Entry<String, String> entryParam = iterator.next();\n" +
                "            mockInvokeMethod.append(\"any(\")\n" +
                "                    .append(entryParam.getValue())\n" +
                "                    .append(\".class\")\n" +
                "                    .append(\")\");\n" +
                "            if (iterator.hasNext()) {\n" +
                "                mockInvokeMethod.append(\",\");\n" +
                "            }\n" +
                "            if (CommonType.getInitParamValue(entryParam.getValue()) == null) {\n" +
                "                refreshTargetImportLines(entryParam.getValue(), statmentDesc.parent, statmentDesc.parent.tempUnitContext);\n" +
                "            }\n" +
                "        }\n" +
                "        mockInvokeMethod.append(\")\");\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 递归获取方法的入参 <br>\n" +
                "     *\n" +
                "     * @param statementDesc\n" +
                "     * @param classDesc\n" +
                "     * @param methodName\n" +
                "     */\n" +
                "    private void findParamTypeFromContext(StatementDesc statementDesc, ClassDesc classDesc, String methodName) {\n" +
                "        if (classDesc != null) {\n" +
                "            Map<String, MethodDesc> map = classDesc.methodMap;\n" +
                "            for (Map.Entry<String, MethodDesc> methodEntry : map.entrySet()) {\n" +
                "                if (methodName.equals(methodEntry.getValue().methodName)) {\n" +
                "                    statementDesc.params = new LinkedHashMap<>(methodEntry.getValue().params);\n" +
                "                    // 如果存在的是泛型，那么就从childMethod对应的definetion中的泛型替换为其中记录的真实泛型\n" +
                "                    for (Map.Entry<String, String> entry : statementDesc.params.entrySet()) {\n" +
                "                        // 如果类型是泛型，即大写的字母，则将该泛型更新为childMethodName中对应的该类的对应的泛型替换\n" +
                "                        int i = 0;\n" +
                "                        if (entry.getValue().matches(\"[A-Z]\")) {\n" +
                "                            ClassDesc cacheClassDesc = Context.register().getClassDesc(UnitUtil.fullClassName(statementDesc.methodClassName, statementDesc.parent));\n" +
                "                            List<String> list = cacheClassDesc.genericType.get(classDesc.simpleName);\n" +
                "                            statementDesc.params.put(entry.getKey(), list.get(i));\n" +
                "                            i++;\n" +
                "                        }\n" +
                "                    }\n" +
                "                    return;\n" +
                "                }\n" +
                "            }\n" +
                "            // 没找到再继续找它的父类\n" +
                "            if (classDesc.hasSuperClass()) {\n" +
                "                Map<String, ClassDesc> superMap = classDesc.superClassRef;\n" +
                "                for (Map.Entry<String, ClassDesc> entry : superMap.entrySet()) {\n" +
                "                    ClassDesc superClassDesc = entry.getValue();\n" +
                "                    if (superClassDesc == null) {\n" +
                "                        superClassDesc = Context.register().getClassDesc(entry.getKey());\n" +
                "                    }\n" +
                "                    if (superClassDesc == null) {\n" +
                "                        return;\n" +
                "                    }\n" +
                "                    findParamTypeFromContext(statementDesc, superClassDesc, methodName);\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    @Override\n" +
                "    public void visit(FieldDesc fieldDesc) {\n" +
                "\n" +
                "    }\n" +
                "\n" +
                "    ClassUnitContext getThreadUnitContext(ClassDesc classDesc) {\n" +
                "        if (unitContextLocal.get() == null) {\n" +
                "            unitContextLocal.set(new ClassUnitContext());\n" +
                "        }\n" +
                "        return unitContextLocal.get().setClassDesc(classDesc);\n" +
                "    }\n" +
                "}";
        TokenWave tokenWave = new TokenWave(ss);
        List<String> list  = tokenWave.getTokens();
        for (String s : list) {
            System.out.println(s+"-->");
        }
    }


    @Test
    public void spiltTokens5() {
        String ss = "/*\n" +
                " * Copyright (C), 2002-2017, 苏宁易购电子商务有限公司\n" +
                " * FileName: PointSummaryAccountDaoImpl.java\n" +
                " * Author:   88381512\n" +
                " * Date:     2017年9月18日 下午5:32:41\n" +
                " * Description: //模块目的、功能描述      \n" +
                " * History: //修改记录\n" +
                " * <author>      <time>      <version>    <desc>\n" +
                " * 修改人姓名             修改时间            版本号                  描述\n" +
                " */\n" +
                "package com.suning.camp.core.consume.dao.point.impl;\n" +
                " \n" +
                "import java.math.BigDecimal;\n" +
                "import java.math.BigInteger;\n" +
                "import java.sql.ResultSet;\n" +
                "import java.sql.SQLException;\n" +
                "import java.sql.Timestamp;\n" +
                "import java.util.HashMap;\n" +
                "import java.util.List;\n" +
                "import java.util.Map;\n" +
                " \n" +
                "import javax.annotation.Resource;\n" +
                " \n" +
                "import org.apache.commons.collections.CollectionUtils;\n" +
                "import org.apache.commons.lang3.ArrayUtils;\n" +
                "import org.springframework.jdbc.core.RowMapper;\n" +
                " \n" +
                "import com.suning.camp.base.constants.IndividualConstant;\n" +
                "import com.suning.camp.base.constants.TableNameConstant;\n" +
                "import com.suning.camp.base.exception.CampException;\n" +
                "import com.suning.camp.base.util.DateUtil;\n" +
                "import com.suning.camp.core.consume.dao.point.PointBatchDao;\n" +
                "import com.suning.camp.core.consume.entity.BaseBatchEntity;\n" +
                "import com.suning.camp.core.consume.entity.BatchChangeEntity;\n" +
                "import com.suning.camp.core.consume.entity.PointBatchEntity;\n" +
                "import com.suning.camp.core.consume.entity.UsedPointEntity;\n" +
                "import com.suning.camp.core.util.ActivityUtils;\n" +
                "import com.suning.camp.rsf.dto.SubtractPointWastStruct;\n" +
                "import com.suning.framework.dal.client.DalClient;\n" +
                "import com.suning.framework.dal.util.DalUtils;\n" +
                " \n" +
                "/**\n" +
                " * 积分账户<br>\n" +
                " * 〈功能详细描述〉\n" +
                " *\n" +
                " * @author 88381512\n" +
                " * @see [相关类/方法]（可选）\n" +
                " * @since [产品/模块版本] （可选）\n" +
                " */\n" +
                "public class PointBatchDaoImpl implements PointBatchDao {\n" +
                "    @Resource(name = \"shardingDalClient\")\n" +
                "    private DalClient shardingDalClient;\n" +
                " \n" +
                "    @Override\n" +
                "    public List<PointBatchEntity> select(PointBatchEntity param) {\n" +
                "        return null;\n" +
                "    }\n" +
                " \n" +
                "    // 暂无\n" +
                "    @Override\n" +
                "    public int delete(PointBatchEntity param) {\n" +
                "        return 0;\n" +
                "    }\n" +
                " \n" +
                "    // 暂无\n" +
                "    @Override\n" +
                "    public int insert(PointBatchEntity param) {\n" +
                "        param.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.execute(\"PointBatch.insert\", param);\n" +
                "    }\n" +
                " \n" +
                "    // 暂无\n" +
                "    @Override\n" +
                "    public int updateByKeySelectively(PointBatchEntity param) {\n" +
                "        param.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.execute(\"PointBatch.updateByKey\", param);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public void update(BaseBatchEntity entity) {\n" +
                "        entity.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        int result = shardingDalClient.execute(\"PointBatch.updateByKey\", entity);\n" +
                "        if (result != 1) {\n" +
                "            throw new RuntimeException(\"积分批次更新失败\");//NOSONAR\n" +
                "        }\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public int getCountOfSummary(PointBatchEntity param) {\n" +
                "        param.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.queryForObject(\"PointBatch.getCountOfSummary\", param, Integer.class);\n" +
                " \n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public List<PointBatchEntity> getListOfSummary(PointBatchEntity param) {\n" +
                "        param.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.getListOfSummary\", param, PointBatchEntity.class);\n" +
                " \n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public int getTotalCountForRsf(Map<String, Object> param) {\n" +
                "        return shardingDalClient.queryForObject(\"PointBatch.getTotalCountForRsf\", param, Integer.class);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public List<PointBatchEntity> getPointBatchForRsf(Map<String, Object> param) {\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.getPointBatchForRsf\", param, POINTBATCHMAPPER);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public BigDecimal selectPointExpireSummaryAmtByEndDate(String custNum, Timestamp currentMonthEndDate) {\n" +
                "        PointBatchEntity param = new PointBatchEntity();\n" +
                "        param.setCustNum(custNum);\n" +
                "        param.setCurrentMonthEndDate(currentMonthEndDate);\n" +
                "        param.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.queryForObject(\"PointBatch.selectExpireBatchAmt\", param, BigDecimal.class);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public List<PointBatchEntity> findToBeExpiredBatches(String custNum) {\n" +
                "        Map<String, Object> param = new HashMap<>();\n" +
                "        param.put(\"custNum\", custNum);\n" +
                "        param.put(\"custNumDivide\", ActivityUtils.setDivideDB(custNum, TableNameConstant.POINT_BATCH));\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.findToBeExpiredBatches\", param, PointBatchEntity.class);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    @SuppressWarnings(\"unchecked\")\n" +
                "    public void batchUpdate(List<PointBatchEntity> updatePointBatchs) throws CampException {\n" +
                "        if (CollectionUtils.isNotEmpty(updatePointBatchs)) {\n" +
                "            Map<String, Object>[] maps = new Map[updatePointBatchs.size()];\n" +
                "            for (int i = 0; i < updatePointBatchs.size(); i++) {\n" +
                "                maps[i] = DalUtils.convertToMap(updatePointBatchs.get(i));\n" +
                "                maps[i].put(\"custNumDivide\",\n" +
                "                        ActivityUtils.setDivideDB(updatePointBatchs.get(i).getCustNum(), TableNameConstant.POINT_BATCH));\n" +
                "            }\n" +
                "            if (!ArrayUtils.isEmpty(maps)) {\n" +
                "                shardingDalClient.batchUpdate(\"PointBatch.updateByKey\", maps);\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    @SuppressWarnings(\"unchecked\")\n" +
                "    public void batchUpdate4RollBack(List<BaseBatchEntity> entityList) {\n" +
                "        if (CollectionUtils.isEmpty(entityList)) {\n" +
                "            return;\n" +
                "        }\n" +
                "        Map<String, Object>[] maps = new Map[entityList.size()];\n" +
                "        for (int i = 0; i < entityList.size(); i++) {\n" +
                "            maps[i] = DalUtils.convertToMap(entityList.get(i));\n" +
                "            maps[i].put(\"custNumDivide\",\n" +
                "                    ActivityUtils.setDivideDB(entityList.get(i).getCustNum(), TableNameConstant.POINT_BATCH));\n" +
                "        }\n" +
                "        if (!ArrayUtils.isEmpty(maps)) {\n" +
                "            int[] result = shardingDalClient.batchUpdate(\"PointBatch.update4RSF\", maps);\n" +
                "            if (result == null || result.length == 0) {\n" +
                "                return;\n" +
                "            }\n" +
                "            for (int i : result) {\n" +
                "                if (i != 1) {\n" +
                "                    throw new RuntimeException(\"积分批次批量更新失败\");// NOSONAR\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public int update4RollBack(PointBatchEntity entity) {\n" +
                "        entity.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.execute(\"PointBatch.update4RSF\", entity);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public Integer querySeqNo(Map<String, Object> paramMap) {\n" +
                "        paramMap.put(\"custNumDivide\", ActivityUtils.setDivideDB(\n" +
                "                String.valueOf(paramMap.get(IndividualConstant.CUST_NUM)), TableNameConstant.POINT_BATCH));\n" +
                "        return shardingDalClient.queryForObject(\"PointBatch.querySeqNo\", paramMap, Integer.class);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    @SuppressWarnings(\"unchecked\")\n" +
                "    public void batchUpdate4Return(List<BaseBatchEntity> entityList) {\n" +
                "        if (CollectionUtils.isEmpty(entityList)) {\n" +
                "            return;\n" +
                "        }\n" +
                "        Map<String, Object>[] maps = new Map[entityList.size()];\n" +
                "        for (int i = 0; i < entityList.size(); i++) {\n" +
                "            maps[i] = DalUtils.convertToMap(entityList.get(i));\n" +
                "            maps[i].put(\"custNumDivide\",\n" +
                "                    ActivityUtils.setDivideDB(entityList.get(i).getCustNum(), TableNameConstant.POINT_BATCH));\n" +
                "        }\n" +
                "        if (!ArrayUtils.isEmpty(maps)) {\n" +
                "            int[] result = shardingDalClient.batchUpdate(\"PointBatch.updateByKey\", maps);\n" +
                "            if (result == null || result.length == 0) {\n" +
                "                return;\n" +
                "            }\n" +
                "            for (int i : result) {\n" +
                "                if (1 != i) {\n" +
                "                    throw new RuntimeException(\"积分批次批量更新失败\");//NOSONAR\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    @SuppressWarnings(\"unchecked\")\n" +
                "    public void batchInsert4Return(List<BaseBatchEntity> entityList) {\n" +
                "        if (CollectionUtils.isNotEmpty(entityList)) {\n" +
                "            Map<String, Object>[] maps = new Map[entityList.size()];\n" +
                "            for (int i = 0; i < entityList.size(); i++) {\n" +
                "                maps[i] = DalUtils.convertToMap(entityList.get(i));\n" +
                "                maps[i].put(\"custNumDivide\",\n" +
                "                        ActivityUtils.setDivideDB(entityList.get(i).getCustNum(), TableNameConstant.POINT_BATCH));\n" +
                "            }\n" +
                "            if (!ArrayUtils.isEmpty(maps)) {\n" +
                "                shardingDalClient.batchUpdate(\"PointBatch.insert\", maps);\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    @SuppressWarnings(\"unchecked\")\n" +
                "    public void batchInsert(List<PointBatchEntity> addPointBatchs) throws CampException {\n" +
                "        if (!CollectionUtils.isEmpty(addPointBatchs)) {\n" +
                "            Map<String, Object>[] maps = new Map[addPointBatchs.size()];\n" +
                "            for (int i = 0; i < addPointBatchs.size(); i++) {\n" +
                "                maps[i] = DalUtils.convertToMap(addPointBatchs.get(i));\n" +
                "                maps[i].put(\"custNumDivide\",\n" +
                "                        ActivityUtils.setDivideDB(addPointBatchs.get(i).getCustNum(), TableNameConstant.POINT_BATCH));\n" +
                "            }\n" +
                "            if (!ArrayUtils.isEmpty(maps)) {\n" +
                "                shardingDalClient.batchUpdate(\"PointBatch.insert\", maps);\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                " \n" +
                "    /**\n" +
                "     * 功能描述: 获取云钻使用明细记录信息\n" +
                "     * \n" +
                "     * param i 分表号 param startTime 开始时间 param endTime 结束时间 param custNumDivide 分库序列号\n" +
                "     * \n" +
                "     * @return List\n" +
                "     */\n" +
                "    @Override\n" +
                "    public List<UsedPointEntity> getUsedPointList(Map<String, Object> param) {\n" +
                "        // 分库查询时必须传递的参数信息\n" +
                "        // param.put(\"custNumDivide\", 0);\n" +
                "        // 查询分表记录数\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.getUsedPointList\", param, UsedPointEntity.class);\n" +
                "    }\n" +
                " \n" +
                "    /**\n" +
                "     * {@inheritDoc}\n" +
                "     */\n" +
                "    @Override\n" +
                "    public List<SubtractPointWastStruct> queryWastPointTransaction(String orderNum, String store, String custNum) {\n" +
                "        PointBatchEntity param = new PointBatchEntity();\n" +
                "        param.setOrderNum(orderNum);\n" +
                "        param.setStore(store);\n" +
                "        param.setCustNum(custNum);\n" +
                "        param.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.queryPointTranction\", param, QUERYWASTPOINTTRANSACTIONMAPPER);\n" +
                "    }\n" +
                " \n" +
                "    /**\n" +
                "     * {@inheritDoc}\n" +
                "     */\n" +
                "    @Override\n" +
                "    public Timestamp querySrcOrderNum(Map<String, Object> param) {\n" +
                "        param.put(\"custNumDivide\",\n" +
                "                ActivityUtils.setDivideDB(String.valueOf(param.get(\"custNum\")), TableNameConstant.POINT_BATCH));\n" +
                "        String timestampStr = shardingDalClient.queryForObject(\"PointBatch.querySrcOrderNum\", param, String.class);\n" +
                "        return (timestampStr == null ? null : DateUtil.getTime(timestampStr));\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public List<BaseBatchEntity> findOriginalAddedBatch(PointBatchEntity entity) {\n" +
                "        entity.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.findOriginalAddedBatch\", entity, ADDMAPPER);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public List<PointBatchEntity> select4RollBack(PointBatchEntity entity) {\n" +
                "        entity.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.select4RollBack\", entity, ROLLBACKMAPPER);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public List<BaseBatchEntity> findUsableBatch(PointBatchEntity entity) {\n" +
                "        entity.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.findUsableBatch\", entity, BATCHMAPPER);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public List<BatchChangeEntity> findOriginSubtractBatch(PointBatchEntity entity) {\n" +
                "        entity.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.findOriginSubtractBatch\", entity, MAPPER);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public int updatePointBatchBalance(BaseBatchEntity entity) {\n" +
                "        entity.setCustNumDivide(TableNameConstant.POINT_BATCH);\n" +
                "        return shardingDalClient.execute(\"PointBatch.updatePointBatchBalance\", entity);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public List<Integer> isExistBatch(Map<String, Object> paramMap) {\n" +
                "        paramMap.put(\"custNumDivide\", ActivityUtils.setDivideDB(\n" +
                "                String.valueOf(paramMap.get(IndividualConstant.CUST_NUM)), TableNameConstant.POINT_BATCH));\n" +
                "        return shardingDalClient.queryForList(\"PointBatch.isExistBatch\", paramMap, Integer.class);\n" +
                "    }\n" +
                " \n" +
                "    @Override\n" +
                "    public int isExistReturnGoods(Map<String, Object> paramMap) {\n" +
                "        paramMap.put(\"custNumDivide\", ActivityUtils.setDivideDB(\n" +
                "                String.valueOf(paramMap.get(IndividualConstant.CUST_NUM)), TableNameConstant.POINT_BATCH));\n" +
                "        return shardingDalClient.queryForObject(\"PointBatch.isExistReturnGoods\", paramMap, Integer.class);\n" +
                "    }\n" +
                " \n" +
                "    private static RowMapper<PointBatchEntity> ROLLBACKMAPPER = new RowMapper<PointBatchEntity>() {\n" +
                "        @Override\n" +
                "        public PointBatchEntity mapRow(ResultSet rs, int rowNum) throws SQLException {\n" +
                "            PointBatchEntity entity = new PointBatchEntity();\n" +
                "            entity.setSeqNo(rs.getInt(\"SEQ_NO\"));\n" +
                "            entity.setBatchNum(rs.getString(\"BATCH_NUM\"));\n" +
                "            entity.setEventType(rs.getString(\"EVENT_TYPE\"));\n" +
                "            entity.setOrderType(rs.getString(\"ORDER_TYPE\"));\n" +
                "            entity.setOrderTypeDesc(rs.getString(\"ORDER_TYPE_DESC\"));\n" +
                "            entity.setBatchType(rs.getString(\"BATCH_TYPE\"));\n" +
                "            entity.setCurrType(rs.getString(\"CURR_TYPE\"));\n" +
                "            entity.setBatchAmt(rs.getBigDecimal(\"BATCH_AMT\"));\n" +
                "            entity.setAcctId(rs.getString(\"ACCT_ID\"));\n" +
                "            entity.setOrderAmt(rs.getBigDecimal(\"ORDER_AMT\"));\n" +
                "            entity.setStore(rs.getString(\"STORE\"));\n" +
                "            entity.setBranch(rs.getString(\"BRANCH\"));\n" +
                "            entity.setReferenceNum(rs.getString(\"REFERENCE_NUM\"));\n" +
                "            entity.setSupplierCode(rs.getString(\"SUPPLIER_CODE\"));\n" +
                "            entity.setSupplierType(rs.getString(\"SUPPLIER_TYPE\"));\n" +
                "            return entity;\n" +
                "        }\n" +
                "    };\n" +
                " \n" +
                "    private static RowMapper<BaseBatchEntity> ADDMAPPER = new RowMapper<BaseBatchEntity>() {\n" +
                "        @Override\n" +
                "        public BaseBatchEntity mapRow(ResultSet rs, int rowNum) throws SQLException {\n" +
                "            BaseBatchEntity entity = new PointBatchEntity();\n" +
                "            entity.setId(BigInteger.valueOf(rs.getLong(\"ID\")));\n" +
                "            entity.setBatchNum(rs.getString(\"BATCH_NUM\"));\n" +
                "            entity.setBatchAmt(rs.getBigDecimal(\"BATCH_AMT\"));\n" +
                "            entity.setBatchBalance(rs.getBigDecimal(\"BATCH_BALANCE\"));\n" +
                "            entity.setAcctId(rs.getString(\"ACCT_ID\"));\n" +
                "            entity.setEndDate(rs.getTimestamp(\"END_DATE\"));\n" +
                "            entity.setBatchType(rs.getString(\"BATCH_TYPE\"));\n" +
                "            entity.setBatchStat(rs.getString(\"BATCH_STAT\"));\n" +
                "            entity.setCurrType(rs.getString(\"CURR_TYPE\"));\n" +
                "            entity.setCustNum(rs.getString(\"CUST_NUM\"));\n" +
                "            entity.setCustType(rs.getString(\"CUST_TYPE\"));\n" +
                "            entity.setSupplierCode(rs.getString(\"SUPPLIER_CODE\"));\n" +
                "            entity.setSupplierType(rs.getString(\"SUPPLIER_TYPE\"));\n" +
                "            entity.setOrderType(rs.getString(\"ORDER_TYPE\"));\n" +
                "            entity.setEventTs(rs.getTimestamp(\"EVENT_TS\"));\n" +
                "            return entity;\n" +
                "        }\n" +
                "    };\n" +
                " \n" +
                "    private static RowMapper<BatchChangeEntity> MAPPER = new RowMapper<BatchChangeEntity>() {\n" +
                "        @Override\n" +
                "        public BatchChangeEntity mapRow(ResultSet rs, int rowNum) throws SQLException {\n" +
                "            BatchChangeEntity entity = new BatchChangeEntity();\n" +
                "            BaseBatchEntity batchEntity = new BaseBatchEntity();\n" +
                "            entity.setSupplierCode(rs.getString(\"SUPPLIER_CODE\"));\n" +
                "            entity.setSupplierType(rs.getString(\"SUPPLIER_TYPE\"));\n" +
                "            entity.setBatchChgAmt(rs.getBigDecimal(\"BATCH_CHG_QTY\"));\n" +
                "            batchEntity.setId(BigInteger.valueOf(rs.getInt(\"ID\")));\n" +
                "            batchEntity.setSupplierType(rs.getString(\"SUPPLIER_TYPE\"));\n" +
                "            batchEntity.setSupplierCode(rs.getString(\"SUPPLIER_CODE\"));\n" +
                "            batchEntity.setBatchNum(rs.getString(\"BATCH_NUM\"));\n" +
                "            batchEntity.setBatchBalance(rs.getBigDecimal(\"BATCH_BALANCE\"));\n" +
                "            batchEntity.setAcctId(rs.getString(\"ACCT_ID\"));\n" +
                "            batchEntity.setEndDate(rs.getTimestamp(\"END_DATE\"));\n" +
                "            batchEntity.setBatchType(rs.getString(\"BATCH_TYPE\"));\n" +
                "            batchEntity.setBatchStat(rs.getString(\"BATCH_STAT\"));\n" +
                "            batchEntity.setCurrType(rs.getString(\"CURR_TYPE\"));\n" +
                "            batchEntity.setCustNum(rs.getString(\"CUST_NUM\"));\n" +
                "            batchEntity.setCustType(rs.getString(\"CUST_TYPE\"));\n" +
                "            entity.setBatch(batchEntity);\n" +
                "            return entity;\n" +
                "        }\n" +
                "    };\n" +
                " \n" +
                "    private static RowMapper<BaseBatchEntity> BATCHMAPPER = new RowMapper<BaseBatchEntity>() {\n" +
                "        @Override\n" +
                "        public BaseBatchEntity mapRow(ResultSet rs, int rowNum) throws SQLException {\n" +
                "            BaseBatchEntity batchEntity = new BaseBatchEntity();\n" +
                "            batchEntity.setSupplierCode(rs.getString(\"SUPPLIER_CODE\"));\n" +
                "            batchEntity.setSupplierType(rs.getString(\"SUPPLIER_TYPE\"));\n" +
                "            batchEntity.setId(BigInteger.valueOf(rs.getInt(\"ID\")));\n" +
                "            batchEntity.setBatchNum(rs.getString(\"BATCH_NUM\"));\n" +
                "            batchEntity.setBatchBalance(rs.getBigDecimal(\"BATCH_BALANCE\"));\n" +
                "            batchEntity.setAcctId(rs.getString(\"ACCT_ID\"));\n" +
                "            batchEntity.setEndDate(rs.getTimestamp(\"END_DATE\"));\n" +
                "            batchEntity.setBatchType(rs.getString(\"BATCH_TYPE\"));\n" +
                "            batchEntity.setBatchStat(rs.getString(\"BATCH_STAT\"));\n" +
                "            batchEntity.setCurrType(rs.getString(\"CURR_TYPE\"));\n" +
                "            batchEntity.setCustNum(rs.getString(\"CUST_NUM\"));\n" +
                "            batchEntity.setCustType(rs.getString(\"CUST_TYPE\"));\n" +
                "            batchEntity.setSeqNo(rs.getInt(\"SEQ_NO\"));\n" +
                "            batchEntity.setBizUpdatedTime(rs.getTimestamp(\"BIZ_UPDATED_TIME\"));\n" +
                "            batchEntity.setEcoType(rs.getString(\"ECO_TYPE\"));\n" +
                "            batchEntity.setOrderType(rs.getString(\"ORDER_TYPE\"));\n" +
                "            batchEntity.setUnityOrderNum(rs.getString(\"UNITY_ORDER_NUM\"));\n" +
                "            batchEntity.setOrderNum(rs.getString(\"ORDER_NUM\"));\n" +
                "            batchEntity.setEffDate(rs.getTimestamp(\"EFF_DATE\"));\n" +
                "            batchEntity.setBatchAmt(rs.getBigDecimal(\"BATCH_AMT\"));\n" +
                "            batchEntity.setReferenceNum(rs.getString(\"REFERENCE_NUM\"));\n" +
                "            batchEntity.setStore(rs.getString(\"STORE\"));\n" +
                "            batchEntity.setBranch(rs.getString(\"BRANCH\"));\n" +
                "            batchEntity.setEventTs(rs.getTimestamp(\"EVENT_TS\"));\n" +
                "            batchEntity.setEventType(rs.getString(\"EVENT_TYPE\"));\n" +
                "            return batchEntity;\n" +
                "        }\n" +
                "    };\n" +
                " \n" +
                "    private static RowMapper<PointBatchEntity> POINTBATCHMAPPER = new RowMapper<PointBatchEntity>() {\n" +
                "        @Override\n" +
                "        public PointBatchEntity mapRow(ResultSet rs, int rowNum) throws SQLException {\n" +
                "            PointBatchEntity batchEntity = new PointBatchEntity();\n" +
                "            batchEntity.setCreatedTime(rs.getTimestamp(\"CREATED_TIME\"));\n" +
                "            batchEntity.setUpdatedTime(rs.getTimestamp(\"UPDATED_TIME\"));\n" +
                "            batchEntity.setEndTime(rs.getTimestamp(\"END_TIME\"));\n" +
                "            batchEntity.setBizCreatedTime(rs.getTimestamp(\"BIZ_CREATED_TIME\"));\n" +
                "            batchEntity.setBizUpdatedTime(rs.getTimestamp(\"BIZ_UPDATED_TIME\"));\n" +
                "            batchEntity.setCreatedByEmployee(rs.getString(\"CREATED_BY_EMPLOYEE\"));\n" +
                "            batchEntity.setUpdatedByEmployee(rs.getString(\"UPDATED_BY_EMPLOYEE\"));\n" +
                "            batchEntity.setCreatedSystem(rs.getString(\"CREATED_SYSTEM\"));\n" +
                "            batchEntity.setUpdatedSystem(rs.getString(\"UPDATED_SYSTEM\"));\n" +
                "            batchEntity.setCreatedChannel(rs.getString(\"CREATED_CHANNEL\"));\n" +
                "            batchEntity.setUpdatedChannel(rs.getString(\"UPDATED_CHANNEL\"));\n" +
                "            batchEntity.setSeqNo(rs.getInt(\"SEQ_NO\"));\n" +
                "            batchEntity.setEcoType(rs.getString(\"ECO_TYPE\"));\n" +
                "            batchEntity.setAcctId(rs.getString(\"ACCT_ID\"));\n" +
                "            batchEntity.setBatchNum(rs.getString(\"BATCH_NUM\"));\n" +
                "            batchEntity.setOrderType(rs.getString(\"ORDER_TYPE\"));\n" +
                "            batchEntity.setOrderTypeDesc(rs.getString(\"ORDER_TYPE_DESC\"));\n" +
                "            batchEntity.setUnityOrderNum(rs.getString(\"UNITY_ORDER_NUM\"));\n" +
                "            batchEntity.setOrderNum(rs.getString(\"ORDER_NUM\"));\n" +
                "            batchEntity.setOrderAmt(rs.getBigDecimal(\"ORDER_AMT\"));\n" +
                "            batchEntity.setCmmdtyCode(rs.getString(\"CMMDTY_CODE\"));\n" +
                "            batchEntity.setCmmdtyName(rs.getString(\"CMMDTY_NAME\"));\n" +
                "            batchEntity.setCmmdtyCatalog(rs.getString(\"CMMDTY_CATALOG\"));\n" +
                "            batchEntity.setCmmdtyBrand(rs.getString(\"CMMDTY_BRAND\"));\n" +
                "            batchEntity.setCmmdtyGroup(rs.getString(\"CMMDTY_GROUP\"));\n" +
                "            batchEntity.setEffDate(rs.getTimestamp(\"EFF_DATE\"));\n" +
                "            batchEntity.setEndDate(rs.getTimestamp(\"END_DATE\"));\n" +
                "            batchEntity.setBatchStat(rs.getString(\"BATCH_STAT\"));\n" +
                "            batchEntity.setBatchType(rs.getString(\"BATCH_TYPE\"));\n" +
                "            batchEntity.setBatchAmt(rs.getBigDecimal(\"BATCH_AMT\"));\n" +
                "            batchEntity.setBatchBalance(rs.getBigDecimal(\"BATCH_BALANCE\"));\n" +
                "            batchEntity.setNewBatchAmt(rs.getBigDecimal(\"NEW_BATCH_AMT\"));\n" +
                "            batchEntity.setNewBatchBalance(rs.getBigDecimal(\"NEW_BATCH_BALANCE\"));\n" +
                "            batchEntity.setBeforeBalanceAmt(rs.getBigDecimal(\"BEFORE_BALANCE_AMT\"));\n" +
                "            batchEntity.setReferenceNum(rs.getString(\"REFERENCE_NUM\"));\n" +
                "            batchEntity.setStore(rs.getString(\"STORE\"));\n" +
                "            batchEntity.setBranch(rs.getString(\"BRANCH\"));\n" +
                "            batchEntity.setOperator(rs.getString(\"OPERATOR\"));\n" +
                "            batchEntity.setEventTs(rs.getTimestamp(\"EVENT_TS\"));\n" +
                "            batchEntity.setCurrType(rs.getString(\"CURR_TYPE\"));\n" +
                "            batchEntity.setEventType(rs.getString(\"EVENT_TYPE\"));\n" +
                "            batchEntity.setCustNum(rs.getString(\"CUST_NUM\"));\n" +
                "            batchEntity.setCustType(rs.getString(\"CUST_TYPE\"));\n" +
                "            batchEntity.setSupplierType(rs.getString(\"SUPPLIER_TYPE\"));\n" +
                "            batchEntity.setSupplierCode(rs.getString(\"SUPPLIER_CODE\"));\n" +
                "            return batchEntity;\n" +
                "        }\n" +
                "    };\n" +
                " \n" +
                "    private static RowMapper<SubtractPointWastStruct> QUERYWASTPOINTTRANSACTIONMAPPER = new RowMapper<SubtractPointWastStruct>() {\n" +
                "        @Override\n" +
                "        public SubtractPointWastStruct mapRow(ResultSet rs, int rowNum) throws SQLException {\n" +
                "            SubtractPointWastStruct subtractPointWastStruct = new SubtractPointWastStruct();\n" +
                "            subtractPointWastStruct.setBatchNum(rs.getString(\"batchNum\"));\n" +
                "            subtractPointWastStruct.setBatchBalance(rs.getBigDecimal(\"batchBalance\"));\n" +
                "            subtractPointWastStruct.setBatchAmt(rs.getBigDecimal(\"batchAmt\"));\n" +
                "            subtractPointWastStruct.setBatchType(rs.getString(\"batchType\"));\n" +
                "            subtractPointWastStruct.setBatchStat(rs.getString(\"batchStat\"));\n" +
                "            subtractPointWastStruct.setOrderType(rs.getString(\"orderType\"));\n" +
                "            subtractPointWastStruct.setCurrType(rs.getString(\"currType\"));\n" +
                "            return subtractPointWastStruct;\n" +
                "        }\n" +
                "    };\n" +
                "}";
        TokenWave tokenWave = new TokenWave(ss);
        List<String> list  = tokenWave.getTokens();
        for (String s : list) {
            System.out.println(s+"-->");
        }
    }


    @Test
    public void spiltTokens6() {
        String ss = "package org.yinxueframework.util.cache.redis;\n" +
                "\n" +
                "import org.yinxueframework.util.core.ValidateUtil;\n" +
                "import redis.clients.jedis.Jedis;\n" +
                "import redis.clients.jedis.JedisPool;\n" +
                "import redis.clients.jedis.Transaction;\n" +
                "import redis.clients.jedis.exceptions.JedisConnectionException;\n" +
                "\n" +
                "import java.util.ArrayList;\n" +
                "import java.util.List;\n" +
                "import java.util.Map;\n" +
                "\n" +
                "/**\n" +
                " * 采用策略模式用于将jedis返回资源池 <br>\n" +
                " *\n" +
                " * @author zengjian\n" +
                " * @create 2018-05-17 16:36\n" +
                " * @since 1.0.0\n" +
                " */\n" +
                "public class RedisClient {\n" +
                "\n" +
                "    private JedisPool jedisPool;\n" +
                "\n" +
                "    public RedisClient() {\n" +
                "    }\n" +
                "\n" +
                "    public RedisClient(JedisPool jedisPool) {\n" +
                "        this.jedisPool = jedisPool;\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 是否存在该key\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @return 存在返回TRUE 不存在false\n" +
                "     */\n" +
                "    public Boolean exists(final String key) {\n" +
                "        return execute(new JedisAction<Boolean>() {\n" +
                "            @Override\n" +
                "            public Boolean doAction(Jedis jedis) {\n" +
                "                return jedis.exists(key);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 设置key过期时间\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @param seconds\n" +
                "     * @return 成功返回1 失败返回0\n" +
                "     */\n" +
                "    public Long expire(final String key, final int seconds) {\n" +
                "        return execute(new JedisAction<Long>() {\n" +
                "            @Override\n" +
                "            public Long doAction(Jedis jedis) {\n" +
                "                return jedis.expire(key, seconds);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 删除key <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @return 返回被删除的key数量\n" +
                "     */\n" +
                "    public Long del(final String key) {\n" +
                "        return execute(new JedisAction<Long>() {\n" +
                "            @Override\n" +
                "            public Long doAction(Jedis jedis) {\n" +
                "                return jedis.del(key);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 返回key的值，没有返回null，不是String类型抛error <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @return value或者null\n" +
                "     */\n" +
                "    public String get(final String key) {\n" +
                "        return execute(new JedisAction<String>() {\n" +
                "            @Override\n" +
                "            public String doAction(Jedis jedis) {\n" +
                "                return jedis.get(key);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 设置 key value，会覆盖旧值，同map.put\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @param value\n" +
                "     * @return 成功返回 \"OK\" 字符串\n" +
                "     */\n" +
                "    public String set(final String key, final String value) {\n" +
                "        return execute(new JedisAction<String>() {\n" +
                "            @Override\n" +
                "            public String doAction(Jedis jedis) {\n" +
                "                return jedis.get(key);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 覆盖旧值，并且设置过期时间 <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @param seconds\n" +
                "     * @param value\n" +
                "     * @return 设置成功返回 \"OK\"\n" +
                "     */\n" +
                "    public String setex(final String key, final int seconds, final String value) {\n" +
                "        return execute(new JedisAction<String>() {\n" +
                "            @Override\n" +
                "            public String doAction(Jedis jedis) {\n" +
                "                return jedis.setex(key, seconds, value);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * hash设置key下的field value值 <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @param field\n" +
                "     * @param value\n" +
                "     * @return 如果是新创建并且设置成功，返回1，如果是已存在，覆盖旧值，返回0\n" +
                "     */\n" +
                "    public Long hset(final String key, final String field, final String value) {\n" +
                "        return execute(new JedisAction<Long>() {\n" +
                "            @Override\n" +
                "            public Long doAction(Jedis jedis) {\n" +
                "                return jedis.hset(key, field, value);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 创建一个新的Map存储或者覆盖旧的 <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @param map\n" +
                "     * @return 成功返回\"OK\"\n" +
                "     */\n" +
                "    public String hmset(final String key, final Map<String, String> map) {\n" +
                "        return execute(new JedisAction<String>() {\n" +
                "            @Override\n" +
                "            public String doAction(Jedis jedis) {\n" +
                "                return jedis.hmset(key, map);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 返回hash表中指定字段，如果没有返回null <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @param field\n" +
                "     * @return\n" +
                "     */\n" +
                "    public String hget(final String key, final String field) {\n" +
                "        return execute(new JedisAction<String>() {\n" +
                "            @Override\n" +
                "            public String doAction(Jedis jedis) {\n" +
                "                return jedis.hget(key, field);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 删除指定哈希中的字段 <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @param fields\n" +
                "     * @return 被成功删除的field的数量\n" +
                "     */\n" +
                "    public Long hdel(final String key, final String... fields) {\n" +
                "        return execute(new JedisAction<Long>() {\n" +
                "            @Override\n" +
                "            public Long doAction(Jedis jedis) {\n" +
                "                return jedis.hdel(key, fields);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 哈希中是否存在指定字段 <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @param field\n" +
                "     * @return\n" +
                "     */\n" +
                "    public Boolean hexists(final String key, final String field) {\n" +
                "        return execute(new JedisAction<Boolean>() {\n" +
                "            @Override\n" +
                "            public Boolean doAction(Jedis jedis) {\n" +
                "                return jedis.hexists(key, field);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 将一个或多个值 value 插入到列表 key 的表头 时间复杂度O(1) <br>\n" +
                "     * 没有会自己创建列表，如果存在的key不是列表类型会返回一个错误 <br>\n" +
                "     *\n" +
                "     * @param key 缓存key\n" +
                "     * @param value []\n" +
                "     * @return Long 列表长度\n" +
                "     */\n" +
                "    public Long lpush(final String key, final String... values) {\n" +
                "        return execute(new JedisAction<Long>() {\n" +
                "            @Override\n" +
                "            public Long doAction(Jedis jedis) {\n" +
                "                return jedis.lpush(key, values);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 获取指定key的hash <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @return\n" +
                "     */\n" +
                "    public Map<String, String> hgetAll(final String key) {\n" +
                "        return execute(new JedisAction<Map<String, String>>() {\n" +
                "            @Override\n" +
                "            public Map<String, String> doAction(Jedis jedis) {\n" +
                "                return jedis.hgetAll(key);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 返回指定key hash的values列表，不存在时 //TODO 返回空list <br>\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @return\n" +
                "     */\n" +
                "    public List<String> hvalues(final String key) {\n" +
                "        return execute(new JedisAction<List<String>>() {\n" +
                "            @Override\n" +
                "            public List<String> doAction(Jedis jedis) {\n" +
                "                return jedis.hvals(key);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 指定的不存在的key设置值，设置成功为true，不成功为false <br>\n" +
                "     * jedis.set(key,value,NX,PX,expire) 成功返回的是OK <br>\n" +
                "     *\n" +
                "     * @param key 缓存key\n" +
                "     * @param value 缓存值\n" +
                "     * @param expire 过期时间:单位毫秒\n" +
                "     * @return 是否设置成功\n" +
                "     */\n" +
                "    public boolean setnx(final String key, final String value, final long expire) {\n" +
                "        try {\n" +
                "            return execute(new JedisAction<Boolean>() {\n" +
                "                @Override\n" +
                "                public Boolean doAction(Jedis jedis) {\n" +
                "                    return null != jedis.set(key, value, \"NX\", \"PX\", expire);\n" +
                "                }\n" +
                "            });\n" +
                "        } catch (JedisConnectionException jce) {\n" +
                "            // log.error(jce.getMessage(), jce);\n" +
                "            return false;\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 功能描述: 在同一个事务中，将数据存入Hash缓存，同时设置缓存失效时间<br>\n" +
                "     *\n" +
                "     * @param key 缓存的key\n" +
                "     * @param datas 缓存的Map\n" +
                "     * @param seconds 缓存失效时间\n" +
                "     */\n" +
                "    public boolean hsetDataAndExpire(final String key, final Map<String, String> datas, final int seconds) {\n" +
                "        try {\n" +
                "            return execute(new JedisAction<Boolean>() {\n" +
                "                @Override\n" +
                "                public Boolean doAction(Jedis jedis) {\n" +
                "                    Transaction tx = jedis.multi();\n" +
                "                    tx.hmset(key, datas);\n" +
                "                    tx.expire(key, seconds);\n" +
                "                    List<Object> exec = tx.exec();\n" +
                "                    return !ValidateUtil.isEmpty(exec);\n" +
                "                }\n" +
                "            });\n" +
                "        } catch (Exception ex) {\n" +
                "//            log.error(ex.getMessage(), ex);\n" +
                "            return false;\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 执行redis脚本语言\n" +
                "     *\n" +
                "     * @param key\n" +
                "     * @param expectValue\n" +
                "     * @param luaScript\n" +
                "     * @return\n" +
                "     */\n" +
                "    public boolean execDelLuaScript(final String key, final String expectValue, final String luaScript) {\n" +
                "        try {\n" +
                "            return execute(new JedisAction<Boolean>() {\n" +
                "                @Override\n" +
                "                public Boolean doAction(Jedis jedis) {\n" +
                "                    // 参数KEY\n" +
                "                    List<String> keys = new ArrayList<>();\n" +
                "                    keys.add(key);\n" +
                "                    // 参数ARGV\n" +
                "                    List<String> args = new ArrayList<>();\n" +
                "                    args.add(expectValue);\n" +
                "                    // 脚本调用g\n" +
                "                    Object result = jedis.eval(luaScript, keys, args);\n" +
                "                    return null != result;\n" +
                "                }\n" +
                "            });\n" +
                "        } catch (JedisConnectionException jce) {\n" +
                "            // 记录异常日志\n" +
                "            // log.error(jce.getMessage(), jce);\n" +
                "            return false;\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 功能描述: 在key对应list的尾部添加对象元素,返回队列的长度 并且防止争用情况下出现问题的情况.<br>\n" +
                "     *\n" +
                "     * @param queueName 队列名\n" +
                "     * @param str 添加的元素\n" +
                "     * @return 返回队列的长度\n" +
                "     */\n" +
                "    public Long pushQueue(final String queueName, final String str) {\n" +
                "        return execute(new JedisAction<Long>() {\n" +
                "            @Override\n" +
                "            public Long doAction(Jedis jedis) {\n" +
                "                Long result;\n" +
                "                Long res = jedis.rpushx(queueName, str);\n" +
                "                if (res == 0) {\n" +
                "                    result = jedis.rpush(queueName, str);\n" +
                "                    return result;\n" +
                "                } else {\n" +
                "                    return res;\n" +
                "                }\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 功能描述:从队列的头部删除元素，并返回删除元素,该值是以对象的形式存储，队列按先进的先出原则取出\n" +
                "     *\n" +
                "     * @param queueName 队列名\n" +
                "     * @return 取出队列数据 如果队列没有数据返回null\n" +
                "     */\n" +
                "    public String popQueue(final String queueName) {\n" +
                "        return execute(new JedisAction<String>() {\n" +
                "            @Override\n" +
                "            public String doAction(Jedis jedis) {\n" +
                "                try {\n" +
                "                    // 执行lpop命令\n" +
                "                    return jedis.lpop(queueName);\n" +
                "                } catch (JedisConnectionException jce) {\n" +
                "                    // 记录异常日志\n" +
                "                    // log.error(jce.getMessage(), jce);\n" +
                "                    return null;\n" +
                "                }\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 将给定 key 的值设为 value ，并返回 key 的旧值(old value)。 key 不存在时，返回 null\n" +
                "     *\n" +
                "     * @param key key值\n" +
                "     * @param value 新值\n" +
                "     * @return String 老值\n" +
                "     */\n" +
                "    public String getSet(final String key, final String value) {\n" +
                "        return execute(new JedisAction<String>() {\n" +
                "            @Override\n" +
                "            public String doAction(Jedis jedis) {\n" +
                "                return jedis.getSet(key, value);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * key 如果不存在 设置成功返回 true， 否则返回false\n" +
                "     *\n" +
                "     * @param key key值\n" +
                "     * @param value value值\n" +
                "     * @return boolean\n" +
                "     */\n" +
                "    public boolean setnx(final String key, final String value) {\n" +
                "        return execute(new JedisAction<Boolean>() {\n" +
                "            @Override\n" +
                "            public Boolean doAction(Jedis jedis) {\n" +
                "                return jedis.setnx(key, value) == 1;\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 功能描述: <br>\n" +
                "     * 执行sequence脚本\n" +
                "     *\n" +
                "     * @param key key值\n" +
                "     * @param fillField 填充域\n" +
                "     * @param luaScript lua脚本\n" +
                "     * @return 返回值\n" +
                "     * @see [相关类/方法](可选)\n" +
                "     * @since [产品/模块版本](可选)\n" +
                "     */\n" +
                "\n" +
                "    public Object execSequenceLuaScript(final String key, final String fillField, final String luaScript) {\n" +
                "        return execute(new JedisAction<Object>() {\n" +
                "\n" +
                "            @Override\n" +
                "            public Object doAction(Jedis jedis) {\n" +
                "                try {\n" +
                "                    // 参数KEY\n" +
                "                    List<String> keys = new ArrayList<>();\n" +
                "                    keys.add(key);\n" +
                "                    // 参数ARGV\n" +
                "                    List<String> args = new ArrayList<>();\n" +
                "                    args.add(fillField);\n" +
                "                    // 脚本调用\n" +
                "                    return jedis.eval(luaScript, keys, args);\n" +
                "                } catch (JedisConnectionException jce) {\n" +
                "                    // 记录异常日志\n" +
                "                    // log.error(jce.getMessage(), jce);\n" +
                "                    throw jce;\n" +
                "                }\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    /**\n" +
                "     * 存储到set集合\n" +
                "     * @param key\n" +
                "     * @param member\n" +
                "     * @return\n" +
                "     */\n" +
                "    public Long sadd(final String key, final String... member) {\n" +
                "        return execute(new JedisAction<Long>() {\n" +
                "            @Override\n" +
                "            public Long doAction(Jedis jedis) {\n" +
                "                return jedis.sadd(key, member);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    /**\n" +
                "     * 从set集合随机返回一个元素\n" +
                "     * @param key\n" +
                "     * @param value\n" +
                "     * @return\n" +
                "     */\n" +
                "    public String spop(final String key) {\n" +
                "        return execute(new JedisAction<String>() {\n" +
                "            @Override\n" +
                "            public String doAction(Jedis jedis) {\n" +
                "                return jedis.spop(key);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    public Long hincrBy(final String key, final String field, final long amount) {\n" +
                "        return execute(new JedisAction<Long>() {\n" +
                "            @Override\n" +
                "            public Long doAction(Jedis jedis) {\n" +
                "                return jedis.hincrBy(key, field, amount);\n" +
                "            }\n" +
                "        });\n" +
                "    }\n" +
                "\n" +
                "    private <T> T execute(JedisAction<T> command) {\n" +
                "        Jedis jedis = jedisPool.getResource();\n" +
                "        try {\n" +
                "            return command.doAction(jedis);\n" +
                "        } catch (Exception e) {\n" +
                "            throw e;\n" +
                "        } finally {\n" +
                "            jedis.close();\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    public JedisPool getJedisPool() {\n" +
                "        return jedisPool;\n" +
                "    }\n" +
                "\n" +
                "    public void setJedisPool(JedisPool jedisPool) {\n" +
                "        this.jedisPool = jedisPool;\n" +
                "    }\n" +
                "}\n";
        TokenWave tokenWave = new TokenWave(ss);
        List<String> list  = tokenWave.getTokens();
        for (String s : list) {
            System.out.println(s+"-->");
        }
    }


}