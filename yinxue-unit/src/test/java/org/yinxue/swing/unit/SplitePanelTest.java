/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: SplitePanelTest
 * Author:   zengjian
 * Date:     2018/9/8 22:52
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.unit;

import org.yinxue.swing.core.panel.YxTextArea;
import org.yinxue.swing.core.util.SwingUtils;

import javax.swing.*;

/**
 * 〈〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/8 22:52
 */
public class SplitePanelTest {

    public static void main(String[] args) {
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        YxTextArea one = new YxTextArea();
        YxTextArea two = new YxTextArea();
        splitPane.setTopComponent(one);
        splitPane.setBottomComponent(two);
        splitPane.setDividerSize(5);
        JFrame frame = SwingUtils.ofJFrameWithMenuBar("123");
        frame.add(splitPane );
        frame.setVisible(true);
        // 要在窗口true后才能设置比例，因为此时的高度和宽度才定下来
        splitPane.setDividerLocation(0.5);
//        splitPane.setDividerLocation(1);
    }

}