package fun.codedesign.yinxue.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * 数组工具类
 *
 * @author zengjian
 * @create 2018-06-06 17:48
 * @since 1.0.0
 */
public final class ArrayUtil {

    private ArrayUtil() {
    }

    public static <T> List<T> toList(T[] array){
        List<T> result = new ArrayList<>(array.length);
        for (T t : array) {
            result.add(t);
        }
        return result;
    }

    public static <T> T[] toArray(List<T> list){
        return list.toArray((T[]) Array.newInstance(list.get(0).getClass(), list.size()));
    }

}
