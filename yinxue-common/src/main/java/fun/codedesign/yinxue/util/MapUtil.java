/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: MapTools
 * Author:   zengjian
 * Date:     2018/9/21 11:29
 * Description: MapTools
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.yinxue.util;

import java.util.Map;

/**
 * Map数据结构工具类 <br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/21 11:29
 */
public final class MapUtil {

    private MapUtil() {
    }

    public static boolean isEmpty(final Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotEmpty(final Map<?, ?> map) {
        return !isEmpty(map);
    }
}