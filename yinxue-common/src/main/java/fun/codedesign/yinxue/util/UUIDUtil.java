package fun.codedesign.yinxue.util;

import java.util.UUID;

/**
 * UUID生成器
 * @author zengjian
 *
 */
public abstract class UUIDUtil {

    /**
     * 获取32位uuid
     * @return
     */
	public static String randomUUID32() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * 获取48位的uuid
	 *
	 * @return
	 */
	public static String randomUUID48() {
		// 取两组UUID拼接字符串
		String s = randomUUID32() + randomUUID32();
		// 去除UUID中的'-',并截取成48位
		return s.substring(0, 8) + s.substring(9, 13)
				+ s.substring(14, 18)
				+ s.substring(19, 23)
				+ s.substring(24, 44)
				+ s.substring(45, 49)
				+ s.substring(50, 54);
	}

}
