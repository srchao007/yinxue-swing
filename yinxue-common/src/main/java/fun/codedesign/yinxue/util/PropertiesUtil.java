/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: PropertiesTools
 * Author:   zengjian
 * Date:     2018/10/21 12:56
 * Description: 属性文件操作工具类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package fun.codedesign.yinxue.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 属性文件操作工具类 <br>
 *
 * @author zengjian
 * @create 2018/10/21 12:56
 */
public final class PropertiesUtil {

    private PropertiesUtil() {
    }

    public static Properties build(String path) {
        Properties prop = new Properties();
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
        try {
            if (is != null) {
                prop.load(is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtil.close(is);
        }
        return prop;
    }

}