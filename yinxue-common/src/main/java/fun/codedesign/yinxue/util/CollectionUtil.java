package fun.codedesign.yinxue.util;

import java.util.Collection;

/**
 * 〈CollectionTools〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/21 10:14
 */
public final class CollectionUtil {

    private CollectionUtil() {
    }

    public static boolean isEmpty(final Collection<?> col) {
        return col == null || col.isEmpty();
    }

    public static boolean isNotEmpty(final Collection<?> col) {
        return !isEmpty(col);
    }

}