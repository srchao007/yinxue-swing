package fun.codedesign.yinxue.util.result;

public class WebResponse<T> {

    private static final int OK_DATA_FLAG = 1;
    private static final int OK_FLAG = 0;
    private static final int ERROR_FLAG = -1;
    private static final WebResponse<?> OK = new WebResponse<>(OK_FLAG, null, "ok");

    private Integer code;
    private T data;
    private String message;

    public WebResponse() {
    }

    public WebResponse(Integer code, T data, String message) {
        this.data = data;
        this.code = code;
        this.message = message;
    }

    @SuppressWarnings("all")
    public static <T> WebResponse<T> ok() {
        return (WebResponse<T>) OK;
    }

    public static <T> WebResponse<T> ok(T data) {
        return WebResponse.of(OK_DATA_FLAG, data, null);
    }

    public static <T> WebResponse<T> fail(String message) {
        return WebResponse.of(ERROR_FLAG, null, message);
    }

    @SuppressWarnings("all")
    private static <T> WebResponse<T> of(Integer code, T data, String message) {
        return new WebResponse<>(code, data, message);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
