package fun.codedesign.yinxue.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  字符串工具
 *
 * @author zengjian
 * @create 2018/9/21 10:00
 */
public final class StringUtil {

    private static final String E = "";
    private static final String S = " ";
    private static final String SS = S + S;
    private static final String SSSS = SS + SS;
    private static final String SSSSSSSS = SSSS + SSSS;
    private static final String D = ".";
    private static final String COMMA = ",";

    private static final Pattern REGEX_NUM = Pattern.compile("^[0-9]+$");
    /**
     * 保留小写字母及数字
     */
    private static final Pattern REGEX_RETAIN_CHAR_NUM = Pattern.compile("[^a-z0-9]+");

    /**
     * 128
     */
    public static final int CAPCITY = 1 << 7;

    private StringUtil() {
    }

    /**
     * String str = null
     * String str = ""
     * String str = "  "
     * @param str 字符串入参
     * @return 空 true 非空 false
     */
    public static boolean isEmpty(final String str) {
        return str == null && str.trim().length() == 0;
    }

    public static boolean isNotEmpty(final String str) {
        return !isEmpty(str);
    }

    /**
     * 字符集合使用指定分隔符进行分隔拼接
     * @param col 集合
     * @param delimiter 指定分隔符号，e.g. ","
     * @return 字符串
     */
    public static String join(final Collection<String> col, final String delimiter) {
        if (col == null || delimiter == null) {
            throw new IllegalArgumentException("collection or delimiter can not be null");
        }
        if (col.isEmpty()) {
            return E;
        }
        Iterator<String> iterator = col.iterator();
        StringBuilder builder = new StringBuilder(CAPCITY);
        while (iterator.hasNext()) {
            String item = iterator.next();
            builder.append(item);
            if (iterator.hasNext()) {
                builder.append(delimiter);
            }
        }
        return builder.toString();
    }

    /**
     * ["A","B","C"] --> "A,B,C"
     * @param col 集合
     * @return 字符串
     */
    public static String joinWithComma(final Collection<String> col) {
        return join(col, COMMA);
    }

    /**
     * ["A","B","C"] --> "A B C"
     * @param col
     * @return
     */
    public static String joinWithCommaSpace(final Collection<String> col) {
        return join(col, COMMA + S);
    }

    /**
     * org.yinxue --> org {@param replace} yinxue
     * @param str
     * @param replace
     * @return
     */
    public static String replaceDot(final String str, final String replace) {
        return str.replaceAll("\\.", replace);
    }

    public static String firstChartoLowerCase(String str) {
        if (str.length() < 2){
            throw new IllegalArgumentException("string length can not be lt 2");
        }
        return str.substring(0, 1).toLowerCase().concat(str.substring(1));
    }

    public static String firstChartoUpperCase(String str) {
        if (str.length() < 2){
            throw new IllegalArgumentException("string length can not be lt 2");
        }
        return str.substring(0, 1).toUpperCase().concat(str.substring(1));
    }

    /**
     * 转换为只包含小写字母的字符串，需要包含数字的
     *
     * @param str
     * @return
     */
    public static String retainLowerCharAndNum(final String str){
        String newStr = str.trim().toLowerCase();
        Matcher m = REGEX_RETAIN_CHAR_NUM.matcher(newStr);
        while(m.find()){
            newStr = newStr.replaceAll(m.group(),"");
        }
        return newStr;
    }
}