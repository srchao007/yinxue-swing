package fun.codedesign.yinxue.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.util.*;

/**
 * 反射工具类 <br>
 *
 * @author zengjian
 * @create 2018-06-06 14:49
 * @since 1.0.0
 * @see net.sf.cglib.core.ReflectUtils
 */
public final class ReflectUtil {

    private ReflectUtil() {
    }

    public static final Class<?>[] EMPTY_CLASS = new Class[]{};

    /**
     * 取得实体类属性 <br>
     *
     * @param src
     * @param <Object>
     * @return
     * @see ArrayUtils
     */
    public static List<Field> getFields(Object src) {
        Class<?> clazz = src.getClass();
        return getFieldsByClass(clazz);
    }

    /**
     * 根据Class对象获得所有除了final之外的属性 <br>
     *
     * @param clzz
     * @return
     */
    public static List<Field> getFieldsByClass(Class<?> clazz) {
        List<Field> result = new LinkedList<>();
        for (Class<?> aClazz = clazz; aClazz != Object.class; aClazz = aClazz.getSuperclass()) {
            Field[] fields = aClazz.getDeclaredFields();
            result.addAll(ArrayUtil.toList(fields));
        }
        // 排除final修饰的属性
        filterFinalField(result);
        return result;
    }

    public static Field[] getFieldByClass(Class<?> clazz) {
        return ArrayUtil.toArray(getFieldsByClass(clazz));
    }

    private static void filterFinalField(List<Field> list) {
        // 删除Final修饰的Field
        Iterator<Field> iterator = list.iterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();
            if (Modifier.isFinal(field.getModifiers())) {
                iterator.remove();
            }
        }
    }

    private static List<Method> getMethodsByClass(Class<?> clazz) {
        Method[] methods = clazz.getDeclaredMethods();
        return ArrayUtil.toList(methods);
    }

    /**
     * 检查该对象属性是否存在null值，或者String类型属性为空值"" <br>
     *
     * @param t
     * @param <T>
     * @return 存在null为false 不存在返回true
     */
    public static <T> boolean checkFieldNotNull(T t) {
        List<Field> fields = getFields(t);
        // 遍历所有的field值进行比较，String类型采用null和""检查
        for (Field field : fields) {
            Object obj = null;
            try {
                // 不设置会报IllegalAccessException异常
                field.setAccessible(true);
                obj = field.get(t);
                if (obj == null) {
                    return false;
                }
                if (String.class == field.getType()) {
                    String value = (String) obj;
                    if (value.trim().equals("")) {
                        return false;
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * 从对象中获得特定名称的属性值，没有返回null <br>
     *
     * @param t
     * @param fieldName
     * @param <T>
     * @return
     */
    public static <T> Object getFieldValue(T t, String fieldName) {
        Field f = null;
        try {
            f = t.getClass().getDeclaredField(fieldName);
            f.setAccessible(true);
            return f.get(t);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将map集合中的值赋值给实体类 <br>
     *
     * @param t
     * @param values
     * @param <T>
     * @throws IllegalAccessException
     */
    public static <T> T fillFieldValues(Map<String, Object> values, Class<T> returnType) throws IllegalAccessException {
        List<Field> fields = getFieldsByClass(returnType);
        T t = (T) newInstance(returnType);
        for (Field field : fields) {
            String fieldName = field.getName().trim().toLowerCase();
            Class type = field.getType();
            if (Modifier.isFinal(field.getModifiers())) {
                continue;
            }
            field.setAccessible(true);
            Object columnValue = values.get(fieldName);
            // 如果为Long类型则使用BigInteger包装对象
            if (columnValue instanceof Long) {
                columnValue = new BigInteger(String.valueOf(columnValue));
            }
            field.set(t, columnValue);
        }
        return t;
    }

    public static <T> List<T> fillFieldValues(List<Map<String, Object>> values, Class<T> returnType) throws IllegalAccessException {
        List<T> resultList = new ArrayList<>(values.size());
        for (Map<String, Object> paramValue : values) {
            resultList.add((T) fillFieldValues(values, returnType));
        }
        return null;
    }

    public static Object newInstance(Class<?> clazz, Class[] parameters,Object[] args){
        return newInstance(getConstructor(clazz, parameters), args);
    }

    public static Object newInstance(Class<?> clazz, Class[] paramters){
        return newInstance(getConstructor(clazz, paramters), null);
    }

    public static Object newInstance(Class<?> clazz){
        return newInstance(getConstructor(clazz, EMPTY_CLASS), null);
    }

    public static Constructor getConstructor(Class<?> clazz, Class[] parameters) {
        Constructor constructor = null;
        try {
            constructor = clazz.getDeclaredConstructor(parameters);
            if (!Modifier.isPublic(constructor.getModifiers())){
                constructor.setAccessible(true);
            }
            return constructor;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("no such constuctor");
        }
    }


    /**
     * 通用的构造器是实例化方法
     *
     * @param str
     * @param parameters
     * @return
     * @see net.sf.cglib.core.ReflectUtils#newInstance(Class)
     */
    public static Object newInstance(Constructor constructor, Object[] args) {
        if (!Modifier.isPublic(constructor.getModifiers())) {
            constructor.setAccessible(true);
        }
        try {
            return constructor.newInstance(args);
        } catch (Exception e) {
            throw new RuntimeException("constructor init failed");
        }
    }


    public static <T> List<Object[]> convertFrom(List<T> objects) {
        return null;
    }
}
