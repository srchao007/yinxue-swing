package fun.codedesign.yinxue.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *  
 *  日期类工具
 *
 * @author zengjian
 * @create 2018/9/21 11:18
 */
public final class DateUtil {

    private static final DateFormat DATE_FORMAT_MILLISECONDS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private static final DateFormat DATE_FORMAT_SECONDS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private DateUtil() {
    }

    public static String getCurrentFormatMilliTime() {
        return DATE_FORMAT_MILLISECONDS.format(new Date());
    }

    public static String getCurrentFormatTime() {
        return DATE_FORMAT_SECONDS.format(new Date());
    }

    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    public static Date getCurrentDate() {
        return new Date();
    }

    public static Long now() {
        return System.currentTimeMillis();
    }
}