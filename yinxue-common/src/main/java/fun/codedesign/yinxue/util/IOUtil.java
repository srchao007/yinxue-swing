package fun.codedesign.yinxue.util;

/**
 * 资源释放类工具 <br>
 * @author zengjian
 * @create 2018-06-06 14:26
 * @since 1.0.0
 */
public final class IOUtil {

    private IOUtil() {
    }

    public static void close(AutoCloseable... args) {
        if (args == null) {
            return;
        }
        for (AutoCloseable arg : args) {
            if (arg != null) {
                try {
                    arg.close();
                } catch (Exception e) {
                    LogUtil.error(args, null, e);
                }
                arg = null;
            }
        }
    }

}
