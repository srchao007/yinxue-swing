package org.yinxue.framework.common;

import org.junit.Assert;
import org.junit.Test;

import fun.codedesign.yinxue.util.ArrayUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayUtilTest {

    @Test
    public void convertToArray() {

        List<String> list = new ArrayList<>();
        list.add("hello");
        list.add("helli");
        list.add("world");
        Assert.assertEquals("[hello, helli, world]",
                Arrays.toString(ArrayUtil.toArray(list)));
    }
}