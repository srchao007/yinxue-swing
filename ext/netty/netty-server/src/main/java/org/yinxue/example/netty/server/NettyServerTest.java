package org.yinxue.example.netty.server;

import org.yinxue.example.netty.core.model.HeartBeatRequest;
import org.yinxue.example.netty.server.util.ChannelHolder;

import java.io.IOException;
import java.util.Random;

/**
 * NettyServerTest {@link org.yinxue.example.netty.server}
 *
 * @author zengjian
 * @date 2019/1/1
 * @since 1.0.0
 */
public class NettyServerTest {

    public static void main(String[] args) throws InterruptedException, IOException {
        final NettyServer server = new NettyServer("localhost",8080);
        new Thread(() -> server.start()).start();
        System.in.read();
        // 主动发消息给客户端
        String resp ="服务器主动消息";
        HeartBeatRequest request =  new HeartBeatRequest();
        request.setOptType(2);
        request.setClientId(new Random().nextLong());
        request.setContentLength(resp.getBytes("utf-8").length);
        request.setContent(resp);
        ChannelHolder.batchMessageToClient(request);
        System.in.read();
    }

}