package org.yinxue.example.netty.server.util;

import io.netty.channel.Channel;
import org.yinxue.example.netty.core.model.HeartBeatRequest;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * ChannelHolder {@link org.yinxue.example.netty.server.util}
 *
 * @author zengjian
 * @date 2019/1/1
 * @since 1.0.0
 */
public class ChannelHolder {

    private static Map<Long, Channel> map = new ConcurrentHashMap<>();

    public static void addChannel(Long key, Channel channel) {
        map.put(key, channel);
    }

    public static void batchMessageToClient(HeartBeatRequest request) {
        for (Channel channel : map.values()) {
            channel.writeAndFlush(request);
        }
    }
}