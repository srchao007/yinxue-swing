package org.yinxue.example.netty.client;

import org.yinxue.example.netty.core.model.HeartBeatRequest;

import java.util.Random;

/**
 * NettyClientTest {@link org.yinxue.example.netty.client}
 *
 * @author zengjian
 * @date 2019/1/1
 * @since 1.0.0
 */
public class NettyClientTest {

    public static void main(String[] args) throws Exception {
        final NettyClient nettyClient = new NettyClient("localhost", 8080);
        new Thread(new Runnable() {
            @Override
            public void run() {
                nettyClient.start();
            }
        }).start();
        // 等待连接
        Thread.sleep(2000);
//        for (int i = 0; i < 10; i++) {
        HeartBeatRequest request = new HeartBeatRequest();
        String resp = "请求" + 1;
        request.setClientId(new Random().nextLong());
        request.setContent(resp);
        request.setContentLength(resp.getBytes("utf-8").length);
        nettyClient.pushMessage(request);
//        }
        System.in.read();
    }

}
