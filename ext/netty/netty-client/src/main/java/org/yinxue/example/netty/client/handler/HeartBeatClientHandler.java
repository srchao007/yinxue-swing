package org.yinxue.example.netty.client.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.yinxue.example.netty.core.model.HeartBeatRequest;

import java.util.Date;

/**
 * HeartBeatClientHandler {@link org.yinxue.example.netty.client.handler}
 *
 * @author zengjian
 * @date 2019/1/1
 * @since 1.0.0
 */
public class HeartBeatClientHandler extends SimpleChannelInboundHandler<HeartBeatRequest> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HeartBeatRequest msg) throws Exception {
        System.out.println(new Date() + " 客户端接收到服务返回消息：" + msg);
    }
}