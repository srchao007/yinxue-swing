package org.yinxue.example.netty.core.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.yinxue.example.netty.core.model.HeartBeatRequest;

/**
 * HeartBeatEncoder {@link org.yinxue.example.netty.core.codec}
 *
 * @author zengjian
 * @date 2019/1/1
 * @since 1.0.0
 */
public class HeartBeatEncoder extends MessageToByteEncoder<HeartBeatRequest> {


    @Override
    protected void encode(ChannelHandlerContext ctx, HeartBeatRequest msg, ByteBuf out) throws Exception {
        if (msg != null) {
            out.writeInt(msg.getOptType());
            out.writeLong(msg.getClientId());
            out.writeLong(msg.getContentLength());
            out.writeBytes(msg.getContent().getBytes("utf-8"));
        } else {
           throw new RuntimeException("HeartBeatRequest is null");
        }
    }
}