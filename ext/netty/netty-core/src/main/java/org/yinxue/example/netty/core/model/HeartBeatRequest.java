package org.yinxue.example.netty.core.model;

/**
 * HeartBeatRequest {@link org.yinxue.example.netty.core.model}
 * 4bytes+ 8bytes+ 8bytes+ contentLength bytes
 * @author zengjian
 * @date 2019/1/1
 * @since 1.0.0
 */
public class HeartBeatRequest {

    private int optType;
    private long clientId;
    private long contentLength;
    private String content;

    public HeartBeatRequest() {
    }

    public int getOptType() {
        return optType;
    }

    public void setOptType(int optType) {
        this.optType = optType;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "HeartBeatRequest{" +
                "optType=" + optType +
                ", clientId=" + clientId +
                ", contentLength=" + contentLength +
                ", content='" + content + '\'' +
                '}';
    }
}