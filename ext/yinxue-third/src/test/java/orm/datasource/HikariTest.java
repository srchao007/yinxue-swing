package orm.datasource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariConfigMXBean;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import com.zaxxer.hikari.pool.HikariPool;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author zengjian
 * @create 2018-06-06 10:12
 * @since 1.0.0
 */
public class HikariTest {

    public static void main(String[] args) throws SQLException {
        List<HikariDataSource> list = new ArrayList<>();
        for (int i = 0; i < 2000; i++) {
            HikariDataSource dataSource = new HikariDataSource();
            dataSource.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/family?useAffectedRows=true&useSSL=true");
            dataSource.setUsername("root");
            dataSource.setPassword("1234");
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setDataSourceJNDI("");
            dataSource.getConnection();
            list.add(dataSource);
        }
    }

    @Test
    public void test() {
        HikariDataSource ds = new HikariDataSource();
        HikariDataSource ds2 = new HikariDataSource(new HikariDataSource());
        HikariDataSource ds3 = new HikariDataSource(new HikariConfig());

        HikariConfig config = new HikariConfig();
        HikariConfig config1 = new HikariConfig(new Properties());
        // properties加载属性文件
        HikariConfig config2 = new HikariConfig("properityFileName");

        HikariPool pool = new HikariPool(new HikariConfig());
        HikariPoolMXBean poolMXBean = new HikariPool(new HikariConfig());
        HikariConfigMXBean configMXBean = new HikariConfig();
        HikariConfigMXBean configMXBean1 = new HikariDataSource();

        try {
            pool.getConnection();
        } catch (SQLException e) {
        }
    }

    @Test
    public void test3() throws SQLException {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/family?useAffectedRows=true&useSSL=true");
        dataSource.setUsername("root");
        dataSource.setPassword("1234");
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        for (int i = 0; i < 1000; i++) {
            Connection con = dataSource.getConnection();
            System.out.println(i + ":" + con.toString());
            con.close();
        }
    }

}
