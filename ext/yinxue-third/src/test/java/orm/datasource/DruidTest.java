package orm.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.util.JdbcConstants;
import org.junit.Test;

import javax.sql.PooledConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zengjian
 * @create 2018-04-10 9:43
 * @since 1.0.0
 */
public class DruidTest {
    public static void main(String[] args) throws SQLException, InterruptedException {
        DruidDataSource druidDataSource = new DruidDataSource();

        ReentrantLock lock = new ReentrantLock(false);
        Condition lockConditon1 = lock.newCondition();
        Condition lockConditon2 = lock.newCondition();
        /*lockConditon1.await();
        lockConditon1.signal();*/

        // 初始化参数
        druidDataSource.setUrl("jdbc:mysql://localhost:3306/family");
        druidDataSource.setUsername("root");
        druidDataSource.setPassword("1234");

        // 得到连接 DruidPooledConnection
        Connection connection = druidDataSource.getConnection();
        connection.prepareStatement("hello");

        // 得到连接 DruidPooledConnection
        PooledConnection pooledConnection = druidDataSource.getPooledConnection();
        Connection conFromPool = pooledConnection.getConnection();
    }

    @Test
    public void test() throws SQLException {
        DruidDataSource druidDataSource = new DruidDataSource();
        // 初始化参数
        druidDataSource.setUrl("jdbc:mysql://localhost:3306/family");
        druidDataSource.setUsername("root");
        druidDataSource.setPassword("1234");

        // 得到连接 DruidPooledConnection
        for (int i = 0; i < 1000; i++) {
            Connection connection = druidDataSource.getConnection();
            System.out.println(connection.toString() + ":" + i);
            //connection.close();
            long count = druidDataSource.getPoolingCount();
            System.out.println("线程池里的:" + count);
        }
    }

    @Test
    public void testAst() {
        String dbType = JdbcConstants.MYSQL;
        List<SQLStatement> statementList = SQLUtils.parseStatements("select * from user", dbType);
        System.out.println(SQLUtils.toSQLString(statementList, dbType));
    }
}
