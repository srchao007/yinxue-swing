package orm.mybatis;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.defaults.DefaultSqlSessionFactory;

/**
 * ibatis测试
 *
 * @author zengjian
 * @create 2018-04-10 20:27
 * @since 1.0.0
 */
public class MybatisTest {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        SqlSessionFactory sqlSessionFactory = new DefaultSqlSessionFactory(configuration);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        sqlSession.selectList("");
    }
}
