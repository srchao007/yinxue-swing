/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: FastJsonTest
 * Author:   zengjian
 * Date:     2018/8/15 16:50
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package parser.json;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/15 16:50
 */
public class FastJsonTest {

    public static class World {

        private String hello;
        private String world;

        public String getHello() {
            return hello;
        }

        public void setHello(String hello) {
            this.hello = hello;
        }

        public String getWorld() {
            return world;
        }

        public void setWorld(String world) {
            this.world = world;
        }
    }

    public static void main(String[] args) {
        String json = "{\"hello\":\"123\",\"world\":\"246\"}";

        Object obj = JSON.parseObject(json, World.class);
        System.out.println(obj.toString());
        System.out.println(ToStringBuilder.reflectionToString(obj));
    }
}