/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: TypeTest
 * Author:   zengjian
 * Date:     2018/8/16 17:50
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package parser.json;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/16 17:50
 */
public class TypeTest {

    static class Hello {

    }

    public static void main(String[] args) {
        Class clzz = Hello.class;
        Type type = (Type) clzz;
        System.out.println(type instanceof ParameterizedType);
        System.out.println(clzz.getTypeParameters());
    }

}