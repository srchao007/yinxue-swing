/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: OpcodesBit
 * Author:   zengjian
 * Date:     2018/8/15 9:22
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package parser.asm;

import org.junit.Test;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/15 9:22
 */
public class OpcodesBit {

    public static void main(String[] args) {

        //System.out.println(OpcodesBit.V1_1 << 16 >> 16);
        //System.out.println(OpcodesBit.V1_2);

        System.out.println(org.objectweb.asm.Opcodes.ACC_PUBLIC);
        System.out.println(org.objectweb.asm.Opcodes.ACC_PRIVATE);
        System.out.println(org.objectweb.asm.Opcodes.ACC_PROTECTED);
        System.out.println(org.objectweb.asm.Opcodes.ACC_STATIC);

        System.out.println(org.objectweb.asm.Opcodes.ACC_INTERFACE);


        System.out.println(org.objectweb.asm.Opcodes.ASM5 == 5 << 16);

        int result = org.objectweb.asm.Opcodes.ACC_INTERFACE + org.objectweb.asm.Opcodes.ACC_PUBLIC;
        System.out.println(result == 0x0201);
        System.out.println(result & org.objectweb.asm.Opcodes.ACC_ABSTRACT);

        System.out.println(000020);

        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        System.out.println(-65535 & 0xFFFF);
        System.out.println(1 & 0xFFFF);

        System.out.println(Integer.toBinaryString(16));
        System.out.println(Integer.toBinaryString(-65535));

        System.out.println((char) 0x1A);

        System.out.println('"');
        System.out.println('\"');
        System.out.println('"' == '\"');
    }

    @Test
    public void  testBit(){
        // 这两个不同因为有1重叠的部分
        System.out.println(36+36);
        System.out.println(36|36);

        // 下面两个是一样的，1不重叠
        System.out.println(1 + (1<<1) + (1<<2));

        int a = 1;
        a|= 1<<1;
        a|= 1<<2;
        System.out.println(a);
    }
}