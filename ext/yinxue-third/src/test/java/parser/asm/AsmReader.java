/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: AsmReader
 * Author:   zengjian
 * Date:     2018/8/14 19:29
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package parser.asm;

import org.junit.Test;
import org.objectweb.asm.*;

import java.io.IOException;
import java.util.Arrays;

import static org.objectweb.asm.Opcodes.ASM7;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/14 19:29
 */
public class AsmReader {

    @Test
    public void testRead() throws IOException {
        ClassReader reader = new ClassReader("parser.asm.BBB");
        reader.accept(new ClassPrinter(ASM7), ClassReader.SKIP_DEBUG);
    }


    @Test
    public void test() {
        try {
            ClassPrinter printer = new ClassPrinter(Opcodes.ASM5);
            ClassReader classReader = new ClassReader("java.lang.Runnable");
            classReader.accept(printer, ClassReader.SKIP_DEBUG);
            // System.out.println(classReader.getClassName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class ClassPrinter extends ClassVisitor {

        public ClassPrinter(int api) {
            super(api);
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            System.out.println(version + ":" + access + ":" + name + ":" + signature + ":" + superName + ":" + Arrays.toString(interfaces));
            super.visit(version, access, name, signature, superName, interfaces);
        }

        @Override
        public void visitSource(String source, String debug) {
            super.visitSource(source, debug);
        }

        @Override
        public ModuleVisitor visitModule(String name, int access, String version) {
            return super.visitModule(name, access, version);
        }

        @Override
        public void visitOuterClass(String owner, String name, String descriptor) {
            super.visitOuterClass(owner, name, descriptor);
        }

        @Override
        public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
            return super.visitAnnotation(descriptor, visible);
        }

        @Override
        public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible) {
            return super.visitTypeAnnotation(typeRef, typePath, descriptor, visible);
        }

        @Override
        public void visitAttribute(Attribute attribute) {
            super.visitAttribute(attribute);
        }

        @Override
        public void visitInnerClass(String name, String outerName, String innerName, int access) {
            super.visitInnerClass(name, outerName, innerName, access);
        }

        @Override
        public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
            System.out.println("-----visitField-----");
            System.out.println("access:" + access);
            System.out.println("name:" + name);
            System.out.println("descriptor:" + descriptor);
            System.out.println("signature:" + signature);
            System.out.println("value:" + value);
            return super.visitField(access, name, descriptor, signature, value);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            System.out.println("-----visitMethod-----");
            System.out.println("access:" + access);
            System.out.println("name:" + name);
            System.out.println("descriptor:" + descriptor);
            System.out.println("signature:" + signature);
            System.out.println("exceptions:" + exceptions);
            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }


        @Override
        public void visitEnd() {
            super.visitEnd();
        }
    }

}