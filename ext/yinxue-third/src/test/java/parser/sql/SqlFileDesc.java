package parser.sql;

public class SqlFileDesc {
    private String absolutePath;
    private String sqlFilename;
    private String dirname;

    public SqlFileDesc() {

    }

    public SqlFileDesc(String absolutePath, String sqlFilename, String dirname) {
        this.absolutePath = absolutePath;
        this.sqlFilename = sqlFilename;
        this.dirname = dirname;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getSqlFilename() {
        return sqlFilename;
    }

    public void setSqlFilename(String sqlFilename) {
        this.sqlFilename = sqlFilename;
    }

    public String getDirname() {
        return dirname;
    }

    public void setDirname(String dirname) {
        this.dirname = dirname;
    }
}
