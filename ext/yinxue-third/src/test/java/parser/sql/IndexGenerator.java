///*
// * Copyright (C), 2002-2015, 苏宁易购电子商务有限公司
// * FileName: SqlGenerator.java
// * Author:   13071472
// * Date:     2015-9-28 上午9:32:29
// */
//package sql;
//
//import com.suning.camp.web.sql.SqlGenerator.Table;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.springframework.core.CollectionUtils;
//
//import java.io.*;
//import java.core.ArrayList;
//import java.core.HashMap;
//import java.core.List;
//import java.core.Map;
//
///**
// * 索引生成器.<br>
// * 仅适用于Mysql.
// *
// * @author 13071472
// */
//@SuppressWarnings("serial")
//public class IndexGenerator {
//
//    // 文件位置
//    //public static final String DOC_PATH = "D:/SVNDoc/中台研发中心文档管理库/库存中心项目群/项目类/库存平台项目/3.系统开发/3.1概要设计/数据存储设计/SN_GAIA_平台库存系统数据库表设计.xlsx";
//    public static final String DOC_PATH = "C:\\Users\\17081493\\Desktop\\Excel\\SN_ECIF_账户系统数据库表分库分表设计1.xlsx";
//
//    // dev-SQL脚本生成路径
//    //public static final String SQL_PATH = "D:\\svnDoc\\中台研发中心文档管理库\\库存中心项目群\\项目类\\库存平台项目\\3.系统开发\\3.6发布\\04发布文件\\01dev环境\\01DB脚本\\";
//    //public static final String SQL_PATH = "E:\\库存中心项目群\\项目类\\库存平台项目\\3.系统开发\\3.6发布\\04发布文件\\SQL一期优化\\01dev环境\\01DB脚本\\";
//
//    // prd-SQL脚本生成路径
//    //public static final String SQL_PATH = "D:\\svnDoc\\中台研发中心文档管理库\\库存中心项目群\\项目类\\库存平台项目\\3.系统开发\\3.6发布\\04发布文件\\04prd环境\\01DB脚本\\";
//    public static final String SQL_PATH = "C:\\Users\\17081493\\Desktop\\campindex\\";
//
//
//    // 简单版本:手动指定【单表】
//    static List<String> singleLst = new ArrayList<String>() {
//        {
//        }
//    };
//
//    // 仅处理list指定的表(list为空视为处理所有表)
//    static List<String> onlyTableList = new ArrayList<String>() {
//        {
//            add("POINT_ACCOUNT");
//            add("COUPON_ACCOUNT");
//            add("DEPOSIT_ACCOUNT");
//            add("POINT_SUMMARY_ACCOUNT");
//            add("POINT_BATCH_LOG");
//            add("POINT_BATCH");
//            add("COUPON_BATCH");
//            add("COUPON_BATCH_LOG");
//            add("DEPOSIT_BATCH");
//            add("DEPOSIT_BATCH_LOG");
//            add("PENDING_ACCOUNT_BATCH");
//            add("SUPPLIER_CODE_CUST_NUM_MAPPING");
//            add("ACCOUNT_CONFIG");
//            add("BRANCH");
//            add("STORE");
//            add("FROZEN_POINT_BATCH");
//            add("FROZEN_POINT_BATCH_LOG");
//            add("PE_FROZEN_POINT_SEND");
//            add("COMMON_CODE");
//            add("MAPPING_RELATION_SHIP");
//            add("CUST_ACCOUNT_CHECK");
//            add("CUST_ACCOUNT_CHECK_DETAIL");
//            add("POINT_LOCK_INFO");
//
//            add("POINT_RATIO_CONFIG");
//            add("CURRENSY_RATE");
//            add("CAMP_PENDING");
//            add("BROADCAST");
//            add("EXPIRE_PENDING");
//
//            add("TASK_PENDING");
//            add("SP_ETL_LOG");
//            add("SEQ_ACCOUNT_BATCH_NUM");
//        }
//    };
//
//    public static void main(String[] args) throws IOException {
//        Map<String, Integer> dbSize = new HashMap<String, Integer>();
//        Map<String, String> dbAlias = new HashMap<String, String>();
//
//        //dev
//        /*dbSize.put("日志库", 2);
//        dbSize.put("核心业务库", 2);
//        dbSize.put("公共库", 1);*/
//
//        //prd
//        dbSize.put("日志库", 10);
//        dbSize.put("核心业务库", 1);
//        dbSize.put("公共库", 1);
//
//        dbAlias.put("日志库", "LOG");
//        dbAlias.put("核心业务库", "BIZ");
//        dbAlias.put("公共库", "PUB");
//
//        IndexGenerator.generateIndex(dbSize, dbAlias);
//    }
//
//    public static void generateIndex(Map<String, Integer> dbSize, Map<String, String> dbAlias) throws IOException {
//        Map<String, List<Table>> tableList = SqlGenerator.tableList();
//        Map<String, List<Index>> tbMap = parseExcel();
//
//        for (Map.Entry<String, List<Table>> entry : tableList.entrySet()) {
//            System.out.println("处理" + entry.getKey());
//            // 生成SQL存放的文件夹
//            File dir = new File(SQL_PATH, dbAlias.get(entry.getKey()));
//            if (!dir.exists() || !dir.isDirectory()) {
//                dir.mkdir();
//            } else {
//                File[] fs = dir.listFiles(new FileFilter() {
//                    @Override
//                    public boolean accept(File pathname) {
//                        return pathname.getName().indexOf("IDX.SQL") > -1;
//                    }
//                });
//                for (File f : fs) {
//                    f.delete();
//                }
//                //dir.mkdir();
//            }
//            for (Table t : entry.getValue()) {
//                if ("Z_DICT".equals(t.getTbName())) {
//                    continue;
//                }
//                System.out.println("处理" + t.getTbName());
//                if(!CollectionUtils.isEmpty(onlyTableList) && !onlyTableList.contains(t.getTbName())){
//                    System.out.println("本次不需处理" + t.getTbName());
//                    continue;
//                }
//                if (t.getNumbers() == 1) {
//                    // 生成单表CREATE SQL
//                    for (int dbNo = 1; dbNo <= dbSize.get(entry.getKey()); dbNo++) {
//                        BufferedWriter writer = null;
//                        try {
//                            writer = new BufferedWriter(new FileWriter(new File(SQL_PATH + dbAlias.get(entry.getKey()),
//                                    dbAlias.get(entry.getKey()) + "_" + dbNo + "_IDX.SQL"), true));
//                            writer.write(prettyPrintIndex(t.getTbName(), tbMap.get(t.getTbName())));
//                        } catch (IOException ex) {
//                        } finally {
//                            try {
//                                writer.flush();
//                                writer.close();
//                            } catch (IOException ex) {
//                            }
//                        }
//                    }
//                } else {
//                    // FOR LOOP 生成CREATE SQL
//                    for (int dbNo = 1; dbNo <= dbSize.get(entry.getKey()); dbNo++) {
//                        BufferedWriter writer = null;
//                        try {
//                            writer = new BufferedWriter(new FileWriter(new File(SQL_PATH + dbAlias.get(entry.getKey()),
//                                    dbAlias.get(entry.getKey()) + "_" + dbNo + "_IDX.SQL"), true));
//                            for (int tbNo = dbNo - 1; tbNo < t.getNumbers(); tbNo += dbSize.get(entry.getKey())) {
//                                writer.write(prettyPrintIndex(t.getTbName(), tbMap.get(t.getTbName()), tbNo));
//                            }
//                        } catch (IOException ex) {
//                        } finally {
//                            try {
//                                writer.flush();
//                                writer.close();
//                            } catch (IOException ex) {
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * 功能描述: 解析EXCEL得到表名--列对应Map<br>
//     *
//     * @return
//     * @throws IOException
//     */
//    public static Map<String, List<Index>> parseExcel() throws IOException {
//
//        // {TableName}-{Index}
//        Map<String, List<Index>> idxMap = new HashMap<String, List<Index>>();
//
//            FileInputStream stream = new FileInputStream(DOC_PATH);
//            XSSFWorkbook wb = new XSSFWorkbook(stream);
//            int sheetCount = wb.getNumberOfSheets();
//            for (int i = 6; i < sheetCount; i++) {
//                Sheet sheet = wb.getSheetAt(i);
//                Cell tbNameCell = sheet.getRow(1).getCell(4);
//                // 表名
//                String tbName = tbNameCell.toString().trim();
//                System.out.println(tbName);
//
//                if ("Z_DICT".equals(tbName)) {
//                    continue;
//                }
//
//                // 定位ID的位置
//                int idIdx = 0;
//                for (int k = 0;; k++) {
//                    Row row = sheet.getRow(k);
//                    if (row != null && row.getCell(1) != null
//                            && "Column Name".equals(row.getCell(1).toString().trim())) {
//                        idIdx = k + 1;
//                        break;
//                    }
//                }
//
//                // 定位Index的位置
//                int idxIdx = 0;
//                for (int k = 0;; k++) {
//                    Row row = sheet.getRow(k);
//                    if (row != null && row.getCell(1) != null
//                            && "Index Name".equals(row.getCell(1).toString().trim())) {
//                        idxIdx = k + 1;
//                        break;
//                    }
//                }
//
//                // 从ID开始遍历
//                List<Index> lstColumn = new ArrayList<Index>();
//                Index index = null;
//                int j = idxIdx;
//                Row row = sheet.getRow(j);
//                while (j < idIdx && row != null && row.getCell(1).toString().trim().length() != 0) {
//                    if("PK_ID".equals(row.getCell(1).toString().trim())){
//                        j++;
//                        row = sheet.getRow(j);
//                        continue;
//                    }
//                    index = new Index();
//                    index.setName(row.getCell(1).toString().trim());
//                    index.setColumns(row.getCell(2).toString().trim());
//                    index.setType(row.getCell(8).toString().trim());
//                    lstColumn.add(index);
//                    j++;
//                    row = sheet.getRow(j);
//                }
//                idxMap.put(tbName, lstColumn);
//            }
//
//        return idxMap;
//    }
//
//    private static String prettyPrintIndex(String tbName, List<Index> lstIndex) {
//        StringBuffer sBuffer = new StringBuffer();
//        if (lstIndex == null || lstIndex.isEmpty()) {
//            return sBuffer.toString();
//        }
//        for (Index index : lstIndex) {
//            if ("UIDX".equals(index.getType())) {
//                sBuffer.append("CREATE UNIQUE INDEX ").append(index.getName()).append(" ON ").append(tbName).append("(")
//                        .append(index.getColumns().replace("+", ",")).append(");\n");
//            } else {
//                sBuffer.append("CREATE INDEX ").append(index.getName()).append(" ON ").append(tbName).append("(")
//                        .append(index.getColumns().replace("+", ",")).append(");\n");
//            }
//        }
//        return sBuffer.toString();
//    }
//
//    private static String prettyPrintIndex(String tbName, List<Index> lstIndex, int tbNo) {
//        StringBuffer sBuffer = new StringBuffer();
//        if (lstIndex.isEmpty()) {
//            return sBuffer.toString();
//        }
//        for (Index index : lstIndex) {
//            if ("UIDX".equals(index.getType())) {
//                sBuffer.append("CREATE UNIQUE INDEX ").append(index.getName()).append("_").append(tbNo).append(" ON ")
//                        .append(tbName).append("_")
//                        .append(tbNo).append("(").append(index.getColumns().replace("+", ",")).append(");\n");
//            } else {
//                sBuffer.append("CREATE INDEX ").append(index.getName()).append(" ON ").append(tbName).append("_")
//                        .append(tbNo).append("(").append(index.getColumns().replace("+", ",")).append(");\n");
//            }
//        }
//        return sBuffer.toString();
//    }
//
//    static class Index {
//        private String name;
//        private String columns;
//        private String type;
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getColumns() {
//            return columns;
//        }
//
//        public void setColumns(String columns) {
//            this.columns = columns;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        @Override
//        public String toString() {
//            return "Index [name=" + name + ", columns=" + columns + ", type=" + type + "]";
//        }
//    }
//
//    static interface TableLocator {
//        int locateByNo(int tbNo);
//    }
//
//}
