package parser.sql;

import java.io.*;
import java.util.ArrayList;

public class createSQL {

    public static void main(String[] args) {
        String tableName = "user";
        int num = 10;

        File inputFile = null;
        FileInputStream fis = null;
        BufferedReader br = null;
        File outputFile = null;
        FileOutputStream out = null;

        String tempstr = null;

        try {
            inputFile = new File("C:\\Users\\jh\\Desktop\\yinxue_user.sql");
            fis = new FileInputStream(inputFile);
            br = new BufferedReader(new InputStreamReader(fis));

            outputFile = new File("C:\\Users\\jh\\Desktop\\yinxue_user2.sql");
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            out = new FileOutputStream(outputFile, false); // 如果追加方式用true

            ArrayList<String> tempList = new ArrayList<String>();
            while ((tempstr = br.readLine()) != null) {
                tempList.add(tempstr);
            }

            int i = -1;
            for (int j = 0; j < num; j++) {
                StringBuffer sb = new StringBuffer();
                for (String s : tempList) {
                    if (s.contains("CREATE TABLE ")) {
                        i++;
                        s = s.replace(tableName, tableName + "_" + i);
                    }

                    if (s.contains("CREATE INDEX ") || s.contains("CREATE UNIQUE INDEX ")) {
                        StringBuffer sb2 = new StringBuffer();
                        int index = s.indexOf(" on ");
                        sb2.append(s.substring(0, index));
                        sb2.append("_");
                        sb2.append(i);
                        sb2.append(s.substring(index));
                        sb2.append("_");
                        sb2.append(i);
                        sb.append(sb2 + "\n");
                        continue;
                    }

                    sb.append(s + "\n");
                }
                sb.append("\n");
                out.write(sb.toString().getBytes("utf-8"));// 注意需要转换对应的字符集
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
                br.close();
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
