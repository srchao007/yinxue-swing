package parser.sql;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 合并SQL文件
 *
 * @author 15031031
 */
public class CombineFile {

    public static final String SQL_PATH = "";
    public static final int DB_SIZE = 10;

    public static void main(String[] args) throws IOException {
        combine();
    }

    static void combine() throws IOException {
        File outFile = new File(SQL_PATH);

        //删除ALL文件
        File[] allFiles = outFile.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isFile() && pathname.getName().indexOf("_ALL.SQL") > -1;
            }
        });
        for (File allFile : allFiles) {
            allFile.delete();
        }

        //取文件夹中的文件
        File[] dir = outFile.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory() && (
                        pathname.getName().equalsIgnoreCase("BIZ") ||
                                pathname.getName().equalsIgnoreCase("LOG") ||
                                pathname.getName().equalsIgnoreCase("PUB"));
            }
        });
        List<SqlFileDesc> paths = new ArrayList<SqlFileDesc>();
        for (File file : dir) {
            File[] sqlFiles = file.listFiles();
            String dirFilename = file.getName();

            for (File sqlFile : sqlFiles) {
                String sqlFilename = sqlFile.getName();
                String absolutePath = sqlFile.getAbsolutePath();

                SqlFileDesc sqlDesc = new SqlFileDesc(absolutePath, sqlFilename, dirFilename);

                paths.add(sqlDesc);
            }
        }

        SqlFileDesc[] sortPaths = sort(paths.toArray(new SqlFileDesc[]{}));
        for (SqlFileDesc path : sortPaths) {
            writefile(path.getAbsolutePath(), path.getSqlFilename(), path.getDirname());
        }
    }

    /**
     * 功能描述: <br>
     * 对路径进行排序, DDL -> IDX -> other
     *
     * @param paths 路径
     * @return
     */
    private static SqlFileDesc[] sort(SqlFileDesc[] paths) {
        int size = paths.length;
        SqlFileDesc temp;
        for (int i = 0; i < size; i++) {
            int k = i;
            for (int j = size - 1; j > i; j--) {
                String path_j = paths[j].getSqlFilename();
                String path_k = paths[k].getSqlFilename();
                if (path_j.indexOf("_DDL.SQL") > -1) {
                    k = j;
                } else if (path_j.indexOf("_IDX.SQL") > -1 && path_k.indexOf("_DDL.SQL") == -1) {
                    k = j;
                }
            }
            temp = paths[i];
            paths[i] = paths[k];
            paths[k] = temp;
        }
        return paths;
    }

    /**
     * 功能描述: <br>
     * 追加SQL文件
     *
     * @param absolutePath
     * @param sqlFilename
     * @param type
     */
    private static void writefile(String absolutePath, String sqlFilename, String type) {
        for (int i = 1; i <= DB_SIZE; i++) {
            // 1.合并DDL.SQL
            String allSqlPath = SQL_PATH + "\\" + type + "_" + i + "_ALL.SQL";
            if (sqlFilename.indexOf(type + "_" + i + "_DDL.SQL") > -1) {
                appendFileContent(allSqlPath, absolutePath, sqlFilename);
                continue;
            } else if (sqlFilename.indexOf(type + "_" + i + "_IDX.SQL") > -1) {
                // 2.合并索引
                appendFileContent(allSqlPath, absolutePath, sqlFilename);
                continue;
            } else if (sqlFilename.indexOf(type + "_" + i + "_") > -1) {
                // 合并数据文件
                appendFileContent(allSqlPath, absolutePath, sqlFilename);
                System.out.println(sqlFilename);
                continue;
            }
        }

    }

    /**
     * 功能描述: <br>
     * 文件复制
     *
     * @param writePath
     * @param readpath
     * @param note
     */
    @SuppressWarnings("unused")
    private static void appendFileContent(String writePath, String readpath, String note) {
        FileOutputStream fos = null;
        FileInputStream fis = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            fos = new FileOutputStream(writePath, true);
            fis = new FileInputStream(readpath);

            byte[] buffer = new byte[1024];
            int byteread = 0;

            //fos.write(("--" + note + ",createTime:" + sdf.format(new Date()) + "--\n").getBytes());

            while ((byteread = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, byteread);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
