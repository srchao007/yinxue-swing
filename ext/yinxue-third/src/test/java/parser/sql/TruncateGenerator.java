///*
// * Copyright (C), 2002-2015, 苏宁易购电子商务有限公司
// * FileName: SqlGenerator.java
// * Author:   13071472
// * Date:     2015-9-28 上午9:32:29
// */
//package sql;
//
//import org.springframework.core.CollectionUtils;
//
//import java.io.*;
//import java.core.ArrayList;
//import java.core.HashMap;
//import java.core.List;
//import java.core.Map;
//
//
///**
// * 批量生成TRUNCATE语句.<br>
// * 仅适用于Mysql.
// *
// * @author 08070433
// */
//@SuppressWarnings("serial")
//public class TruncateGenerator {
//
//    // 文件位置
//    //public static final String DOC_PATH = "D:/SVNDoc/中台研发中心文档管理库/库存中心项目群/项目类/库存平台项目/3.系统开发/3.1概要设计/数据存储设计/SN_GAIA_平台库存系统数据库表设计.xlsx";
//    public static final String DOC_PATH = "E:/库存中心项目群/项目类/库存平台项目/3.系统开发/3.1概要设计/数据存储设计/SN_GAIA_平台库存系统数据库表设计.xlsx";
//
//    // dev-SQL脚本生成路径
//    //public static final String SQL_PATH = "D:\\svnDoc\\中台研发中心文档管理库\\库存中心项目群\\项目类\\库存平台项目\\3.系统开发\\3.6发布\\04发布文件\\01dev环境\\01DB脚本\\";
//    //public static final String SQL_PATH = "E:\\库存中心项目群\\项目类\\库存平台项目\\3.系统开发\\3.6发布\\04发布文件\\SQL一期优化\\01dev环境\\01DB脚本\\";
//
//    // prd-SQL脚本生成路径
//    //public static final String SQL_PATH = "D:\\svnDoc\\中台研发中心文档管理库\\库存中心项目群\\项目类\\库存平台项目\\3.系统开发\\3.6发布\\04发布文件\\04prd环境\\01DB脚本\\";
//    public static final String SQL_PATH = "E:\\库存中心项目群\\项目类\\库存平台项目\\3.系统开发\\3.6发布\\04发布文件\\SQL一期优化\\04prd环境\\01DB脚本\\";
//
//    // 仅处理list指定的表(list为空视为处理所有表)
//    static List<String> onlyTableList = new ArrayList<String>() {
//        {
//            add("Z_STO_EXCULUSION_KEY");
//            add("Z_SO_LOCK");
//            add("Z_EXCULUSION_KEY");
//            add("Z_EXCULUSION_KEY_ORDERNO");
//            add("Z_EXCULUSION_SBP");
//            add("Z_INV_STATUS");
//            add("Z_INV_STATUS_COMB");
//            add("Z_PO_PENDING");
//            add("Z_SBP_PENDING");
//            add("Z_INV_STATUS_PENDING");
//            add("Z_INV_STATUS_OTHER_PENDING");
//            add("Z_INV_STATUS_SYNC");
//            add("Z_INV_STATUS_COMB_SYNC");
//            add("Z_INV_STATUS_LOG");
//            add("Z_INV_STATUS_COMB_LOG");
//            add("Z_INV_STATUS_INIT");
//            add("Z_INV_STATUS_INIT_LOG");
//        }
//    };
//
//    public static void main(String[] args) {
//        Map<String, Integer> dbSize = new HashMap<String, Integer>();
//        Map<String, String> dbAlias = new HashMap<String, String>();
//
//        //dev
//        /*dbSize.put("日志库", 2);
//        dbSize.put("核心业务库", 2);
//        dbSize.put("公共库", 1);*/
//
//        //prd
//        //dbSize.put("日志库", 10);
//        dbSize.put("核心业务库", 10);
//        //dbSize.put("公共库", 1);
//
//        //dbAlias.put("日志库", "LOG");
//        dbAlias.put("核心业务库", "BIZ");
//        //dbAlias.put("公共库", "PUB");
//
//        TruncateGenerator.generateTruncate(dbSize, dbAlias);
//    }
//
//    public static void generateTruncate(Map<String, Integer> dbSize, Map<String, String> dbAlias) {
//        Map<String, List<Table>> tableList = SqlGenerator.tableList();
//
//        for (Map.Entry<String, List<Table>> entry : tableList.entrySet()) {
//            System.out.println("处理" + entry.getKey());
//            // 生成SQL存放的文件夹
//            File dir = new File(SQL_PATH, dbAlias.get(entry.getKey()));
//            if (!dir.exists() || !dir.isDirectory()) {
//                dir.mkdir();
//            } else {
//                File[] fs = dir.listFiles(new FileFilter() {
//                    @Override
//                    public boolean accept(File pathname) {
//                        return pathname.getName().indexOf("TRU.SQL") > -1;
//                    }
//                });
//                for (File f : fs) {
//                    f.delete();
//                }
//                //dir.mkdir();
//            }
//            for (Table t : entry.getValue()) {
//                if ("Z_DICT".equals(t.getTbName())) {
//                    continue;
//                }
//                System.out.println("处理" + t.getTbName());
//                if(!CollectionUtils.isEmpty(onlyTableList) && !onlyTableList.contains(t.getTbName())){
//                    System.out.println("本次不需处理" + t.getTbName());
//                    continue;
//                }
//                if (t.getNumbers() == 1) {
//                    // 生成单表 SQL
//                    for (int dbNo = 1; dbNo <= dbSize.get(entry.getKey()); dbNo++) {
//                        BufferedWriter writer = null;
//                        try {
//                            writer = new BufferedWriter(new FileWriter(new File(SQL_PATH + dbAlias.get(entry.getKey()),
//                                    dbAlias.get(entry.getKey()) + "_" + dbNo + "_TRU.SQL"), true));
//                            writer.write(prettyPrintTru(t.getTbName()));
//                        } catch (IOException ex) {
//                        } finally {
//                            try {
//                                writer.flush();
//                                writer.close();
//                            } catch (IOException ex) {
//                            }
//                        }
//                    }
//                } else {
//                    // FOR LOOP 生成 SQL
//                    for (int dbNo = 1; dbNo <= dbSize.get(entry.getKey()); dbNo++) {
//                        BufferedWriter writer = null;
//                        try {
//                            writer = new BufferedWriter(new FileWriter(new File(SQL_PATH + dbAlias.get(entry.getKey()),
//                                    dbAlias.get(entry.getKey()) + "_" + dbNo + "_TRU.SQL"), true));
//                            for (int tbNo = dbNo - 1; tbNo < t.getNumbers(); tbNo += dbSize.get(entry.getKey())) {
//                                writer.write(prettyPrintTru(t.getTbName(), tbNo));
//                            }
//                        } catch (IOException ex) {
//                        } finally {
//                            try {
//                                writer.flush();
//                                writer.close();
//                            } catch (IOException ex) {
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//
//    private static String prettyPrintTru(String tbName) {
//        StringBuffer sBuffer = new StringBuffer();
//        sBuffer.append("TRUNCATE TABLE ").append(tbName).append(";\n");
//        return sBuffer.toString();
//    }
//
//    private static String prettyPrintTru(String tbName, int tbNo) {
//        StringBuffer sBuffer = new StringBuffer();
//        sBuffer.append("TRUNCATE TABLE ").append(tbName).append("_").append(tbNo).append(";\n");
//        return sBuffer.toString();
//    }
//
//    static interface TableLocator {
//        int locateByNo(int tbNo);
//    }
//
//}
