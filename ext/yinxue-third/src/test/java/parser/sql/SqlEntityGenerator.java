package parser.sql;//package org.yinxueframework.core.sql;
//
//import org.apache.commons.lang3.StringUtils;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.springframework.core.LinkedMultiValueMap;
//import org.springframework.core.MultiValueMap;
//
//import java.io.*;
//import java.core.ArrayList;
//import java.core.HashMap;
//import java.core.List;
//import java.core.Map;
//
//public class SqlEntityGenerator {
//
//    // SN_ZIMS_自开发库存二期数据表设计.xlsx 文件位置
//    public static final String DOC_PATH = "/D:/Svn/库存产品/1开发过程/1.4系统开发/1.4.2 C店库存管理系统/02概要设计/02数据存储设计/SN_CIMS_活动库存执行系统数据库表设计.xlsx";
//
//    // SQL脚本生成路径
//    public static final String SQL_PATH = "D:/Svn/库存产品/1开发过程/1.4系统开发/1.4.3 活动执行系统/02概要设计/02数据存储设计/T";
//
//    // Entity文件生成路径
//    public static final String ENTITY_PATH = "D:/Svn/库存产品/1开发过程/1.4系统开发/1.4.3 活动执行系统/02概要设计/02数据存储设计/T";
//
//    static List<String> orderLst = new ArrayList<String>();
//    static List<String> logLst = new ArrayList<String>();
//    static List<String> publicLst = new ArrayList<String>();
//    static List<String> dropLst = new ArrayList<String>();
//
//    static List<String> singleLst = new ArrayList<String>();
//
//    static Map<String, Index> indexMap = new HashMap<String, Index>();
//
//    static List<String> filterLst = new ArrayList<String>() {
//        private static final long serialVersionUID = 1L;
//        {
//
//        }
//    };
//
//    static MultiValueMap<String, String> filterMap = new LinkedMultiValueMap<String, String>() {
//        private static final long serialVersionUID = 1L;
//
//        {
//            add("Z_PO", "Z_DOC_ITEM_NO");
//            add("Z_STO", "Z_DOC_ITEM_NO");
//            add("Z_RV", "Z_DOC_ITEM_NO");
//        }
//    };
//
//    public static void main(String[] args) {
//        SqlEntityGenerator.sqlGenerator(100, 2, new TableLocator() {
//            @Override
//            public int locateByNo(int tableName) {
//                return tableName % 2 + 1;
//            }
//
//            @Override
//            public int locateByName(String tableName) {
//                return 0;
//            }
//        });
//
//        SqlEntityGenerator.entityGenerator();
//
//        // Map<String, List<Column>> tbMap = parseExcel2();
//
//        // SqlEntityGenerator.idxGenerator();
//
//    }
//
//    public static void test() {
//    }
//
//    public static void alterTable() {
//
//        for (int i = 0; i < 1000; i++) {
//            if (i % 2 != 0) {
//                continue;
//            }
//            System.out.println("ALTER TABLE Z_PO_SNAP" + i + " MODIFY Z_DOC_ITEM_NO VARCHAR(6);");
//        }
//
//    }
//
//    public static void idxGenerator() {
//        parseExcel();
//        for (Map.Entry<String, Index> entry : indexMap.entrySet()) {
//            System.out.println(entry.getValue().toString(entry.getKey()));
//        }
//    }
//
//    public static void entityGenerator() {
//        Map<String, List<Column>> tbMap = parseExcel2();
//        BufferedWriter writer = null;
//        try {
//            for (Map.Entry<String, List<Column>> entry : tbMap.entrySet()) {
//                writer = new BufferedWriter(new FileWriter(new File(ENTITY_PATH, toJavaClassName(entry.getKey())
//                        + ".java")));
//                writer.write(prettyPrintJavaEntity(entry.getKey(), entry.getValue()));
//                writer.flush();
//                writer.close();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     *
//     * 功能描述: <br>
//     *
//     * @param tbSize 分库数
//     * @param dbSize 分表数
//     * @param tbLocator
//     * @return
//     */
//    public static void sqlGenerator(int tbSize, int dbSize, TableLocator tbLocator) {
//        // parseExcel()
//        Map<String, List<Column>> tbMap = parseExcel2();
//
//        BufferedWriter pubDdlWriter = null;
//        BufferedWriter pubInitWriter = null;
//
//        try {
//            pubDdlWriter = new BufferedWriter(new FileWriter(new File(SQL_PATH, "PUB_DDL.SQL")));
//            pubInitWriter = new BufferedWriter(new FileWriter(new File(SQL_PATH, "PUB_INIT.SQL")));
//            // 公共库
//            System.out.println("开始处理公共库,表总数:" + publicLst.size());
//            for (String tbName : publicLst) {
//                try {
//                    pubDdlWriter.write(prettyPrintSql(tbName, tbMap.get(tbName)));
//                } catch (NullPointerException e) {
//                    System.out.println("NP:"+tbName);
//                }
//            }
//            // 单据库
//            // 按表按分库遍历多次
//            for (int dbNo = 1; dbNo <= dbSize; dbNo++) {
//                System.out.println("开始处理单据库" + dbNo + "库,表总数:" + orderLst.size());
//                BufferedWriter orderWriter = new BufferedWriter(new FileWriter(new File(SQL_PATH, "DOC_" + dbNo
//                        + "_DDL.SQL")));
//                for (String tbName : orderLst) {
//                    if (tbMap.get(tbName) == null)
//                        System.out.println(tbName + " NOT EXIST.");
//                    if (isSingleTable(tbName)) {
//                        orderWriter.write(prettyPrintSql(tbName, tbMap.get(tbName)));
//                    } else {
//                        for (int tbNo = 0; tbNo < tbSize; tbNo++) {
//                            if (tbLocator.locateByNo(tbNo) == dbNo) {
//                                orderWriter.write(prettyPrintSql(tbName, tbMap.get(tbName), tbNo));
//                                // pubInitWriter.write(tbDbConf(tbName + "_" + tbNo, "doc" + dbNo));
//                                tbDbConf("T_DOC_" + tbNo, "DB_DOC_" + dbNo);
//                            }
//                        }
//                    }
//                }
//                orderWriter.flush();
//                orderWriter.close();
//            }
//            // 日志库
//            for (int dbNo = 1; dbNo <= dbSize; dbNo++) {
//                System.out.println("开始处理日志库" + dbNo + "库,表总数:" + logLst.size());
//                BufferedWriter logWriter = new BufferedWriter(new FileWriter(new File(SQL_PATH, "LOG_" + dbNo
//                        + "_DDL.SQL")));
//                for (String tbName : logLst) {
//                    if (isSingleTable(tbName)) {
//                        logWriter.write(prettyPrintSql(tbName, tbMap.get(tbName)));
//                    } else {
//                        for (int tbNo = 0; tbNo < tbSize; tbNo++) {
//                            if (tbLocator.locateByNo(tbNo) == dbNo) {
//                                logWriter.write(prettyPrintSql(tbName, tbMap.get(tbName), tbNo));
//                                // pubInitWriter.write(tbDbConf(tbName + "_" + tbNo, "log" + dbNo));
//                                tbDbConf("T_LOG_" + tbNo, "DB_LOG_" + dbNo);
//                            }
//                        }
//                    }
//                }
//                logWriter.flush();
//                logWriter.close();
//            }
//
//            for (Map.Entry<String, String> entry : tbDbMap.entrySet()) {
//                pubInitWriter.write("INSERT INTO Z_DB_TABLE(TABLE_NAME,DB_NO) VALUES ('" + entry.getKey() + "','"
//                        + entry.getValue() + "');\n");
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                pubDdlWriter.flush();
//                pubDdlWriter.close();
//
//                pubInitWriter.flush();
//                pubInitWriter.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//
//    private static String prettyPrintSql(String tbName, List<Column> lstColumn) {
//        StringBuffer sBuffer = new StringBuffer();
//        // DROP TABLE IF EXISTS
//        sBuffer.append("CREATE TABLE ").append(tbName).append("\n");
//        sBuffer.append("(\n");
//        boolean hasID = false;
//        for (Column column : lstColumn) {
//            if (column.getCode().equals("ID") && column.getType().toUpperCase().startsWith("INT")) {
//                column.setType("INT AUTO_INCREMENT");
//            }
//            sBuffer.append("        ").append(column.toString()).append("\n");
//            if ("ID".equals(column.getCode().toUpperCase().trim())) {
//                hasID = true;
//            }
//        }
//        sBuffer.append("        ").append("PRIMARY KEY (ID)").append("\n");
//        sBuffer.append(");\n");
//        if (!hasID) {
//            // System.err.println("TABLE " + tbName + " MISSING COLUMN ID.");
//        }
//        return sBuffer.toString();
//    }
//
//    private static String prettyPrintSql(String tbName, List<Column> lstColumn, int tbNo) {
//        StringBuffer sBuffer = new StringBuffer();
//        // DROP TABLE IF EXISTS
//        sBuffer.append("CREATE TABLE ").append(tbName).append("_").append(tbNo).append("\n");
//        sBuffer.append("(\n");
//        boolean hasID = false;
//        for (Column column : lstColumn) {
//            if (column.getCode().equals("ID") && column.getType().toUpperCase().startsWith("INT")) {
//                column.setType("INT AUTO_INCREMENT");
//            }
//            sBuffer.append("        ").append(column.toString()).append("\n");
//            if ("ID".equals(column.getCode().toUpperCase().trim())) {
//                hasID = true;
//            }
//        }
//        sBuffer.append("        ").append("PRIMARY KEY (ID)").append("\n");
//        sBuffer.append(");\n");
//        if (!hasID) {
//            // System.err.println("TABLE " + tbName + " MISSING COLUMN ID.");
//        }
//        return sBuffer.toString();
//    }
//
//    private static String prettyPrintJavaEntity(String tbName, List<Column> lstColumn) {
//        StringBuffer sBuffer = new StringBuffer();
//
//        String className = toJavaClassName(tbName);
//        // package
//        sBuffer.append("package com.suning.ebuy.aims.entity;").append("\r\n");
//        sBuffer.append("\r\n");
//        // import
//        sBuffer.append("import javax.persistence.Column;").append("\r\n");
//        sBuffer.append("import javax.persistence.Entity;").append("\r\n");
//        sBuffer.append("import javax.persistence.GeneratedValue;").append("\r\n");
//        sBuffer.append("import javax.persistence.GenerationType;").append("\r\n");
//        sBuffer.append("import javax.persistence.Id;").append("\r\n");
//        sBuffer.append("\r\n");
//        sBuffer.append("import java.math.BigDecimal;").append("\r\n");
//        sBuffer.append("import java.core.Date;").append("\r\n");
//        sBuffer.append("\r\n");
//        sBuffer.append("import com.suning.framework.orm.route.annotation.ShardMapping;").append("\r\n");
//        sBuffer.append("\r\n");
//        // class annotation
//        // @Entity(name = "${pmod('Z_SO_',cmmdtyCode)}")
//        // @ShardMapping(shardRef = "docRouter")
//        // @ShardMapping(shardRef = "logRouter")
//        // class
//        if (orderLst.contains(tbName)) {
//            if (singleLst.contains(tbName)) {
//                sBuffer.append("@Entity(name = \"" + tbName + "\")").append("\r\n");
//                sBuffer.append("@ShardMapping(shardRef = \"activity\")");
//            } else {
//                sBuffer.append("@Entity(name = \"${mod('" + tbName + "_',activityNo,100)}\")").append("\r\n");
//                sBuffer.append("@ShardMapping(shardRef = \"activity\")");
//            }
//        } else if (logLst.contains(tbName)) {
//            if (singleLst.contains(tbName)) {
//                sBuffer.append("@Entity(name = \"" + tbName + "\")").append("\r\n");
//            } else {
//                sBuffer.append("@Entity(name = \"${mod('" + tbName + "_',activityNo)}\")").append("\r\n");
//            }
//        } else {
//            sBuffer.append("@Entity(name = \"" + tbName + "\")").append("\r\n");
//            sBuffer.append("@ShardMapping(shardRef = \"activity\")");
//        }
//        sBuffer.append("public class " + className + " {").append("\r\n");
//        sBuffer.append("\r\n");
//        // field
//        for (Column column : lstColumn) {
//            sBuffer.append("/** ").append(column.getName()).append(" */").append("\r\n");
//            sBuffer.append(column.toField()).append("\r\n");
//            sBuffer.append("\r\n");
//        }
//        // annotation
//        // getter
//        // setter
//        for (Column column : lstColumn) {
//
//            /** *@return the cmmdtyCode */
//            sBuffer.append("/** \r\n * @return the ").append(toCamelName(column.getCode())).append("\r\n */")
//                    .append("\r\n");
//            sBuffer.append(column.toGetter()).append("\r\n");
//            sBuffer.append("\r\n");
//
//            /** * @param id the id to set */
//            sBuffer.append("/** \r\n * @param ").append(toCamelName(column.getCode())).append(" the ")
//                    .append(toCamelName(column.getCode())).append(" to set \r\n */").append("\r\n");
//            sBuffer.append(column.toSetter()).append("\r\n");
//        }
//        // end
//        sBuffer.append("}");
//        return sBuffer.toString();
//    }
//
//    public static Map<String, List<Column>> parseExcel2() {
//        // {TableName}-{TableColumn}
//        Map<String, List<Column>> tableMap = new HashMap<String, List<Column>>();
//
//        try {
//            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(DOC_PATH));
//
//            int sheetCount = wb.getNumberOfSheets();
//
//            Row row = null;
//
//            for (int i = 3; i < sheetCount; i++) {
//                Sheet sheet = wb.getSheetAt(i);
//                // System.out.println("=======" + sheet.getSheetName() + "=======");
//                Cell tbNameCell = sheet.getRow(1).getCell(4);
//                String tbName = tbNameCell.toString().trim();
//
//                int j = 7;
//
//                List<Column> lstColumn = new ArrayList<Column>();
//                Column column = null;
//                row = sheet.getRow(j);
//                while (row != null && row.getCell(1).toString().trim().length() != 0) {
//                    column = new Column();
//                    column.setCode(row.getCell(1).toString().trim());
//                    column.setName(row.getCell(2).toString().trim());
//                    column.setType(row.getCell(3).toString().trim());
//                    lstColumn.add(column);
//                    j++;
//                    row = sheet.getRow(j);
//                }
//                tableMap.put(tbName, lstColumn);
//                if (tbName.equals("ACTV_INV_QTY") || tbName.equals("ACTV_INV_LOCK")) {
//                    orderLst.add(tbName);
//                } else {
//                    publicLst.add(tbName);
//                }
//            }
//
//        } catch (FileNotFoundException ex) {
//            ex.printStackTrace();
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        return tableMap;
//    }
//
//    public static Map<String, List<Column>> parseExcel() {
//        // {TableName}-{TableColumn}
//        Map<String, List<Column>> tableMap = new HashMap<String, List<Column>>();
//
//        try {
//            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(DOC_PATH));
//
//            int sheetCount = wb.getNumberOfSheets();
//
//            Sheet tableLst = wb.getSheetAt(1);
//
//            int k = 7;
//            Row row = tableLst.getRow(k);
//            while ((row != null && notEmptyCell(row.getCell(2)))
//                    || (tableLst.getRow(k + 1) != null && notEmptyCell(tableLst.getRow(k + 1).getCell(2)))) {
//                // System.out.println(row.getCell(3).toString().trim());
//
//                // +++++++++++++++过滤器++++++++++++++++
//                if (!filterLst.isEmpty() && !filterLst.contains(row.getCell(3).toString().trim())) {
//                    k++;
//                    row = tableLst.getRow(k);
//                    continue;
//                }
//                if (row.getCell(9).toString().trim().equals("一个库一张表")) {
//                    singleLst.add(row.getCell(3).toString().trim());
//                }
//                if (row.getCell(10) != null && StringUtils.isNotBlank(row.getCell(10).toString())) {
//                    System.out.println("发现三四阶段表:" + row.getCell(3).toString().trim());
//                } else {
//                    k++;
//                    row = tableLst.getRow(k);
//                    continue;
//                }
//                if (row.getCell(2).toString().trim().equals("")) {
//                    k++;
//                    row = tableLst.getRow(k);
//                    continue;
//                }
//                if (row.getCell(2).toString().trim().equals("Doc Data")) {
//                    orderLst.add(row.getCell(3).toString().trim());
//                } else if (row.getCell(2).toString().trim().equals("Log Data")) {
//                    logLst.add(row.getCell(3).toString().trim());
//                } else if (row.getCell(2).toString().trim().equals("Public Data")) {
//                    publicLst.add(row.getCell(3).toString().trim());
//                } else {
//
//                }
//                k++;
//                row = tableLst.getRow(k);
//            }
//
//            System.out.println("TABLE COUNT : " + (orderLst.size() + logLst.size() + publicLst.size()));
//
//            for (int i = 3; i < sheetCount; i++) {
//                Sheet sheet = wb.getSheetAt(i);
//                // System.out.println("=======" + sheet.getSheetName() + "=======");
//                Cell tbNameCell = sheet.getRow(9).getCell(2);
//                String tbName = tbNameCell.toString().trim();
//
//                // ===================
//                // 索引新增
//                // ===================
//                int j = 7;
//
//                row = sheet.getRow(j);
//                while (row != null && row.getCell(3).toString().trim().length() != 0) {
//                    indexMap.put(tbName, new Index(row.getCell(3).toString().trim(), row.getCell(4).toString().trim(),
//                            row.getCell(9).toString().trim()));
//                    j++;
//                    row = sheet.getRow(j);
//                }
//
//                j = 13;
//                List<Column> lstColumn = new ArrayList<Column>();
//                Column column = null;
//                row = sheet.getRow(j);
//                while (row != null && row.getCell(1).toString().trim().length() != 0) {
//                    column = new Column();
//                    column.setCode(row.getCell(1).toString().trim());
//                    column.setName(row.getCell(2).toString().trim());
//                    column.setType(row.getCell(3).toString().trim());
//                    lstColumn.add(column);
//                    j++;
//                    row = sheet.getRow(j);
//                }
//                if (orderLst.contains(tbName) || logLst.contains(tbName) || publicLst.contains(tbName)) {
//                    tableMap.put(tbName, lstColumn);
//                }
//            }
//
//        } catch (FileNotFoundException ex) {
//            ex.printStackTrace();
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        return tableMap;
//    }
//
//    static Map<String, String> tbDbMap = new HashMap<String, String>();
//
//    private static void tbDbConf(String tbName, String dbNo) {
//        tbDbMap.put(tbName, dbNo);
//    }
//
//    private static boolean isSingleTable(String tbName) {
//        return singleLst.contains(tbName);
//    }
//
//    private static boolean notEmptyCell(Cell cell) {
//        return cell != null && !cell.getStringCellValue().trim().equals("");
//    }
//
//    private static String toJavaClassName(String tableName) {
//
//        String str = tableName.substring(tableName.indexOf("_")).toLowerCase();
//
//        while (str.contains("_")) {
//            int i = str.indexOf("_");
//            if (i + 1 < str.length()) {
//                char c = str.charAt(i + 1);
//                String temp = (c + "").toUpperCase();
//                str = str.replace("_" + c, temp);
//            }
//        }
//        str = str.replace("Snapshot", "Snap");
//        str = str.replace("ReservedVoucher", "Rv");
//        str = str.replace("DeliveryNote", "Dn");
//        str = str.replace("Document", "Doc");
//        return str + "Entity";
//    }
//
//    private static String toJavaType(String dbType) {
//        String fieldType = "";
//        if (dbType.toUpperCase().startsWith("VARCHAR")) {
//            fieldType = "String";
//        } else if (dbType.toUpperCase().startsWith("TIMESTAMP")) {
//            fieldType = "Date";
//        } else if (dbType.toUpperCase().startsWith("DECIMAL")) {
//            fieldType = "BigDecimal";
//        } else if (dbType.toUpperCase().startsWith("INT")) {
//            fieldType = "long";
//        } else {
//            fieldType = "String";
//        }
//        return fieldType;
//    }
//
//    private static String toCamelName(String code) {
//        String str = code.toLowerCase();
//        while (str.contains("_")) {
//            int i = str.indexOf("_");
//            if (i + 1 < str.length()) {
//                char c = str.charAt(i + 1);
//                String temp = (c + "").toUpperCase();
//                str = str.replace("_" + c, temp);
//            }
//        }
//        return str;
//    }
//
//    static class Column {
//        String code;
//        String name;
//        String type;
//
//        public String getCode() {
//            return code;
//        }
//
//        public void setCode(String code) {
//            this.code = code;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        @Override
//        public String toString() {
//
//            String columnStr = "";
//
//            if (code.equals("CREATE_TIME")) {
//                columnStr = "CREATE_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP" + ",";
//            } else if (code.equals("UPDATE_TIME")) {
//                columnStr = code.toUpperCase() + " TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00'" + ",";
//            } else if (type.equals("TIMESTAMP")) {
//                columnStr = code.toUpperCase() + " TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00'" + ",";
//            } else {
//                columnStr = code.toUpperCase() + " " + type.toUpperCase() + ",";
//            }
//
//            return columnStr;
//        }
//
//        public String toField() {
//            StringBuffer sBuffer = new StringBuffer();
//
//            sBuffer.append("private ").append(toJavaType(type)).append(" ").append(toCamelName(code)).append(";");
//            return sBuffer.toString();
//        }
//
//        public String toGetter() {
//            StringBuffer sBuffer = new StringBuffer();
//            if (code.toUpperCase().equals("ID")) {
//                if ("long".equals(toJavaType(type))) {
//                    sBuffer.append("@Id@GeneratedValue(strategy = GenerationType.AUTO)@Column(name = \"ID\")").append(
//                            "\r\n");
//                } else {
//                    sBuffer.append("@Id@GeneratedValue(strategy = GenerationType.TABLE)@Column(name = \"ID\")").append(
//                            "\r\n");
//                }
//                sBuffer.append("public " + toJavaType(type) + " getId() {").append("\r\n");
//                sBuffer.append("return id;}").append("\r\n");
//            } else {
//                sBuffer.append("@Column(name = \"" + code + "\")").append("\r\n");
//                sBuffer.append("public " + toJavaType(type) + " get" + toCamelName("_" + code) + "() {").append("\r\n");
//                sBuffer.append("return " + toCamelName(code) + ";}").append("\r\n");
//            }
//
//            return sBuffer.toString();
//        }
//
//        public String toSetter() {
//            StringBuffer sBuffer = new StringBuffer();
//            sBuffer.append(
//                    "public void set" + toCamelName("_" + code) + "(" + toJavaType(type) + " " + toCamelName(code)
//                            + ") {").append("\r\n");
//            sBuffer.append("this." + toCamelName(code) + "=" + toCamelName(code) + ";}").append("\r\n");
//            return sBuffer.toString();
//        }
//
//    }
//
//    static class Index {
//        // Primary Key/Foreign Key/Index/Unique Index
//        String type;
//        String columns;
//        String name;
//
//        public Index(String name, String columns, String type) {
//            super();
//            this.type = type;
//            this.columns = columns;
//            this.name = name;
//        }
//
//        public String toString(String tbName) {
//            StringBuffer sBuffer = new StringBuffer();
//            if (this.type.equals("Primary Key")) {
//            } else if (this.type.equals("Foreign Key")) {
//
//            } else if (this.type.equals("Index")) {
//                sBuffer.append("CREATE INDEX " + name + " ON " + tbName + "(" + buildLst(columns)
//                        + ") ALLOW REVERSE SCANS;");
//            } else if (this.type.equals("Unique Index")) {
//                sBuffer.append("CREATE UNIQUE INDEX " + name + " ON " + tbName + "(" + buildLst(columns)
//                        + ") ALLOW REVERSE SCANS;");
//            }
//            return sBuffer.toString();
//        }
//
//        private String buildLst(String columns) {
//            return columns.replace("+", ",");
//        }
//
//    }
//
//    static interface TableLocator {
//        int locateByNo(int tbNo);
//
//        int locateByName(String tableName);
//    }
//}
