/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: BookEntity
 * Author:   zengjian
 * Date:     2018/10/22 19:04
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package parser.javassist;

/**
 * 〈BookEntity〉<br>
 * 〈〉
 *
 * @author zengjian
 * @create 2018/10/22 19:04
 */
public class BookEntity {

    private String bookName;
    private String bookAuthor;

    public void print() {
        System.out.println("book");
    }

}