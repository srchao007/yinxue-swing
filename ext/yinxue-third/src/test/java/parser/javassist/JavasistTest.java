/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: JavasistTest
 * Author:   zengjian
 * Date:     2018/10/17 17:16
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package parser.javassist;

import javassist.*;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Method;

/**
 * 〈JavasistTest〉<br>
 * 〈〉
 *
 * @author zengjian
 * @create 2018/10/17 17:16
 */
public class JavasistTest {

    /**
     * 取现存的class文件进行修改
     *
     * @throws CannotCompileException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void test01() throws CannotCompileException, IOException, NotFoundException {
        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass = classPool.getCtClass("parser.asm.AsmReader");
        ctClass.addField(new CtField(CtClass.booleanType, "hello", ctClass));
        ctClass.toBytecode();
        Class clazz = ctClass.toClass();
        System.out.println(clazz);
        System.out.println(ctClass);
    }

    /**
     * 用来生成新的class类
     *
     * @throws CannotCompileException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    @Test
    public void newClass() throws CannotCompileException, IllegalAccessException, InstantiationException {
        ClassPool classPool = ClassPool.getDefault();
        CtClass hello = classPool.makeClass("Hello");
        System.out.println(hello);
        Class clazz = hello.toClass();
        System.out.println(clazz);
        Object obj = clazz.newInstance();
        System.out.println(obj);
    }

    @Test
    public void changeClass() throws Exception {
        // BookEntity entity = new BookEntity();
        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass = classPool.get("parser.javasist.BookEntity");
        CtMethod[] ctMethods = ctClass.getMethods();
        for (CtMethod ctMethod : ctMethods) {
            ctMethod.setBody("{return;}");
            ctMethod.insertBefore("System.out.println(\"Before123\");");
            //ctMethod.insertAfter("System.out.println(\"afterAbc\");"); 报错
        }
        Class<?> clazz = ctClass.toClass();
        Object obj = clazz.newInstance();
        Method method = clazz.getMethod("print");
        method.invoke(obj, null);
    }
}