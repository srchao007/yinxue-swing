package actor.iot;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;


public class IotSupervisor extends AbstractActor {

    private final LoggingAdapter logging = Logging.getLogger(getContext().getSystem(), this);

    public static Props props() {
        return Props.create(IotSupervisor.class, IotSupervisor::new);
    }

    @Override
    public void preStart() throws Exception {
        logging.info("IotSupervisor ready start");
    }

    @Override
    public void postStop() throws Exception {
        logging.info("IotSupervisor stopped");
    }

    // 不用处理任何内容
    @Override
    public Receive createReceive() {
        return receiveBuilder().match(String.class,s-> {
            System.out.println(s);
            System.out.println(getSender());
        }).build();
    }
}
