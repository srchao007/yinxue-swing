/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: PatternTest
 * Author:   zengjian
 * Date:     2018/8/24 14:34
 * Description: 不确定自动状态机
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package data.state;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 〈不确定自动状态机〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/24 14:34
 */
public class PatternTest {

    public static void main(String[] args) throws Exception {
        Pattern pattern = Pattern.compile("[a-z]{1,9}");
        String input = "abdeddd";

        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            System.out.println("匹配成功");
            System.out.println(matcher.group());
        } else {
            System.out.println("匹配失败");
        }

        Field field = Pattern.class.getDeclaredField("matchRoot");
        field.setAccessible(true);
        Object o = field.get(pattern);
        Class node = field.getType();

        Method method = Pattern.class.getDeclaredMethod("printObjectTree", new Class[]{node});
        method.setAccessible(true);
        method.invoke(pattern, o);
    }
}