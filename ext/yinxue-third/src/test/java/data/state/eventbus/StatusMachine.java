/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StatusMachine
 * Author:   zengjian
 * Date:     2018/8/24 17:51
 * Description: 
 * History:
 * <author>          <time>          <version>          <desc>
 */
package data.state.eventbus;

public interface StatusMachine {

    Status getStatus(Status currentStatus, Event event);

}