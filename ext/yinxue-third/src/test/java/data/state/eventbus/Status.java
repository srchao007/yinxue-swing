/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Status
 * Author:   zengjian
 * Date:     2018/8/24 17:44
 * Description: 
 * History:
 * <author>          <time>          <version>          <desc>
 *
 */
package data.state.eventbus;

/**
 * 
 *
 * @author zengjian
 * @create 2018/8/24 17:44
 */
public enum Status {

    PERMIT_SUBMIT("permitSubmit", "提交假单"),
    LEADER_PERMITING("leaderPermiting", "领导审批中"),
    LEADER_PERMIT_AGREE("leaderAgree", "领导同意"),
    LEADER_PERMIT_DISAGREE("leaderDisAgree", "领导不同意"),
    LEADER_PERMIT_MODIFY("leaderModify", "领导觉得需要补充材料重修修改"),

    HR_PERMITING("hrPermiting", "hr审批中"),
    HR_PERMIT_AGREE("hrAgree", "hr同意"),
    HR_PERMIT_DISAGREE("hrDisAgree", "hr不同意"),
    HR_PERMIT_MODIFY("hrModify", "hr觉得需要补充材料重修修改"),

    CEO_PERMITING("ceoPermiting", "领导审批中"),
    CEO_PERMIT_AGREE("ceoAgree", "ceo同意"),
    CEO_PERMIT_DISAGREE("ceoDisAgree", "ceo不同意"),
    CEO_PERMIT_MODIFY("ceoModify", "ceo觉得需要补充材料重修修改"),

    PERMIT_SUCCESS("permitSuccess", "请假成功"),
    PERMIT_FAIL("permitFail", "请假失败");

    String type;
    String desc;

    Status(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}