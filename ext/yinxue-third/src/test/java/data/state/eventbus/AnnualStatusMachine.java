/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: AnnualStatusMachine
 * Author:   zengjian
 * Date:     2018/8/24 17:51
 * Description: 
 * History:
 * <author>          <time>          <version>          <desc>
 *
 */
package data.state.eventbus;


/**
 * 
 * 
 * 
 * https://www.jianshu.com/p/8def04b34b3c
 *
 * @author zengjian
 * @create 2018/8/24 17:51
 */
public class AnnualStatusMachine implements StatusMachine {

    @Override
    public Status getStatus(Status currentStatus, Event event) {
        switch (currentStatus) {
            case PERMIT_SUBMIT:
                return Status.LEADER_PERMITING;
            case LEADER_PERMITING:
                return getLeaderPermitStatus(event);
            case LEADER_PERMIT_AGREE:
                return Status.CEO_PERMITING;
            case LEADER_PERMIT_DISAGREE:
                return Status.PERMIT_FAIL;
            case LEADER_PERMIT_MODIFY:
                return getLeaderPermitStatus(event);
            case CEO_PERMITING:
                return getCEOPermitStatus(event);
            case CEO_PERMIT_AGREE:
                return Status.PERMIT_SUCCESS;
            case CEO_PERMIT_DISAGREE:
                return Status.PERMIT_FAIL;
            case CEO_PERMIT_MODIFY:
                return getCEOPermitStatus(event);
            default:
                throw new RuntimeException("不支持当前状态及事件");
        }
    }

    private Status getCEOPermitStatus(Event event) {
        switch (event) {
            case AGREE:
                return Status.CEO_PERMIT_AGREE;
            case REFUSE:
                return Status.CEO_PERMIT_DISAGREE;
            case MODIFY:
                return Status.CEO_PERMIT_MODIFY;
            default:
                throw new RuntimeException("不支持该Event审批意见");
        }
    }

    private Status getLeaderPermitStatus(Event event) {
        switch (event) {
            case AGREE:
                return Status.CEO_PERMITING;
            case REFUSE:
                return Status.PERMIT_FAIL;
            case MODIFY:
                return Status.LEADER_PERMIT_MODIFY;
            default:
                throw new RuntimeException("不支持这个event");
        }
    }
}