/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Event
 * Author:   zengjian
 * Date:     2018/8/24 17:44
 * Description: 审批事件
 * History:
 * <author>          <time>          <version>          <desc>
 */
package data.state.eventbus;

/**
 * 
 * 
 *
 * @author zengjian
 * @create 2018/8/24 17:44
 */
public enum Event {

    AGREE("agree", ""),
    REFUSE("refuse", ""),
    MODIFY("modify", "");

    String type;
    String desc;

    Event(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

}