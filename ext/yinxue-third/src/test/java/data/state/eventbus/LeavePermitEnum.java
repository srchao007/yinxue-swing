/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: LeavePermitEnum
 * Author:   zengjian
 * Date:     2018/8/24 17:39
 * Description: 请假事由
 * History:
 * <author>          <time>          <version>          <desc>
 */
package data.state.eventbus;

/**
 * 
 *
 * @author zengjian
 * @create 2018/8/24 17:39
 */
public enum LeavePermitEnum {

    ANNUAL_LEAVE("annual_leave", "年假"),
    CASUAL_LEAVE("cacual_leave", "事假"),
    MEDICAL_LEAVE("medical_leave", "病假"),
    MARRIAGE_LEAVE("merriage_leave", "婚假");

    String type;
    String desc;

    LeavePermitEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}