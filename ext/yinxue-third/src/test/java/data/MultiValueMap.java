/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: MultiValueMap
 * Author:   zengjian
 * Date:     2018/9/16 21:54
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 
 */
package data;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 
 *
 * @author zengjian
 * @create 2018/9/16 21:54
 */
public class MultiValueMap {

    Map<String, Map<Integer, String>> map = new HashMap<>();

    public String putValue1(String key, String value1) {
        return putValue(key, value1, 1);
    }

    void initIfAbsent(String key) {
        if (map.get(key) == null) {
            map.put(key, new HashMap());
        }
    }

    String putValue(String key, String value, Integer order) {
        initIfAbsent(key);
        return map.get(key).put(order, value);
    }

    public String putValue2(String key, String value2) {
        return putValue(key, value2, 2);
    }

    public String getValue1(String key) {
        return getValue(key, 1);
    }

    public String getValue2(String key) {
        return getValue(key, 2);
    }

    String getValue(String key, Integer order) {
        if (map.get(key) != null && map.get(key).containsKey(order)) {
            return map.get(key).get(order);
        }
        return null;
    }
}