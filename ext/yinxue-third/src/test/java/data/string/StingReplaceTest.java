/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StingReplaceTest
 * Author:   zengjian
 * Date:     2018/8/17 11:23
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package data.string;

import org.junit.Test;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/17 11:23
 */
public class StingReplaceTest {

    public static void main(String[] args) {
        String tep = Temp.temp;

        long time1 = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            String str = tep.replace("$xxx", "360");
        }
        long time2 = System.currentTimeMillis();
        System.out.println(time2 - time1);
        for (int i = 0; i < 1000000; i++) {
            StringBuilder sb = new StringBuilder();
            sb.append(Temp.temp1).append("360").append(Temp.temp2);
            String str = sb.toString();
        }
        long time3 = System.currentTimeMillis();
        System.out.println(time3 - time2);
    }

    @Test
    public void trimCharacter() {
        String ss = " src/main/java/com/suning/soa/promotion/entity/promotion/ActScopePosOptRequest.java";
        System.out.println(ss.codePointAt(0));
        System.out.println(ss.indexOf(0) == '\t');
        System.out.println(ss.indexOf(0) == '\u2610');
        System.out.println(ss.indexOf(0) == ' ');
    }

}