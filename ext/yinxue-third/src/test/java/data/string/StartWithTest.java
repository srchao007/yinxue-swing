/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StartWithTest
 * Author:   zengjian
 * Date:     2018/9/14 17:03
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 */
package data.string;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/14 17:03
 */
public class StartWithTest {

    public static void main(String[] args) {
        String regex = "(public |abstract |static|class |final |interface |enum |@interface )";
        String dst = "public class OrderActivityDto extends BaseActivityTicketDto {";
        String regexModify = "public |abstract |static|class |final |interface |enum |@interface ";
        System.out.println(dst.startsWith(regex));
        System.out.println(dst.startsWith(regexModify));
        // 不是正则表达式是普通字符
    }

}