package data.system;

/**
 * @author zengjian
 * @create 2018-04-23 11:16
 * @since 1.0.0
 */
public class StringCompare {
    public static void main(String[] args) {
        String s1 = "张三";
        String s2 = "张文";
        System.out.println(s1.compareTo(s2));
    }
}
