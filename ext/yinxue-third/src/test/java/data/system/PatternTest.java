package data.system;

/**
 * 正则匹配
 *
 * @author zengjian
 * @create 2018-04-12 15:47
 * @since 1.0.0
 */
public class PatternTest {
    public static void main(String[] args) {
        String text = "public static class AccountStruct implements Serializable{";
        boolean flag = text.matches("(private|public|protected)?\\s+(static)?\\s+class[\\s\\S]*");
        System.out.println(flag);
    }
}
