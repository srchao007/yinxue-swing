package data.system;


import org.junit.Test;

import java.util.Map;
import java.util.Properties;

/**
 * jvm系统相关
 *
 * @author zengjian
 * @create 2018-04-10 20:20
 * @since 1.0.0
 */
public class SystemTest {
    public static void main(String[] args) {
        Properties properties = System.getProperties();

        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }

        //  设置在VM options中运行属性 -DconfigurePath=hello，则可以取到
        System.out.println(System.getProperty("configurePath"));
    }

    @Test
    public void testCurrentPath() {
        String currentPath = System.getProperty("user.dir");
        System.out.println(currentPath);
        System.out.println(System.getProperty("user.home"));
    }
}
