package data.system;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author zengjian
 * @create 2018-05-22 19:45
 * @since 1.0.0
 */
public class Test {

    public static void main(String[] args) {
//        String str = "abcdef";
//        System.out.println(getNoRepeatedString(str));

        String str = "abcdef";
        System.out.println(str.substring(0, str.length()));
    }

    public static String getNoRepeatedString(String text) {
        char[] chars = text.toCharArray();
        List<Character> list = new LinkedList<>();
        Map<Integer, String> results = new HashMap<>();
        // 1.采用一个list存储字段，contains判断是否为重复的，如果碰见重复的字段，截取该字段存储，
        // 2.遍历取得list，取长度最大的字符串返回
        abc:
        for (int i = 0, length = chars.length; i < length; i++) {
            for (int j = i; j < length; j++) {
                char c = chars[j];
                if (!list.contains(c)) {
                    list.add(c);
                } else {
                    String subText = text.substring(i, j);
                    list.clear();
                    results.put(subText.length(), subText);
                    break;
                }
                if (list.size() == length) {
                    results.put(text.length(), text);
                    break abc;
                }
            }
        }

        Integer a = 0;
        for (Map.Entry<Integer, String> entry : results.entrySet()) {
            a = Math.max(entry.getKey(), a);
        }
        return results.get(a);
    }
}


