package data.system;

import java.util.Map.Entry;

/**
 * @author zengjian
 * @create 2018-04-23 9:41
 * @since 1.0.0
 */
public class EntryTest {
    public static void main(String[] args) {
        Entry entry = new Entry() {
            @Override
            public Object getKey() {
                return null;
            }

            @Override
            public Object getValue() {
                return null;
            }

            @Override
            public Object setValue(Object value) {
                return null;
            }
        };
    }
}
