package data.system;

import java.util.Arrays;

/**
 * @author zengjian
 * @create 2018-04-19 10:20
 * @since 1.0.0
 */
public class ArrayTest {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 5, 6, 7, 87,};
        int[] arr2 = Arrays.copyOfRange(arr, 2, 3);
        System.out.println(arr2.length);
        System.out.println(arr.length);
    }
}
