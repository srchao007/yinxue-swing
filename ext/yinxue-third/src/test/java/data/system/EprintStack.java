package data.system;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * 打印堆栈信息
 *
 * @author zengjian
 * @create 2018-04-13 14:28
 * @since 1.0.0
 */
public class EprintStack {
    public static void main(String[] args) {
        try {
            FileInputStream fileInputStream = new FileInputStream("11");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
