/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: TryCatchOrder
 * Author:   zengjian
 * Date:     2018/10/26 9:36
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package data.system;

import org.junit.Test;

/**
 * 〈TryCatchOrder〉<br>
 * 〈〉
 *
 * @author zengjian
 * @create 2018/10/26 9:36
 */
public class TryCatchOrder {

    class State {
        Exception exception;
    }

    class Entity {
        String errorMsg;
    }

    @Test
    public void try12() {
        State state = new State();
        Entity entity = new Entity();

        Exception exception = new Exception();
        try {
            throw new RuntimeException("异常");
        } catch (RuntimeException e) {
            state.exception = e;
        } finally {
            entity.errorMsg = state.exception.getMessage();
        }
        System.out.println(entity.errorMsg);
    }

}