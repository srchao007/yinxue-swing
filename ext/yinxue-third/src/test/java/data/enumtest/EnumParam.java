/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: EnumParam
 * Author:   zengjian
 * Date:     2018/8/23 11:03
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 */
package data.enumtest;

/**
 * 
 * 
 *
 * @author zengjian
 * @create 2018/8/23 11:03
 */
public class EnumParam {

    static void HelloEnum(Enum file) {
        System.out.println(file.toString() + file.ordinal());
    }

    public static void main(String[] args) {
        HelloEnum(ABC.HELLO);
    }
}

enum ABC {

    HELLO

}