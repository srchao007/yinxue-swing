/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: OneListTest
 * Author:   zengjian
 * Date:     2018/8/17 11:17
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 
 */
package data;

/**
 * 
 * 
 *
 * @author zengjian
 * @create 2018/8/17 11:17
 */
public class OneListTest {

    public static void main(String[] args) {
        OneList list = new OneList();
        for (Object o : list) {
            System.out.println(o);
        }
    }

}