/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: HashBit
 * Author:   zengjian
 * Date:     2018/8/23 10:06
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 *
 */
package data.hash;

import java.util.HashMap;

/**
 * 
 *
 *
 * @author zengjian
 * @create 2018/8/23 10:06
 */
public class HashBit {

    public static void main(String[] args) {
        int hash = 1123444559;
        int bucket = hash & 31;
        System.out.println(bucket); // 15

        int bucket2 = hash % 31;
        System.out.println(bucket2); // 2

        // bit col.hash 运算怎么得到相对比较均匀的hash值呢，对每一个bit进行hash运算
        String str = "abcdefghhhh";
        int h = 0;
        for (int i = 0, length = str.length(); i < length; i++) {
            h = h * 31 + str.charAt(i);
        }
        System.out.println(h);
        System.out.println(h & 15);
//        System.out.println(h % 16);

        System.out.println(-1525161727 & 15);


        HashMap<String, String> map = new HashMap<>();
        map.put(str, str);
    }

}