package nio.nio;

import java.io.IOException;

/**
 * @author zengjian
 * @create 2018-05-27 10:37
 * @since 1.0.0
 */
public class NIO2Test {

    public static void main(String[] args) throws IOException, InterruptedException {
        new Thread(new NIO2Server()).start();
        Thread.sleep(1000);
        new Thread(new NIO2Client()).start();
    }


}
