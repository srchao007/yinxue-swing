///**
// * Copyright (C), 2017-2018, XXX有限公司
// * FileName: DataMonitor
// * Author:   zengjian
// * Date:     2018/9/25 9:41
// * Description: DataMonitor
// * History:
// * <author>          <time>          <version>          <desc>
// * 作者姓名           修改时间           版本号              描述
// */
//package zookeeper;
//
//import org.apache.zookeeper.AsyncCallback;
//import org.apache.zookeeper.WatchedEvent;
//import org.apache.zookeeper.Watcher;
//import org.apache.zookeeper.data.Stat;
//
///**
// * 〈DataMonitor〉<br>
// * 〈一句话描述〉
// *
// * @author zengjian
// * @create 2018/9/25 9:41
// */
//public class DataMonitor implements Watcher, AsyncCallback.StatCallback {
//
//    boolean dead;
//
//    @Override
//    public void processResult(int rc, String path, Object ctx, Stat stat) {
//
//    }
//
//    @Override
//    public void process(WatchedEvent event) {
//
//    }
//
//
//    interface DataMonitorListener {
//
//        /**
//         * 数据状态改变
//         *
//         * @param bytes
//         */
//        void exsit(byte[] datas);
//
//        /**
//         * 会话无效
//         *
//         * @param rc
//         */
//        void close(int rc);
//
//    }
//}