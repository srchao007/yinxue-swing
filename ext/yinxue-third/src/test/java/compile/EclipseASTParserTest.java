// package compile;

// import org.eclipse.jdt.core.dom.AST;
// import org.eclipse.jdt.core.dom.ASTParser;
// import org.eclipse.jdt.core.dom.CompilationUnit;
// import org.junit.Test;

// /**
//  * EclipseASTParserTest <br>
//  *
//  * @author zengjian
//  * @create 2018/11/20 13:08
//  * @since 1.0.0
//  */
// public class EclipseASTParserTest {

//     String source = "package fun.codedesign.ast.dom;\n" +
//             "\n" +
//             "public class PrintVisitor implements Visitor {\n" +
//             "    @Override\n" +
//             "    public boolean visit(ExpressionNode node) {\n" +
//             "        System.out.println(\"visit expressionNode\");\n" +
//             "        return false;\n" +
//             "    }\n" +
//             "\n" +
//             "    @Override\n" +
//             "    public boolean visit(StatementNode node) {\n" +
//             "        System.out.println(\"visit statementNode\");\n" +
//             "        return false;\n" +
//             "    }\n" +
//             "}\n";

//     @Test
//     public void parseSourceCode() {
//         ASTParser astParser = ASTParser.newParser(AST.JLS10);
//         // 生成编译单元
//         astParser.setKind(ASTParser.K_COMPILATION_UNIT);
//         // TODO
//         astParser.setSource(source.toCharArray());
//         astParser.setResolveBindings(true);
//         CompilationUnit compilationUnit = (CompilationUnit) astParser.createAST(null);
//         System.out.println(compilationUnit.toString());
//     }
// }