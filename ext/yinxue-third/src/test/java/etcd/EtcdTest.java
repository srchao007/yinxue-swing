package etcd;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import io.etcd.jetcd.ByteSequence;
import io.etcd.jetcd.Client;
import io.etcd.jetcd.KV;
import io.etcd.jetcd.Watch;
import io.etcd.jetcd.kv.GetResponse;
import io.etcd.jetcd.watch.WatchResponse;

/**
 * https://github.com/etcd-io/jetcd
 */
public class EtcdTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // create client
        Client client = Client.builder().endpoints("http://10.5.200.36:2379").build();
        KV kvClient = client.getKVClient();
        client.getWatchClient().watch(ByteSequence.from("test_key".getBytes()), new Watch.Listener() {
            @Override
            public void onNext(WatchResponse response) {
                System.out.println("listen key:" + response);
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("listen error:" + throwable);
            }

            @Override
            public void onCompleted() {
                System.out.println("listen complete");
            }
        });
        Thread.sleep(1000);
        ByteSequence key = ByteSequence.from("test_key".getBytes());
        ByteSequence value = ByteSequence.from("test_value".getBytes());

        // put the key-value
        kvClient.put(key, value);

        // get the CompletableFuture
        CompletableFuture<GetResponse> getFuture = kvClient.get(key);

        // get the value from CompletableFuture
        GetResponse response = getFuture.get();

        // delete the key
        kvClient.delete(key);
    }
}
