/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: BeanFactoryTest
 * Author:   zengjian
 * Date:     2018/10/17 16:23
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package spring.context;

import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 〈BeanFactoryTest〉<br>
 * 〈〉
 *
 * @author zengjian
 * @create 2018/10/17 16:23
 */
public class BeanFactoryTest {

    @Test
    public void testBeanFactory() {
        BeanFactory beanFactory = new AnnotationConfigApplicationContext(BeanFactoryTest.class);
        AnnotationConfigApplicationContext annotationConfigApplicationContext = (AnnotationConfigApplicationContext) beanFactory;
        annotationConfigApplicationContext.scan(BeanFactoryTest.class.getPackage().getName());
    }

}