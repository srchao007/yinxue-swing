package spring.context;

/**
 * @author zengjian
 * @create 2018-05-29 10:59
 * @since 1.0.0
 */
public class SimpleBean {
    public void execute() {
        System.out.println("Simple Bean");
    }
}
