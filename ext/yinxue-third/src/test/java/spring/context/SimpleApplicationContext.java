package spring.context;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

/**
 * @author zengjian
 * @create 2018-05-29 10:58
 * @since 1.0.0
 */
public class SimpleApplicationContext {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(/*SpringConfig.class*/);
        context.register(SpringConfig.class);
        context.refresh();
        SimpleBean bean = (SimpleBean) context.getBean("simpleBean");

        String propsInfo = context.getEnvironment().getPropertySources().toString();
        System.out.println(propsInfo);

        Map<String, Object> map = context.getEnvironment().getSystemProperties();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }

        bean.execute();
    }
}
