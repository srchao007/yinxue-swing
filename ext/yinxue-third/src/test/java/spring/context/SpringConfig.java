package spring.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import spring.bean.SimpleNameGenerator;

/**
 * @author zengjian
 * @create 2018-05-29 10:58
 * @since 1.0.0
 */
@Configuration
//@Profile({"dev"})
@ComponentScan(basePackages = "org.yinxueframework.test", nameGenerator = SimpleNameGenerator.class)
public class SpringConfig {

    /**
     * 这里如果不注明名称，会导致key为 getInstance而不是simpleBean
     *
     * @return
     */
    @Bean("simpleBean")
    public SimpleBean getInstance() {
        return new SimpleBean();
    }

}
