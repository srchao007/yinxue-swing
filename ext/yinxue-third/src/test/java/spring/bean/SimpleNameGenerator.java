package spring.bean;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.stereotype.Component;

/**
 * @author zengjian
 * @create 2018-05-29 10:04
 * @since 1.0.0
 */
@Component
public class SimpleNameGenerator implements BeanNameGenerator {
    @Override
    public String generateBeanName(BeanDefinition definition, BeanDefinitionRegistry registry) {
        String name = definition.getBeanClassName();
        name = handle(name);
        registry.registerBeanDefinition(name, definition);
        return name;
    }

    private String handle(String name) {
        return name.substring(name.lastIndexOf(".") + 1);
    }
}
