package spring.bean;

import org.springframework.core.env.PropertySource;

/**
 * @author zengjian
 * @create 2018-05-29 14:32
 * @since 1.0.0
 */
public class SimplePropertySource extends PropertySource<String> {

    public SimplePropertySource(String name, String source) {
        super(name, source);
    }

    public SimplePropertySource(String name) {
        super(name);
    }

    @Override
    public Object getProperty(String name) {
        return this.source;
    }
}
