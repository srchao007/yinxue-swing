package spring.bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * IoC容器测试
 *
 * @author zengjian
 * @create 2018-04-10 17:12
 * @since 1.0.0
 */
public class SpringTest {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");
        applicationContext.getBean("dataSource");
        applicationContext.getAutowireCapableBeanFactory();
    }
}
