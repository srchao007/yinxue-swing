package reactor;

import org.junit.Test;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

public class MonoTest {

    @Test
    public void testBasic() {
        Mono mono = Mono.just("hello");
        Disposable disposable = mono.subscribe();
        mono.defaultIfEmpty(Mono.just("abc"));
        mono.and(Mono.just("and")).subscribe(System.out::println);
//        mono.subscribe(System.out::println);
    }

    @Test
    public void testFrom() {
        Mono.from(Mono.justOrEmpty("Hello")).subscribe(System.out::println);
    }

    @Test
    public void testSink() {
        Mono.create(sink -> {
//            sink.error(new RuntimeException());
//            sink.success();
//            sink.success("123");
            sink.onDispose(null);
            sink.onCancel(null);
        }).subscribe(System.out::println);
    }

    @Test
    public void testCall() {
        Mono.fromCallable((Callable<Object>) () -> {
            System.out.println(Thread.currentThread().getName());
            return 123;
        }).subscribe(System.out::println);
        System.out.println(Thread.currentThread().getName());
    }

    @Test
    public void testCompletionStage() {
        Mono.fromCompletionStage(CompletableFuture::new);
    }

    @Test
    public void testDo() {
        Mono.create(sink -> {
            sink.error(new RuntimeException());
            sink.success();
            sink.success("123");
        })
                .doOnSuccess(s -> System.out.println(s))
                .doOnError(error -> System.out.println(error))
                .doAfterTerminate(() -> System.out.println("terminate"))
                .doFirst(() -> System.out.println("first"))
                .doFinally(signalType -> System.out.println("signalType:" + signalType))
                .subscribe(System.out::println).dispose();
    }

    @Test
    public void testSubcription() {
        Mono.sequenceEqual(Mono.just("123"), Mono.just("123"))
                .doOnSubscribe(subscription -> subscription.cancel())
                .subscribe(System.out::println);
    }

    @Test
    public void testEqual() {
        Mono.sequenceEqual(Mono.just("123"), Mono.just("abc"), (s, s2) -> true).subscribe(System.out::println);
    }

    @Test
    public void testJustOrEmpty() {
        String abc = "123";
        Mono.justOrEmpty(abc).subscribe(System.out::println);
    }

    @Test
    public void testEx() {
        Mono.error(new RuntimeException()).subscribe();
    }

    @Test
    public void testWhen() {
        Mono.when(Mono.just("123"));
    }

    @Test
    public void test2() {

    }

}
