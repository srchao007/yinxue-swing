package reactor;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.time.Duration;
import java.util.Optional;
import java.util.function.Supplier;

public class FluxTest {

    @Test
    public void testFlux() {
        Flux.fromArray(new String[]{"123", "abc", "def", "fff"}).subscribe(System.out::println);
    }

    @Test
    public void testCreate() {
        Flux.create(fluxSink -> {
            fluxSink.next("123");
            fluxSink.next("bc");
            fluxSink.next("d");
        }).subscribe(System.out::println);
    }

    @Test
    public void testSchedule() throws IOException {
        Flux.interval(Duration.ofSeconds(5),
                Schedulers.newSingle("hello"))
                .subscribe(System.out::println);
        System.in.read();
    }

    @Test
    public void testSchedule2(){
        Flux.range(1,10).then(Mono.just("123")).subscribe(System.out::println,System.err::println,()-> System.out.println("complete"));
    }

    public static void main(String[] args) {
        Mono.just("123").subscribe(System.out::println);
        Mono.justOrEmpty("123");
        Mono.justOrEmpty(Optional.empty());
        Mono.defer(() -> Mono.just(null));
        Mono.create(monoSink -> monoSink.onRequest(value -> System.out.println(value)).success());
        Flux.create(fluxSink -> fluxSink.next("flux123").next("flux456").complete()).subscribe(System.out::println);
        String hi = Mono.just("hi").block();
        System.out.println(hi);
    }

    public void testMono() {
        Mono.just("123").then(Mono.just("456")).subscribe(System.out::println);
        Mono.justOrEmpty(null).subscribe(System.out::println);
        Mono.create(monoSink -> monoSink.success("123")).subscribe(System.out::println);
        Mono.defer((Supplier<Mono<?>>) () -> Mono.just("123")).subscribe(System.out::println);
    }

    public void testFlux2() {
        Flux.create(fluxSink -> fluxSink.next("123").next("456").next("789")).subscribe(System.out::println);
        // Flux.create(fluxSink -> fluxSink.next("sss").next("456").next("789")).subscribe(o -> Integer.parseInt((String) o));
        Flux.create(fluxSink -> fluxSink.next("123").next("456"), FluxSink.OverflowStrategy.BUFFER);
    }
}
