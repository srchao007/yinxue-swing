package system;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author zengjian
 * @create 2018-05-26 15:06
 * @since 1.0.0
 */
public class LinkedListTest {

    static class Entity {
        String name;
        String sex;

        public Entity(String name, String sex) {
            this.name = name;
            this.sex = sex;
        }

        public void build(Entity entity, String name) {
            entity.name = name;
            entity.sex = name;
        }
    }

    public static void main(String[] args) {
        List<Entity> results = new LinkedList<>();
        results.add(new Entity("张三", "女"));
        results.add(new Entity("李四", "男"));


        for (Entity result : results) {
            result.build(result, "123");
        }

//        for (Entity result : results) {
//            result.name = "张三";
//            System.out.println(result);
//        }

        results.add(new Entity("流星", "不男不女"));

        Map<String, Integer> countMap = new HashMap<>();
        for (Entity entity : results) {
            if (!countMap.containsKey(entity.name)) {
                countMap.put(entity.name, 1);
            } else {
                int count = countMap.get(entity.name);
                count++;
                countMap.put(entity.name, (count));
                entity.name = entity.name + count;
            }
        }

        for (Entity result : results) {


        }

    }
}
