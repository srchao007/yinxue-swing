/*
 * Copyright (C), 2018-2018, ZCMS
 * FileName: MysqlUtil.java
 * Author:   DINGYONG
 * Date:     2018年4月24日 下午1:32:36
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package util.dingyong;

/**
 * mysql相关工具类 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author 阿丁
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public final class MysqlUtil {

    /**
     * 将mysql数据库列类型转换成java字段类型 功能描述:
     *
     * @param columnType
     * @return
     */
    protected static final String convertType(String columnType) {

        if ("VARCHAR".equalsIgnoreCase(columnType) || "CHAR".equalsIgnoreCase(columnType)
                || "ntextr".equalsIgnoreCase(columnType) || "TEXT".equalsIgnoreCase(columnType)
                || "nvarchar".equalsIgnoreCase(columnType)) {
            columnType = "String";
        } else if ("INTEGER".equalsIgnoreCase(columnType)) {
            columnType = "Long";
        } else if ("INT".equalsIgnoreCase(columnType) || "TINYINT".equalsIgnoreCase(columnType)
                || "SMALLINT".equalsIgnoreCase(columnType) || "MEDIUMINT".equalsIgnoreCase(columnType)
                || "BOOLEAN".equalsIgnoreCase(columnType)) {
            // 这边对于boolen类型，在mysql数据库中，个人认为用int类型代替较好，对bit操作不是很方便，尤其是在具有web页面开发的项目中，表示0/1，对应java类型的Integer较好
            columnType = "Integer";
        } else if ("BIT".equalsIgnoreCase(columnType)) {
            columnType = "boolean";
        } else if ("BIGINT".equalsIgnoreCase(columnType)) {
            columnType = "BigInteger";
        } else if ("FLOAT".equalsIgnoreCase(columnType)) {
            columnType = "Float";
        } else if ("DOUBLE".equalsIgnoreCase(columnType)) {
            columnType = "Double";
        } else if ("DECIMAL".equalsIgnoreCase(columnType)) {
            columnType = "BigDecimal";
        } else if ("DATE".equalsIgnoreCase(columnType) || "YEAR".equalsIgnoreCase(columnType)) {
            columnType = "Date";
        } else if ("TIME".equalsIgnoreCase(columnType)) {
            columnType = "Time";
        } else if ("DATETIME".equalsIgnoreCase(columnType) || "TIMESTAMP".equalsIgnoreCase(columnType)) {
            columnType = "Timestamp";
        } else if ("BLOB".equalsIgnoreCase(columnType)) {
            // 对于boolen类型，在mysql数据库中，个人认为用int类型代替较好，对bit操作不是很方便，尤其是在具有web页面开发的项目中，表示0/1，对应java类型的Integer较好。
            columnType = "byte[]";
        } else {
            //TODO 拋列类型错误异常信息
        }
        return columnType;
    }
}
