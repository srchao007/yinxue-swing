/*
 * Copyright (C), 2018-2018, ZCMS
 * FileName: TableEntity.java
 * Author:   DINGYONG
 * Date:     2018年4月24日 下午2:08:15
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package util.dingyong;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author 阿丁
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class TableEntity {

    // 表名
    private String tableName;
    // 表列
    private List<ColumnEntity> tableColumn = new ArrayList<>();

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<ColumnEntity> getTableColumn() {
        return tableColumn;
    }

    public void setTableColumn(List<ColumnEntity> tableColumn) {
        this.tableColumn = tableColumn;
    }

}
