/*
 * Copyright (C), 2018-2018, ZCMS
 * FileName: ColumnEntity.java
 * Author:   DINGYONG
 * Date:     2018年4月24日 下午2:09:12
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package util.dingyong;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author 阿丁
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ColumnEntity {

    private String columnName;//列名
    private String columnType;//列类型
    private String columnSize;//列大小
    private boolean isNull;//是否为空
    private boolean isAuto;//是否自增
    private String columnMemo;//列注释

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getColumnSize() {
        return columnSize;
    }

    public void setColumnSize(String columnSize) {
        this.columnSize = columnSize;
    }

    public boolean isNull() {
        return isNull;
    }

    public void setNull(boolean isNull) {
        this.isNull = isNull;
    }

    public boolean isAuto() {
        return isAuto;
    }

    public void setAuto(boolean isAuto) {
        this.isAuto = isAuto;
    }

    public String getColumnMemo() {
        return columnMemo;
    }

    public void setColumnMemo(String columnMemo) {
        this.columnMemo = columnMemo;
    }


}
