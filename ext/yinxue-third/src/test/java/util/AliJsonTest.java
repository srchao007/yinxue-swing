package util;


import com.alibaba.fastjson.JSONArray;

/**
 * alibabajson工具分析
 *
 * @author zengjian
 * @create 2018-04-12 20:06
 * @since 1.0.0
 */
public class AliJsonTest {
    public static void main(String[] args) {
        Object obj = JSONArray.parse("{'name':123,'num':321}");
        System.out.println(obj);

        Object obj2 = JSONArray.parseObject("{'name':123,'num':321}", Name.class);
        System.out.println(obj2);

        Object obj3 = JSONArray.parseArray("['name':123,'num':321]");
        System.out.println(obj3);

        String context = JSONArray.toJSONString(new Name());
        System.out.println(context);
    }

    private static class Name {
        private String name;
        private String num;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }
    }


}
