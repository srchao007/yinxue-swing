package util;

import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * ThreadTest {@link util} <br>
 *
 * @author zengjian
 * @date 2019/7/26 16:15
 * @since 1.0.0
 */
public class ThreadTest {

    @Test
    public void testThread() throws IOException {
        new Thread(()->{
            while (true){
                System.out.println("now time:"+new Date());
                try {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(10));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        System.in.read();
    }

    @Test
    public void testScheduled() throws IOException {
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(()->{
            System.out.println("now time:"+new Date());
        }, 10, 5, TimeUnit.SECONDS);
        System.in.read();
    }
}