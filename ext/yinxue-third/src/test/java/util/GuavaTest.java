package util;

import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.junit.Test;

/**
 * @author zengjian
 * @create 2018-04-23 11:33
 * @since 1.0.0
 */
public class GuavaTest {
    public static void main(String[] args) {
        Lists.newArrayList();
    }

    public void newCollection() {

    }

    @Test
    public void testBoolean() {
        boolean abc = false;
        boolean fff = true;
        abc &= fff;
        System.out.println(abc);
    }

    @Test
    public void event(){
        EventBus eventBus = new EventBus();
        eventBus.register(new PrintListener());
        eventBus.post(new PrintEvent());
    }

    public class PrintListener {
        @Subscribe
        public void handle(PrintEvent event){
            System.out.println(event.getMessage());
        }
    }

    public class PrintEvent{
        String message = "text";

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

