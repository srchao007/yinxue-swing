package third.spider;

/**
 * 爬虫接口 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 15:41
 * @since 1.0.0
 */
public interface Spider {


    interface Rule {

        /**
         * 目标对象是否符合规则
         *
         * @param target
         * @return
         */
        boolean match(Object target);
    }

}
