/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: DownLoaderException
 * Author:   zengjian
 * Date:     2018/7/27 17:10
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package third.spider;

/**
 * 〈下载页面异常〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/27 17:10
 */
public class DownLoaderException extends Exception {

    public DownLoaderException() {
    }

    public DownLoaderException(String message) {
        super(message);
    }

    public DownLoaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public DownLoaderException(Throwable cause) {
        super(cause);
    }

    public DownLoaderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}