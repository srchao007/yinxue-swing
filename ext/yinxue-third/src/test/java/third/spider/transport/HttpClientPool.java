/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: HttpClientPool
 * Author:   zengjian
 * Date:     2018/7/25 10:19
 * Description: 池化客户端
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package third.spider.transport;

import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.http.impl.client.CloseableHttpClient;

/**
 * 〈池化客户端〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/25 10:19
 */
public class HttpClientPool extends GenericObjectPool<CloseableHttpClient> {

    public HttpClientPool(PooledObjectFactory<CloseableHttpClient> factory) {
        super(factory);
    }
}