/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: Downloader
 * Author:   zengjian
 * Date:     2018/10/8 10:11
 * Description: 页面下载器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package third.spider;

/**
 * 〈页面下载器〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 10:11
 */
public interface Downloader {

    enum Mode {
        /**
         * 直连url地址
         */
        DIRECT,

        /**
         * 采用代理访问url地址
         */
        PROXY;
    }

}