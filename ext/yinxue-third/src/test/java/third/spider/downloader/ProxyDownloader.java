/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: ProxyDownloader
 * Author:   zengjian
 * Date:     2018/10/8 10:12
 * Description: 代理下载器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package third.spider.downloader;


import third.spider.Downloader;

/**
 * 〈代理下载器〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 10:12
 */
public class ProxyDownloader extends SimpleDownloader implements Downloader {

    public ProxyDownloader() {
        super(DEFAULT_HOST, DEFAULT_PORT);
    }
}