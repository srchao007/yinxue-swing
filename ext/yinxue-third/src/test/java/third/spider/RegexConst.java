package third.spider;

/**
 * 正则表达式常量 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-10 16:27
 * @since 1.0.0
 */
public final class RegexConst {

    /**
     *  http地址
     * 'https://xxxx.abc123.xx/u123'
     * "https://xxxx.xx123.xx/u123"
     * 'http://xxxx.abc123.xx/u123'
     * "http://xxxx.abc123.xx/#u123_?hello=1234"
     */
    public static final String HTTP_URL_REGEX = "[\\\"\\']{1}http[s]?://[a-zA-Z#0-9\\\\./\\?=-_]+[\\\"\\']{1}";

    /**
     * a标签Href中的值，包含了绝对路径和相对路径
     */
    public static final String HREF_URL_REGEX = "href=[\"\\']{1}[:a-zA-Z#0-9\\\\./\\?=-_]+[\"\\']{1}";

    /**
     * a标签
     * <a href=//www.baidu.com/more/ name=tj_briicon class=bri style="display: block;">更多产品</a>
     * <a href=["']*[/\.a-zA-Z0-9 \s_=":;'\{\}]*>[^(</a>)]*</a>
     * 关于剔除某个单词或者某段字符的方法，需要按如下方式
     * (!?pattern) 表示不包含该pattern (!=pattern)表示包含该pattern
     * 参考 https://www.jb51.net/article/52491.htm
     *
     */
    public static final String A_TAG_REGEX = "<a href=([\\s\\S](?!</a>))*>([\\s\\S](?!</a>))*</a>";

    /**
     * a标签文本
     * >更多产品<
     * >激光雷达与摄像头，未来哪种会成为自动驾驶的核心传感器呢？<
     */
    public static final String A_TAG_TEXT_REGEX = ">([\\s\\S](</a>))*<";

    private RegexConst() {
    }
}
