package third.redis;

import redis.clients.jedis.Jedis;

/**
 * redis 回调
 * redis命令行，根据执行的命令不同返回不同的对象T
 * @author zengjian
 * @create 2018-05-17 17:39
 * @since 1.0.0
 */
public interface RedisCallback<T> {
    /**
     *  通过jedis执行redis指令
     * @param jedis
     * @return T 指定对象
     */
    T doAction(Jedis jedis);
}
