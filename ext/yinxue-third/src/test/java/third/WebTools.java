package third;

import javax.servlet.http.HttpServletRequest;

/**
 * web请求处理 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-06-19 21:15
 * @since 1.0.0
 */
public abstract class WebTools {

    /**
     * 返回去除contextPath之后的路径
     *
     * @param request
     * @return
     */
    public static String getTrimRequestUri(HttpServletRequest request) {
        return request.getRequestURI().replace(request.getContextPath(), "");
    }


    /**
     * http://localhost:8080/application/ 前缀<br>
     *
     * @param request
     * @return
     */
    public static String getBasePath(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getRemoteHost() + ":" + request.getServerPort() + request.getContextPath();
    }

}
