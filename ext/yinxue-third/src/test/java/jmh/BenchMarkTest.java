/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: BenchMarkTest
 * Author:   zengjian
 * Date:     2018/9/23 9:34
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package jmh;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/23 9:34
 */
public class BenchMarkTest {

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
                .include(TargetMethod.class.getSimpleName())
                .warmupIterations(5)
                .measurementIterations(5)
                .forks(1)
                .build();
        new Runner(options).run();
    }
}