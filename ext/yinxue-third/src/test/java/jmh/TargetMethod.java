/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: TargetMethod
 * Author:   zengjian
 * Date:     2018/9/23 9:36
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package jmh;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/23 9:36
 */
public class TargetMethod {

    @Benchmark
    @BenchmarkMode(Mode.All)
    public void print() {
//        for (int i = 0; i < 100000; i++) {
//            //System.out.println(i);
//        }
    }


}