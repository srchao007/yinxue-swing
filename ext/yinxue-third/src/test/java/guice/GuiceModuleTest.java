package guice;


import com.google.inject.*;


/**
 * https://github.com/google/guice/wiki/GettingStarted
 * public final class MyWebServer {
 * public void start() {
 * ...
 * }
 * <p>
 * public static void main(String[] args) {
 * // Creates an injector that has all the necessary dependencies needed to
 * // build a functional server.
 * Injector injector = Guice.createInjector(
 * new RequestLoggingModule(),
 * new RequestHandlerModule(),
 * new AuthenticationModule(),
 * new DatabaseModule(),
 * ...);
 * // Bootstrap the application by creating an instance of the server then
 * // start the server to handle incoming requests.
 * injector.getInstance(MyWebServer.class)
 * .start();
 * }
 * }
 * https://github.com/google/guice/wiki/MISSING_CONSTRUCTOR 构造器问题 @Inject 或者 @Proviers
 */
public class GuiceModuleTest {
    public static void main(String[] args) {
        // 两个module之间是否可以相互注入？ module之间注解的实例是可以相互注入的
        Injector injector = Guice.createInjector(new ModuleA(), new ModuleB());
        Class2A two = injector.getInstance(Class2A.class);
        System.out.println(two);
    }
}

class Class1A {

}

class ModuleA extends AbstractModule {

    @Provides
    @Singleton
    public Class1A module1ClassA() {
        return new Class1A();
    }
}

class Class2A {

    final Class1A class1A;

    public Class2A(Class1A class1A) {
        this.class1A = class1A;
    }
}

class ModuleB extends AbstractModule {

    @Provides
    @Singleton
    public Class2A class2A(Class1A class1A) {
        return new Class2A(class1A);
    }
}
