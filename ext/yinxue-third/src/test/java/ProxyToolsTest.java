
import org.junit.Test;

import third.ProxyUtil;

public class ProxyToolsTest {

    public interface Hello{
        void hello();
    }

    /**
     * 加 static 避免和这个类关联在一起
     * 成员内部类new 时会出现 java.collection.IllegalArgumentException: Superclass has no null constructors but no arguments were given的报错 <》
     */
    public static class WorldImpl {
        public void world(){
            System.out.println("----------world no interface----");
        }
    }


    public class HelloImpl implements Hello {
        @Override
        public void hello() {
            System.out.println("hello word");
        }
    }

    class Invoker implements ProxyUtil.Interceptor {

        @Override
        public Object preHandle() {
            printCurrentTime();
            return null;
        }

        private void printCurrentTime() {
            System.out.println(System.currentTimeMillis());
        }

        @Override
        public Object handleResult(Object result) {
            printCurrentTime();
            return null;
        }

        @Override
        public Object postHandle() {
            printCurrentTime();
            return null;
        }

    }

//    @EchoTest
//    public void buildProxy() {
//        HelloImpl hello  = new HelloImpl();
//        Hello hello1 = (Hello) ProxyTools.doCreateProxyWithInterceptor(hello, new Invoker());
//        hello1.hello();
//    }
//
//    @EchoTest
//    public void buildProxy2() {
//        WorldImpl WorldImpl  = new WorldImpl();
//        WorldImpl WorldImpl2 = (WorldImpl) ProxyTools.doCreateProxyWithInterceptor(WorldImpl, new Invoker());
//        WorldImpl2.world();
//    }



    static class GetHello {
        String getHello(){
            return "hello";
        }

        String getWorld(){
            return "world";
        }
    }


    interface IHello {
        String sayHello(String str);
    }

    @Test
    public void buildInterfaceProxy(){
        IHello hello = (IHello) ProxyUtil.createProxyByCGLIB(IHello.class);
        String str = hello.sayHello("hello");
        System.out.println(str);
    }
}