

import java.sql.*;

/**
 * @author zengjian
 * @create 2018-06-08 11:52
 * @since 1.0.0
 */
public class TryResource {
    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/yinxue?serverTimezone=UTC&user=root&password=1234";
        String sql = "SELECT * FROM user";

        try (Connection connection = DriverManager.getConnection(url);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql);
        ) {
            resultSet.next();
            System.out.println(resultSet.getString(1));
        }
    }
}
