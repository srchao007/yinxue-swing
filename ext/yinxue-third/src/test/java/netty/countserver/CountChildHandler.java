/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: CountChildHandler
 * Author:   zengjian
 * Date:     2018/9/20 21:02
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package netty.countserver;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/20 21:02
 */
public class CountChildHandler extends ChannelInitializer<SocketChannel> {

    private AtomicBoolean startRecorded = new AtomicBoolean(false);
    private long startime;
    private long endtime;
    private long sCheduleEndTime;
    private AtomicLong count = new AtomicLong(0);

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new DelimiterBasedFrameDecoder(1 << 7, Unpooled.wrappedBuffer(("\n").getBytes())));
        ch.pipeline().addLast(new StringDecoder());
        ch.pipeline().addLast(new StringEncoder());
        ch.pipeline().addLast(new SimpleChannelInboundHandler() {
            @Override
            protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
                if (startRecorded.compareAndSet(false, true)) {
                    startime = System.currentTimeMillis();
                    sCheduleEndTime = startime + 30000;
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            System.out.println(endtime - startime + "ms:" + count.get() + "请求次数");
                        }
                    }, new Date(sCheduleEndTime));

                }
                System.out.println(Thread.currentThread().getName() + "CountServer接收到请求:\n" + msg);
//                final ChannelFuture future1 = ctx.writeAndFlush("服务器返回消息" + new Date().toString());
//                future1.addListener(new GenericFutureListener<io.netty.core.concurrent.Future<? super Void>>() {
//                    @Override
//                    public void operationComplete(io.netty.core.concurrent.Future<? super Void> future) throws Exception {
//                        if (future.isSuccess()) {
//                            System.out.println("消息返回成功");
//                        }
//                    }
//                });

                count.incrementAndGet();
                endtime = System.currentTimeMillis();
            }
        });
    }
}