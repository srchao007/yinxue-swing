/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: BioClient
 * Author:   zengjian
 * Date:     2018/9/13 9:29
 * Description: 请求客户端
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package netty.benchmark;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import netty.countserver.CountServer;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 〈请求客户端〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/13 9:29
 */
public class BenchMarkClient {

    private final Bootstrap bootstrap = new Bootstrap();
    private final EventLoopGroup child = new NioEventLoopGroup();
    private volatile AtomicBoolean started = new AtomicBoolean(false);
    private volatile AtomicLong count = new AtomicLong(0);

    private ChannelFuture channelFuture;
    private final ChannelHandler channelHandler = new BenchMarkClientHandler();

    public BenchMarkClient(String host, int port) {
        try {
            channelFuture = bootstrap.group(child)
                    .channel(NioSocketChannel.class)
                    .handler(channelHandler)
                    .connect(host, port).sync();
            // channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage() {
        final String req = "线程:" + Thread.currentThread().getName() + ";消息" + (count.incrementAndGet() + "\n");
        channelFuture.channel().writeAndFlush(req)
                .addListener(new GenericFutureListener<Future<? super Void>>() {
                    @Override
                    public void operationComplete(Future<? super Void> future) throws Exception {
                        if (future.isSuccess()) {
                            System.out.println("发送请求:" + req + "成功");
                        }
                    }
                });
    }

    public static void main(String[] args) throws Exception {
//        BenchMarkClient benchMarkClient = new BenchMarkClient("localhost",8080);
//        benchMarkClient.sendMessage();
        ExecutorService executorService0 = Executors.newFixedThreadPool(1);
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService0.execute(() -> {
            CountServer countServer = new CountServer();
            countServer.start("localhost", 8080);
        });


        final CyclicBarrier cyclicBarrier = new CyclicBarrier(10);
        for (int i = 0; i < 10; i++) {
            executorService.execute(new Runnable() {
                BenchMarkClient benchMarkClient = new BenchMarkClient("localhost", 8080);

                @Override
                public void run() {
                    try {
                        Thread.currentThread().sleep(3000L);
                        cyclicBarrier.await();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    for (int j = 0; j < 100000; j++) {
                        benchMarkClient.sendMessage();
                    }
                }
            });
        }
        System.in.read();
    }
}