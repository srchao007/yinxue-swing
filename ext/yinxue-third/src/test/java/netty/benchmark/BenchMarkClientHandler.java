/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: BenchMarkClientHandler
 * Author:   zengjian
 * Date:     2018/9/21 15:31
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package netty.benchmark;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;


/**
 * 〈BenchMarkClientHandler〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/21 15:31
 */
public class BenchMarkClientHandler extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new StringDecoder());
        ch.pipeline().addLast(new StringEncoder());
    }
}