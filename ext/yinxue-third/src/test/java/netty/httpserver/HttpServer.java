/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: HttpServer
 * Author:   zengjian
 * Date:     2018/9/12 8:53
 * Description: http服务器
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package netty.httpserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * 〈HttpServer〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/12 8:53
 */
public class HttpServer {

    EventLoopGroup boss = new NioEventLoopGroup();
    EventLoopGroup worker = new NioEventLoopGroup();
    Channel serverChannel;

    public HttpServer() {
    }

    public void start() {
        ServerBootstrap serverBootstrap = new ServerBootstrap().group(boss, worker)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG, 1000)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new HttpInitializerHandler());
        serverChannel = serverBootstrap.bind(8080).syncUninterruptibly().channel();
        serverChannel.closeFuture().syncUninterruptibly();
    }

    public void stop() {
        boss.shutdownGracefully();
        worker.shutdownGracefully();
    }

    public void sendMessage(String message) {
        serverChannel.writeAndFlush(message);
    }

    public static void main(String[] args) {
        new HttpServer().start();
    }

}