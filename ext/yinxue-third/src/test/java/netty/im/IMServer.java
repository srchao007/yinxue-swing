package netty.im;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import netty.telnet.TelnetServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IMServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(TelnetServer.class);

    public void start(String host, int port) throws InterruptedException {
        ChannelFuture channelFuture = new ServerBootstrap()
                .group(new NioEventLoopGroup())
                .channel(NioServerSocketChannel.class)
                .childHandler(new ServerHandler()).bind(host, port).sync();
        LOGGER.info("监听地址 host:{} port: {}", host, port);
        channelFuture.channel().closeFuture().sync();
    }

    public static void main(String[] args) throws InterruptedException {
        new IMServer().start("10.5.12.100", 8096);
    }
}
