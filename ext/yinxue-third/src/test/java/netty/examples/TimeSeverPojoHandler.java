package netty.examples;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import netty.examples.pojo.UnixTime;

/**
 * @author zengjian
 * @create 2018-06-04 17:29
 * @since 1.0.0
 */
public class TimeSeverPojoHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // 连通之后主动发送消息给到客户端，并且监听其事件
        // 此时会触发write 和 flush事件

        UnixTime time = new UnixTime();
        ChannelFuture f = ctx.writeAndFlush(time);
        System.out.println("服务端连通后，主动发送信息:" + time.toString());
        f.addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
    }
}
