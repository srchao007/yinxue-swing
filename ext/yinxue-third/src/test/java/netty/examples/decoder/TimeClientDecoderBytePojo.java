package netty.examples.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import netty.examples.pojo.UnixTime;

import java.util.List;

/**
 * @author zengjian
 * @create 2018-06-01 18:04
 * @since 1.0.0
 */
public class TimeClientDecoderBytePojo extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if (in.readableBytes() < 4) {
            return;
        }
        long value = in.readUnsignedInt();
        System.out.println("客户端接收到字节:" + value);
        UnixTime time = new UnixTime(value);
        out.add(time);
        System.out.println("客户端转换为实体类:" + time.toString());
    }
}
