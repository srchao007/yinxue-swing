package netty.examples.pojo;

import java.util.Date;

/**
 * 采用一个pojo来替代byteBuf类型的msg <br>
 *
 * @author zengjian
 * @create 2018-06-01 17:59
 * @since 1.0.0
 */
public class UnixTime {

    private final long value;

    public UnixTime() {
        this(System.currentTimeMillis() / 1000L + 2208988800L);
    }

    public UnixTime(long value) {
        this.value = value;
    }

    public long value() {
        return value;
    }

    @Override
    public String toString() {
        return new Date((value() - 2208988800L) * 1000L).toString();
    }
}
