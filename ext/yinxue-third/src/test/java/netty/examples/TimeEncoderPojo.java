package netty.examples;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import netty.examples.pojo.UnixTime;

/**
 * @author zengjian
 * @create 2018-06-04 17:32
 * @since 1.0.0
 */
public class TimeEncoderPojo extends ChannelOutboundHandlerAdapter {

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        System.out.println("服务端编码启动...");
        UnixTime time = (UnixTime) msg;
        ByteBuf byteBuf = ctx.alloc().buffer(4);
        byteBuf.writeInt((int) time.value());
        System.out.println("转换为byte发送...");
        ctx.write(byteBuf, promise);
    }
}
