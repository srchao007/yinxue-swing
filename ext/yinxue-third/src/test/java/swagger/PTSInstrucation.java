package swagger;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.*;

// 非final的都为默认值
@Getter
@RequiredArgsConstructor
public class PTSInstrucation {

    private List<Assert> assertions = Collections.singletonList(new Assert());
    private long createTime = System.currentTimeMillis();
    private String description = null;
    private boolean enableAssertion = true;
    private boolean enableExtract = false;
    private boolean enableThinkTime = false;
    private List<String> extracts = Collections.emptyList();
    private String id = UUID.randomUUID().toString().replaceAll("-","");
    private boolean ignoreStatus = false;
    private long modifyTime = createTime;
    private final String name;
    private String orgId = "5e6b6d10f43847ee8666c2f5a4a3b485";
    private String orgName = null;
    private String projectId = "6df947a872cb465eb18588300b4cb4aa";
    private final Request request;
    private String staffId= "6a494f9c7a0f4947aeda743e4a33db25";
    private int thinkTime= 0;
    private String type= "HTTP";

    @Getter
    static class Assert {
        String comparision = "GREATER_THAN_EQUAL";
        String expectValue = "-1";
        String field = "RESPONSE_BODY";
        boolean invert = true;
        String jsonPath = "$.ret";
        boolean matchRegex = false;
        String matchRule = "CONTAINS";
        boolean not = false;
        String patterns = "";
        int size = 0;
        String type = "JSON";
    }

    @Getter
    @RequiredArgsConstructor
    static class Request {
        final String body;
        String code = "";
        final String contentType;
        final List<Map<String,String>> formData;
        final List<Param> headers = new ArrayList<>();
        String host = "";
        int insertCount = 1;
        final String method;
        final List<Param> params;
        final String path;
        String port = "";
        String protocol = "http";
        String sampleId = UUID.randomUUID().toString().replaceAll("-","");
        String sqlType = "SELECT";
        String tableName ="";
    }

    @Getter
    @RequiredArgsConstructor
    static class Param {
        final String key;
        final String value;
    }
}
