package excel.easyexcel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.WriteHandler;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * 是否可以满足，读，写，追加写。
 * 项目地址: https://github.com/alibaba/easyexcel
 */
public class EasyExcelTest {

    @Test
    public void read() {
        System.out.println("123");
        EasyExcel.read("E:/code/note/database/basic.xlsx", new ReadListener<Object>() {

            @Override
            public void invoke(Object data, AnalysisContext context) {
                // TODO Auto-generated method stub
                System.out.println(data);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext context) {
                // TODO Auto-generated method stub

            }

        }).sheet().doRead();
    }

    @Test
    public void write() {
        EasyExcel.write("./test4.xlsx", Data.class).needHead(false).sheet().doWrite(data());
    }

    public List<Data> data() {
        // 更换这个数据会覆盖写
        List<Data> list = new ArrayList<>();
        list.add(new Data("10000", 14, "male"));
        list.add(new Data("20000", 18, "male"));
        list.add(new Data("30000", 20, "female"));
        return list;
    }

    @lombok.Data
    @NoArgsConstructor
    @AllArgsConstructor
    class Data {

        // @ExcelProperty("名称")
        private String name;

        // @ExcelProperty("年龄")
        private Integer age;

        // @ExcelProperty("性别")
        private String sex;
    }

    /**
     * 把原来写的当作是模板，然后追加内容进去needHead设置为false，这样不会添加相应的头
     * 参考：https://blog.csdn.net/qq_44732146/article/details/126245157
     */
    @Test
    public void append() {
        EasyExcel.write("./test7.xlsx", Data.class).needHead(false)
        .withTemplate("./test3.xlsx")
        .sheet().doWrite(data());
    }

    /**
     * 数据目前看只能是全量的读写
     */
    @Test
    public void modify() {
        EasyExcel.write("./test7.xlsx", Data.class)
        .needHead(false)
        .registerWriteHandler(new CellWriteHandler() {
            // TODO
        })
        .withTemplate("./test3.xlsx")
        .sheet().doWrite(data());
    }
}
