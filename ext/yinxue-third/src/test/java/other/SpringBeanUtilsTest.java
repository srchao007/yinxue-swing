package other;

import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.FatalBeanException;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 9:34
 * @since 1.0.0
 */
public class SpringBeanUtilsTest {

    @Test(expected = FatalBeanException.class)
    public void test() {
        Person person = new Person();
        person.setAge(11);
        person.setName("zhangsan");
        person.setPosition(new Person.Position() {
            {
                setPositionName("经理");
                setSalary(20000);
            }
        });

        PersonX personX = new PersonX();
        BeanUtils.copyProperties(person, personX);
        System.out.println(person);
        System.out.println(personX);
    }
}
