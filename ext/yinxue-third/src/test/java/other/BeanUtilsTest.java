package other;

import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

/**
 * BeanUtils的拷贝情况 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 9:20
 * @since 1.0.0
 */
public class BeanUtilsTest {

    /**
     * 测试apache的BeanUtils
     */
    @Test(expected = IllegalArgumentException.class)
    public void test2() throws InvocationTargetException, IllegalAccessException {
        Person person = new Person();
        person.setAge(11);
        person.setName("zhangsan");
        person.setPosition(new Person.Position() {
            {
                setPositionName("经理");
                setSalary(20000);
            }
        });

        PersonX personX = new PersonX();
        BeanUtils.copyProperties(personX, person);
        System.out.println(person);
        System.out.println(personX);
    }

}
