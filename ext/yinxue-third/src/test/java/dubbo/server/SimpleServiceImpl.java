package dubbo.server;


import dubbo.service.SimpleService;

/**
 * @author zengjian
 * @create 2018-05-30 16:26
 * @since 1.0.0
 */
public class SimpleServiceImpl implements SimpleService {
    @Override
    public void sayHello() {
        System.out.println("simple service output");
    }
}
