package dubbo.client;

import dubbo.service.SimpleService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 客户端远程调用
 *
 * @author zengjian
 * @create 2018-06-13 9:38
 * @since 1.0.0
 */
public class Client {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-dubbo-client.xml");
        context.start();
        SimpleService simpleService = (SimpleService) context.getBean("simpleService");
        simpleService.sayHello();
    }
}
