package dubbo.service;

/**
 * @author zengjian
 * @create 2018-05-30 16:26
 * @since 1.0.0
 */
public interface SimpleService {

    void sayHello();
}
