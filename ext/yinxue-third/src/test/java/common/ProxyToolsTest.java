package common;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import third.ProxyUtil;

import org.junit.Test;

import java.lang.reflect.Method;

public class ProxyToolsTest {

    public interface IHello {
        void hello();
    }

    public class Hello implements IHello {

        @Override
        public void hello() {
            System.out.println("hello方法");
        }
    }

    @Test
    public void createProxyByJDK() {
        IHello hello = new Hello();
        IHello proxy = (IHello) ProxyUtil.createProxyByJDK(hello);
        proxy.hello();
    }

    @Test
    public void craeteProxyByCGLib(){
        IHello ihello = (ProxyToolsTest.IHello) ProxyUtil.createProxyByCGLIB(IHello.class);
        ihello.hello();
    }

    @Test
    public void craeteProxyByCGLib2(){
        IHello ihello = (ProxyToolsTest.IHello) ProxyUtil.doCreateProxyByCGLIB(IHello.class, new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
                return "123";
            }
        });
        ihello.hello();
    }
}