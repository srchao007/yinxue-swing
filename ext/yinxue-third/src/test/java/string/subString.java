package string;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zengjian
 * @create 2018-05-22 13:59
 * @since 1.0.0
 */
public class subString {

    public static void main(String[] args) {
        String str = "AAABCCDDDCB";
//        str = "AABBBABBBA";
//        System.out.println(deleteSameChar(str));
//        System.out.println(reverse(str));
//     System.out.println(halfReverse(str));
        System.out.println(str.startsWith("A-Z"));


//        System.out.println(maxLengthMatch("ilikealibabab", new String[]{"i", "like", "ali", "liba", "baba", "alibaba"}));

    }

    /**
     * 转换为数组
     * 遍历，开始计数，为2或者>2替换，记录
     * 不符合条件的继续往下遍历
     * 递归生成新的字符串
     *
     * @param text
     * @return
     */
    public static String deleteSameChar(String text) {
        char[] chars = text.toCharArray();
        char sameChar = ' ';
        StringBuilder builder = new StringBuilder(text);
        for (int i = 0, length = chars.length; i < length; i++) {
            int count = 0;
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[j] == chars[i]) {
                    count++;
                } else {
                    break;
                }
            }

            if (count >= 2) {
                sameChar = chars[i];
                // 重新组合字符串
                builder.delete(0, builder.length());
                String pre = text.substring(0, i);
                String post = text.substring(i + count + 1);
                builder.append(pre).append(post);
                return deleteSameChar(builder.toString());
            }
        }
        return builder.toString();
    }

    /**
     * 转换成StringBuilder
     *
     * @param text
     * @return
     */
    public static String reverse(String text) {
        StringBuilder builder = new StringBuilder(text);
        builder.reverse();
        return builder.toString();
    }

    public static String halfReverse(String text) {
        StringBuilder builder = new StringBuilder(text);
        for (int i = 0, length = builder.length(); i < length / 2; i++) {
            char c = builder.charAt(i);
            builder.setCharAt(i, builder.charAt(length - i - 1));
            builder.setCharAt(length - i - 1, c);
        }
        return builder.toString();
    }


    public static String maxLengthMatch(String text, String[] dic) {
        Map<String, String> dicMap = new HashMap<>();
        int length = 0;
        for (String s : dic) {
            length = Math.max(length, s.length());
            dicMap.put(s, s);
        }
        // 获得最长匹配的长度
        if (length == 0) {
            return "n/a";
        }

        StringBuilder builder = new StringBuilder();
        String sub = "";

        for (int i = 0, size = text.length(); i < size; ) {
            int count = length;
            while (count > 0) {
                try {
                    sub = text.substring(i, i + count);
                    // 此处可能存在越界异常，说明超出范围了，那把这个异常catch 把sub重新截取
                } catch (Exception e) {
                    sub = text.substring(i, length);
                }

                if (dicMap.containsKey(sub)) {
                    builder.append(sub).append(" ");
                    i = i + count >= length ? length : i + 1;
                    break;
                } else {
                    count--;
                }
            }
            if (count == 0) {
                break;  // 说明无法按字典切分，不符合，返回n/a
            }
        }
        if (builder.length() == 0) {
            return "n/a";
        }
        return builder.toString();
    }
}
