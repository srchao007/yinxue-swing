package string;

import java.io.UnsupportedEncodingException;

/**
 * @author zengjian
 * @create 2018-05-30 14:51
 * @since 1.0.0
 */
public class UnicodeString {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String str = "\u5907\u6CE8\uFF1A";

        // str = new String(str.getBytes("utf-8"),"utf-8");

        System.out.println(str);

    }

}
