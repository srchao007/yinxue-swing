package http;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServerOptions;

/**
 * vertx-core说明文档：https://vertx.io/docs/vertx-core/java/
 */
public class HttpServer {
    public static void main(String[] args) {
        Vertx vertx =  Vertx.vertx(new VertxOptions().setWorkerPoolSize(10));
        vertx.createHttpServer(new HttpServerOptions().setPort(8989))
            .requestHandler(request ->{
                request.response()
                // .closeHandler(e->System.out.println("close: close !"))
                .endHandler(e->System.out.println("response end!"))
                .end("123");
            })
            .connectionHandler(con -> {
                System.out.println("local address:"+ con.localAddress());
                System.out.println("remote address:"+ con.remoteAddress());
            })
            .exceptionHandler(throwable-> throwable.printStackTrace())
            .listen( result-> {
                if(result.succeeded()) {
                    System.out.println("listen port:" + result.result().actualPort());
                } else {
                    System.out.println("lisen failed?:" + result.failed());
                }
            });
            //.close(result-> System.out.println("close:" + result.succeeded()));
    }
}
