package eventbus;

import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileProps;
import io.vertx.core.file.FileSystem;

/**
 * https://vertx.io/docs/vertx-core/java/
 */
public class VertxCore {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        System.out.println(vertx);
        vertx = Vertx.vertx(new VertxOptions().setWorkerPoolSize(40));
//        vertx.setPeriodic(1000, new Handler<Long>() {
//            @Override
//            public void handle(Long event) {
//                // event=0
//                System.out.println("Event:" + event);
//            }
//        });
        FileSystem fs = vertx.fileSystem();
        Future<FileProps> future = fs.props("/aaa.txt");
        future.onComplete(new Handler<AsyncResult<FileProps>>() {
            @Override
            public void handle(AsyncResult<FileProps> event) {
                if (event.succeeded()) {
                    System.out.println("Success:" + event.result());
                } else if (event.failed()) {
                    System.out.println("Failed:" + event);
                } else {
                    System.out.println("Event:" + event);
                }
            }
        });
        fs.createFile("/abc.txt")
                .compose(v -> fs.writeFile("/abc.txt", Buffer.buffer()))
                .compose(v-> fs.move("/abc.txt","cba.txt"));

    }
}
