package eventbus;

import io.vertx.core.Vertx;

public class EventBus {
    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        String topic = "Hello";
        vertx.eventBus().consumer(topic, message -> {
            System.out.println("Receive Message:" + message.body());
        }).completionHandler(res -> System.out.println("Consumer register result:" + res.succeeded()));
        // 可被多个consumer消费
        vertx.eventBus().publish(topic, "Hello Vertx XXXX with publish");
        // 仅被一个consumer消费
        vertx.eventBus().send(topic, "Hello Vertx XXXX with send");
    }
}


