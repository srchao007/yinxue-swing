package eventbus;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;

/**
 * You can think of a verticle as a bit like an actor in the Actor Model.
 * 可以用多种语言编写, 可以依赖多种语言的Verticle文件
 */
public class VerticlesTest {

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new MyVerticle());
        vertx.deployVerticle(new NoBlockVerticle());
    }

    static class MyVerticle extends AbstractVerticle {

        // Called when verticle is deployed
        @Override
        public void start() throws Exception {
            System.out.println("Start");
        }

        // Optional - called when verticle is undeployed
        @Override
        public void stop() throws Exception {
            System.out.println("Stop");
        }
    }

    static class NoBlockVerticle extends AbstractVerticle {

        @Override
        public void start(Promise<Void> startPromise) throws Exception {
            HttpServer httpserver = vertx.createHttpServer().requestHandler(request -> {
                request.response().putHeader("content-type", "text/plain").end("Hello vertx! zzz");
            });
            httpserver.listen(8192, res -> {
                if (res.succeeded()) {
                    startPromise.complete();
                } else {
                    startPromise.fail(res.cause());
                }
            });
        }
    }
}
