package example;

import io.vertx.core.AbstractVerticle;

public class EventBus {

    public static class Receiver extends AbstractVerticle {
        
        public static void main(String[] args) {
            Runner.runClusteredExample(Receiver.class);
        }

        @Override
        public void start() throws Exception {

            io.vertx.core.eventbus.EventBus eb = vertx.eventBus();

            eb.consumer("ping-address", message -> {

                System.out.println("Received message: " + message.body());
                // Now send back reply
                message.reply("pong!");
            });

            System.out.println("Receiver ready!");
        }
    }

    public static class Sender extends AbstractVerticle {

        public static void main(String[] args) {
            Runner.runClusteredExample(Sender.class);
        }

        @Override
        public void start() throws Exception {
            io.vertx.core.eventbus.EventBus eb = vertx.eventBus();
            eb.consumer("ping-address", message -> {

                System.out.println("Received message: " + message.body());
                // Now send back reply
                message.reply("pong!");
            });

            System.out.println("Receiver ready!");
        }
    }
}
