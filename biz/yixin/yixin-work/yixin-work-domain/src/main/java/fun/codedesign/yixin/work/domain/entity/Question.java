package fun.codedesign.yixin.work.domain.entity;

import java.util.UUID;

// import fun.codedesign.ddd.domain.AbstractEntity;
import fun.codedesign.yixin.work.domain.event.QuestionAnsweredEvent;
import fun.codedesign.yixin.work.domain.value.ModifyInfo;

public class Question /* extends AbstractEntity<String, Question> */ {

    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 描述
     */
    private String description;

    /**
     * 修改信息
     */
    public ModifyInfo info;

    public Question() {

    }

    public Question(String title, String description, ModifyInfo info) {
        this.id = generateId();
        this.title = title;
        this.description = description;
        this.info = info;
    }

    public String title() {
        return this.title;
    }

    public String description() {
        return this.description;
    }

    public String createUserId() {
        return this.info.createUserId();
    }

    public Long createTime() {
        return this.info.createTime();
    }

    public Long updateTime() {
        return this.info.updateTime();
    }

    public String generateId() {
        return UUID.randomUUID().toString().replace("_", "");
    }

    public QuestionAnsweredEvent answer(String answerContent, String answerUserId) {
        // Todo
        return null;
    }
}
