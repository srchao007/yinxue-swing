package fun.codedesign.yixin.work.domain.entity;

import fun.codedesign.ddd.domain.IRepository;
import fun.codedesign.yixin.work.domain.value.QuestionSpec;

public interface QuestionRepository extends IRepository<String, Question, QuestionSpec> {

}
