package fun.codedesign.yixin.work.domain.event;

// import fun.codedesign.ddd.domain.AbstractEvent;
// import fun.codedesign.ddd.domain.IEventRepository;
// import fun.codedesign.ddd.domain.IEventVisitor;

public interface QuestionEventRepository/*  extends IEventRepository<AbstractEvent, IEventVisitor> */ {

   default void save(QuestionCreatedEvent event) {

   }

    default  void save(QuestionAnsweredEvent event) {

    }
}
