package fun.codedesign.yixin.work.domain.service;

import fun.codedesign.yixin.work.domain.entity.Question;
import fun.codedesign.yixin.work.domain.entity.QuestionFactory;
import fun.codedesign.yixin.work.domain.entity.QuestionRepository;
import fun.codedesign.yixin.work.domain.event.QuestionAnsweredEvent;
import fun.codedesign.yixin.work.domain.event.QuestionCreatedEvent;
import fun.codedesign.yixin.work.domain.event.QuestionEventRepository;

public class QuestionService {

    private final QuestionEventRepository questionEventRepository;
    private final QuestionRepository  questionRepository;
    private final QuestionFactory questionFactory;

    public QuestionService(QuestionEventRepository questionEventRepository, QuestionRepository questionRepository, QuestionFactory questionFactory) {
        this.questionEventRepository = questionEventRepository;
        this.questionRepository = questionRepository;
        this.questionFactory = questionFactory;
    }

    public QuestionCreatedEvent createQuestion(String title, String description, String userId) {
        Question question = questionFactory.create(title, description, userId);
        QuestionCreatedEvent event = new QuestionCreatedEvent(question);
        questionEventRepository.save(event);
        return event;
    }

    public QuestionAnsweredEvent answerQuestion(String questionId, String answer, String answerUserId) {
        Question question = questionRepository.findById(questionId);
        QuestionAnsweredEvent event = question.answer(answer, answerUserId);
        questionEventRepository.save(event);
        return event;
    }
}
