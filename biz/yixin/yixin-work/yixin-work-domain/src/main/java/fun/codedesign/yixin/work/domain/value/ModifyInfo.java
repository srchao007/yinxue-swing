package fun.codedesign.yixin.work.domain.value;

public class ModifyInfo {

    private Long createTime;
    private Long updateTime;
    private String createUserId;
    private String updateUseId;
    private Long seqNo;

    
    public ModifyInfo() {
        // 为了兼容序列化
    }

    public ModifyInfo(Long createTime, Long updateTime, String createUserId, String updateUseId, Long seqNo) {
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.createUserId = createUserId;
        this.updateUseId = updateUseId;
        this.seqNo = seqNo;
    }

    // public ModifyInfo withCreateTime(Long createTime) {
    //     this.createTime = createTime;
    //     return this;
    // }

    // public ModifyInfo withUpdateTime(Long updateTime) {
    //     this.updateTime = updateTime;
    //     return this;
    // }

    // public ModifyInfo withCreateUserId(String createUserId) {
    //     this.createUserId = createUserId;
    //     return this;
    // }

    // public ModifyInfo withUpdateUserId(String updateUserId) {
    //     this.updateUseId = updateUserId;
    //     return this;
    // }

    // public ModifyInfo withSeqNo(Long nextSeqNo) {
    //     this.seqNo = nextSeqNo;
    //     return this;
    // }

    public String createUserId() {
        return this.createUserId;
    }

    public String updateUserId() {
        return this.updateUseId;
    }

    public Long createTime() {
        return this.createTime;
    }

    public Long updateTime() {
        return this.updateTime;
    }

    public Long seqNo() {
        return this.seqNo;
    }
}
