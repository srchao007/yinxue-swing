package fun.codedesign.yixin.work.domain.event;

// import fun.codedesign.ddd.domain.AbstractEvent;
import fun.codedesign.yixin.work.domain.entity.Question;

public class QuestionCreatedEvent /* extends AbstractEvent */ {
    private Question question;
    private Long createTime;

    private String createUserId;

    public QuestionCreatedEvent() {
    }

    public QuestionCreatedEvent(Question question) {
        this.question = question;
        this.createTime = question.createTime();
        this.createUserId = question.createUserId();
    }

    public Question question() {
        return this.question;
    }

    public Long createTime() {
        return this.createTime;
    }

    public String createUserId() {
        return this.createUserId;
    }
}
