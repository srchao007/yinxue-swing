package fun.codedesign.yixin.work.domain.entity;

import fun.codedesign.yixin.work.domain.value.ModifyInfo;

public class QuestionFactory {

    public Question create(String title, String description, String userId) {
        Long createTime = System.currentTimeMillis();
        ModifyInfo info = new ModifyInfo(createTime, createTime, userId, userId, 1L);
        return new Question(title, description, info);
    }

}
