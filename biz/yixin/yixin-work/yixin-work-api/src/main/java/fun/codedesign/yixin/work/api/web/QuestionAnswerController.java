package fun.codedesign.yixin.work.api.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuestionAnswerController {

    @PostMapping("/questions/ask")
    public ResponseEntity<String> askQuestion() {
        return ResponseEntity.ok("子曰:三人行,必有我师焉");
    }

}
