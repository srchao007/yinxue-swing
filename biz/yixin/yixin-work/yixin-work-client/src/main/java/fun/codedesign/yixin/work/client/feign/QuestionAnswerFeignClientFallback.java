package fun.codedesign.yixin.work.client.feign;

import org.springframework.http.ResponseEntity;

public class QuestionAnswerFeignClientFallback implements QuestionAnswerFeignClient {


    public QuestionAnswerFeignClientFallback() {
        System.out.println("---QuestionAnswerFeignClientFallback--");
    }

    @Override
    public ResponseEntity<String> askAnswer() {
        String fallbackMessage = "QuestionAnswerFeignClient fallback";
        System.out.println(fallbackMessage);
        return ResponseEntity.ok(fallbackMessage);
    }

}
