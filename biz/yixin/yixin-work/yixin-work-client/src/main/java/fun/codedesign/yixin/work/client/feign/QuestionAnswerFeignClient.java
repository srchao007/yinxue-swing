package fun.codedesign.yixin.work.client.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * name 和 value需要至少一个，否则报错
 */
@FeignClient(name = "yixin", contextId = "askQuestion", url = "localhost:8889", fallback = QuestionAnswerFeignClientFallback.class)
public interface QuestionAnswerFeignClient {

    @GetMapping("/askQuestion")
    ResponseEntity<String> askAnswer();
}
