package fun.codedesign.yixin.work.client.feign;

import org.springframework.cloud.openfeign.FallbackFactory;

public class QuestionAnswerFeignClientFallbackFactory implements FallbackFactory<QuestionAnswerFeignClientFallback> {

    @Override
    public QuestionAnswerFeignClientFallback create(Throwable cause) {
        System.out.println("cause:" + cause);
        return new QuestionAnswerFeignClientFallback();
    }

}
