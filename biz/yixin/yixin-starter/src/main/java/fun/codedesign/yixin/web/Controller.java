package fun.codedesign.yixin.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import fun.codedesign.yixin.work.client.feign.QuestionAnswerFeignClient;

@RestController
public class Controller {

    private QuestionAnswerFeignClient questionAnswerFeignClient;

    public Controller(QuestionAnswerFeignClient questionAnswerFeignClient) {
        this.questionAnswerFeignClient = questionAnswerFeignClient;
    }


    @GetMapping("/")
    public String hello() {
        return "HelloWorld2";
    }

    @GetMapping("/fallback")
    public String helloFeignClientFallback() {
        return questionAnswerFeignClient.askAnswer().getBody();
    }
    
}
