package fun.codedesign.yixin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class YixinApplication {
    public static void main(String[] args) {
        SpringApplication.run(YixinApplication.class, args);
    }
}
