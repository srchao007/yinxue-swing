package fun.codedesign.yixin.doctor.infra.repository;

import java.util.List;

import com.alibaba.excel.EasyExcel;

import fun.codedesign.yixin.doctor.domain.entity.YiAn;
import fun.codedesign.yixin.doctor.domain.repository.YiAnRepository;

public class YiAnExcelRepository implements YiAnRepository {

    @Override
    public List<YiAn> findAll() {
        // TODO Auto-generated method stub
        List<YiAn> yians = EasyExcel.read("E:/code/note/database/yian.xlsx").head(YiAn.class).doReadAllSync();
        return yians;
    }
    
}
