package fun.codedesign.yixin.doctor.domain.entity;

import java.util.List;

public class DoctorAggregate {

    private List<Fangji> fangjis;
    private List<Guji> gujis;
    private List<Mai> mais;
    private List<Xiyao> xiyaos;
    private List<Xuewei> xueweis;
    private List<YiAn> yians;
    private List<Zheng> zhengs;
    private List<ZhiFa> zhifas;
    private List<Zhongyao> zhongyaos;

    public List<Fangji> getFangjis() {
        return fangjis;
    }
    public void setFangjis(List<Fangji> fangjis) {
        this.fangjis = fangjis;
    }
    public List<Guji> getGujis() {
        return gujis;
    }
    public void setGujis(List<Guji> gujis) {
        this.gujis = gujis;
    }
    public List<Mai> getMais() {
        return mais;
    }
    public void setMais(List<Mai> mais) {
        this.mais = mais;
    }
    public List<Xiyao> getXiyaos() {
        return xiyaos;
    }
    public void setXiyaos(List<Xiyao> xiyaos) {
        this.xiyaos = xiyaos;
    }
    public List<Xuewei> getXueweis() {
        return xueweis;
    }
    public void setXueweis(List<Xuewei> xueweis) {
        this.xueweis = xueweis;
    }
    public List<YiAn> getYians() {
        return yians;
    }
    public void setYians(List<YiAn> yians) {
        this.yians = yians;
    }
    public List<Zheng> getZhengs() {
        return zhengs;
    }
    public void setZhengs(List<Zheng> zhengs) {
        this.zhengs = zhengs;
    }
    public List<ZhiFa> getZhifas() {
        return zhifas;
    }
    public void setZhifas(List<ZhiFa> zhifas) {
        this.zhifas = zhifas;
    }
    public List<Zhongyao> getZhongyaos() {
        return zhongyaos;
    }
    public void setZhongyaos(List<Zhongyao> zhongyaos) {
        this.zhongyaos = zhongyaos;
    }
}
