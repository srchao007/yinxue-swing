package fun.codedesign.yixin.doctor.domain.repository;

import fun.codedesign.ddd.domain.BaseRepository;
import fun.codedesign.yixin.doctor.domain.entity.YiAn;

public interface YiAnRepository extends BaseRepository<String, YiAn, Object> {

}
