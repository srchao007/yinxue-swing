package fun.codedesign.yixin.doctor.domain.value;


public class Sex {

    public static final Sex MALE = new Sex("MALE","","男", "");
    public static final Sex FEMALE = new Sex("FEMALE","","女","");
    public static final Sex OTHER = new Sex("OTHER","","其他","");

    private String value;
    private String code;
    private String desc;
    private String ext;

    public Sex(String value, String code, String desc, String ext) {
        this.value = value;
        this.code = code;
        this.desc = desc;
        this.ext = ext;
    }
}
