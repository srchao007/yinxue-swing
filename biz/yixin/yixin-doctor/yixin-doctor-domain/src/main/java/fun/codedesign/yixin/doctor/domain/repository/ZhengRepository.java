package fun.codedesign.yixin.doctor.domain.repository;

import fun.codedesign.ddd.domain.BaseRepository;
import fun.codedesign.yixin.doctor.domain.entity.Zheng;

public interface ZhengRepository extends BaseRepository<String, Zheng, Object> {

}
