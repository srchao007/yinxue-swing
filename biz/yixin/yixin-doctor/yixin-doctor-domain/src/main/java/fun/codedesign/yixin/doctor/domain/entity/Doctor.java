package fun.codedesign.yixin.doctor.domain.entity;

public class Doctor {

    /**
     * 根据症状描述得到病"证"名称
     * @param desc
     * @return
     */
    String check(String desc) {
        /**
         * 1. 提取症状，主症，及次要症状
         * 2. 遍历症状 与 “证” 对比，依次匹配得到 最相似的“证”
         * 3. 人工校正 “证”，依据上述症状对比，理论及相似医案
         * 4. 根据“证”取用方剂及治法
         * 5. 开方/治疗方案
         * 6. 发出医案事件，存储跟踪
         * 难点：相似度，诊断准确性
         * 形成“证”的条件：
         * 充分条件 A1&A2： 必须满足的条件
         * N取1个条件即可 A1||A2||A3：满足一个条件即可
         * ...
         * 
         */

        return "";       
    }

    
}
