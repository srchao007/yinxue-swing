package fun.codedesign.yixin.doctor.domain.entity;


public class YiAn {

    private String id;
    private String title;
    private String content;
    private String doctor;
    private String reference;
    private String analyse;
    private String tag;
    private String source;

    
    public YiAn() {
    }

    public YiAn(String id, String title, String content, String doctor, String reference) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.doctor = doctor;
        this.reference = reference;
    }

    /**
     * 根据文本自动化解析并创建医案
     * @param text
     * @return
     */
    public static YiAn create(String text) {
        return new YiAn();
    }

    /**
     * 根据传入信息构建医案
     * @param name 姓名
     * @param age 年龄
     * @param sex 性别
     * @param desc 症状描述
     * @param result 就诊说明
     * @return
     */
    public static YiAn create(String name, Integer age, String sex, String desc, String result) {
        return new YiAn();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getAnalyse() {
        return analyse;
    }

    public void setAnalyse(String analyse) {
        this.analyse = analyse;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
