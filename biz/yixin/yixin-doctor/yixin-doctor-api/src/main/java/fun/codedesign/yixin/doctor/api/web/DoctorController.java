package fun.codedesign.yixin.doctor.api.web;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fun.codedesign.yinxue.util.result.WebResponse;
import fun.codedesign.yixin.doctor.domain.entity.DoctorAggregate;
import fun.codedesign.yixin.doctor.domain.entity.YiAn;
import fun.codedesign.yixin.doctor.domain.repository.YiAnRepository;
import fun.codedesign.yixin.doctor.infra.repository.YiAnExcelRepository;

@RestController
public class DoctorController {

    private final YiAnRepository yiAnRepository;
    
    public DoctorController() {
        this.yiAnRepository = new YiAnExcelRepository();
    }

    @GetMapping("/yixin/doctor/search")
    public WebResponse<DoctorAggregate> search(
        @RequestParam("keyword") String keyword, @RequestParam(name = "pageSize",required = false, defaultValue="10") Integer pageSize, @RequestParam(name="pageNo",required = false, defaultValue = "1") Integer pageNo) {
        DoctorAggregate aggregate = new DoctorAggregate();
        List<YiAn> yians = yiAnRepository.findAll().subList((pageNo-1) * pageSize, pageSize);
        aggregate.setYians(yians);
        return WebResponse.ok(aggregate);
    }

    @PostMapping("/doctor/doctors")
    public WebResponse addDoctor() {
        return WebResponse.ok();
    }

    @DeleteMapping("/doctor/docotrs/{doctorId}")
    public WebResponse deleteDoctor(@PathVariable String doctorId) {
        return WebResponse.ok();
    }
    
}
