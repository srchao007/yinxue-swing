package fun.codedesign.yixin.doctor.api.web;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fun.codedesign.yinxue.util.result.PageResult;
import fun.codedesign.yinxue.util.result.WebResponse;
import fun.codedesign.yixin.doctor.api.model.QueryZhengReq;
import fun.codedesign.yixin.doctor.api.model.QueryZhengResp;

@RestController
public class ZhengController {

    /**
     * 根据症状查询证的类型
     * @param QueryZhengReq
     * @return
     */
    @PostMapping("/doctor/zhengs/search")
    public WebResponse<PageResult<QueryZhengResp>> queryZheng(@RequestBody QueryZhengReq queryZhengReq) {
        return WebResponse.ok();
    }
    
}
