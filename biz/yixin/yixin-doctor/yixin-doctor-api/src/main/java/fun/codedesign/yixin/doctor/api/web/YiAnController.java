package fun.codedesign.yixin.doctor.api.web;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fun.codedesign.yinxue.util.result.WebResponse;
import fun.codedesign.yixin.doctor.domain.entity.YiAn;
import fun.codedesign.yixin.doctor.domain.repository.YiAnRepository;
import fun.codedesign.yixin.doctor.infra.repository.YiAnExcelRepository;

@RestController
public class YiAnController {

    private final YiAnRepository yiAnRepository;
    
    public YiAnController() {
        this.yiAnRepository = new YiAnExcelRepository();
    }

    @PostMapping("/doctor/yians")
    public WebResponse<String> addYiAn(@RequestBody YiAn req) {
        return WebResponse.ok();
    }

    @GetMapping("/doctor/yians")
    public WebResponse<List<YiAn>> queryYiAn(@RequestParam(required = false) Integer pageSize, @RequestParam(required = false) Integer pageNo) {
        List<YiAn> yians = yiAnRepository.findAll();
        return WebResponse.ok(yians);
    }

    @PutMapping("/doctor/yians/{id}")
    public WebResponse<String> updateYiAn(Object req) {
        return WebResponse.ok();
    }

    @DeleteMapping("/doctor/yians/{id}")
    public WebResponse<String> deleteYiAn(@PathVariable String id) {
        return WebResponse.ok();
    }
}
