package com.yinxue.spider.spring.boot.autoconfigure;

/**
 * SpiderRunner {@link com.yinxue.spider.spring.boot.autoconfigure} <br>
 *
 * @author zengjian
 * @date 2019/2/23 13:37
 * @since 1.0.0
 */
public class SpiderRunner {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}