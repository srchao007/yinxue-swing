package com.yinxue.spider.spring.boot.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * SpiderAutoConfiguration {@link com.yinxue.spider.spring.boot.autoconfigure} <br>
 *
 * @author zengjian
 * @date 2019/2/23 13:38
 * @since 1.0.0
 */
@Configuration
@ConditionalOnClass(SpiderRunner.class)
@EnableConfigurationProperties(SpiderProperties.class)
public class SpiderAutoConfiguration {


    @Bean
    @ConditionalOnMissingBean
    public SpiderRunner spiderRunner(SpiderProperties properties){
        SpiderRunner runner = new SpiderRunner();
        runner.setUrl(properties.getSeedUrl());
        return runner;
    }
}