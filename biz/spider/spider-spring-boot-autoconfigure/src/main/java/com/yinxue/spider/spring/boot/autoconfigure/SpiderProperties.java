package com.yinxue.spider.spring.boot.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * SpiderProperties {@link com.yinxue.spider.spring.boot.autoconfigure} <br>
 *
 * @author zengjian
 * @date 2019/2/23 13:29
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = "yinxue.spider")
public class SpiderProperties {

    /**
     * 种子url
     */
    private String seedUrl;


    public String getSeedUrl() {
        return seedUrl;
    }

    public void setSeedUrl(String seedUrl) {
        this.seedUrl = seedUrl;
    }


}