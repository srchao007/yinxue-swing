package org.yinxue.example.spiderx.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpiderxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpiderxApplication.class, args);
	}

}
