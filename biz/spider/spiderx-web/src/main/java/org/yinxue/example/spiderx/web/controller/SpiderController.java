package org.yinxue.example.spiderx.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yinxue.example.spiderx.application.services.SpiderAppService;
import org.yinxue.example.spiderx.domain.values.WebPage;

import javax.annotation.Resource;
import java.util.List;

/**
 * SpiderController <br>
 *
 * @author zengjian
 * @date 2019/6/13 15:14
 * @since 1.0.0
 */
@RestController
public class SpiderController {

    @Resource
    private  SpiderAppService spiderAppService;

    @GetMapping("/spider")
    public List<WebPage> test(@RequestParam(required = false) String url) {
        if (url == null || url.isEmpty()) {
            url = "https://www.csdn.net/";
        }
        return spiderAppService.spider(url);
    }
}