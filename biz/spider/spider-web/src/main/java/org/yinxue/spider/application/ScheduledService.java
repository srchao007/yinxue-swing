package org.yinxue.spider.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.yinxue.spider.core.spider.Spider;
import org.yinxue.spider.domain.entities.SpiderUrlInfo;
import org.yinxue.spider.domain.repositories.SpiderRepository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 定时任务
 *
 * @author zengjian
 * @create 2018-04-16 9:13
 * @since 1.0.0
 */
@Service
public class ScheduledService {

    private  final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Resource
    private SpiderRepository spiderRepository;

    private int count = 0;

    @Scheduled(fixedRate = 3000)
    public void printTask() {
        System.out.println("Task handle" + (++count));
    }

    @Scheduled(cron = "*/6 * * * * ?")
    public void printCurrentTime() {
        System.out.println(new Date());
    }

    /**
     * 查询出采集的url地址中标题为空的，进行连接补全地址
     */
    @Scheduled(fixedRate = 3000)
    public void spiderATag() {
        List<SpiderUrlInfo> list = spiderRepository.findByUrlTitle("");
        if (!list.isEmpty()) {
            for (SpiderUrlInfo spiderUrlInfo : list) {
                String urlTitle = new Spider().parseTitle(spiderUrlInfo.getUrl());
                spiderUrlInfo.setUrlTitle(urlTitle);
                spiderRepository.save(spiderUrlInfo);
            }
        }
        LOGGER.info("URL标题补全任务:完成{}条url标题补全" , list.size());
    }
}
