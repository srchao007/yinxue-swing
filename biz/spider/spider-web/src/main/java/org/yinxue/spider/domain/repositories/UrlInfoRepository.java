package org.yinxue.spider.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.yinxue.spider.domain.entities.UrlInfo;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 18:06
 * @since 1.0.0
 */
public interface UrlInfoRepository extends JpaRepository<UrlInfo, Long> {
}
