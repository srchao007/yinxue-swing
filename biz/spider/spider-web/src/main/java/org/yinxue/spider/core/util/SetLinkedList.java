package org.yinxue.spider.core.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * SetLinkedList {@link org.yinxue.spider.core.util}
 *
 * @author zengjian
 * @date 2019/2/14
 * @since 1.0.0
 */
public class SetLinkedList<E> extends LinkedList<E> {

    private Set<E> set = new HashSet<>();

    @Override
    public boolean add(E e) {
        if (set.contains(e)) {
            return false;
        }
        set.add(e);
        return super.add(e);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        for (E e : c) {
            add(e);
        }
        return true;
    }
}