package org.yinxue.spider.web.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.yinxue.spider.domain.entities.SpiderUrlInfo;
import org.yinxue.spider.domain.entities.UrlInfo;
import org.yinxue.spider.domain.repositories.SpiderRepository;
import org.yinxue.spider.domain.repositories.UrlInfoRepository;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 爬取的url控制层<br>
 *
 * @author zengjian
 * @create 2018-07-12 15:14
 * @since 1.0.0
 */
@Controller
@RequestMapping("/spider")
public class SpiderController {

    @Resource
    private SpiderRepository spiderRepository;

    @Resource
    private UrlInfoRepository urlInfoRepository;


    @RequestMapping("/list")
    public String list(HttpServletRequest request) {
        List<SpiderUrlInfo> list = spiderRepository.findAll();
        request.setAttribute("spideUrls", list);
        return "spider/list";
    }

    @RequestMapping("/saveToFavorite/{id}")
    public String add2Favitor(@PathVariable Long id) {
        SpiderUrlInfo spiderUrlInfo = spiderRepository.getOne(id);
        urlInfoRepository.save(convert2UrlInfo(spiderUrlInfo));
        spiderRepository.deleteById(id);
        return "redirect:/spider/list";
    }

    private UrlInfo convert2UrlInfo(SpiderUrlInfo spiderUrlInfo) {
        UrlInfo seedUrl = new UrlInfo();
        seedUrl.setUrl(spiderUrlInfo.getUrl());
        seedUrl.setTitle(spiderUrlInfo.getUrlTitle());
        Date currentTime = new Date();
        seedUrl.setCreateTime(currentTime);
        return seedUrl;
    }

    /**
     * 批量删除
     *
     * @param ids 以,分隔的id序列
     * @return
     */
    @RequestMapping("/delete")
    public String delete(@RequestParam("ids") String ids) {
        if (ids == null || ids.trim().equals("")) {
            return "redirect:/url/list";
        }
        String[] idArray = ids.split(",");
        for (String id : idArray) {
            spiderRepository.deleteById(Long.valueOf(id));
        }
        return "redirect:/spider/list";
    }

    /**
     * 批量收藏
     *
     * @param ids
     * @return
     */
    @RequestMapping("/favorite")
    public String favorite(@RequestParam("ids") String ids,HttpServletRequest request) {
        if (ids == null || ids.trim().equals("")) {
            return "redirect:/url/list";
        }
        List<UrlInfo> list = findAndconvert2UrlInfo(ids);
        if (!list.isEmpty()){
            urlInfoRepository.saveAll(list);
            return "redirect:/url/list";
        }
        request.setAttribute("msg", "收藏的url地址为空");
        return "/spider/list";
    }

    private List<UrlInfo> findAndconvert2UrlInfo(String ids) {
        String[] idArray = ids.split(",");
        List<Long> list = new ArrayList<>(idArray.length);
        for (String s : idArray) {
            list.add(Long.valueOf(s));
        }
        List<SpiderUrlInfo> results = null;
        if (!list.isEmpty()){
            results = spiderRepository.findAllById(list);
        }

        List<UrlInfo> urlList = new ArrayList<>();
        if (results!=null && !results.isEmpty()){
            for (SpiderUrlInfo result : results) {
                urlList.add(convert2UrlInfo(result));
                spiderRepository.deleteById(result.getId());
            }
        }
        return urlList;
    }
}
