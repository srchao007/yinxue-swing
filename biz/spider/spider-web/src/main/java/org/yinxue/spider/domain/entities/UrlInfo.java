package org.yinxue.spider.domain.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * url信息实体类 <br>
 * <pre>
 *     spring data jpa的引用步骤:
 *     1. 实体类加上注解
 *     2.
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-11 17:34
 * @since 1.0.0
 */
@Entity
@Data
public class UrlInfo extends BaseEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String url;
    private String title;

}
