package org.yinxue.spider.domain.entities;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * BaseEntity {@link org.yinxue.spider.domain.entities} <br>
 *
 * @author zengjian
 * @date 2019/8/3 11:57
 * @since 1.0.0
 */
@Data
@MappedSuperclass
public class BaseEntity {

    private Date createTime;
    private Date updateTime;
    private Long version;

    public void set4Create(){
        Date currentTime = new Date();
        this.setCreateTime(currentTime);
        this.setUpdateTime(currentTime);
        this.setVersion(1L);
    }

}