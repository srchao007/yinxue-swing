//package org.yinxue.spider.web.controller.admin;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//import org.yinxue.spider.core.spider.Spider;
//import org.yinxue.spider.domain.entities.UrlInfo;
//import org.yinxue.spider.domain.repositories.UrlInfoRepository;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Date;
//import java.util.List;
//
///**
// * 收藏的URL地址<br>
// * <pre>
// * </pre>
// *
// * @author zengjian
// * @create 2018-07-11 17:27
// * @since 1.0.0
// */
//@Controller
//@RequestMapping("/url")
//public class UrlController {
//
//    @Autowired
//    private UrlInfoRepository urlInfoRepository;
//
//
//    /**
//     * 按时间顺序降序排序
//     *
//     * @param request
//     * @return
//     */
//    @GetMapping("/list")
//    public String queryList(HttpServletRequest request) {
//        // 分页查询
//        Sort sort = new Sort(Sort.Direction.DESC, "createTime");
//        Pageable pageable = new PageRequest(0, 10, sort);
//        List<UrlInfo> list = urlInfoRepository.findAll(pageable).getContent();
//        request.setAttribute("urls", list);
//        return "/url/list";
//    }
//
//    @PostMapping("/update")
//    public String update(UrlInfo info) {
//        urlInfoRepository.save(info);
//        return "redirect:/url/list";
//    }
//
//    @RequestMapping("/delete/{id}")
//    public String delete(@PathVariable Long id) {
//        urlInfoRepository.deleteById(id);
//        return "redirect:/url/list";
//    }
//
//    /**
//     * 批量删除
//     *
//     * @param ids 以,分隔的id序列
//     * @return
//     */
//    @RequestMapping("/delete")
//    public String delete(@RequestParam("ids") String ids) {
//        if (ids == null || ids.trim().equals("")) {
//            return "redirect:/url/list";
//        }
//        String[] idArray = ids.split(",");
//        for (int i = 0, length = idArray.length; i < length; i++) {
//            urlInfoRepository.deleteById(Long.valueOf(idArray[i]));
//        }
//        return "redirect:/url/list";
//    }
//
//    /**
//     * 将url信息以及标题保存到相应的数据库表中
//     *
//     * @param url
//     * @return
//     */
//    @PostMapping("/save")
//    public String save(@RequestParam("url") String url) {
//        String title = new Spider().parseTitle(url);
//        UrlInfo seedUrl = new UrlInfo();
//        seedUrl.setTitle(title);
//        seedUrl.setUrl(url);
//        addBaseParam(seedUrl);
//        urlInfoRepository.save(seedUrl);
//        return "redirect:/url/list";
//    }
//
//    private void addBaseParam(UrlInfo seedUrl) {
//        Date currentTime = new Date();
//        seedUrl.setCreateTime(currentTime);
//    }
//}
