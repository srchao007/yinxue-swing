package org.yinxue.spider.domain.entities;

import lombok.Data;

import javax.persistence.*;

/**
 * 爬取的Url地址 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-12 14:55
 * @since 1.0.0
 */
@Entity
@Table(indexes = {@Index(name = "url_index", columnList = "url", unique = false)})
@Data
public class SpiderUrlInfo extends BaseEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String urlTitle;
    private String url;
    private String spiderUrl;
}
