package org.yinxue.spider.core.util;

/**
 * StringUtils {@link org.yinxue.spider.core.util}
 *
 * @author zengjian
 * @date 2019/2/17
 * @since 1.0.0
 */
public abstract class StringUtils {

    public static boolean isNotEmpty(String value){
        return !isEmpty(value);
    }

    public static boolean isEmpty(String value){
        return value == null || value.trim().isEmpty();
    }

    public static boolean isAnyEmpty(String ...values){
        for (String value : values) {
            if (isEmpty(value)){
                return true;
            }
        }
        return false;
    }

    public static boolean startsWith(String value, String ...prefixs){
        for (String prefix : prefixs) {
            if (value.startsWith(prefix)){
                return true;
            }
        }
        return false;
    }

}