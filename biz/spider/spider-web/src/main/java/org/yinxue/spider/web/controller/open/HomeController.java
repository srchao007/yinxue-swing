package org.yinxue.spider.web.controller.open;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * HomeController {@link org.yinxue.spider.web.controller}
 *
 * @author zengjian
 * @date 2019/2/12
 * @since 1.0.0
 */
@Controller
@RequestMapping
public class HomeController {

    @GetMapping
    public String home(){
        return "index";
    }

    @GetMapping("index")
    public String home2(){
        return "index2el";
    }


}