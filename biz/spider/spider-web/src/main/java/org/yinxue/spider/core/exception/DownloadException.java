package org.yinxue.spider.core.exception;

/**
 * DownloadException {@link org.yinxue.spider.core.exception}
 *
 * @author zengjian
 * @date 2019/2/14
 * @since 1.0.0
 */
public class DownloadException extends RuntimeException {

}