package org.yinxue.spider.core.model;

import java.util.Objects;

/**
 * ImgTag {@link org.yinxue.spider.core.model} <br>
 *
 * @author zengjian
 * @date 2019/2/14 12:54
 * @since 1.0.0
 */
public class ImgTag implements Comparable<ImgTag> {

    /**
     * 解析最终可访问Url
     */
    private String url;

    /**
     * 原始src属性值
     */
    private String src;
    private String srcUrl;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getSrcUrl() {
        return srcUrl;
    }

    public void setSrcUrl(String srcUrl) {
        this.srcUrl = srcUrl;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImgTag imgTag = (ImgTag) o;
        return Objects.equals(src, imgTag.src);
    }

    @Override
    public int hashCode() {
        return Objects.hash(src);
    }

    @Override
    public String toString() {
        return url + "  "+ src + "  "+ srcUrl;
    }

    @Override
    public int compareTo(ImgTag o) {
        return this.src.compareTo(o.src);
    }
}