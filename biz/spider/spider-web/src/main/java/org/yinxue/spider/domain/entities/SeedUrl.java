package org.yinxue.spider.domain.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 自定义需要采集的网站 <br>
 *
 * @author zengjian
 * @create 2018-07-12 15:27
 * @since 1.0.0
 */
@Entity
@Data
public class SeedUrl extends BaseEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String url;
    private String title;
    private Integer spiderCount;

    @Override
    public void set4Create() {
        super.set4Create();
        this.setSpiderCount(0);
    }
}
