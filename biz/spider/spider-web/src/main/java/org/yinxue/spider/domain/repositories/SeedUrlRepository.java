package org.yinxue.spider.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.yinxue.spider.domain.entities.SeedUrl;

/**
 *
 * @author zengjian
 * @create 2018-07-12 15:27
 * @since 1.0.0
 */
public interface SeedUrlRepository extends JpaRepository<SeedUrl, Long> {

}
