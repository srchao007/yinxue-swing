package org.yinxue.spider.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.yinxue.spider.domain.entities.SpiderUrlInfo;

import java.util.List;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-12 15:16
 * @since 1.0.0
 */
public interface SpiderRepository extends JpaRepository<SpiderUrlInfo, Long> {
    List<SpiderUrlInfo> findByUrlTitle(String urlTitle);
}
