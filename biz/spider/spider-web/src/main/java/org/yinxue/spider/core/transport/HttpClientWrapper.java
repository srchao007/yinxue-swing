/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: HttpClientWrapper
 * Author:   zengjian
 * Date:     2018/7/25 10:22
 * Description: 客户端包装类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.spider.core.transport;

import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.http.impl.client.CloseableHttpClient;

/**
 * 〈客户端包装类〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/25 10:22
 */
public class HttpClientWrapper extends DefaultPooledObject<CloseableHttpClient> {

    /**
     * Create a new instance that wraps the provided object so that the pool can
     * track the base.state of the pooled object.
     *
     * @param object The object to wrap
     */
    public HttpClientWrapper(CloseableHttpClient object) {
        super(object);
    }
}