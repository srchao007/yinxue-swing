package org.yinxue.spider.core.downloader;

import org.junit.Assert;
import org.junit.Test;

/**
 * DownloaderTest {@link org.yinxue.spider.core.downloader}
 *
 * @author zengjian
 * @date 2019/2/12
 * @since 1.0.0
 */
public class DownloaderTest {

    public static final Downloader DOWNLOADER = new Downloader();

    @Test
    public void downloadHtml1() {
        doDownlod("https://www.baidu.com/");
    }

    @Test
    public void downloadHtml2() {
        doDownlod("https://www.cnblogs.com/zbw911/p/6194087.html");
    }

    @Test
    public void downloadHtml3() {
        doDownlod(null);
    }

    private void doDownlod(String url) {
        String result = DOWNLOADER.downloadHtml(url);
        System.out.println(result);
        Assert.assertNotNull("网页下载失败", result);
    }
}