package org.yinxue.spider.core.util;

import org.junit.Test;

/**
 * UrlUtilsTest {@link org.yinxue.spider.core.util}
 *
 * @author zengjian
 * @date 2019/2/18
 * @since 1.0.0
 */
public class UrlUtilsTest {

    @Test
    public void parseAbsoluteUrl() {
        System.out.println(UrlUtils.parseAbsoluteUrl("images/sponsorship/IEEE_Computer_Society.png", "http://www.blockchain-ieee.org"));
    }
}