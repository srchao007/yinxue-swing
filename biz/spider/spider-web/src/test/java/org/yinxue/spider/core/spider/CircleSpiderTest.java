package org.yinxue.spider.core.spider;

import org.junit.Test;
import org.yinxue.spider.core.model.ATag;
import org.yinxue.spider.core.model.ImgTag;

import java.util.List;

/**
 * CircleSpiderTest {@link org.yinxue.spider.core.spider}
 *
 * @author zengjian
 * @date 2019/2/13
 * @since 1.0.0
 */
public class CircleSpiderTest {

    @Test
    public void start1() throws Exception {
        CircleSpider circleSpider = doStart("http://www.spring4all.com/");
        List<ATag> result = circleSpider.getAtags();
        for (ATag aTag : result) {
            System.out.println(aTag);
        }
        System.out.println(result.size()+"条");
    }

    @Test
    public void start2() throws Exception {
        CircleSpider circleSpider = doStart("https://www.csdn.net/");
        List<ATag> result = circleSpider.getAtags();
        for (ATag aTag : result) {
            System.out.println(aTag);
        }
        System.out.println(result.size()+"条");
    }

    @Test
    public void startImgSrc() throws Exception {
        CircleSpider circleSpider = doStart("https://www.csdn.net/");
        List<ImgTag> result = circleSpider.getImgTags();
        for (ImgTag imgTag : result) {
            System.out.println(imgTag);
        }
        System.out.println(result.size()+"条");
    }

    private CircleSpider doStart(final String url) throws Exception {
        final CircleSpider circleSpider = new CircleSpider();
        new Thread(new Runnable() {
            @Override
            public void run() {
                circleSpider.start(url);
            }
        }).start();
        // 3Os
        Thread.sleep(30000);
        circleSpider.stop();
        return circleSpider;
    }
}