package org.yinxue.spider.core.parser;

import org.junit.Ignore;
import org.junit.Test;
import org.yinxue.spider.core.downloader.Downloader;
import org.yinxue.spider.core.model.ATag;
import org.yinxue.spider.core.util.UrlUtils;

import java.util.List;

/**
 * ParserTest {@link org.yinxue.spider.core.parser} <br>
 *
 * @author zengjian
 * @date 2019/2/12 13:06
 * @since 1.0.0
 */
public class ParserTest {

    private Parser parser = new Parser();
    private Downloader downloader = new Downloader();

    @Test
    public void parseUrl1() {
        doParse("http://www.spring4all.com/");
    }

    @Test
    public void parseUrl2() {
        doParse("https://es.xiaoleilu.com/index.html");
    }

    @Test
    public void parseUrl3() {
        doParse("http://www.youku.com/");
    }

    @Test
    public void parseUrl4() {
        doParse("https://www.cnblogs.com/zbw911/p/6194087.html");
    }

    @Test
    public void parseUrl5() {
        doParse("http://www.spring4all.com/article/653");
    }

    @Test
    @Ignore
    public void parseUrl6(){
        doParse("http://www.ed2000.com/");
    }

    private void doParse(String url) {
        String html = downloader.downloadHtml(url);
        String baseUrl = UrlUtils.parseBaseUrl(url);
        List<ATag> aTags = parser.parseATag(html, baseUrl);
        for (ATag aTag : aTags) {
            System.out.println(aTag);
        }
        System.out.println(aTags.size()+"条");
    }
}