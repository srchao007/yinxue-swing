package org.yinxue.example.spiderx.domain.entities;


import org.yinxue.example.spiderx.domain.values.WebSiteId;

import java.util.List;

/**
 * WebSiteRepository
 *
 * @author zengjian
 * @date 2019/6/10
 * @since 1.0.0
 */
public interface WebSiteRepository {

    void save(WebSite webSite);

    WebSite findById(WebSiteId webSiteId);

    void deleteById(WebSiteId webSiteId);

    List<WebSite> findAll();

}