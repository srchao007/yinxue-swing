package org.yinxue.example.spiderx.domain.entities;


import org.yinxue.example.spiderx.domain.utils.model.ATag;
import org.yinxue.example.spiderx.domain.utils.spider.Spider;
import org.yinxue.example.spiderx.domain.values.WebPage;
import org.yinxue.example.spiderx.domain.values.WebSiteId;

import java.util.ArrayList;
import java.util.List;

/**
 * WebSite
 *
 * @author zengjian
 * @date 2019/6/10
 * @since 1.0.0
 */
public class WebSite {

    private WebSiteId webSiteId;
    private String homeUrl;
    private List<WebPage> relativePages;

    public WebSite(String homeUrl) {
        this.homeUrl = homeUrl;
        this.webSiteId = new WebSiteId(homeUrl);
    }

    public List<WebPage> spider() {
        Spider spider = new Spider();
        List<ATag> atags = spider.parseATag(homeUrl);
        relativePages = new ArrayList<>(atags.size());
        atags.forEach(atag->{
            relativePages.add(new WebPage(atag.getUrl(), spider.downloadHtml(atag.getUrl()), spider.parseTitle(atag.getUrl())));
        });
        return relativePages;
    }

    public List<WebPage> relativePages(){
        return relativePages;
    }
}