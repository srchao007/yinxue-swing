package org.yinxue.example.spiderx.domain.values;

/**
 * WebPage
 *
 * @author zengjian
 * @date 2019/6/10
 * @since 1.0.0
 */

public class WebPage {

    private String url;
    private String content;
    private String title;

    public WebPage() {
    }

    public WebPage(String url, String content, String title) {
        this.url = url;
        this.content = content;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}