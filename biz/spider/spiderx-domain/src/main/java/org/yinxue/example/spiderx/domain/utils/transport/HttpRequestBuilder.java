/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: HttpRequestBuilder
 * Author:   zengjian
 * Date:     2018/7/25 10:29
 * Description: 构建访问请求
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.example.spiderx.domain.utils.transport;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

/**
 * 〈构建访问请求〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/7/25 10:29
 */
public class HttpRequestBuilder {

    private static final int SOCKET_TIME = 30000;
    private static final int CONNECT_TIME = 3000;

    /**
     * 创建Get请求
     *
     * @param url
     * @return
     */
    public static HttpUriRequest buildHttpGet(String url) {
        HttpGet request = new HttpGet(url);
        RequestConfig config = RequestConfig.custom().build();
        request.setConfig(config);
        return request;
    }

    /**
     * 创建代理Get请求
     *
     * @param url
     * @param proxyHost 代理地址
     * @param proxyPort 代理端口
     * @return
     */
    public static HttpUriRequest buildProxyHttpGet(String url, HttpHost proxy) {
        HttpGet request = new HttpGet(url);
        request.setConfig(RequestConfig.custom()
                .setSocketTimeout(SOCKET_TIME)
                .setConnectTimeout(CONNECT_TIME)
                .setProxy(proxy)
                //.setCookieSpec(COOKIE)
                .build());
        return request;
    }
}