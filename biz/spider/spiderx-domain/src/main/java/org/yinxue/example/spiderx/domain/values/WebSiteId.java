package org.yinxue.example.spiderx.domain.values;

/**
 * WebSiteId
 *
 * @author zengjian
 * @date 2019/6/10
 * @since 1.0.0
 */
public class WebSiteId {

    private String value;

    public WebSiteId(String value) {
        this.value = value;
    }
}