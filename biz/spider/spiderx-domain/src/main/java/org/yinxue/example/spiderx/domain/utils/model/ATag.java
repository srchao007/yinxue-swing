package org.yinxue.example.spiderx.domain.utils.model;

import java.util.Objects;

/**
 * ATag {@link org.yinxue.spider.core.parser} <br>
 *
 * @author zengjian
 * @date 2019/2/12 13:21
 * @since 1.0.0
 */
public class ATag implements Comparable<ATag> {

    /**
     * 解析后的url
     */
    private String url;

    /**
     * 文本内容
     */
    private String text;

    /**
     * 原始href属性
     *
     * @param url
     */
    private String href;

    /**
     * 完整标签内容
     */
    private String outerHtml;

    /**
     * 爬取结果的源地址
     */
    private String srcUrl;

    public ATag() {
    }

    public ATag(String url) {
        this(url, null);
    }

    public ATag(String url, String text) {
        this.url = url;
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getOuterHtml() {
        return outerHtml;
    }

    public void setOuterHtml(String outerHtml) {
        this.outerHtml = outerHtml;
    }

    public String getSrcUrl() {
        return srcUrl;
    }

    public void setSrcUrl(String srcUrl) {
        this.srcUrl = srcUrl;
    }

    @Override
    public String toString() {
        return url + " " + text + " " + href;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ATag aTag = (ATag) o;
        return Objects.equals(url, aTag.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url);
    }

    @Override
    public int compareTo(ATag o) {
        return this.url.compareTo(o.url);
    }
}