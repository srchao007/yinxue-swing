package org.yinxue.example.spiderx.application.services;


import org.yinxue.example.spiderx.domain.values.WebPage;

import java.util.List;

/**
 * SpiderAppService {@link org.yinxue.example.spiderx.application.services} <br>
 *
 * @author zengjian
 * @date 2019/6/28 11:28
 * @since 1.0.0
 */
public interface SpiderAppService {

     List<WebPage> spider(String url);

}