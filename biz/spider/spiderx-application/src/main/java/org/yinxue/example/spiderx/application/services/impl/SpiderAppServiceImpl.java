package org.yinxue.example.spiderx.application.services.impl;

import org.springframework.stereotype.Service;
import org.yinxue.example.spiderx.application.services.SpiderAppService;
import org.yinxue.example.spiderx.domain.entities.WebSite;
import org.yinxue.example.spiderx.domain.values.WebPage;

import java.util.List;


/**
 * SpiderAppServiceImpl {@link org.yinxue.example.spiderx.application.services.impl} <br>
 *
 * @author zengjian
 * @date 2019/6/28 11:28
 * @since 1.0.0
 */
@Service
public class SpiderAppServiceImpl implements SpiderAppService {

    @Override
    public List<WebPage> spider(String url){
        WebSite webSite = new WebSite(url);
        webSite.spider();
        //webSiteRepository.save(webSite);
        return webSite.relativePages();
    }
}