import org.junit.Test;
import org.yinxue.framework.jdbc.datasource.YXDataSource;

import fun.codedesign.yinxue.util.PropertiesUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class YinxueYXDataSourceTest {

    @Test
    public void testGetOnly() throws SQLException, IOException {
        YXDataSource YXDataSource = new YXDataSource(PropertiesUtil.build("jdbc.properties"));
        for (int i = 0; i < 25; i++) {
            Connection connection = YXDataSource.getConnection();
            System.out.println(connection + ":" + (i + 1));
            //connection.close();
        }
        System.in.read();
    }

    @Test
    public void testGetRelease() throws InterruptedException {
        final YXDataSource YXDataSource = new YXDataSource();
        new Thread(){
            @Override
            public void run() {
                Connection connection = null;
                Set<Connection> set = new HashSet<>();
                for (int i = 0; i < 1000; i++) {
                    try {
                        connection = YXDataSource.getConnection();
                        set.add(connection);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    System.out.println(connection + ":" + (i + 1));
                }
                System.out.println(set.size());
            }
        }.start();
        Thread.currentThread().join();

    }

}