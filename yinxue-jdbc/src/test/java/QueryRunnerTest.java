import org.apache.commons.dbutils.QueryRunner;
import org.junit.Test;
import org.yinxue.framework.jdbc.datasource.YXDataSource;

import java.sql.SQLException;

/**
 * <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-07 17:07
 * @since 1.0.0
 */
public class QueryRunnerTest {

    @Test
    public void test() throws SQLException {
        QueryRunner runner = new QueryRunner(new YXDataSource());
        runner.update("update user set password = '123' where username='root'");
    }

}
