package org.yinxue.framework.jdbc;

import org.junit.Test;
import org.yinxue.framework.jdbc.annotation.TableName;
import org.yinxue.framework.jdbc.datasource.YXDataSource;

import fun.codedesign.yinxue.util.DateUtil;
import fun.codedesign.yinxue.util.PropertiesUtil;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class JdbcClientTest {

    @Test
    public void insert() {
        // RedisClient client = new RedisClient(new JedisPool());
    }

    @TableName("TABLE_NAME")
    public static class Entity {

    }

    @Test
    public void testTableName() {
        // Assert.assertEquals("jdbc_client_test", JdbcClient.getDefaultTableName(new JdbcClientTest()));
    }

    @Test
    public void testTableName2() {
        // Assert.assertEquals("TABLE_NAME", JdbcClient.doGetTableName(new Entity()));
    }

    @Test
    public void testInsert() throws Exception {
        JdbcClient client = new JdbcClient();
        client.setDataSource(new YXDataSource(PropertiesUtil.build("jdbc.properties")));
        int count = client.insert(new BookEntity("lisi", "123"));
        System.out.println("影响行数:" + count);
    }

//    /**
//     * 1min 1000条
//     *
//     * @throws Exception
//     */
//    @Test
//    public void testInsert2() throws Exception {
//        JdbcClient client = new JdbcClient();
//        client.setDataSource(new YXDataSource(PropertiesTools.build("jdbc.properties")));
//        long time1 = System.currentTimeMillis();
//        int count = 0;
//        for (int i = 10000; i < 90000; i++) {
//            count += client.insert(buildCustInfoEntity(i));
//            System.out.println(count);
//        }
//        long time2 = System.currentTimeMillis();
//        System.out.println("影响行数:" + count);
//        System.out.println("耗时间:" + (time2 - time1));
//    }
//
//    /**
//     *
//     *
//     * @throws Exception
//     */
//    @Test
//    public void testBatchInsert2() throws Exception {
//        JdbcClient client = new JdbcClient();
//        client.setDataSource(new YXDataSource(PropertiesTools.build("jdbc.properties")));
//        List<DiyaCustInfoEntity> list = new ArrayList<>(100000);
//        for (int i = 1990000; i < 2000000; i++) {
//            list.add(buildCustInfoEntity(i));
//        }
//        long time1 = System.currentTimeMillis();
//        int count = client.insertBatch(list);
//        long time2 = System.currentTimeMillis();
//        System.out.println("影响行数:" + count);
//        System.out.println("耗时间:" + (time2 - time1));
//    }
//
//    /**
//
//     * 10万10万的加
//     *
//     * @throws Exception
//     */
//    @Test
//    public void testBatchInsert3() throws Exception {
//        JdbcClient client = new JdbcClient();
//        client.setDataSource(new YXDataSource(PropertiesTools.build("jdbc.properties")));
//
//        int limit = 2000000;
//        int start = 0;
//        int count = 0;
//        int realCount = 0;
//        List<DiyaCustInfoEntity> list = new ArrayList<>(100000);
//        while (start < limit) {
//            list.add(buildCustInfoEntity(start));
//            start++;
//            count++;
//            if (count == 100000) {
//                long time1 = System.currentTimeMillis();
//                realCount += client.insertBatch(list);
//                long time2 = System.currentTimeMillis();
//                System.out.println("影响行数:" + count);
//                System.out.println("耗时间:" + (time2 - time1));
//                count = 0;
//                list.clear();
//            }
//        }
//    }


    /**
     * 历史数据
     * 影响行数:100000  耗时间:71073
     * 影响行数:1000条  耗时间:1min
     * 信息表造数据，先新增1100
     * 2018-10-31
     *
     * @throws Exception
     */
    @Test
    public void testBatchInsert4() throws Exception {
        JdbcClient client = new JdbcClient();
        client.setDataSource(new YXDataSource(PropertiesUtil.build("jdbc-q.properties")));

        int limit = 1100;
        int start = 0;
        int realCount = 0;
        List<DiyaCustInfoEntity> list = new ArrayList<>(100000);
        while (start < limit) {
            list.add(buildCustInfoEntity(start));
            start++;
            if (list.size() == 1000) {
                long time1 = System.currentTimeMillis();
                realCount += client.insertBatch(list);
                long time2 = System.currentTimeMillis();
                System.out.println("影响行数:" + realCount);
                System.out.println("耗时间:" + (time2 - time1));
                realCount = 0;
                list.clear();
            }
        }
    }

    private DiyaCustInfoEntity buildCustInfoEntity(int i) {
        DiyaCustInfoEntity param = new DiyaCustInfoEntity();
        param.setCertNum("362411112312366" + i);
        param.setMobileNum("1311164543"+i);
        param.setCustType("11100010");
        param.setCardNum("123456789-" + i);
        param.setName("zhangsan-" + i);
        param.setName2("saner" + i);
        param.setGender("124000000010");
        param.setBirthDate(Timestamp.valueOf("1980-12-31 00:00:00"));
        param.setDiyaStoreCode("301" + i);
        param.setEmail(i+"abc@email.com");
        param.setProvince("0000");
        param.setCity("025");
        param.setDistrict("0035");
        param.setAddressDetail("南京市玄武区徐庄软件园-" + i);
        param.setPostCode("210000");
        param.setFixedTelephone("0755-99993033");
        param.setMobileTelephone("13111111111");
        param.setCreatedTime(DateUtil.getCurrentTimestamp());
        param.setUpdatedTime(DateUtil.getCurrentTimestamp());
        param.setDealNum(0);
        param.setSeqNo(1);
        param.setErrorMsg("");
        return param;
    }
}