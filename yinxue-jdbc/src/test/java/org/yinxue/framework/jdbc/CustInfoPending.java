/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: CustInfoPending
 * Author:   zengjian
 * Date:     2018/10/23 9:15
 * Description: 待处理表
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc;

/**
 * 〈CustInfoPending〉<br> 
 * 〈待处理表〉
 *
 * @author zengjian
 * @create 2018/10/23 9:15
 */
public class CustInfoPending {

    private Long cust_id;
    private Integer retryTime;

    public Long getCust_id() {
        return cust_id;
    }

    public void setCust_id(Long cust_id) {
        this.cust_id = cust_id;
    }

    public Integer getRetryTime() {
        return retryTime;
    }

    public void setRetryTime(Integer retryTime) {
        this.retryTime = retryTime;
    }
}