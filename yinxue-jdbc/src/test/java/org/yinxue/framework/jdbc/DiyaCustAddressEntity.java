/**
 * Copyright (C), 2002-2018, 苏宁易购电子商务有限公司
 * FileName: DiyaCustAddressEntity
 * Author:   88383079
 * Date:     2018/10/26 11:39
 * Description: 迪亚会员地址
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc;

import java.sql.Timestamp;

/**
 * 〈DiyaCustAddressEntity〉<br> 
 * 〈迪亚会员地址〉
 *
 * @author 88383079
 * @create 2018/10/26 11:39
 */
public class DiyaCustAddressEntity  {

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 手机号
     */
    private String mobileNum;
    /**
     * 省
     */
    private String province;
    /**
     *  市
     */
    private String city;
    /**
     * 区
     */
    private String district;
    /**
     * 镇
     */
    private String town;
    /**
     * 详细地址
     */
    private String addressDetail;

    /**
     * 邮政编码
     */
    private String postCode;
    /**
     * 固定电话
     */
    private String fixedTelephone;
    /**
     * 联系手机
     */
    private String mobileTelephone;

    /**
     * 创建时间
     */
    private Timestamp createdTime;
    /**
     * 更新时间
     */
    private Timestamp updatedTime;
    /**
     * 版本号
     */
    private Integer seqNo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getFixedTelephone() {
        return fixedTelephone;
    }

    public void setFixedTelephone(String fixedTelephone) {
        this.fixedTelephone = fixedTelephone;
    }

    public String getMobileTelephone() {
        return mobileTelephone;
    }

    public void setMobileTelephone(String mobileTelephone) {
        this.mobileTelephone = mobileTelephone;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Timestamp updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }
}
