/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: BookEntity
 * Author:   zengjian
 * Date:     2018/10/8 10:34
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/10/8 10:34
 */
public class BookEntity {

    private String bookname;
    private String author;

    public BookEntity(String bookname, String author) {
        this.bookname = bookname;
        this.author = author;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}