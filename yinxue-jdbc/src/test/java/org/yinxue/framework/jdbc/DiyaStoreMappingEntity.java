/**
 * Copyright (C), 2002-2018, 苏宁易购电子商务有限公司
 * FileName: DiyaStoreMappingEntity
 * Author:   88383079
 * Date:     2018/10/30 10:42
 * Description: 迪亚门店编码映射表
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc;

import java.sql.Timestamp;

/**
 * 〈DiyaStoreMappingEntity〉<br>
 * 〈迪亚门店编码映射表〉
 *
 * @author 88383079
 * @create 2018/10/30 10:42
 */
public class DiyaStoreMappingEntity  {

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 迪亚门店编号
     */
    private String diyaStoreCode;
    /**
     * 易购门店编号
     */
    private String storeCode;
    /**
     * 易购分公司编号
     */
    private String branchCode;
    /**
     * 创建时间
     */
    private Timestamp createdTime;
    /**
     * 更新时间
     */
    private Timestamp updatedTime;
    /**
     * 版本号
     */
    private Integer seqNo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiyaStoreCode() {
        return diyaStoreCode;
    }

    public void setDiyaStoreCode(String diyaStoreCode) {
        this.diyaStoreCode = diyaStoreCode;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Timestamp updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }
}
