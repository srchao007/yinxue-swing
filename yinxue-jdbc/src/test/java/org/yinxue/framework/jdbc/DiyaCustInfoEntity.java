/**
 * Copyright (C), 2002-2018, 苏宁易购电子商务有限公司
 * FileName: DiyaCustInfoEntity
 * Author:   88383079
 * Date:     2018/10/22 16:21
 * Description: 迪亚天天会员实体类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc;

import org.yinxue.framework.jdbc.annotation.TableName;

import java.sql.Timestamp;

/**
 * 〈DiyaCustInfoEntity〉<br> 
 * 〈迪亚天天会员实体类〉
 *
 * @author 88383079
 * @create 2018/10/22 16:21
 */
@TableName("diya_cust_info")
public class DiyaCustInfoEntity {

    /**
     * 序列号
     */
    private static final long serialVersionUID = -5324061232746019446L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 会员编号
     */
    private String custNum;
    /**
     * 身份证号
     */
    private String certNum;
    /**
     * 手机号
     */
    private String mobileNum;

    /**
     * 迪亚卡号
     */
    private String cardNum;

    /**
     * 会员类型:线上线下
     */
    private String custType;
    /**
     * 姓名
     */
    private String name;
    /**
     * 别名1
     */
    private String name2;
    /**
     * 性别
     */
    private String gender;
    /**
     * 出生日期
     */
    private Timestamp birthDate;
    /**
     * 迪亚门店
     */
    private String diyaStoreCode;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String district;
    /**
     * 镇
     */
    private String town;

    /**
     * 详细地址
     */
    private String addressDetail;
    /**
     * 邮政编码
     */
    private String postCode;
    /**
     * 固定电话
     */
    private String fixedTelephone;
    /**
     * 联系手机
     */
    private String mobileTelephone;
    /**
     * 处理状态，取各bit记录rsf调用节点
     */
    private int dealStat;
    /**
     * 处理次数
     */
    private Integer dealNum;
    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 创建时间
     */
    private Timestamp createdTime;

    /**
     * 更新时间
     */
    private Timestamp updatedTime;

    /**
     * 版本号
     */
    private Integer seqNo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustNum() {
        return custNum;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public String getCertNum() {
        return certNum;
    }

    public void setCertNum(String certNum) {
        this.certNum = certNum;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getDiyaStoreCode() {
        return diyaStoreCode;
    }

    public void setDiyaStoreCode(String diyaStoreCode) {
        this.diyaStoreCode = diyaStoreCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getFixedTelephone() {
        return fixedTelephone;
    }

    public void setFixedTelephone(String fixedTelephone) {
        this.fixedTelephone = fixedTelephone;
    }

    public String getMobileTelephone() {
        return mobileTelephone;
    }

    public void setMobileTelephone(String mobileTelephone) {
        this.mobileTelephone = mobileTelephone;
    }

    public int getDealStat() {
        return dealStat;
    }

    public void setDealStat(int dealStat) {
        this.dealStat = dealStat;
    }

    public Integer getDealNum() {
        return dealNum;
    }

    public void setDealNum(Integer dealNum) {
        this.dealNum = dealNum;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Timestamp updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }
}
