/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: CustInfo
 * Author:   zengjian
 * Date:     2018/10/22 16:28
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc;

import lombok.Data;

import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * 〈CustInfo〉<br> 
 * 〈〉
 *
 * @author zengjian
 * @create 2018/10/22 16:28
 */
@Data
public class CustInfo {

    private Long id;
    private String certNo;
    private String mobileNo;
    private String cardNo;
    private String name;
    private String name2;
    private String gender;
    private Timestamp birthDate;
    private String activatedStore;
    private String email;
    private String province;
    private String city;
    private String district;
    private String road;
    private String houseNum;
    private String addressDetail;
    private String postCode;
    private String fixedTelephone;
    private String mobileTelephone;
    private String dealStat;
    private String dealNum;
    private String errorMsg;
    private Date createdTime;
    private Timestamp updatedTime;
    private Date bizTime;
    private String createdByEmployee;
    private String updateByEmployee;
    private String createdSystem;
    private String updatedSystem;
    private String seqNo;
    private String srcIp;
    private String srcMacAddr;
    private String trmlUsername;
    private String trmlIdentity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getActivatedStore() {
        return activatedStore;
    }

    public void setActivatedStore(String activatedStore) {
        this.activatedStore = activatedStore;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getHouseNum() {
        return houseNum;
    }

    public void setHouseNum(String houseNum) {
        this.houseNum = houseNum;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getFixedTelephone() {
        return fixedTelephone;
    }

    public void setFixedTelephone(String fixedTelephone) {
        this.fixedTelephone = fixedTelephone;
    }

    public String getMobileTelephone() {
        return mobileTelephone;
    }

    public void setMobileTelephone(String mobileTelephone) {
        this.mobileTelephone = mobileTelephone;
    }

    public String getDealStat() {
        return dealStat;
    }

    public void setDealStat(String dealStat) {
        this.dealStat = dealStat;
    }

    public String getDealNum() {
        return dealNum;
    }

    public void setDealNum(String dealNum) {
        this.dealNum = dealNum;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Timestamp updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Date getBizTime() {
        return bizTime;
    }

    public void setBizTime(Date bizTime) {
        this.bizTime = bizTime;
    }

    public String getCreatedByEmployee() {
        return createdByEmployee;
    }

    public void setCreatedByEmployee(String createdByEmployee) {
        this.createdByEmployee = createdByEmployee;
    }

    public String getUpdateByEmployee() {
        return updateByEmployee;
    }

    public void setUpdateByEmployee(String updateByEmployee) {
        this.updateByEmployee = updateByEmployee;
    }

    public String getCreatedSystem() {
        return createdSystem;
    }

    public void setCreatedSystem(String createdSystem) {
        this.createdSystem = createdSystem;
    }

    public String getUpdatedSystem() {
        return updatedSystem;
    }

    public void setUpdatedSystem(String updatedSystem) {
        this.updatedSystem = updatedSystem;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getSrcIp() {
        return srcIp;
    }

    public void setSrcIp(String srcIp) {
        this.srcIp = srcIp;
    }

    public String getSrcMacAddr() {
        return srcMacAddr;
    }

    public void setSrcMacAddr(String srcMacAddr) {
        this.srcMacAddr = srcMacAddr;
    }

    public String getTrmlUsername() {
        return trmlUsername;
    }

    public void setTrmlUsername(String trmlUsername) {
        this.trmlUsername = trmlUsername;
    }

    public String getTrmlIdentity() {
        return trmlIdentity;
    }

    public void setTrmlIdentity(String trmlIdentity) {
        this.trmlIdentity = trmlIdentity;
    }
}