package org.yinxue.framework.jdbc;

import org.yinxue.framework.jdbc.entity.BaseEntity;

import java.sql.Connection;
import java.util.List;

/**
 * 操作数据库的接口类 <br>
 * <pre>
 * </pre>
 *
 * @author zengjian
 * @create 2018-07-06 23:29
 * @since 1.0.0
 */
public abstract class Jdbc {

    abstract <T> Integer insert(T entity) throws Exception;

    abstract <T> Integer insertBatch(List<T> entityList) throws Exception;

    abstract <T> Integer delete(T entity) throws Exception;

    abstract <T> Integer update(T entity) throws Exception;

    abstract <T> List<T> queryList(T entity) throws Exception;

    abstract <T> Integer queryCount(T entity) throws Exception;

    interface JdbcAction<T> {
        /**
         * 回调
         * @param con
         * @return
         * @throws Exception
         */
        T doAction(Connection con) throws Exception;
    }

}
