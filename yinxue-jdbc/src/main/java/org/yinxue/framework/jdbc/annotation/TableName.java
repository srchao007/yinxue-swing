/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: TableName
 * Author:   zengjian
 * Date:     2018/10/16 17:49
 * Description: 实体类对应的表名称
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc.annotation;

import java.lang.annotation.*;

/**
 * 〈TableName〉<br>
 * 〈实体类对应的表名称〉
 *
 * @author zengjian
 * @create 2018/10/16 17:49
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface TableName {

    String value() default "";

}