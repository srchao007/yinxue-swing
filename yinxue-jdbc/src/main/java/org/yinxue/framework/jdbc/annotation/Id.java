/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: Id
 * Author:   zengjian
 * Date:     2018/10/24 17:14
 * Description: 主键ID注解
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc.annotation;

import java.lang.annotation.*;

/**
 * 〈Id〉<br> 
 * 〈主键ID注解〉
 *
 * @author zengjian
 * @create 2018/10/24 17:14
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface Id {

}