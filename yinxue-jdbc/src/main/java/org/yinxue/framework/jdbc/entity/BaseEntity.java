/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: BaseEntity
 * Author:   zengjian
 * Date:     2018/10/24 17:14
 * Description: 提供的可扩展基类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc.entity;

import org.yinxue.framework.jdbc.annotation.Id;

/**
 * 〈BaseEntity〉<br> 
 * 〈提供的可扩展基类〉
 *
 * @author zengjian
 * @create 2018/10/24 17:14
 */
public class BaseEntity {

    @Id
    private Long id;

}