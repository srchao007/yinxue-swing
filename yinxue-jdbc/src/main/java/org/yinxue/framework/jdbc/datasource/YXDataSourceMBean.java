/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: YXDataSourceMBean
 * Author:   zengjian
 * Date:     2018/10/16 17:02
 * Description: 用于监控
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc.datasource;

import java.util.Properties;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 〈YXDataSourceMBean〉<br> 
 * 〈用于监控〉
 *
 * @author zengjian
 * @create 2018/10/16 17:02
 */
public interface YXDataSourceMBean {

    public String getUsername();

    public void setUsername(String username);

    public String getPassword();

    public void setPassword(String password);

    public String getUrl();

    public void setUrl(String url);

    public String getDriverName();

    public void setDriverName(String driverName);

    public Properties getPropeties();

    public void setPropeties(Properties propeties);

    public Integer getInitialSize();

    public void setInitialSize(Integer initialSize);

    public Integer getMaxActive();

    public void setMaxActive(Integer maxActive);

    public long getMaxWait();

    public void setMaxWait(long maxWait);

    public long getMaxIdle();

    public void setMaxIdle(long maxIdle);

    public long getMinIdle();

    public void setMinIdle(long minIdle);

    public ThreadLocal<YXConnection> getLocal();

    public void setLocal(ThreadLocal<YXConnection> local);

    public YXConnection[] getConnections();

    public void setConnections(YXConnection[] connections);

    public YXConnection[] getExtraConnections();

    public void setExtraConnections(YXConnection[] extraConnections);

    public int getSize();

    public void setSize(int size);

    public int getExtraSize();

    public void setExtraSize(int extraSize);

    public boolean isPooled();

    public void setPooled(boolean pooled);

    public boolean isExtraPooled();

    public void setExtraPooled(boolean extraPooled);

    public ReentrantLock getCreateLock();

    public void setCreateLock(ReentrantLock createLock);

    public ReentrantLock getExtraCreateLock();

    public void setExtraCreateLock(ReentrantLock extraCreateLock);

}