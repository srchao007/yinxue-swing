/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: JdbcConstant
 * Author:   zengjian
 * Date:     2018/10/16 14:39
 * Description: JDBC常量
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc.constant;

/**
 * 〈JdbcConstant〉<br>
 * 〈JDBC常量〉
 *
 * @author zengjian
 * @create 2018/10/16 14:39
 */
public interface JdbcConstant {

    /**
     * 连接状态
     */
    int NO_USE = 1;
    int USING = 2;



}