/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: YXDataSourceFactory
 * Author:   zengjian
 * Date:     2018/10/16 16:03
 * Description: YADataSource工厂类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.framework.jdbc.datasource;

import fun.codedesign.yinxue.util.PropertiesUtil;

/**
 * 〈YXDataSourceFactory〉<br> 
 * 〈YADataSource工厂类〉
 *
 * @author zengjian
 * @create 2018/10/16 16:03
 */
public class YXDataSourceFactory {

    public static YXDataSource getYXDateSource(){
        return new YXDataSource(PropertiesUtil.build("/jdbc.properties"));
    }

}