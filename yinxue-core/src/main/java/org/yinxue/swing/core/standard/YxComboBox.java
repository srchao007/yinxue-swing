package org.yinxue.swing.core.standard;

import javax.swing.*;
import java.awt.event.ItemListener;

public class YxComboBox<E> extends JComboBox<E> {

    public YxComboBox addYxItemListener(ItemListener aListener) {
        super.addItemListener(aListener);
        return this;
    }

}
