package org.yinxue.swing.core.util;

public interface Callback {

    void call(Object target);

}
