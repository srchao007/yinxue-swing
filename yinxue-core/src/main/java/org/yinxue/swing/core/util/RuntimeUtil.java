package org.yinxue.swing.core.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class RuntimeUtil {

    public static void main(String[] args) throws IOException {
        // windows 使用 & 拼接命令
//        exec("cd /d E:\\code\\solar-designer & git pull");
        exec("java -classpath \"D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\charsets.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\deploy.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\access-bridge-64.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\cldrdata.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\dnsns.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\jaccess.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\jfxrt.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\localedata.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\nashorn.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\sunec.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\sunjce_provider.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\sunmscapi.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\sunpkcs11.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\ext\\zipfs.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\javaws.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\jce.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\jfr.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\jfxswt.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\jsse.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\management-agent.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\plugin.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\resources.jar;D:\\Program Files\\Java\\jdk1.8.0_202\\jre\\lib\\rt.jar;E:\\code\\yinxue-swing\\yinxue-unit\\out\\production\\classes;E:\\code\\yinxue-swing\\yinxue-unit\\out\\production\\resources;E:\\code\\yinxue-swing\\yinxue-core\\out\\production\\classes;D:\\Program Files\\JetBrains\\IntelliJ IDEA Community Edition 2023.1.3\\lib\\idea_rt.jar\" org.yinxue.swing.unit.main.UnitPanel2");
    }

    public static void exec(String command) {
        execCallback(command, target -> System.out.println(target));
    }

    public static void execCallback(String command, Callback callable) {
        BufferedReader reader = null;
        try {
            // 调用CMD命令
            ProcessBuilder processBuilder = new ProcessBuilder("cmd.exe", "/c", command); // /c参数表示执行后关闭CMD窗口
            processBuilder.redirectErrorStream(true); // 将错误输出流与标准输出流合并
            Process process = processBuilder.start();

            // 获取命令输出结果
            InputStream inputStream = process.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("GBK"))); // 设置编码为GBK?
            String line;
            while ((line = reader.readLine()) != null) {
                callable.call(line);
            }

            // 等待命令执行完成
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                CloseUtil.close(reader);
            }
        }
    }

    // TODO 优化
    public static void execAysnc(String command) {
        new Thread(() -> exec(command)).start();
    }

    // TODO 优化
    public static void execAysncCallback(String command, Callback callback) {
        new Thread(() -> execCallback(command, callback)).start();
    }

}
