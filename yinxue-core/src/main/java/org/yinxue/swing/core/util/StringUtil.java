package org.yinxue.swing.core.util;

/**
 * 字符串处理工具类
 *
 * @author zengjian
 * @create 2018-04-11 17:11
 * @since 1.0.0
 */
public class StringUtil {

    /**
     * 字符串为空返回true
     *
     * @param str
     * @return true|false
     */
    public static boolean isEmpty(final String str) {
        return null == str || "".equals(str.trim());
    }

    /**
     * 字符串不为空返回true
     *
     * @param str
     * @return true|false
     */
    public static boolean isNotEmpty(final String str) {
        return !isEmpty(str);
    }

    /**
     * 首字母小写 <br>
     * 如: InnerName --> innerName;
     *
     * @param str
     * @return true|false
     */
    public static String toLowerCaseFirstChar(String str) {
        if (StringUtil.isEmpty(str)) {
            return "";
        }
        return str.substring(0, 1).toLowerCase().concat(str.substring(1));
    }

    /**
     * 首字母大写 <br>
     * 如: innerName --> InnerName;
     *
     * @param str
     * @return
     */
    public static String toUpperCaseFirstChar(String str) {
        return str.substring(0, 1).toUpperCase().concat(str.substring(1));
    }
}
