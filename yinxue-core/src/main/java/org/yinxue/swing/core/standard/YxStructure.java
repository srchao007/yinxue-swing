/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: YinxueComponent
 * Author:   zengjian
 * Date:     2018/8/14 11:17
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.core.standard;

/**
 * 〈定义面板设计的接口〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/14 11:17
 */
public interface YxStructure {


    /**
     * 初始化及整体布局
     */
    default void init() {

    }

    /**
     * 细致布局
     */
    default void position() {

    }

    /**
     * 添加监听逻辑
     */
    default void action() {

    }


    /**
     * 销毁实例
     */
    default void destory() {

    }

}