package org.yinxue.swing.core.standard;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

public class YxButton extends JButton {

    public YxButton addYxActionListener(ActionListener l) {
        super.addActionListener(l);
        return this;
    }

    public YxButton addYxKeyListener(KeyListener l) {
        super.addKeyListener(l);
        return this;
    }
}
