package org.yinxue.swing.core.util;

/**
 * 资源释放类Util <br>
 *
 * @author zengjian
 * @create 2018-06-06 14:26
 * @since 1.0.0
 */
public abstract class CloseUtil {

    public static void close(AutoCloseable... args) {
        if (args == null) {
            return;
        }
        for (AutoCloseable arg : args) {
            if (arg != null) {
                try {
                    arg.close();
                } catch (Exception e) {
                    LogUtil.error(CloseUtil.class, "io close failed", e);
                }
                arg = null;
            }
        }
    }
}
