package org.yinxue.swing.core.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtil {
    private static HttpURLConnection con;

    public static void main(String[] args) throws Exception {
        URL url = new URL("https://www.snowfure.com");
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        System.out.println(content.toString());
    }

    public static String doExecute(String serverUrl, String method) throws Exception {
        URL url = new URL(serverUrl);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod(method);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        System.out.println(content);
        return content.toString();
    }

    public static void post(String host) throws Exception {

    }

    public static void get() {

    }

    public static void put() {

    }

    public static void delete() {

    }

}
