package org.yinxue.swing.core.frame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class YxFrame extends JFrame {

    Map<String, JMenu> menuMap = new HashMap<>();

    public YxFrame() throws HeadlessException {
        setJMenuBar(new JMenuBar());
        setLocationRelativeTo(this.getOwner());
        setBackground(Color.BLACK);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public YxFrame(GraphicsConfiguration gc) {
        super(gc);
        setJMenuBar(new JMenuBar());
        setLocationRelativeTo(this.getOwner());
        setBackground(Color.BLACK);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public YxFrame(String title) throws HeadlessException {
        super(title);
        setJMenuBar(new JMenuBar());
        setLocationRelativeTo(this.getOwner());
        setBackground(Color.BLACK);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public YxFrame(String title, GraphicsConfiguration gc) {
        super(title, gc);
        setJMenuBar(new JMenuBar());
        setLocationRelativeTo(this.getOwner());
        setBackground(Color.BLACK);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public YxFrame size(int width, int height) {
        Dimension size = new Dimension(width, height);
        this.setSize(size);
        this.setLocationRelativeTo(this.getOwner());
        this.setPreferredSize(size);
        return this;
    }

    // https://blog.csdn.net/weixin_42247720/article/details/80509052
    public YxFrame popMenu(String parentName, String childMenuName, String[] childMenus, ActionListener[] listeners, ActionListener defaultListener) {
        JMenu parentMenu = getAndPutCacheMenu(parentName);

        JMenu childMenu = new JMenu(childMenuName);
//        parentMenu.add(childMenuName);

        for (int i = 0; i < childMenus.length; i++) {
            JMenuItem menuItem = new JMenuItem(childMenus[i]);
            if ((listeners.length==0 || listeners==null) && defaultListener!=null) {
                menuItem.addActionListener(defaultListener);
            } else {
                menuItem.addActionListener(listeners[i]);
            }
            childMenu.add(menuItem);
        }

        parentMenu.add(childMenu);

        return this;
    }

    JMenu getAndPutCacheMenu(String parentName) {
        JMenu jMenu = null;
        if (menuMap.containsKey(parentName)) {
            jMenu = menuMap.get(parentName);
        } else {
            jMenu = new JMenu(parentName);
            menuMap.put(parentName, jMenu);
            // 初次加入Bar中
            getJMenuBar().add(jMenu);
        }
        return jMenu;
    }

    public YxFrame menu(String parentName, String childName, ActionListener listener) {
        if (childName == null) {
            return this;
        }
        JMenuItem item = new JMenuItem(childName);
        if (listener!=null) {
            item.addActionListener(listener);
        }
        JMenu jMenu = getAndPutCacheMenu(parentName);
        jMenu.add(item);
        return this;
    }

    /**
     * 增加子菜单
     *
     * @param parent
     * @param childName
     * @param listener
     * @return
     */
    public YxFrame menu2(String parent, String childName, ActionListener listener) {


        return this;
    }

    public YxFrame panel(JPanel jPanel) {
        this.add(jPanel);
        return this;
    }

    public YxFrame visible(boolean b) {
        this.setVisible(b);
        return this;
    }
}
