package org.yinxue.swing.core.action;

import org.yinxue.swing.core.util.EnvUtil;
import org.yinxue.swing.core.util.FileUtil;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class FileChooserActionListener implements ActionListener {

    static final String TEXT_PROJECT = "text_project";
    public String filePath;

    public String text;

    ActionListener actionListener;

    JFileChooser chooser;

    public FileChooserActionListener() {
        filePath = EnvUtil.getProperty(TEXT_PROJECT, System.getProperty("user.dir"));
        chooser = new JFileChooser("", FileSystemView.getFileSystemView());
        chooser.setPreferredSize(new Dimension(800, 600));
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setMultiSelectionEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int open = chooser.showOpenDialog(null);
        if (JFileChooser.APPROVE_OPTION == open) {
            filePath = chooser.getSelectedFile().getAbsolutePath();
//            jText_inputpath.setText(filePath);
            text = FileUtil.readText(new File(filePath));
            if (actionListener != null) {
                actionListener.actionPerformed(null);
            }
        } else {
            // doNoting
        }
    }

    public FileChooserActionListener addYxValueChange(ActionListener actionListener) {
        this.actionListener = actionListener;
        return this;
    }
}
