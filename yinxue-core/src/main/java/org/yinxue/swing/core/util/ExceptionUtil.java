/**
 * Copyright (C), 2017-2018, yinxue-soft
 * FileName: ExceptionUtil
 * Author:   zengjian
 * Date:     2018/10/24 16:51
 * Description: 异常打印堆栈类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.core.util;

/**
 * 〈ExceptionUtil〉<br> 
 * 〈异常打印堆栈类〉
 *
 * @author zengjian
 * @create 2018/10/24 16:51
 */
public class ExceptionUtil {

    public static String printStack(Exception e){
        StringBuilder builder = new StringBuilder(512);
        builder.append("异常类型:"+e.getClass().getSimpleName());
        StackTraceElement[] elements = e.getStackTrace();
        for (StackTraceElement element : elements) {
            builder.append("\n");
            builder.append("at ").append(element.toString());
        }
        return builder.toString().substring(0, 1024);
    }

}