/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: SwingUtil
 * Author:   zengjian
 * Date:     2018/8/13 10:18
 * Description: Swing相关的工具类
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.core.util;

import org.yinxue.swing.core.frame.YxFrame;
import org.yinxue.swing.core.panel.YxTextArea;
import org.yinxue.swing.core.standard.YxPanel;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.io.File;
import java.util.Objects;

/**
 * 〈Swing相关的工具类〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/8/13 10:18
 */
public class SwingUtils {

    public static void initDefaultUI() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void initUI2() {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JFrame ofJFrame() {
        return ofJFrame(false);
    }

    public static JFrame ofJFrame(boolean resizeable) {
        final JFrame frame = ofJFrame("simple", 885, 636);
        frame.setResizable(resizeable);
        return frame;
    }

    /**
     * 创建一个带标题栏的窗口
     *
     * @return
     */
    public static JFrame ofJFrameWithMenuBar() {
        return ofJFrameWithMenuBar("simple");
    }

    public static JFrame ofJFrameWithMenuBar(String title) {
        return ofJFrameWithMenuBar(title, 885, 636, new YxPanel());
    }

    public static JFrame ofJFrameWithMenuBar(String title, int width, int height, JPanel jPanel) {
        final JFrame jFrame = ofJFrame(title, width, height);

        JMenu jMenu_file = new JMenu("菜单");
        JMenuItem jMenuItem_setting = new JMenuItem("设置");
        JMenuItem jMenuItem_quit = new JMenuItem("退出");
        jMenu_file.add(jMenuItem_setting);
        jMenu_file.add(jMenuItem_quit);

        JMenu jMenu_about = new JMenu("关于");
        JMenuItem jMenuItem_help = new JMenuItem("帮助");
        JMenuItem jMenuItem_about = new JMenuItem("关于");
        jMenu_about.add(jMenuItem_help);
        jMenu_about.add(jMenuItem_about);

        JMenuBar jMenuBar = new JMenuBar();
        jMenuBar.add(jMenu_file);
        jMenuBar.add(jMenu_about);
        jFrame.setJMenuBar(jMenuBar);

        jMenuItem_about.addActionListener(e -> JOptionPane.showMessageDialog(null, "by zengjian"));
        jFrame.add(jPanel);
        return jFrame;
    }

    public static JFrame ofJFrame(String title, int width, int height) {
        final JFrame jFrame = new JFrame(title);
        jFrame.setSize(width, height);
        jFrame.setLocationRelativeTo(jFrame.getOwner());
        jFrame.setBackground(Color.BLACK);
        jFrame.setPreferredSize(new Dimension(width, height));
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        return jFrame;
    }

    public static JMenu ofJMenu(String parentName, JMenuItem... items) {
        JMenu jMenu = new JMenu(parentName);
        for (JMenuItem item : items) {
            jMenu.add(item);
        }
        return jMenu;
    }

    public static JMenuItem ofMenuItem(String text, Action action) {
        JMenuItem item = new JMenuItem();
        item.setText(text);
        item.setAction(action);
        return item;
    }

    public static JMenuBar ofJMenuBar(JMenu... menus) {
        JMenuBar jMenuBar = new JMenuBar();
        for (JMenu menu : menus) {
            jMenuBar.add(menu);
        }
        return jMenuBar;
    }

    public static YxFrame createYxFrame(String title, int width, int height, JMenuBar jMenuBar) {
        final YxFrame jFrame = new YxFrame(title);
        jFrame.setSize(width, height);
        jFrame.setLocationRelativeTo(jFrame.getOwner());
        jFrame.setBackground(Color.BLACK);
        jFrame.setPreferredSize(new Dimension(width, height));
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setJMenuBar(jMenuBar);
        return jFrame;
    }


    public static Image ofImage(String name) {
        return new ImageIcon(Thread.currentThread().getContextClassLoader().getResource(name)).getImage();
    }

    public static JButton createToolButton(String name) {
        JButton button = ofJButton(name, new Dimension(1000, 600));
        button.setContentAreaFilled(false);
        return button;
    }

    public static JButton ofEmptyButton() {
        final JButton button = new JButton();
        Dimension dimension = new Dimension(102, 23);
        button.setPreferredSize(dimension);
        button.setMaximumSize(dimension);
        button.setMinimumSize(dimension);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        button.setFocusable(true);
        button.setMargin(new Insets(0, 0, 0, 0));
        return button;
    }

    public static JButton ofJButton(String text) {
        return ofJButton(text, new Dimension(130, 30));
    }

    public static JButton ofJButton(String text, int width, int height) {
        return ofJButton(text, new Dimension(width, height));
    }

    public static JButton ofJButton(String text, Dimension size) {
        JButton jButton = new JButton(text);
        jButton.setMaximumSize(size);
        jButton.setMinimumSize(size);
        jButton.setPreferredSize(size);
        jButton.setOpaque(true);
        return jButton;
    }

    public static JButton ofEmptyButton(int width, int height) {
        JButton button = ofJButton(null, width, height);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        button.setFocusable(true);
        button.setMargin(new Insets(0, 0, 0, 0));
        return button;
    }

    public static TitledBorder createBorder() {
        return BorderFactory.createTitledBorder("");
    }

    public static JPanel ofJPanel(LayoutManager layoutManager) {
        return new JPanel(layoutManager);
    }

    public static JPanel ofJPanelWithBoder() {
        JPanel jPanel = new JPanel(new GridBagLayout());
        jPanel.setBorder(BorderFactory.createTitledBorder(""));
        return jPanel;
    }

    public static JPanel ofJPanelWithBorderSize(int width, int height) {
        JPanel jPanel = new JPanel(new GridBagLayout());
        jPanel.setBorder(BorderFactory.createTitledBorder(""));
        Dimension size = new Dimension(width, height);
        jPanel.setPreferredSize(size);
        jPanel.setMinimumSize(size);
        jPanel.setMaximumSize(size);
        return jPanel;
    }

    public static JLabel ofJLable(String text) {
        return new JLabel(text);
    }

    public static void ofFixSize(JPanel jPanel, Dimension dimension) {
        jPanel.setMaximumSize(dimension);
        jPanel.setMinimumSize(dimension);
        jPanel.setPreferredSize(dimension);
    }

    public static JPanel ofJPanel(LayoutManager layoutManager, int width, int height) {
        JPanel jPanel = new JPanel(layoutManager);
        Dimension size = new Dimension(width, height);
        jPanel.setMaximumSize(size);
        jPanel.setMinimumSize(size);
        jPanel.setPreferredSize(size);
        jPanel.setBorder(BorderFactory.createEtchedBorder());
        return jPanel;
    }

    public static GridBagConstraints ofGridBagConstraints() {
        GridBagConstraints g = new GridBagConstraints();
        g.gridx = 0;
        g.gridy = 0;
        g.weightx = 1;
        g.weighty = 1;
        g.fill = GridBagConstraints.BOTH;
        g.insets = new Insets(5, 5, 5, 5);
        return g;
    }

    public static YxTextArea ofJTextAreaWithJScroll() {
        return new YxTextArea();
    }

    public static JTree ofJTree() {
        DefaultMutableTreeNode node = new DefaultMutableTreeNode("客户端列表");
        return new JTree(node);
    }

    public static ImageIcon getICon(Class clazz, String file) {
        return new ImageIcon(clazz.getResource(file));
    }

}