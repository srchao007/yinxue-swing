package org.yinxue.swing.core.panel;

import org.yinxue.swing.core.standard.YxButton;
import org.yinxue.swing.core.standard.YxPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class YxInputChoose extends YxPanel {

    public JLabel jLabel_project;
    public JTextField jText_inputpath;
    public YxButton jButton_choose;

    ActionListener actionListener;

    JFileChooser chooser;

    @Override
    public void init() {
        jLabel_project = createLabel("项目路径:");
        jText_inputpath = createTextField(10);
        jButton_choose = createYxButton("选择路径", 110, 30);

        chooser = new JFileChooser();
        chooser.setPreferredSize(new Dimension(800, 600));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setMultiSelectionEnabled(true);
    }

    @Override
    public void position() {
        add(jLabel_project, refreshPosition(0, 0, 1, 1, 0.05, 0, H));
        add(jText_inputpath, refreshPosition(1, 0, 1, 1, 0.9, 0, H));
        add(jButton_choose, refreshPosition(2, 0, 1, 1, 0.05, 0, H));
    }

    @Override
    public void action() {
        jButton_choose.addYxActionListener(e -> {
            int open = chooser.showOpenDialog(null);
            if (JFileChooser.APPROVE_OPTION == open) {
                String filePath = chooser.getSelectedFile().getAbsolutePath();
                jText_inputpath.setText(filePath);
                if (actionListener != null) {
                    actionListener.actionPerformed(null);
                }
            } else {
                // doNoting
            }
        });
    }

    public void addYxValueChange(ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public void setText(String text) {
        jText_inputpath.setText(text);
    }

    public String getText() {
        return jText_inputpath.getText();
    }

    public static void main(String[] args) {
        run(YxInputChoose.class, "YxInputChoosePanel", 1600, 900);
    }
}
