package org.yinxue.swing.core.standard;

import org.yinxue.swing.core.panel.YxTextArea;
import org.yinxue.swing.core.panel.YxTree;
import org.yinxue.swing.core.util.SwingUtils;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.awt.*;

/**
 * 面板框架，数据初始化、布局、监听器
 * <p>
 * 工具类，main测试
 */
public class YxPanel extends JPanel implements YxStructure {

    public GridBagConstraints position = new GridBagConstraints();

    {
        position.insets = new Insets(5, 5, 5, 5);
    }

    public int H = GridBagConstraints.HORIZONTAL;
    public int V = GridBagConstraints.VERTICAL;
    public int BOTH = GridBagConstraints.BOTH;


    /**
     * TODO 构造器先初始化，私有变量则后初始化，会出现空指针错误
     * 所以可以将变量初始化放在init里
     */
    public YxPanel() {
        setLayout(new GridBagLayout());
        init();
        position();
        action();
    }

    public YxPanel(int width, int height) {
        this();
        setPreferredSize(new Dimension(width, height));
    }


    public YxPanel addComponent(Component comp, int x, int y, int width, int height, double weightx, double weighty, int fill) {
        add(comp, refreshPosition(x, y, width, height, weightx, weighty, fill));
        return this;
    }

    @Override
    public void add(Component comp, Object constraints) {


        super.add(comp, constraints);
    }

    public JButton createButton(String text, int width, int height) {
        return SwingUtils.ofJButton(text, width, height);
    }

    /**
     * 创建可级联添加监听器的按钮
     *
     * @param text
     * @param width
     * @param height
     * @return
     */
    public YxButton createYxButton(String text, int width, int height) {
        Dimension size = new Dimension(width, height);
        YxButton jButton = new YxButton();
        jButton.setText(text);
        jButton.setMaximumSize(size);
        jButton.setMinimumSize(size);
        jButton.setPreferredSize(size);
        jButton.setOpaque(true);
        return jButton;
    }

    public YxButton createYxButton(String text) {
        return createYxButton(text, 140, 80);
    }

    /**
     * 创建带滚动条的文字框
     *
     * @return
     */
    public YxTextArea createYxTextArea() {
        return new YxTextArea();
    }


    public JLabel createLabel(String text) {
        return new JLabel(text);
    }

    public JTextField createTextField(int columns) {
        return new JTextField(columns);
    }

    /**
     * 创建自定义Yx面板，Grid布局
     *
     * @return
     */
    public YxPanel createYxPanel() {
        return new YxPanel();
    }

    public JTree createJTree() {
        return new JTree();
    }

    public YxTree createYxTree() {
        return new YxTree();
    }

    public JTree createJTree(TreeNode root) {
        return new JTree(root);
    }

    /**
     * 创建树节点
     *
     * @return
     */
    public DefaultMutableTreeNode createTreeNode(Object userObject) {
        return new DefaultMutableTreeNode(userObject);
    }

    /**
     * 创建流式布局面板
     *
     * @return
     */
    public JPanel createFlowPanel() {
        return new JPanel(new FlowLayout());
    }

    /**
     * 创建下拉框
     *
     * @param <T>
     * @return
     */
    public <T> JComboBox<T> createComboBox() {
        return new JComboBox<>();
    }

    /**
     * 创建默认为items列表下拉框, TODO宽高
     *
     * @param items
     * @param <T>>
     * @return
     */
    public <T> JComboBox<T> createComboBox(T... items) {
        JComboBox<T> box = new JComboBox<>();
        for (T item : items) {
            box.addItem(item);
        }
        return box;
    }

    public <T> YxComboBox<T> createYxComboBox(T... items) {
        YxComboBox<T> box = new YxComboBox<>();
        for (T item : items) {
            box.addItem(item);
        }
        return box;
    }


    /**
     * 创建固定之村大小的列表下拉框
     *
     * @param width
     * @param height
     * @param items
     * @param <T>
     * @return
     */
    public <T> JComboBox<T> createComboBox(int width, int height, T... items) {
        JComboBox<T> box = this.createComboBox(items);
        box.setPreferredSize(new Dimension(width, height));
        return box;
    }

    /**
     * 创建带滚动条的多行文本输入框
     *
     * @return
     */
    public YxTextArea creaetYxTextArea() {
        return new YxTextArea();
    }

    /**
     * 创建复选框
     *
     * @param text
     * @return
     */
    public JCheckBox createCheckBox(String text) {
        return new JCheckBox(text);
    }


    /**
     * 刷新本面板的坐标排布位置，复用GridBagConstraints对象
     *
     * @param x
     * @param y
     * @param width
     * @param height
     * @param weightx
     * @param weighty
     * @param fill
     * @return
     */
    public GridBagConstraints refreshPosition(int x, int y, int width, int height, double weightx, double weighty, int fill) {
        position.gridx = x;
        position.gridy = y;
        position.gridwidth = width;
        position.gridheight = height;
        position.weightx = weightx;
        position.weighty = weighty;
        position.fill = fill;
        position.insets = new Insets(0,0,0,0);
        return position;
    }



    public GridBagConstraints refreshPosition(int x, int y, int width, int height, double weightx, double weighty, int fill, int anchor) {
        refreshPosition(x,y,width, height,weightx, weighty, fill);
        position.anchor = anchor;
        return position;
    }

    public GridBagConstraints refreshPosition(int x, int y, int width, int height, double weightx, double weighty, int fill, Insets insets) {
        refreshPosition(x,y,width, height,weightx, weighty, fill);
        position.insets = insets;
        return position;
    }

    public static JFrame ofJFrameWithMenuBar(String title, int width, int height, JPanel jPanel) {
        return SwingUtils.ofJFrameWithMenuBar(title, width, height, jPanel);
    }


    public static void run(Class<?> clazz) {
        run(clazz, "YxPanel", 885, 636);
    }



    public static void run(Class<?> clazz, String title, int width, int height) {
        SwingUtilities.invokeLater(() -> {
            SwingUtils.initDefaultUI();
            try {
                JPanel jPanel = (JPanel) clazz.newInstance();
                JFrame jFrame = SwingUtils.ofJFrameWithMenuBar(title, width, height, jPanel);
                jFrame.setVisible(true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static void main(String[] args) {
        run(YxPanel.class);
    }
}
