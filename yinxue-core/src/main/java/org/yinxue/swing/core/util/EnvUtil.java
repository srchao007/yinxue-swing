package org.yinxue.swing.core.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * 本机环境、路径、属性配置等
 * System.getProperty()参数大全
 * # java.version                                Java Runtime Environment version
 * # java.vendor                                Java Runtime Environment vendor
 * # java.vendor.url                           Java vendor URL
 * # java.home                                Java installation directory
 * # java.vm.specification.version   Java Virtual Machine specification version
 * # java.vm.specification.vendor    Java Virtual Machine specification vendor
 * # java.vm.specification.name      Java Virtual Machine specification name
 * # java.vm.version                        Java Virtual Machine implementation version
 * # java.vm.vendor                        Java Virtual Machine implementation vendor
 * # java.vm.name                        Java Virtual Machine implementation name
 * # java.specification.version        Java Runtime Environment specification version
 * # java.specification.vendor         Java Runtime Environment specification vendor
 * # java.specification.name           Java Runtime Environment specification name
 * # java.class.version                    Java class format version number
 * # java.class.path                      Java class path
 * # java.library.path                 List of paths to search when loading libraries
 * # java.io.tmpdir                       Default temp file path
 * # java.compiler                       Name of JIT compiler to use
 * # java.ext.dirs                       Path of extension directory or directories
 * # os.name                              Operating system name
 * # os.arch                                  Operating system architecture
 * # os.version                       Operating system version
 * # file.separator                         File separator ("/" on UNIX)
 * # path.separator                  Path separator (":" on UNIX)
 * # line.separator                       Line separator ("/n" on UNIX)
 * # user.name                        User’s account name
 * # user.home                              User’s home directory
 * # user.dir                               User’s current working directory
 */
public class EnvUtil {

    private static PropFile yinxueSetting = new PropFile(System.getProperty("user.home"), "yinxue-setting.properties");

    public static void main(String[] args) {
        String env = System.getProperty("user.dir");
        System.out.println(env);
        System.out.println(System.getProperty("user.home"));
    }

    public static void setProperty(String key, String value) {
        yinxueSetting.setProperty(key, value);
    }

    public static String getProperty(String key) {
        return yinxueSetting.getProperty(key);
    }

    public static String getProperty(String key, String defaultValue) {
        String value = getProperty(key);
        return value == null ? defaultValue : value;
    }

    public static String readText() {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(Files.newInputStream(new File(System.getProperty("user.home"), "yinxue-setting.properties").toPath()), StandardCharsets.UTF_8));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return builder.toString();
    }

    public static File readFile() {
        return new File(System.getProperty("user.home"), "yinxue-setting.properties");
    }
}
