package fun.codedesign.yinxue.text.service;

import java.util.List;

public interface TipService {


    /**
     * 根据输入字段，得到提示字段
     * @param inputText
     * @return
     */
    List<String> getTip(String inputText);



}
