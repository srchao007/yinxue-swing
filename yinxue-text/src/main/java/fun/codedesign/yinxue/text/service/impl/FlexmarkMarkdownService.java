package fun.codedesign.yinxue.text.service.impl;

import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.MutableDataSet;
import fun.codedesign.yinxue.text.service.MarkdownService;

public class FlexmarkMarkdownService implements MarkdownService {
    @Override
    public String html(String markdown) {
        MutableDataSet options = new MutableDataSet();

        // uncomment to set optional extensions
//        options.set(Parser.EXTENSIONS, Arrays.asList(TablesExtension.create()/*, StrikethroughExtension.create()*/));

        // uncomment to convert soft-breaks to hard breaks
        //options.set(HtmlRenderer.SOFT_BREAK, "<br />\n");

        Parser parser = Parser.builder(options).build();
        HtmlRenderer renderer = HtmlRenderer.builder(options).build();

        // You can re-use parser and renderer instances
        Node document = parser.parse(markdown);
        String html = renderer.render(document);  // "<p>This is <em>Sparta</em></p>\n"
        System.out.println(html);
        return html;
    }
}
