package fun.codedesign.yinxue.text.panel;

import org.yinxue.swing.core.util.FileUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.*;
import java.io.File;

public class ConsolePanel extends JScrollPane {

    private JTextArea textArea = new JTextArea();
    public JPopupMenu rightClickMenu;
    public Clipboard clipboard;

    public ConsolePanel() {
        super(VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_AS_NEEDED);
        textArea.setLineWrap(true);
        setViewportView(textArea);
        getVerticalScrollBar().setUnitIncrement(20);
        textArea.setFont(new Font("微软雅黑", Font.PLAIN, 16));

        rightClickMenu = new JPopupMenu();

        textArea.addInputMethodListener(new InputMethodListener() {
            @Override
            public void inputMethodTextChanged(InputMethodEvent event) {
                System.out.println(event);
            }

            @Override
            public void caretPositionChanged(InputMethodEvent event) {
                System.out.println(event);

            }
        });

        clipboard = this.getToolkit().getSystemClipboard();

        textArea.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                if (e.getButton() == MouseEvent.BUTTON3) {
                    rightClickMenu.show(textArea, x, y);
                }
            }
        });

        {
            JMenuItem clear = new JMenuItem("清空");
            JMenuItem copy = new JMenuItem("复制");
            JMenuItem cut = new JMenuItem("剪切");
            JMenuItem v = new JMenuItem("粘贴");


            clear.addActionListener(e -> {
                // TODO
                clear();
            });


            copy.addActionListener(e -> {
                copy();
            });

            cut.addActionListener(e -> {
                cut();
            });

            v.addActionListener(e -> {
                v();
            });

            rightClickMenu.add(clear);
//            rightClickMenu.add(copy);
//            rightClickMenu.add(cut);
//            rightClickMenu.add(v);

            clear.addActionListener(e -> {
                textArea.setText("");
            });
        }


        textArea.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                System.out.println(e.toString());
                System.out.println("keyPressed" + e.toString());
                // CTRL+C  : https://blog.csdn.net/withiter/article/details/17247617
                if (e.getModifiers() == 2 && e.getKeyCode() == 67) {
                    copy();
                }
                // CTRL+V
                if (e.getModifiers() == 2 && e.getKeyCode() == 86) {
                    v();
                }
                // CTRL + X
                if (e.getModifiers() == 2 && e.getKeyCode() == 88) {
                    cut();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
    }

    private void clear() {

    }

    private void v() {
        Transferable contents = clipboard.getContents(this);
        DataFlavor flavor = DataFlavor.stringFlavor;
        if (contents.isDataFlavorSupported(flavor)) {
            try {
                String str = (String) contents.getTransferData(flavor);
                textArea.insert(str, textArea.getCaretPosition());
//                        textArea.append(str);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void copy() {
        String tempText = textArea.getSelectedText();
        StringSelection editText = new StringSelection(tempText);
        clipboard.setContents(editText, null);
    }

    private void cut() {
        copy();
        int start = textArea.getSelectionStart();
        int end = textArea.getSelectionEnd();
        textArea.replaceRange("", start, end); //从Text1中删除被选取的文本。
    }

    public ConsolePanel setText(String text) {
        textArea.setText(text);
        return this;
    }

    public String getText() {
        return textArea.getText();
    }

    public void append(String msg) {
        textArea.append(msg + "\n");
    }

    public void appendAndRefresh(String msg) {
        textArea.append(msg + "\n");
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }

    public JTextArea getTextArea() {
        return this.textArea;
    }

    /**
     * 刷新光标位置
     */
    public void refreshCaretPosition() {
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }

    public ConsolePanel addYxKeyListener(KeyListener l) {
        textArea.addKeyListener(l);
        // 实现拖动文件至文本窗口显示  https://blog.csdn.net/java_faep/article/details/53523401
        textArea.setTransferHandler(new TransferHandler() {

            private static final long serialVersionUID = 1L;

            @Override
            public boolean importData(JComponent comp, Transferable t) {
                try {
                    Object o = t.getTransferData(DataFlavor.javaFileListFlavor);

                    String filepath = o.toString();
                    if (filepath.startsWith("[")) {
                        filepath = filepath.substring(1);
                    }
                    if (filepath.endsWith("]")) {
                        filepath = filepath.substring(0, filepath.length() - 1);
                    }
                    System.out.println(filepath);
                    textArea.setText(FileUtil.readText(new File(filepath)));
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean canImport(JComponent comp, DataFlavor[] flavors) {
                for (int i = 0; i < flavors.length; i++) {
                    if (DataFlavor.javaFileListFlavor.equals(flavors[i])) {
                        return true;
                    }
                }
                return false;
            }
        });
        return this;
    }
}
