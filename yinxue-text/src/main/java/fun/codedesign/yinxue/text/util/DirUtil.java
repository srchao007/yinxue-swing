package fun.codedesign.yinxue.text.util;

import org.yinxue.swing.core.util.EnvUtil;
import org.yinxue.swing.core.util.StringUtil;

public interface DirUtil {

    String DIRS = "dirs";

    static String[] getDirs() {
        String dirs = EnvUtil.getProperty(DIRS);
        if (StringUtil.isNotEmpty(dirs)) {
            String[] dirArray = dirs.split(";");
            return dirArray;
        }
        return new String[]{};
    }

}
