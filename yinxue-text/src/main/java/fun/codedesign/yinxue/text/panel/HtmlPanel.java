package fun.codedesign.yinxue.text.panel;

import org.yinxue.swing.core.standard.YxPanel;
import org.yinxue.swing.core.util.SwingUtils;

import javax.swing.*;
import java.awt.*;

public class HtmlPanel extends JScrollPane {

    JEditorPane jEditorPane;
    JFrame jFrame;

    public HtmlPanel() {
        super(VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jEditorPane = new JEditorPane();
        jEditorPane.setContentType("text/html");
        setViewportView(jEditorPane);
        getVerticalScrollBar().setUnitIncrement(20);
        jEditorPane.setFont(new Font("微软雅黑", Font.PLAIN, 20));
    }

    public void doRun() {
        SwingUtilities.invokeLater(() -> {
            SwingUtils.initDefaultUI();
            try {
                JFrame jFrame = SwingUtils.ofJFrame("html预览", 800, 600);
                jFrame.add(this);
                this.jFrame = jFrame;
                jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                jFrame.setVisible(true);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    // https://qa.1r1g.com/sf/ask/125335031/
    public void setHtml(String html) {
        jEditorPane.setText(html);
    }
}
