package fun.codedesign.yinxue.text.service.impl;

import fun.codedesign.yinxue.text.service.MarkdownService;
import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import java.util.Arrays;

public class CommonMarkdownService implements MarkdownService {


    @Override
    public String html(String markdown) {
        // https://github.com/commonmark/commonmark-java  parser和renderer都需要加载这个扩展插件
        Parser parser = Parser.builder().extensions(Arrays.asList(TablesExtension.create())).build();
        Node document = parser.parse(markdown);
        HtmlRenderer renderer = HtmlRenderer.builder().extensions(Arrays.asList(TablesExtension.create())).build();
        String html = renderer.render(document);
        System.out.println(html);
        return html;
    }
}
