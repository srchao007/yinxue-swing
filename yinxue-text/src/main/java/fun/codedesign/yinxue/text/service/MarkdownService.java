package fun.codedesign.yinxue.text.service;

public interface MarkdownService {

    String html(String markdown);
}
