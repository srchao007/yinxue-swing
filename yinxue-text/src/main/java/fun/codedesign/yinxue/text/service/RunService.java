package fun.codedesign.yinxue.text.service;

import org.yinxue.swing.core.domain.FileObject;

import java.io.File;

public interface RunService {

    String runCode(FileObject file);

}
