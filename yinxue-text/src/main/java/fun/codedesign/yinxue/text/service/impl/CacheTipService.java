package fun.codedesign.yinxue.text.service.impl;

import fun.codedesign.yinxue.text.service.TipService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CacheTipService implements TipService {
    Set<String> tips = new HashSet<>();


    {
        initMarkdownTips();
        initTextTips();
        initJavaTips();
        initProjectTips();
    }

    private void initMarkdownTips() {
        tips.add("#");
        tips.add("##");
        tips.add("###");
        tips.add("####");
        tips.add("#####");
        tips.add("![]()");
    }

    private void initTextTips() {
        tips.add("您好");
        tips.add("您好,zengyi");
        tips.add("您好,xinxin");
    }

    private void initJavaTips(){
        // TODO classloader中获取？
    }

    private void initProjectTips() {
//        new Thread(() -> {
//            while (true) {
//                String[] dirs = DirUtil.getDirs();
//                for (String dir : dirs) {
//                    FileUtil.batchReadFileByThread(dir, new JavaFilter());
//                }
//                Map<JavaFile, ClassDesc> javaMaps = Context.register().getCreateFileMap();
//                for (ClassDesc classDesc : javaMaps.values()) {
//                    Map<String, MethodDesc> methodmap = classDesc.methodMap;
//                    for (MethodDesc value : methodmap.values()) {
//                        tips.add(value.methodName);
//                    }
//                }
//                try {
//                    Thread.sleep(5000);
//                } catch (InterruptedException e) {
//                    throw new RuntimeException(e);
//                }
//            }
//        }).start();
    }

    @Override
    public List<String> getTip(String inputText) {
        return tips.stream().filter(tip -> tip.startsWith(inputText) || tip.contains(inputText)).limit(10).collect(Collectors.toList());
    }
}
