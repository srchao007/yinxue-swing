package fun.codedesign.yinxue.text.domain;

import org.yinxue.swing.core.domain.FileObject;
import org.yinxue.swing.core.util.EnvUtil;

import java.io.File;

/**
 * 编辑器相应配置类
 */
public class EditConfig {

    static final String TEXT_PROJECT = "text_project";

    /**
     * 最后被选中的可编辑文件(忽略选中文件夹)
     */
    private FileObject lastSelectedFile;

    /**
     * 最后选中的文件夹
     */
    private FileObject lastSelectedDir;

    private FileObject projectDir;

    public EditConfig() {
        String filePath = EnvUtil.getProperty(TEXT_PROJECT, System.getProperty("user.dir"));
        File file = new File(filePath);
        this.projectDir  = new FileObject(file.getName(), file);
    }

    public EditConfig lastSelectedDir(FileObject lastSelectedDir) {
        this.lastSelectedDir = lastSelectedDir;
        return this;
    }

    public EditConfig lastSelectedFile(FileObject lastSelectedFile) {
        this.lastSelectedFile = lastSelectedFile;
        return this;
    }

    public EditConfig projectDir(FileObject projectDir) {
        this.projectDir = projectDir;
        // 记录到配置日志当中
        EnvUtil.setProperty(TEXT_PROJECT, projectDir.file.getAbsolutePath());
        return this;
    }

    public FileObject lastSelectedDir() {
        return this.lastSelectedDir;
    }

    public FileObject lastSelectedFile() {
        return this.lastSelectedFile;
    }

    public FileObject projectDir() {
        return this.projectDir;
    }
}
