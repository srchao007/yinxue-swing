package fun.codedesign.yinxue.json.service;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface JsonService {

    /**
     * json美化
     *
     * @param json
     * @return
     */
    String jsonPretty(String json) throws Exception;

    String jsonOneLine(String json);

    /**
     * 识别javaType类型
     *
     * @param json
     * @return
     */
    String generateJavaType(String json) throws Exception;

    /**
     * 生成pojo类
     *
     * @param json
     * @return
     */
    String generateJavaCode(String json) throws Exception;

    /**
     * 生成groovy代码
     *
     * @param json
     * @return
     */
    default String generateGroovyCode(String json) throws Exception {
        return "TODO";
    }
}
