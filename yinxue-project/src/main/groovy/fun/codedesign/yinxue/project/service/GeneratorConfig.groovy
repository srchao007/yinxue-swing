package fun.codedesign.yinxue.project.service

class GeneratorConfig {

    static String generateConfigPrefix = '''
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration
        PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
'''

    static void main(String[] args) {
        def binding = ["projectPath":"13"]

        def engine = new groovy.text.XmlTemplateEngine()
        String generateConfig = '''
<generatorConfiguration>
    <context id="my" targetRuntime="MyBatis3">
        <commentGenerator>
            <property name="suppressDate" value="false"/>
            <property name="suppressAllComments" value="true"/>
        </commentGenerator>

        <jdbcConnection driverClass="com.mysql.cj.jdbc.Driver"
                        connectionURL="jdbc:mysql://123.249.27.200:3306/snowfure?useUnicode=true%26characterEncoding=UTF-8%26serverTimezone=Asia/Shanghai%26useSSL=false" userId="root"
                        password=""/>

        <javaModelGenerator targetPackage="com.xxx.sz.ff.repository.web.model.domain"
                            targetProject="${projectPath}/src/main/java">
            <property name="enableSubPackages" value="true"/>
            <property name="trimStrings" value="true"/>
        </javaModelGenerator>

        <sqlMapGenerator targetPackage="mapper"
                         targetProject="${projectPath}/src/main/resources">
            <property name="enableSubPackages" value="true"/>
        </sqlMapGenerator>

        <javaClientGenerator targetPackage="com.xxx.sz.ff.repository.web.dao"
                             targetProject="${projectPath}" type="XMLMAPPER">
            <property name="enableSubPackages" value="true"/>
        </javaClientGenerator>
        
        <table tableName="book" domainObjectName="BookDo" mapperName="BookMapper"
               enableCountByExample="false" enableUpdateByExample="false"
               enableDeleteByExample="false" enableSelectByExample="false"
               selectByExampleQueryId="false">
        </table>
    </context>
</generatorConfiguration>
'''
        def template = engine.createTemplate(generateConfig).make(binding)
        println generateConfigPrefix + template.toString()
    }
}
