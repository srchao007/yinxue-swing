package fun.codedesign.yinxue.project.domain

import fun.codedesign.yinxue.project.ProjectPanel

class ProjectInfo {

    String projectName

    /**
     * 父路径
     */
    String parentPath

    /**
     * 包含哪些模块
     */
    List<String> modules

    /**
     * DDD模型还是MVC
     */
    StructType structType

    /**
     * 分层结构
     */
    List<String> layers

    /**
     * 公共域
     */
    List<String> common

    /**
     * 支撑域内容
     */
    List<String> support

    /**
     * 分层依赖
     */
    List<String> depends


    List<String> parentFiles
    List<String> subFiles

    /**
     * 数据模型
     */
    List<DataModule> dataModelList

    def clean() {

    }

    def build() {
        // 根目录中拷贝 rootGradleSetting.gradle 和 build.gradle
        File root = new File(parentPath + File.separator + projectName)
        if (!root.exists()) {
            println root.mkdir();
        }
        File rootGradleSetting = new File(root, "settings.gradle")
        rootGradleSetting << "rootProject.name = '${projectName}'\n"

        copyFile("/file/build.gradle", root, "build.gradle")
        copyFile("/file/gradle.properties", root, "gradle.properties")
        copyFile("/file/README.md", root, "README.md")
        modules.forEach { module ->
            // 构建模块父目录
            File subParentFile = new File(root, module)
            boolean success = subParentFile.mkdir()
            println(success)
            // 构建模块子目录
            layers.forEach { layer ->
                File moduleLayerDir = new File(subParentFile, "${module}-${layer}")
                println(moduleLayerDir.mkdir())
                // 拷贝build.gradle
                if ("web" == layer) {
                    new File(moduleLayerDir, "build.gradle") << """dependencies {
    api project(":${module}:${module}-application")
}
"""
                } else if ("application" == layer) {
                    new File(moduleLayerDir, "build.gradle") << """dependencies {
    api project(":${module}:${module}-domain")
    api project(":${module}:${module}-infrastructure")
}
"""
                } else if ("domain" == layer) {
                    new File(moduleLayerDir, "build.gradle") << """dependencies {

}
"""
                } else if ("infrastructure" == layer) {
                    new File(moduleLayerDir, "build.gradle") << """dependencies {
    api project(":${module}:${module}-domain")
}
"""
                } else if ("client" == layer) {
                    new File(moduleLayerDir, "build.gradle") << """dependencies {
}
"""
                }
                createDevelopDirs(moduleLayerDir)
                // rootGradleSetting.gradle追加各模块
                rootGradleSetting << """include ":${module}:${module}-${layer}"\n"""
            }
        }

        // 公共域
        def commonTop = new File(root, "common")
        println commonTop.mkdirs()
        common.forEach { commonModule ->
            def commonModuleFile = new File(commonTop, commonModule)
            println commonModuleFile.mkdir()
            new File(commonModuleFile, "build.gradle") << """dependencies {

}
"""
            createDevelopDirs(commonModuleFile)
            rootGradleSetting << """include ":common:${commonModule}"\n"""
        }
        // 支撑域
        def supportTop = new File(root, "support")
        println supportTop.mkdirs()
        support.forEach { supportModule ->
            def supportModuleFile = new File(supportTop, supportModule)
            println supportModuleFile.mkdir()
            createDevelopDirs(supportModuleFile)
            rootGradleSetting << """include ":support:${supportModule}"\n"""
        }
    }

    private void createDevelopDirs(File subModuleFile) {
        File groovyMain = new File(subModuleFile, "src/main/groovy")
        File javaMain = new File(subModuleFile, "src/main/java")
        File resources = new File(subModuleFile, "src/main/resources")
        println(groovyMain.mkdirs())
        println(javaMain.mkdirs())
        println(resources.mkdirs())
    }

    /**
     *
     * @param resourcePath /sub/build.gradle
     * @param pathName
     */
    def copyTemplate(String resourcePath, Map binding, File parentPath, String pathName) {
        String template = File.getResource(resourcePath).getText("utf-8")
//            def engine = new SimpleTemplateEngine()
//            def text = engine.createTemplate(template).make(binding).toString()
        new File(parentPath, pathName) << template
//            File subBuildGradle = new File(parentPath, pathName).withWriter("utf-8", { it.writeLine(text) })
    }

    def copyFile(String resourcePath, File parentPath, String pathName) {
        String text = ProjectPanel.class.getResource(resourcePath).getText("utf-8")
        // 存在编码问题GBK
//            new File(parentPath, pathName) << text
        new File(parentPath, pathName).withWriter('utf-8') { writer ->
            writer.writeLine text
        }
    }

    String getCreatedPath() {
        File root = new File(parentPath + File.separator + projectName)
        return root.getAbsolutePath()
    }
}

