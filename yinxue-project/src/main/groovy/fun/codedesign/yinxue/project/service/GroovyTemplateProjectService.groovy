package fun.codedesign.yinxue.project.service

import com.baomidou.mybatisplus.generator.FastAutoGenerator
import com.baomidou.mybatisplus.generator.config.OutputFile
import fun.codedesign.yinxue.project.domain.ApiInfo
import fun.codedesign.yinxue.project.domain.DaoInfo
import fun.codedesign.yinxue.project.domain.DomainInfo
import fun.codedesign.yinxue.project.domain.ProjectInfo
import fun.codedesign.yinxue.project.domain.ServiceInfo
import fun.codedesign.yinxue.project.domain.StructType
import groovy.yaml.YamlSlurper
import org.mybatis.generator.api.ShellRunner

/**
 * 初始化一个项目
 * 基于gradle
 * 使用groovy模板
 * 有数据模型入参
 */
//@SpringBootApplication
class GroovyTemplateProjectService implements ProjectService {

    def ys = new YamlSlurper()

    static void main(String[] args) {
        println "Project Init"
    }

    @Override
    def createProject(String absolutePath, String yaml) {
        def yamlObject = ys.parseText(yaml)
        def project = new ProjectInfo().tap {
            projectName = yamlObject.projectName
            parentPath = absolutePath
            modules = yamlObject.modules
            structType = StructType.DDD
            layers = yamlObject.layers
            depends = yamlObject.depends
            support = yamlObject.support
            common = yamlObject.common
            parentFiles = ["setting.gradle", "build.gradle"]
            subFiles = ["sub/build.gradle"]
        }
        project.clean()
        project.build()
        return project.getCreatedPath()
    }

    @Override
    def createORM(String absolutePath, String jdbc) {
        FastAutoGenerator.create("jdbc:mysql://123.249.27.200:3306/snowfure?1allowMultiQueries=true&useSSL=false&characterEncoding=utf8", "root", "")
                .globalConfig(builder -> {
                    builder.author("123") // 设置作者
//                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir(absolutePath); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.snowfure.book") // 设置父包名
//                            .moduleName("mapper") // 设置父包模块名
                            .entity("model")
                            .mapper("mapper")
                            .pathInfo(Collections.singletonMap(OutputFile.mapper, absolutePath)); // 设置mapperXml生成路径
                }).strategyConfig(builder -> {
            builder.addInclude("book"); // 设置需要生成的表名
//                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
        })
                .execute();
    }

    @Override
    def createMybatis(String absolutePath, String jdbc) {
        String generateConfig = '''
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration
        PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">

<generatorConfiguration>
    <context id="my" targetRuntime="MyBatis3">
        <commentGenerator>
            <property name="suppressDate" value="false"/>
            <property name="suppressAllComments" value="true"/>
        </commentGenerator>

        <jdbcConnection driverClass="com.mysql.cj.jdbc.Driver"
                        connectionURL="jdbc:mysql://123.249.27.200:3306/snowfure?useUnicode=true%26characterEncoding=UTF-8%26serverTimezone=Asia/Shanghai%26useSSL=false" userId="root"
                        password="zengjian"/>

        <javaModelGenerator targetPackage="com.xxx.sz.ff.repository.web.model.domain"
                            targetProject="F:\\IdeaProjects\\ff\\ff-repository-web\\src\\main\\java">
            <property name="enableSubPackages" value="true"/>
            <property name="trimStrings" value="true"/>
        </javaModelGenerator>

        <sqlMapGenerator targetPackage="mapper"
                         targetProject="${project}\\src\\main\\resources">
            <property name="enableSubPackages" value="true"/>
        </sqlMapGenerator>

        <javaClientGenerator targetPackage="com.xxx.sz.ff.repository.web.dao"
                             targetProject="${}" type="XMLMAPPER">
            <property name="enableSubPackages" value="true"/>
        </javaClientGenerator>

        <table tableName="book" domainObjectName="BookDo" mapperName="BookMapper"
               enableCountByExample="false" enableUpdateByExample="false"
               enableDeleteByExample="false" enableSelectByExample="false"
               selectByExampleQueryId="false">
        </table>
    </context>
</generatorConfiguration>
'''


        String config = File.getResource("/config/generatorConfig.xml").getFile()
        String[] arg = ["-configfile", config, "-overwrite"]
        ShellRunner.main(arg);
        return null
    }

    @Override
    def appendWebControllerCode(String absolutePath, ApiInfo apiInfo) {
        return null
    }

    @Override
    def appendServiceCodeAndConverter(String absolutePath, ServiceInfo serviceInfo) {
        return null
    }

    @Override
    def appendDaoMapperDO(String absolutePath, DaoInfo daoInfo) {
        return null
    }

    @Override
    def appendDomainEntities(String absolutePath, DomainInfo domainInfo) {
        return null
    }
}
