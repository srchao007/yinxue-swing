package fun.codedesign.yinxue.project.service

import fun.codedesign.yinxue.project.domain.ApiInfo
import fun.codedesign.yinxue.project.domain.DaoInfo
import fun.codedesign.yinxue.project.domain.DomainInfo
import fun.codedesign.yinxue.project.domain.ServiceInfo

interface  ProjectService {

    /**
     * 根据路径和yaml配置文件, 生成项目结构
     * @param absolutePath
     * @param yaml
     * @return
     */
    def createProject(String absolutePath, String yaml)

    def createORM(String absolutePath, String jdbc)

    def createMybatis(String absolutePath, String jdbc)

    def appendWebControllerCode(String absolutePath, ApiInfo apiInfo)

    def appendServiceCodeAndConverter(String absolutePath, ServiceInfo serviceInfo)

    def appendDaoMapperDO(String absolutePath, DaoInfo daoInfo)

    def appendDomainEntities(String absolutePath, DomainInfo domainInfo)

}
