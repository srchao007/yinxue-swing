package fun.codedesign.yinxue.project

import fun.codedesign.yinxue.project.service.GroovyTemplateProjectService
import fun.codedesign.yinxue.project.service.ProjectService
import groovy.swing.SwingBuilder
import org.yinxue.swing.core.panel.YxInputChoose
import org.yinxue.swing.core.panel.YxTextArea
import org.yinxue.swing.core.standard.YxPanel
import org.yinxue.swing.core.util.EnvUtil

import javax.swing.JTree
import java.awt.*

class ProjectPanel extends YxPanel {

    ProjectService projectService = new GroovyTemplateProjectService()

    static String yaml = '''projectName: hello-project
modules:
  - user
  - room
  - project 
  - system 
  - price 
  - device
layers:
  - web
  - application
  - client
  - infrastructure
  - domain
support:
  - hello-client
  - hello2-client
depends: 
  - web->application
  - application->domain
  - application->infrastructure
  - infrastructure->domain
common:
  - common-web
  - common-tool
'''

    YxInputChoose topInputPanel
    YxTextArea topYamlInput
    YxTextArea downInfoOutput
    YxPanel rightButtonPanel

    @Override
    void init() {
        topInputPanel = new YxInputChoose()
        topInputPanel.setText(EnvUtil.getProperty("user_project", System.getProperty("user.home")))

        topYamlInput = new YxTextArea().setText(yaml)
        downInfoOutput = new YxTextArea()
        rightButtonPanel = createYxPanel()
        rightButtonPanel.setPreferredSize(new Dimension(300, 400))

        rightButtonPanel.add(createYxButton("生成项目清单", 140, 80).addYxActionListener {
            try {
                def path = projectService.createProject( topInputPanel.getText(), topYamlInput.getText())
                downInfoOutput.setText("创建目录是:" + path)
            } catch (Exception e) {
                e.printStackTrace()
                downInfoOutput.setText("创建异常:" + e.getMessage())
            }
        }, rightButtonPanel.refreshPosition(0, 0, 1, 1, 1, 1, H))
        rightButtonPanel.add(createYxButton("生成mybatis-plus实体类及mapper", 140, 80).addYxActionListener {
            def projectPath = topInputPanel.getText()
            projectService.createORM(projectPath, "")
        }, rightButtonPanel.refreshPosition(0, 1, 1, 1, 1, 1, H))
        rightButtonPanel.add(createYxButton("生成mybatis实体类及mapper", 140, 80).addYxActionListener {

        }, rightButtonPanel.refreshPosition(0, 2, 1, 1, 1, 1, H))
        rightButtonPanel.add(createYxButton("生成jpa实体类mapper", 140, 80).addYxActionListener {
//            rightButtonPanel.setVisible(false)
        }, rightButtonPanel.refreshPosition(0, 3, 1, 1, 1, 1, H))
    }

    @Override
    void position() {
        add(topInputPanel, refreshPosition(0, 0, 2, 1, 1, 0.2, H))
        add(topYamlInput, refreshPosition(0, 1, 1, 1, 1, 0.4, BOTH))
        add(downInfoOutput, refreshPosition(0, 2, 1, 1, 1, 0.4, BOTH))
        add(rightButtonPanel, refreshPosition(1, 1, 1, 3, 1, 1, BOTH))
    }

    @Override
    void action() {

    }

    static void main(String[] args) {
        run(ProjectPanel.class, "project init tool", 1600, 900)
    }
}
