package fun.codedesign.ddd.domain;


@SuppressWarnings("all")
public abstract class BaseEntity<ID, E extends BaseEntity> {

    protected ID id;

    public ID id() {
        return this.id;
    }

    public E id(ID id) {
        this.id = id;
        return (E) this;
    }

}
