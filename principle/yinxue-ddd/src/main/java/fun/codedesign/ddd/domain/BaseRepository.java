package fun.codedesign.ddd.domain;

import java.util.List;

public interface BaseRepository<ID, Entity, Spec> {

    default Entity findById(ID id) {
        return null;
    }

    default List<Entity> findBySpec(Spec spec) {
        return null;
    }

    default List<Entity> findAll() {
        return null;
    }

    default void save(Entity entity) {
        return;
    }

    default void batchSave(List<Entity> entity) {
        return;
    }
}
