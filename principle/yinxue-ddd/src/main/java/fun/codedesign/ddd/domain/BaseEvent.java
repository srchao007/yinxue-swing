package fun.codedesign.ddd.domain;

/**
 * @see AbstractEvent
 */
@Deprecated
public abstract class BaseEvent /* implements Event */ {

    public void visit(BaseEventVisitor visitor) {
        try {
            if (this.accept(visitor)) {
                // visitor.visit(this);
            }
        } catch (Throwable e) {
            // handleThrowable
        } finally {
            // visitor.visitEnd(this);
        }
    }

    public boolean accept(BaseEventVisitor visitor) {
        return true;
    }
}
