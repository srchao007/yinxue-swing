package fun.codedesign.ddd.domain;


@Deprecated
public interface BaseEventRepository<Event extends BaseEvent, Visitor extends BaseEventVisitor> {

    default void save(Event event) {
        // doNothing
    }

    default void save(Event event, Visitor visitor) {
        event.visit(visitor);
    }

}
