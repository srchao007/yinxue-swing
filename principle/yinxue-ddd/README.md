## DDD领域驱动设计框架

java是一个强约束性的语言，而对于DDD而言，也存在类似一种约束，但同时又有其发展和灵活性，比如说以下的一些约束可以提出来对比

java：
面向对象的结构性语言

ddd：
entity：是有一种有唯一标志的实体
value：是不可变对象，可以不具备实体类的标志性
event：不可变对象，一次性生成，分内部事件和外部事件
repository：一种存储介质，用来还原entity value aggregate
domain-service: 领域内服务，用来操纵多个entity 或者 一个aggregate
bounded-context：限界上下文，实际可以由多个aggregate组合而成，可以类比于现在的微服务
aggregate：聚合体，用来作为事务的最小边界，发起事务请求时最好从aggregate入口进去，只有这个入口才可以进行修改，避免出现竞争
    - ？ 这里的竞争实际上还得需要由锁或者资源隔离来做。


框架的目的
通过使用java面向对象的特征和ddd的约束，尽可能的把编写代码的方式变得模板化、简单化，并且又能兼顾业务开发的灵活性，和其他框架的结合性。
concept：所有关于ddd的概念抽象，不做任何实现，只作为标记使用
tool：一个草稿版本用来实验可以抽象和实现的内容的

client: 对外暴露层，提供client包等
api: 对外服务层，提供web，rpc等对外服务层
application: 可以用来组装多个aggregate的层
domain: 核心层，aggregate entity等所在的核心领域
infra: 基础设置层，依赖反转的地方
test： use case所在的地方，用来编写相应的用例的，单独的一个test其实还更好，比哪种分散的测试更加完整。

以上的每一层都可以单独再分模块

依赖关系


api -》 application -》 domain 《- infra
api -》 application -》 infra
                    -》 client
api -》 client

application如果不做抽象，则可以直接依赖infra层
application做抽象，则可能infra需要往上实现 application的内容，如果这样，infra可以再做一层分层，以避免循环依赖的问题

domain层往下走到


其他家的做法
cola https://github.com/alibaba/COLA.git
application依赖infra，并且拆分了非常细的执行指令，这里的代码是不是太冗余了。

spring boot admin  https://github.com/codecentric/spring-boot-admin
事件流的方式，事件流是如何重新构建出一个entity的，instance 发出了 instanceEvent， event又重新还原出来instance




 


