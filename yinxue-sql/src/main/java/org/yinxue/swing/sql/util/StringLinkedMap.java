/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StringLinkedMap
 * Author:   zengjian
 * Date:     2018/9/7 11:09
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.sql.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/7 11:09
 */
public class StringLinkedMap extends LinkedHashMap<String, String> {

    @Override
    public String toString() {
        Iterator<Map.Entry<String, String>> iterator = this.entrySet().iterator();
        StringBuilder builder = new StringBuilder(size() << 4);
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            builder.append(entry.getKey()).append(" ").append(entry.getValue());
            if (iterator.hasNext()) {
                builder.append("\n");
            }
        }
        return builder.toString();
    }
}