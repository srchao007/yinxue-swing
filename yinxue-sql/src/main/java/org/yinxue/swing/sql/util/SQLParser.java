//package org.yinxue.swing.sql.util;
//
//import com.alibaba.druid.sql.ast.SQLStatement;
//import com.alibaba.druid.sql.dialect.postgresql.parser.PGSQLStatementParser;
//
//public class SQLParser {
//
//    String POSTGRESQL_DDL_PATTERN = "";
//
//    private String sql;
//
//    public SQLParser(String sql) {
//        this.sql = sql;
//    }
//
//    public String parse() {
//        PGSQLStatementParser parser = new PGSQLStatementParser(sql);
//        SQLStatement sqlStatement =  parser.parseSet();
//        return null;
//    }
//
//    public static void main(String[] args) {
//        String sql = "CREATE TABLE \"public\".\"ymall_agent_return_goods_order_item\" (\n" +
//                "  \"id\" int4 NOT NULL DEFAULT nextval('ymall_agent_return_goods_order_item_id_seq'::regclass),\n" +
//                "  \"order_id\" varchar(64) COLLATE \"pg_catalog\".\"default\",\n" +
//                "  \"return_order_id\" varchar(64) COLLATE \"pg_catalog\".\"default\",\n" +
//                "  \"return_order_item_id\" varchar(64) COLLATE \"pg_catalog\".\"default\",\n" +
//                "  \"order_item_id\" varchar(64) COLLATE \"pg_catalog\".\"default\",\n" +
//                "  \"service_fee\" varchar(255) COLLATE \"pg_catalog\".\"default\",\n" +
//                "  \"used_fee\" numeric(64),\n" +
//                "  \"create_time\" timestamp(6),\n" +
//                "  \"update_time\" timestamp(6),\n" +
//                "  \"create_user\" varchar(64) COLLATE \"pg_catalog\".\"default\",\n" +
//                "  \"update_user\" varchar(64) COLLATE \"pg_catalog\".\"default\",\n" +
//                "  \"version\" int4,\n" +
//                "  \"product_id\" varchar(64) COLLATE \"pg_catalog\".\"default\",\n" +
//                "  CONSTRAINT \"ymall_agent_return_goods_order_item_pkey\" PRIMARY KEY (\"id\")\n" +
//                ")\n" +
//                ";\n" +
//                "\n" +
//                "ALTER TABLE \"public\".\"ymall_agent_return_goods_order_item\" \n" +
//                "  OWNER TO \"postgres\";\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"order_id\" IS '订单ID';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"return_order_id\" IS '退货订单ID';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"return_order_item_id\" IS '退货订单行ID';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"order_item_id\" IS '订单行ID';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"service_fee\" IS '手续费';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"used_fee\" IS '损耗费';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"create_time\" IS '记录创建时间';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"update_time\" IS '记录更新时间';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"create_user\" IS '记录创建用户';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"update_user\" IS '记录修改用户';\n" +
//                "\n" +
//                "COMMENT ON COLUMN \"public\".\"ymall_agent_return_goods_order_item\".\"version\" IS '版本号';";
//        SQLParser parser = new SQLParser(sql);
//        parser.parse();
//    }
//}
