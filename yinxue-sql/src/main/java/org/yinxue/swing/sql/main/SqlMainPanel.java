package org.yinxue.swing.sql.main;


import org.yinxue.swing.core.util.SwingUtils;

import javax.swing.*;
import java.awt.*;

public class SqlMainPanel extends JPanel {

    private JTabbedPane jTabbedPane = new JTabbedPane();

    public SqlMainPanel() {
        jTabbedPane.addTab("sql处理2", new SQLPanel2());
        setLayout(new GridLayout(1, 1, 5, 5));
        add(jTabbedPane);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                SwingUtils.initDefaultUI();
                JFrame frame = SwingUtils.ofJFrameWithMenuBar("yinxue-sql");
                frame.add(new SqlMainPanel());
                frame.setIconImage(new ImageIcon(SqlMainPanel.class.getResource("/sql_icon.png")).getImage());
                frame.setVisible(true);
            }
        });
    }
}
