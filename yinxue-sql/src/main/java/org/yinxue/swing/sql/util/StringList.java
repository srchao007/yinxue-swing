/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: StringList
 * Author:   zengjian
 * Date:     2018/9/7 11:01
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.sql.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 〈〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/7 11:01
 */
public class StringList extends ArrayList<String> {

    public StringList() {
    }

    public StringList(int initialCapacity) {
        super(initialCapacity);
    }

    public StringList(Collection<? extends String> c) {
        super(c);
    }

    @Override
    public String toString() {
        Iterator iterator = iterator();
        StringBuilder builder = new StringBuilder(size());
        while (iterator.hasNext()){
            builder.append(iterator.next());
            if (iterator.hasNext()){
                builder.append("\n");
            }
        }
        return builder.toString();
    }
}