/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: SQLPanel2
 * Author:   zengjian
 * Date:     2018/9/6 20:07
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.sql.main;

import org.yinxue.swing.core.panel.YxTextArea;
import org.yinxue.swing.core.standard.YxStructure;
import org.yinxue.swing.core.util.ExceptionUtil;
import org.yinxue.swing.core.util.SwingUtils;
import org.yinxue.swing.sql.constant.SQLConfig;
import org.yinxue.swing.sql.util.SQLUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

/**
 * 〈〉<br>
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/6 20:07
 */
public class SQLPanel2 extends JPanel implements YxStructure {

    JPanel jPanel_top = new JPanel(new GridBagLayout());
    JLabel jLabel_databaseType = new JLabel("数据库类型:");
    JComboBox<String> jComboBox_database = new JComboBox<>();

    JLabel jLabel_inputType = new JLabel("输入类型:");
    JComboBox<String> jComboBox_inputType = new JComboBox<>();

    JLabel jLabel_codeStyle = new JLabel("语句风格:");
    JComboBox<String> jComboBox_codeStyle = new JComboBox<>();

    JPanel jPanel_middle = new JPanel(new GridBagLayout());
    YxTextArea one = new YxTextArea();
    YxTextArea two = new YxTextArea();

    JPanel jPanel_right = new JPanel(new GridBagLayout());
    JPanel jPanel_button_top = new JPanel(new GridBagLayout());
    JButton jButton_query = SwingUtils.ofJButton("SQL查询语句", 130, 30);
    JButton jButton_add = SwingUtils.ofJButton("SQL新增语句", 130, 30);
    JButton jButton_update = SwingUtils.ofJButton("SQL更新语句", 130, 30);
    JButton jButton_delete = SwingUtils.ofJButton("SQL删除语句", 130, 30);

    JPanel jPanel_button_buttom = new JPanel(new GridBagLayout());
    JButton jButton_extractField = SwingUtils.ofJButton("提取SQL字段", 130, 30);
    JButton jButton_extractFieldAndType = SwingUtils.ofJButton("提取字段和类型", 130, 30);
    JButton jButton_convertField = SwingUtils.ofJButton("字段驼峰转换", 130, 30);
    JButton jButton_convert2EntityField = SwingUtils.ofJButton("生成实体类字段", 130, 30);


    public SQLPanel2() {
        init();
        position();
        action();
    }

    @Override
    public void init() {
        this.setLayout(new GridBagLayout());
        GridBagConstraints g1 = new GridBagConstraints();
        g1.gridx = 0;
        g1.gridy = 0;
        g1.fill = GridBagConstraints.BOTH;
        g1.insets = new Insets(5, 5, 5, 5);
        g1.weightx = 1;

        this.add(jPanel_top, g1);

        g1.gridy = 1;
        g1.weighty = 1;
        this.add(jPanel_middle, g1);

        GridBagConstraints g2 = new GridBagConstraints();
        g2.gridx = 0;
        g2.gridy = 0;
        g2.fill = GridBagConstraints.BOTH;
        g2.insets = new Insets(5, 5, 5, 5);
        g2.weightx = 1;
        g2.weighty = 1;

        jPanel_middle.add(one, g2);

        g2.gridx = 0;
        g2.gridy = 1;
        jPanel_middle.add(two, g2);

        g2.gridx = 1;
        g2.gridy = 0;
        g2.weightx = 0;
        g2.gridheight = 2;
        jPanel_middle.add(jPanel_right, g2);

        Font timeNewRoman = new Font("Timenew Romans", Font.PLAIN, 12);

        jComboBox_database.addItem("MySQL数据库");
        jComboBox_database.addItem("DB2数据库");
        jComboBox_database.addItem("MongDB数据库");
        jComboBox_database.addItem("Hive数据仓库");
        jComboBox_database.setFont(timeNewRoman);

        jComboBox_inputType.addItem("SQL_DDL语句");
        jComboBox_inputType.addItem("POJO实体类");
        jComboBox_inputType.setFont(timeNewRoman);

        jComboBox_codeStyle.addItem("SpringJDBCTemplate");
        jComboBox_codeStyle.addItem("MybatisMapper");
        jComboBox_codeStyle.addItem("PreparedStatement");
        jComboBox_codeStyle.setFont(timeNewRoman);


        jPanel_top.setBorder(BorderFactory.createEmptyBorder());

        jPanel_right.setPreferredSize(new Dimension(200, 563));
        jPanel_right.setMinimumSize(new Dimension(200, 563));
        jPanel_right.setMaximumSize(new Dimension(200, 563));
        jPanel_right.setBorder(BorderFactory.createEtchedBorder());

        one.append(SQLConfig.defaultSQL);
    }

    @Override
    public void position() {
        GridBagConstraints g1 = new GridBagConstraints();
        g1.gridx = 0;
        g1.gridy = 0;
        g1.weightx = 0;
        g1.fill = GridBagConstraints.BOTH;
        Insets insets5 = new Insets(5, 5, 5, 5);
        g1.insets = insets5;

        jPanel_top.add(jLabel_databaseType, g1);

        g1.gridx = 1;
        g1.weightx = 1;
        jPanel_top.add(jComboBox_database, g1);


        g1.gridx = 2;
        g1.weightx = 0;
        g1.insets = new Insets(5, 20, 5, 5);
        jPanel_top.add(jLabel_inputType, g1);


        g1.gridx = 3;
        g1.weightx = 1;
        g1.insets = insets5;
        jPanel_top.add(jComboBox_inputType, g1);

        g1.gridx = 4;
        g1.weightx = 0;
        g1.insets = new Insets(5, 20, 5, 5);
        jPanel_top.add(jLabel_codeStyle, g1);

        g1.ipadx = 0;
        g1.gridx = 5;
        g1.weightx = 1;
        g1.insets = insets5;
        jPanel_top.add(jComboBox_codeStyle, g1);


        GridBagConstraints g2 = new GridBagConstraints();
        g2.gridx = 0;
        g2.gridy = 0;
        g2.weightx = 0;
        g2.fill = GridBagConstraints.HORIZONTAL;
        g2.insets = insets5;
        jPanel_button_top.add(jButton_add, g2);
        jPanel_button_buttom.add(jButton_extractField, g2);
        g2.gridy = 1;
        jPanel_button_top.add(jButton_query, g2);
        jPanel_button_buttom.add(jButton_extractFieldAndType, g2);
        g2.gridy = 2;
        jPanel_button_top.add(jButton_update, g2);
        jPanel_button_buttom.add(jButton_convertField, g2);

        g2.gridy = 3;
        jPanel_button_top.add(jButton_delete, g2);
        jPanel_button_buttom.add(jButton_convert2EntityField, g2);

        GridBagConstraints g3 = new GridBagConstraints();
        g3.fill = GridBagConstraints.HORIZONTAL;
        g3.gridx = 0;
        g3.gridy = 0;
        g3.insets = insets5;
        g3.weightx = 1;
        g3.weighty = 1;
        g3.anchor = GridBagConstraints.NORTH; // NORTH依赖于weightx weighty权重设置为1
        jPanel_right.add(jPanel_button_top, g3);

        g3.anchor = GridBagConstraints.SOUTH;
        g3.gridx = 0;
        g3.gridy = 1;
        //jPanel_button_top.setBorder(BorderFactory.createEtchedBorder());
        //jPanel_button_buttom.setBorder(BorderFactory.createEtchedBorder());
        jPanel_right.add(jPanel_button_buttom, g3);

    }

    @Override
    public void action() {
        jButton_extractField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String text = one.getText();
                    int inputIndex = jComboBox_inputType.getSelectedIndex();
                    switch (inputIndex) {
                        case 0:
                            two.setText(SQLUtil.extractSQLFieldAsList(text).toString());
                            break;
                        case 1:
                            two.setText(SQLUtil.extractJavaField(text).toString());
                            break;
                        default:
                            throw new RuntimeException("暂不支持");
                    }
                } catch (Exception e1) {
                    two.setText("发生异常:" + e1.getClass() + "异常信息:" + e1.getMessage());
                }
            }
        });

        jButton_extractFieldAndType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String text = one.getText();
                    int inputIndex = jComboBox_inputType.getSelectedIndex();
                    switch (inputIndex) {
                        case 0:
                            two.setText(SQLUtil.extractSQLFieldAndTypeAsMap(text).toString());
                            break;
                        case 1:
                            two.setText(SQLUtil.extractJavaFieldAndTypeAsMap(text).toString());
                            break;
                        default:
                            throw new RuntimeException("暂不支持");
                    }
                } catch (Exception e1) {
                    two.setText("发生异常:" + e1.getClass() + "异常信息:" + e1.getMessage());
                }
            }
        });

        jButton_convertField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String text = one.getText();
                    int inputIndex = jComboBox_inputType.getSelectedIndex();
                    switch (inputIndex) {
                        case 0:
                            String fields = SQLUtil.extractSQLFieldAsList(text).toString();
                            two.setText(SQLUtil.toCamelCase(fields));
                            break;
                        case 1:
                            String fields2 = SQLUtil.extractJavaField(text).toString();
                            two.setText(SQLUtil.toUnderLineCase(fields2).toUpperCase());
                            break;
                        default:
                            throw new RuntimeException("暂不支持");
                    }
                } catch (Exception e1) {
                    two.setText("发生异常:" + e1.getClass() + "异常信息:" + e1.getMessage());
                }
            }
        });

        jButton_convert2EntityField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String text = one.getText();
                    int inputIndex = jComboBox_inputType.getSelectedIndex();
                    switch (inputIndex) {
                        case 0:
                            two.setText(SQLUtil.createEntityField(one.getText()));
                            break;
                        case 1:
                            two.setText(SQLUtil.createSQLDDL(one.getText()));
                            break;
                        default:
                            throw new RuntimeException("暂不支持");
                    }

                } catch (Exception e1) {
                    two.setText(ExceptionUtil.printStack(e1));
                }
            }
        });

        jButton_query.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int index = jComboBox_codeStyle.getSelectedIndex();
                    int index2 = jComboBox_inputType.getSelectedIndex();
                    String text = one.getText();
                    if (index2 == 1) { // 如果是pojo类先将pojo转换为ddl语句
                        text = SQLUtil.createSQLDDL(text);
                    }
                    switch (index) {
                        case 0:
                            two.setText(SQLUtil.convertToQuery(text));
                            break;
                        case 1:
                            two.setText(SQLUtil.convertToQueryForMybatis(text));
                            break;
                        case 2:
                            two.setText(SQLUtil.convertToDeleteForPs(text));
                            break;
                        default:
                            throw new RuntimeException("暂不支持");
                    }

                } catch (Exception e1) {
                    two.setText("发生异常:" + e1.getClass() + "异常信息:" + e1.getMessage());
                }
            }
        });

        jButton_add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int index = jComboBox_codeStyle.getSelectedIndex();
                    String text = one.getText();
                    int index2 = jComboBox_inputType.getSelectedIndex();
                    if (index2 == 1) {
                        text = SQLUtil.createSQLDDL(text);
                    }
                    switch (index) {
                        case 0:
                            two.setText(SQLUtil.convertToInsert(text));
                            break;
                        case 1:
                            two.setText(SQLUtil.convertToInsertForMybatis(text));
                            break;
                        case 2:
                            two.setText(SQLUtil.convertToInsertForPs(text));
                            break;
                        default:
                            throw new RuntimeException("暂不支持");
                    }
                } catch (Exception e1) {
                    two.setText("发生异常:" + e1.getClass() + "异常信息:" + e1.getMessage());
                }
            }
        });

        jButton_update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int index = jComboBox_codeStyle.getSelectedIndex();
                    String text = one.getText();
                    int index2 = jComboBox_inputType.getSelectedIndex();
                    if (index2 == 1) {
                        text = SQLUtil.createSQLDDL(text);
                    }
                    switch (index) {
                        case 0:
                            two.setText(SQLUtil.convertToUpdate(text));
                            break;
                        case 1:
                            two.setText(SQLUtil.convertToUpdateForMybatis(text));
                            break;
                        case 2:
                            two.setText(SQLUtil.convertToUpdateForPs(text));
                            break;
                        default:
                            throw new RuntimeException("暂不支持");
                    }
                } catch (Exception e1) {
                    two.setText("发生异常:" + e1.getClass() + "异常信息:" + e1.getMessage());
                }
            }
        });

        jButton_delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int index = jComboBox_codeStyle.getSelectedIndex();
                    String text = one.getText();
                    int index2 = jComboBox_inputType.getSelectedIndex();
                    if (index2 == 1) {
                        text = SQLUtil.createSQLDDL(text);
                    }
                    switch (index) {
                        case 0:
                            two.setText(SQLUtil.convertToDelete(text));
                            break;
                        case 1:
                            two.setText(SQLUtil.convertToDeleteForMybatis(text));
                            break;
                        case 2:
                            two.setText(SQLUtil.convertToDeleteForPs(text));
                            break;
                        default:
                            throw new RuntimeException("暂不支持");
                    }
                } catch (Exception e1) {
                    two.setText("发生异常:" + e1.getClass() + "异常信息:" + e1.getMessage());
                }
            }
        });

        jComboBox_inputType.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                int index = ((JComboBox) e.getSource()).getSelectedIndex();
                switch (index) {
                    case 0:
                        jButton_extractField.setText("提取SQL字段");
                        jButton_convertField.setText("字段驼峰转换");
                        jButton_convert2EntityField.setText("生成实体类字段");
                        one.setText(SQLConfig.defaultSQL);
                        break;
                    case 1:
                        jButton_extractField.setText("提取POJO字段");
                        jButton_convertField.setText("字段下划线转换");
                        jButton_convert2EntityField.setText("生成SQL建表语句");
                        one.setText(SQLConfig.defaultPojo);
                        break;
                    default:
                        throw new RuntimeException("暂不支持");
                }
            }
        });

    }

    @Override
    public void destory() {

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                SwingUtils.initDefaultUI();
                JFrame jFrame = SwingUtils.ofJFrameWithMenuBar("yinxue-sql");
                jFrame.add(new SQLPanel2());
                jFrame.setVisible(true);
            }
        });
    }

    public static void run() {
        SwingUtilities.invokeLater(() -> {
            SwingUtils.initDefaultUI();
            try {
                JPanel jPanel = SQLPanel2.class.newInstance();
                JFrame jFrame = SwingUtils.ofJFrameWithMenuBar("yinxue-sql", 1280, 800, jPanel);
                jFrame.setVisible(true);
                jFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }
}