package org.yinxue.swing.sql.constant;

import org.yinxue.swing.core.util.LogUtil;

import java.io.IOException;
import java.io.InputStream;

/**
 * 默认字符串值
 *
 * @author zengjian
 * @create 2018-03-31 19:48
 * @since 1.0.0
 */
public class SQLConfig {

    public static String defaultSQL = "";
    public static String defaultPojo = "";

    String PATH = "D:";

    static {
        InputStream is1 = SQLConfig.class.getResourceAsStream("/defaultsql");
        InputStream is2 = SQLConfig.class.getResourceAsStream("/defaultpojo");
        try {
            byte[] bytes1 = new byte[is1.available()];
            is1.read(bytes1);
            defaultSQL =  new String(bytes1,"utf-8");

            byte[] bytes2 = new byte[is2.available()];
            is2.read(bytes2);
            defaultPojo = new String(bytes2,"utf-8");
        } catch (IOException e) {
            LogUtil.error(SQLConfig.class, "初始化默认语句失败", e);
        }
    }


}
