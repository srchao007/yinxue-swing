/**
 * Copyright (C), 2017-2018, XXX有限公司
 * FileName: DatabasePanel
 * Author:   zengjian
 * Date:     2018/9/10 15:35
 * Description: 数据库表设计
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package org.yinxue.swing.sql.main;

import org.yinxue.swing.core.standard.YxPanel;
import org.yinxue.swing.core.standard.YxStructure;
import org.yinxue.swing.core.util.SwingUtils;

import javax.swing.*;
import java.awt.*;

/**
 * 〈数据库表设计〉<br> 
 * 〈一句话描述〉
 *
 * @author zengjian
 * @create 2018/9/10 15:35
 */
public class DatabasePanel extends YxPanel implements YxStructure {

    JPanel top = new JPanel(new GridBagLayout());
    JButton jButton_input = SwingUtils.ofJButton("表格导入", 130, 30);
    JButton jButton_output = SwingUtils.ofJButton("表单导出", 130, 30);
    JButton jButton_sharing = SwingUtils.ofJButton("分表设置", 130, 30);
    JButton jButton_createDDL = SwingUtils.ofJButton("生成建表语句", 130, 30);
    JButton jButton_createIndex = SwingUtils.ofJButton("创建索引", 130, 30);

    JPanel jPanel_left = new JPanel(new GridBagLayout());
    JTree jTree = new JTree();

    JScrollPane jScrollPane = new JScrollPane();
    JTable jTable = new JTable(new Object[10][6], new Object[]{"字段名称","字段类型","长度","主键","唯一","描述"});

//    public DatabasePanel() {
//        init();
//        position();
//        action();
//    }

    @Override
    public void init() {
        setLayout(new GridBagLayout());
        GridBagConstraints g1 = new GridBagConstraints();
        g1.insets = new Insets(5,5,5,5);
        g1.gridx = 0;
        g1.gridy = 0;
        g1.weighty = 1;
        g1.weightx = 0;
        g1.fill = GridBagConstraints.BOTH;

        jPanel_left.setPreferredSize(new Dimension(200,400));
        add(jPanel_left, g1);
        g1.gridx = 1;
        g1.weightx = 1;
        jScrollPane.setViewportView(jTable);
        add(jScrollPane, g1);

    }

    @Override
    public void position() {
        GridBagConstraints g1 = new GridBagConstraints();
        g1.insets = new Insets(5,5,5,5);
        g1.gridx = 0;
        g1.gridy = 0;
        g1.weighty = 1;
        g1.weightx = 1;
        g1.fill = GridBagConstraints.BOTH;
        jPanel_left.setBorder(BorderFactory.createEtchedBorder());
        jPanel_left.add(jTree, g1);



    }

    @Override
    public void action() {

    }

    @Override
    public void destory() {

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            SwingUtils.initDefaultUI();
            JFrame jFrame = SwingUtils.ofJFrameWithMenuBar("yinxue-sql");
            jFrame.add(new DatabasePanel());
            jFrame.setVisible(true);
        });
    }
}